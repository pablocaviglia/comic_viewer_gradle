package com.apocalipta.comic.constants;

/**
 * Created by pablo on 05/03/15.
 */
public enum UserLoginState {
    LOGGED,
    NEW,
    NEED_CLIENT_UPDATE
}
