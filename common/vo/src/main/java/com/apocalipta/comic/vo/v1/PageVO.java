package com.apocalipta.comic.vo.v1;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pablo on 21/12/14.
 */
public class PageVO implements Serializable {

    private static final long serialVersionUID = 8434422747640428108L;

    private long id;
    private int pageIndex;

    private long imageFileSize;
    private long thumbFileSize;

    private int imageWidth;
    private int imageHeight;

    private int thumbWidth;
    private int thumbHeight;

    private List<NavigationItemVO> navigationItemVOList;

    private Integer version;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public long getImageFileSize() {
        return imageFileSize;
    }

    public void setImageFileSize(long imageFileSize) {
        this.imageFileSize = imageFileSize;
    }

    public long getThumbFileSize() {
        return thumbFileSize;
    }

    public void setThumbFileSize(long thumbFileSize) {
        this.thumbFileSize = thumbFileSize;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

    public int getThumbWidth() {
        return thumbWidth;
    }

    public void setThumbWidth(int thumbWidth) {
        this.thumbWidth = thumbWidth;
    }

    public int getThumbHeight() {
        return thumbHeight;
    }

    public void setThumbHeight(int thumbHeight) {
        this.thumbHeight = thumbHeight;
    }

    public List<NavigationItemVO> getNavigationItemVOList() {
        return navigationItemVOList;
    }

    public void setNavigationItemVOList(List<NavigationItemVO> navigationItemVOList) {
        this.navigationItemVOList = navigationItemVOList;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }
}