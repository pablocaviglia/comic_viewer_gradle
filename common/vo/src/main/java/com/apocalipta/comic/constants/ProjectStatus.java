package com.apocalipta.comic.constants;

public enum ProjectStatus {
	FIRST_REGISTRATION,
	PENDING_APPROVAL,
	APPROVED;
}