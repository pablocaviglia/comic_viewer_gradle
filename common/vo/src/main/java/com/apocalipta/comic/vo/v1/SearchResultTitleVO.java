package com.apocalipta.comic.vo.v1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SearchResultTitleVO implements Serializable {

    private boolean error;
    private long total;
	private List<TitleVO> elements = new ArrayList<TitleVO>();

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<TitleVO> getElements() {
		return elements;
	}

	public void setElements(List<TitleVO> elements) {
		this.elements = elements;
	}

	public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}