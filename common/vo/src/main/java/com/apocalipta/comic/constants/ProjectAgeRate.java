package com.apocalipta.comic.constants;

public enum ProjectAgeRate {
	ALL,
	PLUS_13,
	PLUS_18;
}