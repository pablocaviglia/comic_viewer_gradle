package com.apocalipta.comic.constants;

/**
 * Created by pablo on 02/03/15.
 */
public enum UserPlatform {
    FACEBOOK,
    GOOGLE_PLUS
}
