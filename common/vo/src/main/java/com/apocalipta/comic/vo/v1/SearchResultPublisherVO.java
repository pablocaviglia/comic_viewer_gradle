package com.apocalipta.comic.vo.v1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SearchResultPublisherVO implements Serializable {

    private boolean error;
    private long total;
	private List<PublisherVO> elements = new ArrayList<PublisherVO>();

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<PublisherVO> getElements() {
		return elements;
	}

	public void setElements(List<PublisherVO> elements) {
		this.elements = elements;
	}
}