package com.apocalipta.comic.vo.v1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SearchResultPublicationVO implements Serializable {

    private static final long serialVersionUID = 6588073933636508504L;

    private boolean error;
    private long total;
	private List<PublicationVO> elements = new ArrayList<PublicationVO>();

	public long getTotal() {
		return total;
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public List<PublicationVO> getElements() {
		return elements;
	}

	public void setElements(List<PublicationVO> elements) {
		this.elements = elements;
	}

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}