package com.apocalipta.comic.vo.v1;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by pablo on 21/12/14.
 */
public class ProjectVO implements Serializable {

    private static final long serialVersionUID = -7702192598167485429L;

    private String ageRate;
    private String tags;
    private Boolean orientalReading;
    private LanguageVO nativeLanguage;
    private LanguageVO defaultLanguage;
    private Map<Long,String> credits;
    private Map<Long,String> name;
    private Map<Long,String> chapterName;
    private Map<Long,String> description;
    private Map<Long,String> legal;
    private List<PageVO> pages;
    private List<LanguageVO> languages;
    private ProjectGenreVO genre;
    private List<AuthorVO> authors;
    private TitleVO titleVO;

    public String getAgeRate() {
        return ageRate;
    }

    public void setAgeRate(String ageRate) {
        this.ageRate = ageRate;
    }

    public Boolean getOrientalReading() {
        return orientalReading;
    }

    public void setOrientalReading(Boolean orientalReading) {
        this.orientalReading = orientalReading;
    }

    public LanguageVO getNativeLanguage() {
        return nativeLanguage;
    }

    public void setNativeLanguage(LanguageVO nativeLanguage) {
        this.nativeLanguage = nativeLanguage;
    }

    public Map<Long, String> getCredits() {
        return credits;
    }

    public void setCredits(Map<Long, String> credits) {
        this.credits = credits;
    }

    public Map<Long, String> getName() {
        return name;
    }

    public void setName(Map<Long, String> name) {
        this.name = name;
    }

    public Map<Long, String> getChapterName() {
        return chapterName;
    }

    public void setChapterName(Map<Long, String> chapterName) {
        this.chapterName = chapterName;
    }

    public Map<Long, String> getDescription() {
        return description;
    }

    public void setDescription(Map<Long, String> description) {
        this.description = description;
    }

    public List<PageVO> getPages() {
        return pages;
    }

    public void setPages(List<PageVO> pages) {
        this.pages = pages;
    }

    public List<LanguageVO> getLanguages() {
        return languages;
    }

    public void setLanguages(List<LanguageVO> languages) {
        this.languages = languages;
    }

    public LanguageVO getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(LanguageVO defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public ProjectGenreVO getGenre() {
        return genre;
    }

    public void setGenre(ProjectGenreVO genre) {
        this.genre = genre;
    }

    public List<AuthorVO> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorVO> authors) {
        this.authors = authors;
    }

    public Map<Long, String> getLegal() {
        return legal;
    }

    public void setLegal(Map<Long, String> legal) {
        this.legal = legal;
    }

    public TitleVO getTitleVO() {
        return titleVO;
    }

    public void setTitleVO(TitleVO titleVO) {
        this.titleVO = titleVO;
    }
}