package com.apocalipta.comic.vo.v1;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by pablo on 14/05/15.
 */
public class TitleVO implements Serializable {

    private Long id;
    private PublisherVO publisherVO;
    private String code;
    private LanguageVO defaultLanguage;
    private List<LanguageVO> languages;
    private Map<Long,String> name;
    private Map<Long,String> information;
    private Long version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PublisherVO getPublisherVO() {
        return publisherVO;
    }

    public void setPublisherVO(PublisherVO publisherVO) {
        this.publisherVO = publisherVO;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LanguageVO getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(LanguageVO defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public List<LanguageVO> getLanguages() {
        return languages;
    }

    public void setLanguages(List<LanguageVO> languages) {
        this.languages = languages;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Map<Long, String> getName() {
        return name;
    }

    public void setName(Map<Long, String> name) {
        this.name = name;
    }

    public Map<Long, String> getInformation() {
        return information;
    }

    public void setInformation(Map<Long, String> information) {
        this.information = information;
    }
}