package com.apocalipta.comic.vo.v1;

import java.io.Serializable;

/**
 * Created by pablo on 21/12/14.
 */
public class LanguageVO implements Serializable {

    private static final long serialVersionUID = -7021903537050763070L;

    private Long id;
    private String code;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
