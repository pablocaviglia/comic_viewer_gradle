package com.apocalipta.comic.vo.v1;

import java.io.Serializable;

/**
 * Created by pablo on 22/12/14.
 */
public class PublicationVO implements Serializable {

    private static final long serialVersionUID = -7999025904008958660L;

    private Long id;
    private String status;
    private Integer version;
    private PublisherVO publisherVO;
    private ProjectVO projectVO;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public ProjectVO getProjectVO() {
        return projectVO;
    }

    public void setProjectVO(ProjectVO projectVO) {
        this.projectVO = projectVO;
    }

    public PublisherVO getPublisherVO() {
        return publisherVO;
    }

    public void setPublisherVO(PublisherVO publisherVO) {
        this.publisherVO = publisherVO;
    }
}
