package com.apocalipta.comic.vo.v1;

import java.io.Serializable;

/**
 * Created by pablo on 09/01/15.
 */
public class UserI18NTextVO implements Serializable {

    private Long id;
    private Long languageId;
    private String tag;
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}