package com.apocalipta.comic.vo.v1;

import java.io.Serializable;

/**
 * Created by pablo on 16/04/15.
 */
public class ProjectGenreVO implements Serializable {

    private static final long serialVersionUID = -6916467343668109706L;

    private Long id;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}