package com.apocalipta.comic.vo.v1;

import com.apocalipta.comic.constants.ResultStatus;

import java.io.Serializable;

/**
 * Created by pablo on 05/03/15.
 */
public class ResultVO implements Serializable {

    private ResultStatus resultStatus;
    private Object result;

    public ResultStatus getResultStatus() {
        return resultStatus;
    }

    public void setResultStatus(ResultStatus resultStatus) {
        this.resultStatus = resultStatus;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }
}
