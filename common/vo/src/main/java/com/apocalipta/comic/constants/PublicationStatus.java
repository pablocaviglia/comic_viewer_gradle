package com.apocalipta.comic.constants;

/**
 * Created by pablo on 21/12/14.
 */
public enum PublicationStatus {
    PUBLISHED,
    UNPUBLISHED,
    PENDING_APPROVAL
}
