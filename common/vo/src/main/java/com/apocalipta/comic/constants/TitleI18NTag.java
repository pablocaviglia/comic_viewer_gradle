package com.apocalipta.comic.constants;

/**
 * Created by pablo on 08/01/15.
 */
public enum TitleI18NTag {
    NAME,
    INFORMATION
}
