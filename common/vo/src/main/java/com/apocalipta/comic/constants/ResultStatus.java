package com.apocalipta.comic.constants;

/**
 * Created by pablo on 05/03/15.
 */
public enum ResultStatus {
    OK,
    ERROR
}
