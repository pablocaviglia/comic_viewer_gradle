package com.apocalipta.comic.vo.v1;

import java.io.Serializable;

/**
 * Created by pablo on 21/12/14.
 */
public class SubtitleVO implements Serializable {

    private static final long serialVersionUID = -4281937901210979693L;

    private String subtitle;
    private Long languageId;

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }
}
