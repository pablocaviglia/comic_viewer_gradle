package com.apocalipta.comic.vo.v1;

import java.util.ArrayList;
import java.util.List;

public class SearchResultVO<E> {

    private boolean error;
    private int total;
	private List<E> elements = new ArrayList<E>();

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<E> getElements() {
		return elements;
	}

	public void setElements(List<E> elements) {
		this.elements = elements;
	}

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
