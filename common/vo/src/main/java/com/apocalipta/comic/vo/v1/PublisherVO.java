package com.apocalipta.comic.vo.v1;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by pablo on 22/12/14.
 */
public class PublisherVO implements Serializable {

    private static final long serialVersionUID = -7284581309572251812L;

    private Long id;
    private String name;
    private Long version;
    private LanguageVO defaultLanguage;
    private List<LanguageVO> languages;
    private Map<Long,String> information;
    private Long creationDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<Long, String> getInformation() {
        return information;
    }

    public void setInformation(Map<Long, String> information) {
        this.information = information;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public LanguageVO getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(LanguageVO defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public List<LanguageVO> getLanguages() {
        return languages;
    }

    public void setLanguages(List<LanguageVO> languages) {
        this.languages = languages;
    }

    public Long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Long creationDate) {
        this.creationDate = creationDate;
    }
}