package com.apocalipta.comic.constants;

/**
 * Created by pablo on 08/01/15.
 */
public enum ProjectI18NTag {
    NAME,
    CHAPTER_NAME,
    DESCRIPTION,
    LEGAL,
    CREDITS
}
