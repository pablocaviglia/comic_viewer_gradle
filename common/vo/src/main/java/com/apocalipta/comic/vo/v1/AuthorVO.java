package com.apocalipta.comic.vo.v1;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pablo on 17/04/15.
 */
public class AuthorVO implements Serializable {

    private Long id;
    private String type;
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}