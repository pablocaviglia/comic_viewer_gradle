package com.apocalipta.comic.vo.v1;

import java.io.Serializable;

/**
 * Created by pablo on 20/04/15.
 */
public class ProjectExtendedVO extends ProjectVO implements Serializable {

    private static final long serialVersionUID = -5363016584881686758L;

    private Long id;
    private String status;
    private String code;
    private String projectName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}