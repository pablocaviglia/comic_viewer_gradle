package com.apocalipta.comic.vo.v1;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pablo on 21/12/14.
 */
public class NavigationItemVO implements Serializable {

    private static final long serialVersionUID = -7860414846974626649L;

    private Long id;
    private Integer x;
    private Integer y;
    private Integer width;
    private Integer height;
    private Integer navigationItemIndex;
    private List<SubtitleVO> subtitleVOList;
    private boolean synthetic;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getNavigationItemIndex() {
        return navigationItemIndex;
    }

    public void setNavigationItemIndex(Integer navigationItemIndex) {
        this.navigationItemIndex = navigationItemIndex;
    }

    public List<SubtitleVO> getSubtitleVOList() {
        return subtitleVOList;
    }

    public void setSubtitleVOList(List<SubtitleVO> subtitleVOList) {
        this.subtitleVOList = subtitleVOList;
    }

    public boolean isSynthetic() {
        return synthetic;
    }

    public void setSynthetic(boolean synthetic) {
        this.synthetic = synthetic;
    }
}