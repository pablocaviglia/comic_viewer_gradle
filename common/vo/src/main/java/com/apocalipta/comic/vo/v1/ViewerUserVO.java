package com.apocalipta.comic.vo.v1;

import com.apocalipta.comic.constants.Gender;
import com.apocalipta.comic.constants.UserPlatform;

import java.io.Serializable;

/**
 * Created by pablo on 02/03/15.
 */
public class ViewerUserVO implements Serializable {

    private static final long serialVersionUID = 3748812317302628326L;

    private long id;
    private String username;
    private String firstName;
    private String lastName;
    private String fullName;
    private String email;
    private String locale;
    private String photoUrl;
    private Gender gender;
    private UserPlatform platform;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public UserPlatform getPlatform() {
        return platform;
    }

    public void setPlatform(UserPlatform platform) {
        this.platform = platform;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
