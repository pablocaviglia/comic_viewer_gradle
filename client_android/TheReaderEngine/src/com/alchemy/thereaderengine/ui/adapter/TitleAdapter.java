package com.alchemy.thereaderengine.ui.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.alchemy.thereaderengine.ui.util.UILUtil;
import com.alchemy.thereaderengine.util.TitleUtil;
import com.apocalipta.comic.vo.v1.TitleVO;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by pablo on 01/05/15.
 */
public class TitleAdapter extends BaseAdapter {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private Context context;
    private Vector<TitleVO> titles = new Vector<>();

    public TitleAdapter(Context context) {
        this.context = context;
    }

    public ArrayList<TitleVO> getTitles() {
        return new ArrayList<>(titles);
    }

    public void clearTitles() {
        this.titles.clear();
        notifyDataSetChanged();
    }

    public void setTitles(List<TitleVO> titles) {
        this.titles.clear();
        addTitles(titles);
    }

    public void addTitles(List<TitleVO> titleList) {
        this.titles.addAll(titleList);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LinearLayout titleLayout;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            titleLayout = (LinearLayout) inflater.inflate(R.layout.title_item, parent, false);
        } else {
            titleLayout = (LinearLayout) convertView;
        }

        final TitleVO titleVO = titles.get(position);
        titleLayout.setTag(titleVO);

        //name
        String coide = titleVO.getCode();
        ((TextView) titleLayout.findViewById(R.id.titleCode)).setText(coide);

        //name
        String name = TitleUtil.getLocalizedText(titleVO, titleVO.getName());
        ((TextView) titleLayout.findViewById(R.id.titleName)).setText(name);

        //information
        final TextView informationText = ((TextView) titleLayout.findViewById(R.id.titleInformation));
        final String information = TitleUtil.getLocalizedText(titleVO, titleVO.getInformation());

        informationText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTitleInformation(information);
            }
        });

        informationText.setText(information);
        ViewTreeObserver vto = informationText.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                ViewTreeObserver obs = informationText.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);

                if (informationText.getLineCount() > 3) {
                    int lineEndIndex = informationText.getLayout().getLineEnd(2);
                    String text = informationText.getText().subSequence(0, lineEndIndex - 3).toString();
                    String next = "<font color='#EE0000'>" + context.getResources().getString(R.string.read_mode) + "</font>";
                    informationText.setText(Html.fromHtml(text + "... " + next));
                }
            }
        });

        //icon
        final ImageView icon = (ImageView) titleLayout.findViewById(R.id.titleIconImage);
        String url = LogicFacade.getTitleIconURL(titleVO.getId(), titleVO.getVersion());
        UILUtil.downloadImage(url, icon, true, new UILUtil.UILDownloadListener() {
            @Override
            public void loadingStarted() {

            }
            @Override
            public void downloadFinished(boolean success) {
                if(!success) {
                    icon.setImageResource(R.drawable.tre_logo);
                }
            }
        });

        return titleLayout;
    }

    private void showPublisherInfo(String info) {
        if(info != null && !info.trim().equalsIgnoreCase("")) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);

            dialog.setTitle(context.getString(R.string.description));
            dialog.setMessage(info);

            dialog.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                }
            });
            dialog.show();
        }
    }

    private void showTitleInformation(String information) {
        if(information != null && !information.trim().equalsIgnoreCase("")) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);

            dialog.setTitle(context.getString(R.string.information));
            dialog.setMessage(information);

            dialog.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                }
            });
            dialog.show();
        }
    }
}