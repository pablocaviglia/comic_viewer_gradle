package com.alchemy.thereaderengine.ui.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;

import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.ui.view.GenericView;
import com.alchemy.thereaderengine.ui.view.HelpPageView;
import com.alchemy.thereaderengine.ui.view.TutorialView;

import java.util.HashMap;
import java.util.Map;

public class HelpPagerAdapter extends PagerAdapter {

    public static final Map<Integer, View> VIEWS_MAP = new HashMap<>();

    private MainMenuActivity mainMenuActivity;

	public static final int HELP_IN_1 = 0;
    public static final int HELP_IN_2 = 1;
    public static final int HELP_IN_3 = 2;
    public static final int HELP_SLIDE = 3;
    public static final int HELP_TAP = 4;
    public static final int HELP_ZOOM = 5;
    public static final int HELP_PRESS = 6;
    public static final int HELP_DOUBLE_TAP = 7;
    public static final int HELP_OUT_1 = 8;
    public static final int HELP_OUT_2 = 9;

    private LayoutInflater inflater;

    public HelpPagerAdapter(MainMenuActivity mainMenuActivity, OnTouchListener onTouchListener, TutorialView tutorialView) {

        inflater = (LayoutInflater) mainMenuActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        this.mainMenuActivity = mainMenuActivity;
        VIEWS_MAP.put(HELP_IN_1, new HelpPageView(mainMenuActivity, onTouchListener, tutorialView, R.drawable.tutorial_1, true));
        VIEWS_MAP.put(HELP_IN_2, new HelpPageView(mainMenuActivity, onTouchListener, tutorialView, R.drawable.tutorial_2, true));
        VIEWS_MAP.put(HELP_IN_3, new HelpPageView(mainMenuActivity, onTouchListener, tutorialView, R.drawable.tutorial_3, true));
        VIEWS_MAP.put(HELP_SLIDE, new HelpPageView(mainMenuActivity, onTouchListener, tutorialView, R.drawable.tutorial_4, true));
        VIEWS_MAP.put(HELP_TAP, new HelpPageView(mainMenuActivity, onTouchListener, tutorialView, R.drawable.tutorial_5, true));
        VIEWS_MAP.put(HELP_DOUBLE_TAP, new HelpPageView(mainMenuActivity, onTouchListener, tutorialView, R.drawable.tutorial_6, true));
        VIEWS_MAP.put(HELP_ZOOM, new HelpPageView(mainMenuActivity, onTouchListener, tutorialView, R.drawable.tutorial_7, true));
        VIEWS_MAP.put(HELP_PRESS, new HelpPageView(mainMenuActivity, onTouchListener, tutorialView, R.drawable.tutorial_8, true));
        VIEWS_MAP.put(HELP_OUT_1, new HelpPageView(mainMenuActivity, onTouchListener, tutorialView, R.drawable.tutorial_9, false));
        VIEWS_MAP.put(HELP_OUT_2, new HelpPageView(mainMenuActivity, onTouchListener, tutorialView, R.drawable.tutorial_exit, false));
	}

	public int getCount() {
		return VIEWS_MAP.size();
	}

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {

        //inflate the view
        View instanceView = VIEWS_MAP.get(position);

        //inflate the view layout
        View viewLayout = inflater.inflate(R.layout.help_page_view, null, false);

        //set the layout
        instanceView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

        //get the generic view
        GenericView genericView = (GenericView) instanceView;
        //add it
        genericView.addView(viewLayout);
        //init the view
        genericView.onCreateView(viewLayout);

        //add it to the collection
        collection.addView(instanceView, 0);
        //set the tag
        instanceView.setTag("TAGHelp_" + position);

        return instanceView;
	}

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

	@Override
	public boolean isViewFromObject(View arg0, Object view) {
		return arg0 == view;
	}

	@Override
	public Parcelable saveState() {
		return null;
	}

}