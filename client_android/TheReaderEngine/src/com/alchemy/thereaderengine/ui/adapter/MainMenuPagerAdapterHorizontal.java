package com.alchemy.thereaderengine.ui.adapter;

import android.app.Activity;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout.LayoutParams;

import com.alchemy.thereaderengine.ui.view.GenericView;
import com.alchemy.thereaderengine.ui.view.MyLibraryView;
import com.alchemy.thereaderengine.ui.view.AllPublicationsView;
import com.alchemy.thereaderengine.ui.view.PublicationListGenericView;
import com.alchemy.thereaderengine.ui.view.PublisherListView;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.util.ArrayList;
import java.util.List;

public class MainMenuPagerAdapterHorizontal extends PagerAdapter {

    private static final Logger LOGGER = LoggerManager.getLogger();

    public static final int VIEW_ALL 			= 0;
    public static final int VIEW_MY_LIBRARY     = 1;
	public static final int VIEW_PUBLISHERS     = 2;

	private List<GenericView> views;
	
	public MainMenuPagerAdapterHorizontal(Activity activity, PublicationListGenericView.PublicationListLoaderListener publicationListLoaderListener) {
		views = new ArrayList<>();
		views.add(VIEW_ALL, new AllPublicationsView(activity, publicationListLoaderListener));
        views.add(VIEW_MY_LIBRARY, new MyLibraryView(activity, publicationListLoaderListener));
		views.add(VIEW_PUBLISHERS, new PublisherListView(activity));
	}
	
	public int getCount() {
		return views.size();
	}
	
	public Object instantiateItem(ViewGroup collection, int position) {
		View view = views.get(position);
		view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		collection.addView(view, 0);
		return view;
	}

    public void refreshViews() {
        for(GenericView genericView : views) {
            if(genericView instanceof PublicationListGenericView) {
                ((PublicationListGenericView)genericView).getPublicationAdapter().notifyDataSetChanged();
            }
        }
    }

    public void refreshMyLibrary() {
        ((PublicationListGenericView)views.get(VIEW_MY_LIBRARY)).getPublicationsGeneric();
    }
	
	@Override
	public void destroyItem(ViewGroup arg0, int arg1, Object view) {
		arg0.removeView((View) view);
	}
	
	@Override
	public boolean isViewFromObject(View arg0, Object view) {
		return arg0 == view;
	}
	
	@Override
	public Parcelable saveState() {
		return null;
	}

	public List<GenericView> getViews() {
		return views;
	}
}