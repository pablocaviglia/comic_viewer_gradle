package com.alchemy.thereaderengine.ui.adapter;

import android.app.Activity;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.alchemy.thereaderengine.ui.view.ConfigView;
import com.alchemy.thereaderengine.ui.view.GenericView;
import com.alchemy.thereaderengine.ui.view.PublicationListGenericView;
import com.alchemy.thereaderengine.ui.view.SearchByPublisherView;
import com.alchemy.thereaderengine.ui.view.SearchByTitleView;
import com.alchemy.thereaderengine.ui.view.SearchCustomView;
import com.alchemy.thereaderengine.ui.view.component.CustomViewPager;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 18/02/15.
 */
public class SearchPagerAdapterHorizontal extends PagerAdapter {

    private static final Logger LOGGER = LoggerManager.getLogger();

    public static final int VIEW_CUSTOM         = 0;
    public static final int VIEW_BY_PUBLISHER   = 1;
    public static final int VIEW_BY_TITLE       = 2;

    private List<GenericView> views;

    public SearchPagerAdapterHorizontal(Activity activity) {
        views = new ArrayList<>();
        views.add(VIEW_CUSTOM, new SearchCustomView(activity));
        views.add(VIEW_BY_PUBLISHER, new SearchByPublisherView(activity));
        views.add(VIEW_BY_TITLE, new SearchByTitleView(activity));
    }

    public int getCount() {
        return views.size();
    }

    public Object instantiateItem(ViewGroup collection, int position) {
        View view = getView(position);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        collection.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup viewGroup, int arg1, Object view) {
        viewGroup.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View arg0, Object view) {
        return arg0 == view;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public View getView(int view) {
        return views.get(view);
    }

    public void refreshViews() {
        for(GenericView genericView : views) {
            genericView.refreshView();
        }
    }

    public List<GenericView> getViews() {
        return views;
    }

}