package com.alchemy.thereaderengine.ui.adapter;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.ui.view.PublicationInfoView;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.ui.view.GenericView;
import com.alchemy.thereaderengine.ui.view.PublicationReadView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pablo on 18/02/15.
 */
public class PublicationMenuPagerAdapterHorizontal extends PagerAdapter {

    public static final Map<Integer, Integer> LAYOUTS_MAP = new HashMap<Integer, Integer>();
    public static final Map<Integer, GenericView> VIEWS_MAP = new HashMap<Integer, GenericView>();

    public static final int VIEW_READ = 0;
    public static final int VIEW_INFO = 1;

    private MainMenuActivity mainMenuActivity;

    public PublicationMenuPagerAdapterHorizontal(MainMenuActivity mainMenuActivity) {
        this.mainMenuActivity = mainMenuActivity;

        //layouts
        LAYOUTS_MAP.put(VIEW_READ, R.layout.publication_read_view);
        LAYOUTS_MAP.put(VIEW_INFO, R.layout.publication_info_view);

        //views
        VIEWS_MAP.put(VIEW_READ, new PublicationReadView(mainMenuActivity));
        VIEWS_MAP.put(VIEW_INFO, new PublicationInfoView(mainMenuActivity));

    }

    public int getCount() {
        return LAYOUTS_MAP.size();
    }

    public Object instantiateItem(ViewGroup collection, int position) {

        LayoutInflater inflater = (LayoutInflater) mainMenuActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //inflate the view
        View instanceView = VIEWS_MAP.get(position);

        //inflate the view layout
        View viewLayout = inflater.inflate(LAYOUTS_MAP.get(position),null, false);

        //set the layout
        instanceView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));

        if(instanceView instanceof GenericView) {
            GenericView genericView = (GenericView) instanceView;
            //add it
            genericView.addView(viewLayout);
            //init the view
            genericView.onCreateView(viewLayout);
        }

        //add it to the collection
        collection.addView(instanceView, 0);
        //set the tag
        instanceView.setTag("TAGPublication_" + position);

        return instanceView;
    }

    @Override
    public void destroyItem(ViewGroup arg0, int arg1, Object view) {
        arg0.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View arg0, Object view) {
        return arg0 == view;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public View getView(int view) {
        return VIEWS_MAP.get(view);
    }
}