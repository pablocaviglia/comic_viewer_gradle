package com.alchemy.thereaderengine.ui.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.alchemy.thereaderengine.ui.util.UILUtil;
import com.alchemy.thereaderengine.util.PublicationUtil;
import com.apocalipta.comic.constants.ProjectAgeRate;
import com.apocalipta.comic.vo.v1.AuthorVO;
import com.apocalipta.comic.vo.v1.PublicationVO;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by pablo on 10/12/14.
 */
public class PublicationAdapter extends BaseAdapter {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private Context context;
    private Vector<PublicationVO> publications = new Vector<>();

    public PublicationAdapter(Context context) {
        this.context = context;
    }

    public ArrayList<PublicationVO> getPublications() {
        return new ArrayList<>(publications);
    }

    public void clearPublications() {
        this.publications.clear();
        notifyDataSetChanged();
    }

    public void setPublications(List<PublicationVO> publications) {
        this.publications.clear();
        addPublications(publications);
    }

    public void addPublications(List<PublicationVO> publicationList) {
        this.publications.addAll(publicationList);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return publications.size();
    }
    @Override
    public Object getItem(int position) {
        return null;
    }
    @Override
    public long getItemId(int position) {
        return position;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LinearLayout publicationLayout;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            publicationLayout = (LinearLayout) inflater.inflate(com.alchemy.thereaderengine.R.layout.publication_item, parent, false);
        } else {
            publicationLayout = (LinearLayout) convertView;
        }

        final PublicationVO publication = publications.get(position);
        publicationLayout.setTag(publication);

        //name
        String name = PublicationUtil.getLocalizedText(publication, publication.getProjectVO().getName());
        ((TextView) publicationLayout.findViewById(com.alchemy.thereaderengine.R.id.publicationName)).setText(name);

        //chapter name
        String chapterName = PublicationUtil.getLocalizedText(publication, publication.getProjectVO().getChapterName());
        ((TextView) publicationLayout.findViewById(com.alchemy.thereaderengine.R.id.publicationChapterName)).setText(chapterName);

        //publisher name
        String publisherName = publication.getPublisherVO().getName();
        TextView publisherNameText = ((TextView) publicationLayout.findViewById(R.id.publisherNameText));
        publisherNameText.setText(publisherName);
        publisherNameText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainMenuActivity mainMenuActivity = (MainMenuActivity) context;
                mainMenuActivity.showPublisher(publication.getPublisherVO().getId(), true);
            }
        });

        //authors
        List<AuthorVO> authors = publication.getProjectVO().getAuthors();
        if(null != authors && authors.size() > 0) {
            String authorsStr = "";
            int i = 0;
            for(AuthorVO authorVO : authors) {
                authorsStr += (i==0 ? "" : " / ") + authorVO.getName();
                i++;
            }
            ((TextView) publicationLayout.findViewById(R.id.authorsText)).setText(authorsStr);
            publicationLayout.findViewById(R.id.authorsLinearLayout).setVisibility(View.VISIBLE);
        }
        else {
            publicationLayout.findViewById(R.id.authorsLinearLayout).setVisibility(View.GONE);
        }

        //genre
        if(null != publication.getProjectVO().getGenre()) {
            ((TextView) publicationLayout.findViewById(R.id.genreText)).setText(publication.getProjectVO().getGenre().getDescription());
            publicationLayout.findViewById(R.id.genreLinearLayout).setVisibility(View.VISIBLE);
        }
        else {
            publicationLayout.findViewById(R.id.genreLinearLayout).setVisibility(View.GONE);
        }

        //description
        final String description = PublicationUtil.getLocalizedText(publication, publication.getProjectVO().getDescription());
        final TextView descriptionText = ((TextView) publicationLayout.findViewById(com.alchemy.thereaderengine.R.id.publicationDescription));
        descriptionText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPublicationDescription(description);
            }
        });

        descriptionText.setText(description);
        ViewTreeObserver vto = descriptionText.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                ViewTreeObserver obs = descriptionText.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);

                if (descriptionText.getLineCount() > 3) {
                    int lineEndIndex = descriptionText.getLayout().getLineEnd(2);
                    String text = descriptionText.getText().subSequence(0, lineEndIndex - 3).toString();
                    String next = "<font color='#EE0000'>" + context.getResources().getString(com.alchemy.thereaderengine.R.string.read_mode) + "</font>";
                    descriptionText.setText(Html.fromHtml(text + "... " + next));
                }
            }
        });

        ProjectAgeRate projectAgeRate = ProjectAgeRate.valueOf(publication.getProjectVO().getAgeRate());
        ImageView ageRateImageView = (ImageView) publicationLayout.findViewById(com.alchemy.thereaderengine.R.id.agerate_image);
        if(projectAgeRate == ProjectAgeRate.PLUS_18) {
            ageRateImageView.setImageResource(com.alchemy.thereaderengine.R.drawable.rate_a);
        }
        else if(projectAgeRate == ProjectAgeRate.PLUS_13) {
            ageRateImageView.setImageResource(com.alchemy.thereaderengine.R.drawable.rate_t);
        }
        else {
            ageRateImageView.setImageResource(com.alchemy.thereaderengine.R.drawable.rate_e);
        }

        //image
        ImageView coverPreview = (ImageView) publicationLayout.findViewById(com.alchemy.thereaderengine.R.id.coverPreview);
        String url = LogicFacade.getPublicationCoverURL(publication.getId(), publication.getVersion());
        UILUtil.downloadImage(url, coverPreview, true);

        return publicationLayout;
    }

    private void showPublicationDescription(String description) {
        if(description != null && !description.trim().equalsIgnoreCase("")) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);

            dialog.setTitle(context.getString(com.alchemy.thereaderengine.R.string.description));
            dialog.setMessage(description);

            dialog.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                }
            });
            dialog.show();
        }
    }
}
