package com.alchemy.thereaderengine.ui.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.alchemy.thereaderengine.ui.util.UILUtil;
import com.alchemy.thereaderengine.util.PublisherUtil;
import com.apocalipta.comic.vo.v1.PublisherVO;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

/**
 * Created by pablo on 01/05/15.
 */
public class PublisherAdapter extends BaseAdapter {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private SimpleDateFormat sdf = new SimpleDateFormat("LLLL dd, yyyy");

    private Context context;
    private Vector<PublisherVO> publishers = new Vector<>();

    public PublisherAdapter(Context context) {
        this.context = context;
    }

    public ArrayList<PublisherVO> getPublishers() {
        return new ArrayList<>(publishers);
    }

    public void clearPublishers() {
        this.publishers.clear();
        notifyDataSetChanged();
    }

    public void setPublishers(List<PublisherVO> publishers) {
        this.publishers.clear();
        addPublishers(publishers);
    }

    public void addPublishers(List<PublisherVO> publisherList) {
        this.publishers.addAll(publisherList);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return publishers.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final LinearLayout publisherLayout;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            publisherLayout = (LinearLayout) inflater.inflate(R.layout.publisher_item, parent, false);
        } else {
            publisherLayout = (LinearLayout) convertView;
        }

        final PublisherVO publisherVO = publishers.get(position);
        publisherLayout.setTag(publisherVO);

        //name
        String name = publisherVO.getName();
        ((TextView) publisherLayout.findViewById(R.id.publisherName)).setText(name);

        //member since
        String memberSince = context.getString(R.string.member_since) + " " + sdf.format(new Date(publisherVO.getCreationDate()));
        ((TextView) publisherLayout.findViewById(R.id.publisherCreationDate)).setText(memberSince);

        //information
        final String information = PublisherUtil.getLocalizedText(publisherVO, publisherVO.getInformation());
        final TextView informationText = ((TextView) publisherLayout.findViewById(R.id.publisherInformation));
        informationText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPublisherInfo(information);
            }
        });

        informationText.setText(information);
        ViewTreeObserver vto = informationText.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                ViewTreeObserver obs = informationText.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);

                if (informationText.getLineCount() > 3) {
                    int lineEndIndex = informationText.getLayout().getLineEnd(2);
                    String text = informationText.getText().subSequence(0, lineEndIndex - 3).toString();
                    String next = "<font color='#EE0000'>" + context.getResources().getString(com.alchemy.thereaderengine.R.string.read_mode) + "</font>";
                    informationText.setText(Html.fromHtml(text + "... " + next));
                }
            }
        });

        //avatar
        final ImageView avatarPreview = (ImageView) publisherLayout.findViewById(R.id.publisherAvatarImage);
        String url = LogicFacade.getPublisherAvatarURL(publisherVO.getId(), publisherVO.getVersion());
        UILUtil.downloadImage(url, avatarPreview, true);

        return publisherLayout;
    }

    private void showPublisherInfo(String info) {
        if(info != null && !info.trim().equalsIgnoreCase("")) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);

            dialog.setTitle(context.getString(com.alchemy.thereaderengine.R.string.description));
            dialog.setMessage(info);

            dialog.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                }
            });
            dialog.show();
        }
    }
}