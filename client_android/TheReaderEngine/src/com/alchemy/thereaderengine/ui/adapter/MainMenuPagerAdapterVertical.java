package com.alchemy.thereaderengine.ui.adapter;

import android.app.Activity;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.ui.view.ConfigView;
import com.alchemy.thereaderengine.ui.view.TutorialView;
import com.alchemy.thereaderengine.ui.view.component.CustomViewPager;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 18/02/15.
 */
public class MainMenuPagerAdapterVertical extends PagerAdapter {

    private static final Logger LOGGER = LoggerManager.getLogger();

    public static final int VIEW_MAINMENU       = 0;
    public static final int VIEW_PUBLICATION    = 1;
    public static final int VIEW_CONFIG         = 2;
    public static final int VIEW_PUBLISHER      = 3;
    public static final int VIEW_SEARCH         = 4;
    public static final int VIEW_TITLE          = 5;
    public static final int VIEW_TUTORIAL       = 6;

    private List<View> views;

    public MainMenuPagerAdapterVertical(MainMenuActivity activity) {
        views = new ArrayList<>();
        views.add(VIEW_MAINMENU, new CustomViewPager(activity));
        views.add(VIEW_PUBLICATION, new CustomViewPager(activity));
        views.add(VIEW_CONFIG, new ConfigView(activity));
        views.add(VIEW_PUBLISHER, new CustomViewPager(activity));
        views.add(VIEW_SEARCH, new CustomViewPager(activity));
        views.add(VIEW_TITLE, new CustomViewPager(activity));
        views.add(VIEW_TUTORIAL, new TutorialView(activity));
    }

    public int getCount() {
        return views.size();
    }

    public Object instantiateItem(ViewGroup collection, int position) {
        View view = getView(position);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        collection.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup viewGroup, int arg1, Object view) {
        viewGroup.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(View arg0, Object view) {
        return arg0 == view;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public View getView(int view) {
        return views.get(view);
    }
}