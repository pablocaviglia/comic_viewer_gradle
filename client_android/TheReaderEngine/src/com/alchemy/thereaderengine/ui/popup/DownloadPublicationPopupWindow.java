package com.alchemy.thereaderengine.ui.popup;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.alchemy.thereaderengine.enums.PublicationDownloadState;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.listener.PublicationDownloadListener;
import com.alchemy.thereaderengine.model.DownloadPageStatus;
import com.alchemy.thereaderengine.ui.view.DownloadPublicationView;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

/**
 * Created by pablo on 03/01/15.
 */
public class DownloadPublicationPopupWindow extends PopupWindow implements PopupWindow.OnDismissListener, PublicationDownloadListener {

    private static final Logger LOGGER = LoggerManager.getLogger();
    private DownloadPublicationView downloadPublicationView;

    public DownloadPublicationPopupWindow(Context context) {
        super(context);

        //hide & show animation
        setAnimationStyle(R.style.popup_animation);

        //set view
        downloadPublicationView = new DownloadPublicationView(context);
        setContentView(downloadPublicationView);

        //dont allow close
        setFocusable(true);

        setOnDismissListener(this);

        setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        update();

    }

    public void show() {
        //show popup
        showAtLocation(getContentView(), Gravity.CENTER, 0, 0);
        //listen for publication download events
        LogicFacade.registerPublicationDownloadListener(this);
    }

    @Override
    public void onDismiss() {
        //listen for publication download events
        LogicFacade.unregisterPublicationDownloadListener(this);
    }

    @Override
    public void publicationDownloadProgress(final DownloadPageStatus downloadPageStatus) {
        final Activity activity = (Activity) getContentView().getContext();
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(!downloadPageStatus.transferFinished) {
                    downloadPublicationView.progress(downloadPageStatus);
                }
                else {

                    //set the new status
                    downloadPublicationView.progress(downloadPageStatus);

                    //dismiss dialog
                    dismiss();

                    if(downloadPageStatus.publicationDownloadState == PublicationDownloadState.DOWNLOADED) {
                        Toast.makeText(activity, activity.getString(R.string.publication_downloaded), Toast.LENGTH_LONG).show();
                    }
                    else if(downloadPageStatus.publicationDownloadState == PublicationDownloadState.ERROR) {
                        showDownloadErrorDialog(activity);
                    }

                    //unregister listener
                    LogicFacade.unregisterPublicationDownloadListener(DownloadPublicationPopupWindow.this);
                }
            }
        });
    }

    public void showDownloadErrorDialog(final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AlertDialog alertDialog = new AlertDialog.Builder(activity).create();

                int iconResId = android.R.drawable.ic_dialog_alert;
                String titleStr = activity.getString(R.string.publication_download);
                String descStr = activity.getString(R.string.publication_download_error);

                // Setting Dialog Title
                alertDialog.setTitle(titleStr);

                // Setting Dialog Message
                alertDialog.setMessage(descStr);

                // Setting Icon to Dialog
                alertDialog.setIcon(iconResId);

                // Setting OK Button
                alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                // Showing Alert Message
                alertDialog.show();
            }
        });
    }
}