package com.alchemy.thereaderengine.ui.util;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

import com.alchemy.thereaderengine.App;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * Created by pablo on 28/12/14.
 */
public class UILUtil {

    public static Bitmap downloadImageSync(final String uri, boolean secure) {
        final ImageLoader imageLoader = secure ? App.secureImageLoader : App.normalImageLoader;
        return imageLoader.loadImageSync(uri, App.options);
    }

    public static InputStream downloadImageStreamSync(final String uri, boolean secure) {
        if(null != uri) {
            final ImageLoader imageLoader = secure ? App.secureImageLoader : App.normalImageLoader;
            Bitmap bitmap = imageLoader.loadImageSync(uri, App.options);
            if(null != bitmap) {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();
                ByteArrayInputStream bs = new ByteArrayInputStream(bitmapdata);
                return bs;
            }
            return null;
        }
        return null;
    }

    public static void downloadImage(final String uri, final ImageView imageView, boolean secure) {
        downloadImage(uri, imageView, secure, null);
    }

    public static void downloadImage(final String uri, final ImageView imageView, boolean secure, final UILDownloadListener listener) {
        final ImageLoader imageLoader = secure ? App.secureImageLoader : App.normalImageLoader;
        imageLoader.displayImage(
                uri,
                imageView,
                App.options,
                new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String arg0, View arg1) {
                        if(listener != null) {
                            listener.loadingStarted();
                        }
                    }
                    @Override
                    public void onLoadingFailed(String url, View arg1, FailReason reason) {
                        if(listener != null) {
                            listener.downloadFinished(false);
                        }
                    }
                    @Override
                    public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
                        if(listener != null) {
                            listener.downloadFinished(true);
                        }
                    }
                    @Override
                    public void onLoadingCancelled(String arg0, View arg1) {
                        if(listener != null) {
                            listener.downloadFinished(false);
                        }
                    }
                });
    }

    public static void removeFromCache(String uri, boolean secure) {
        ImageLoader imageLoader = secure ? App.secureImageLoader : App.normalImageLoader;
        MemoryCacheUtils.removeFromCache(uri, imageLoader.getMemoryCache());
        DiskCacheUtils.removeFromCache(uri, imageLoader.getDiskCache());
    }

    public interface UILDownloadListener {
        void loadingStarted();
        void downloadFinished(boolean success);
    }
}
