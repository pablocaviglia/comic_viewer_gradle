package com.alchemy.thereaderengine.ui.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.alchemy.thereaderengine.MainMenuActivity;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.io.IOException;
import java.io.InputStream;

public class ParallaxViewPager extends ViewPager {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private InputStream backgroundInputStream;
    private int savedWidth = -1;
    private int savedHeight = -1;
    private int savedMaxNumPages = -1;
    private Bitmap savedBitmap;

    private int maxNumPages =0;
    private int imageHeight;
    private int imageWidth;
    private float zoomLevel;
    private float overlapLevel;
    private Rect src = new Rect(), dst = new Rect();

    private boolean pagingEnabled = true;
    private boolean parallaxEnabled = true;

    private Paint paint = new Paint();

    public ParallaxViewPager(Context context) {
        super(context);
    }

    public ParallaxViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @SuppressLint("NewApi")
    private int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else {
            return data.getByteCount();
        }
    }

    private void setNewBackground() {
        if (null == backgroundInputStream)
            return;

        if (maxNumPages == 0)
            return;

        if (getWidth()==0 || getHeight()==0)
            return;

        if ((savedHeight == getHeight()) && (savedWidth == getWidth()) && (savedMaxNumPages == maxNumPages))
            return;

        paint.setAlpha(100);

        try {

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(backgroundInputStream, null, options);

            imageHeight = options.outHeight;
            imageWidth = options.outWidth;
            LOGGER.i("imageHeight=" + imageHeight + ", imageWidth=" + imageWidth);

            zoomLevel = ((float) imageHeight) / getHeight();  // we are always in 'fitY' mode

            options.inJustDecodeBounds = false;
            options.inSampleSize = Math.round(zoomLevel);

            if (options.inSampleSize > 1) {
                imageHeight = imageHeight / options.inSampleSize;
                imageWidth = imageWidth / options.inSampleSize;
            }

            zoomLevel = ((float) imageHeight) / getHeight();  // we are always in 'fitY' mode
            overlapLevel = zoomLevel * Math.min(Math.max(imageWidth / zoomLevel - getWidth(), 0) / (maxNumPages - 1), getWidth() * 2); // how many pixels to shift for each panel

            LOGGER.i("imageHeight=" + imageHeight + ", imageWidth=" + imageWidth + ", zoomLevel=" + zoomLevel + ", overlapLevel=" + overlapLevel);


            backgroundInputStream.reset();
            savedBitmap = BitmapFactory.decodeStream(backgroundInputStream, null, options);

            LOGGER.i("real bitmap size = " + sizeOf(savedBitmap) / 1024);
            LOGGER.i("savedBitmap.getHeight()=" + savedBitmap.getHeight() + ", savedBitmap.getWidth()=" + savedBitmap.getWidth());

            backgroundInputStream.close();
        } catch (IOException e) {
            LOGGER.e("Cannot decode: " + e.getMessage());
            return;
        }

        savedHeight = getHeight();
        savedWidth = getWidth();
        savedMaxNumPages = maxNumPages;

        ((MainMenuActivity)getContext()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                invalidate();
            }
        });
    }
    int currentPosition =-1;
    float current_offset=0.0f;

    @Override
    protected void onPageScrolled(int position, float offset, int offsetPixels) {
        super.onPageScrolled(position, offset, offsetPixels);
        currentPosition = position;
        current_offset = offset;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (savedBitmap != null && parallaxEnabled) {
            if (currentPosition == -1)
                currentPosition = getCurrentItem();
            // maybe we could get the current position from the getScrollX instead?
            src.set((int) (overlapLevel * (currentPosition + current_offset)), 0,
                    (int) (overlapLevel * (currentPosition + current_offset) + (getWidth() * zoomLevel)), imageHeight);

            dst.set(getScrollX(), 0,
                    getScrollX() + canvas.getWidth(), canvas.getHeight());

            canvas.drawColor(Color.WHITE);
            canvas.drawBitmap(savedBitmap, src, dst, paint);
        }
    }

    public void setMaxPages(int maxNumPages) {
        this.maxNumPages = maxNumPages;
        setNewBackground();
    }

    public void setBackgroundStream(InputStream backgroundInputStream) {
        this.savedWidth = 0;
        this.savedHeight = 0;
        this.backgroundInputStream = backgroundInputStream;
        setNewBackground();
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        if (savedBitmap != null && parallaxEnabled)
            setNewBackground();
    }

    @Override
    public void setCurrentItem(int item) {
        super.setCurrentItem(item);
        currentPosition = item;
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.pagingEnabled) {
            return super.onTouchEvent(event);
        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        // fix for https://github.com/JakeWharton/Android-ViewPagerIndicator/issues/72
        if(isFakeDragging()) {
            return false;
        }
        if (this.pagingEnabled) {
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }

    public boolean isPagingEnabled() {
        return pagingEnabled;
    }

    /**
     * Enables or disables paging for this ViewPagerParallax.
     */
    public void setPagingEnabled(boolean pagingEnabled) {
        this.pagingEnabled = pagingEnabled;
    }

    public boolean isParallaxEnabled() {
        return parallaxEnabled;
    }

    /**
     * Enables or disables parallax effect for this ViewPagerParallax.
     * @param parallaxEnabled
     */
    public void setParallaxEnabled(boolean parallaxEnabled) {
        this.parallaxEnabled = parallaxEnabled;
    }

    protected void onDetachedFromWindow() {
        if (savedBitmap != null) {
            savedBitmap.recycle();
            savedBitmap = null;
        }
        super.onDetachedFromWindow();
    }
}