package com.alchemy.thereaderengine.ui.viewer;

import android.widget.Toast;

import com.alchemy.thereaderengine.App;
import com.alchemy.thereaderengine.ComicViewerActivity;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.enums.AutoPlayTime;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

/**
 * Created by pablo on 11/03/15.
 */
public class PlayControlsEntity extends Rectangle implements PlayerButtonListener {

    private static final Logger LOGGER = LoggerManager.getLogger();

    public static final int PLAY_BAR_HEIGHT = 100;

    private PlayerControlButton autoplayBackSprite;
    private PlayerControlButton autoplayPauseSprite;
    private PlayerControlButton autoplayPlaySprite;
    private PlayerControlButton autoplayStopSprite;
    private PlayerControlButton autoplayX1Sprite;
    private PlayerControlButton autoplayX2Sprite;
    private PlayerControlButton autoplayX3Sprite;
    private PlayerControlButton autoplayX4Sprite;

    private ComicViewerActivity comicViewerActivity;

    public PlayControlsEntity(ComicViewerActivity comicViewerActivity) {
        super(0, 0, App.SCREEN_WIDTH, PLAY_BAR_HEIGHT, comicViewerActivity.getVertexBufferObjectManager());
        this.comicViewerActivity = comicViewerActivity;

        //back color
        setColor(Color.TRANSPARENT);

        int BUTTON_WIDTH = 104;
        int BUTTONS_QNTY = 4;

        float buttonsContainerWidth = BUTTON_WIDTH * BUTTONS_QNTY;
        float buttonsContainerX = (getWidth() - buttonsContainerWidth) / 2f;

        Rectangle buttonContainer = new Rectangle(buttonsContainerX, 0, buttonsContainerWidth, getHeight(), getVertexBufferObjectManager());
        buttonContainer.setColor(Color.TRANSPARENT);

        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("");

        {
            //autoplay back
            BitmapTextureAtlas texture = new BitmapTextureAtlas(comicViewerActivity.getTextureManager(), 104, 104, TextureOptions.DEFAULT);
            ITextureRegion textureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(texture, comicViewerActivity, "autoplay_back.png", 0, 0);
            texture.load();
            autoplayBackSprite = new PlayerControlButton(BUTTON_WIDTH * 2f, textureRegion, getVertexBufferObjectManager());
        }

        {
            //autoplay pause
            BitmapTextureAtlas texture = new BitmapTextureAtlas(comicViewerActivity.getTextureManager(), 104, 104, TextureOptions.DEFAULT);
            ITextureRegion textureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(texture, comicViewerActivity, "autoplay_pause.png", 0, 0);
            texture.load();
            autoplayPauseSprite = new PlayerControlButton(BUTTON_WIDTH * 0f, textureRegion, getVertexBufferObjectManager());
        }

        {
            //autoplay play
            BitmapTextureAtlas texture = new BitmapTextureAtlas(comicViewerActivity.getTextureManager(), 104, 104, TextureOptions.DEFAULT);
            ITextureRegion textureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(texture, comicViewerActivity, "autoplay_play.png", 0, 0);
            texture.load();
            autoplayPlaySprite = new PlayerControlButton(BUTTON_WIDTH * 0f, textureRegion, getVertexBufferObjectManager());
        }

        {
            //autoplay stop
            BitmapTextureAtlas texture = new BitmapTextureAtlas(comicViewerActivity.getTextureManager(), 104, 104, TextureOptions.DEFAULT);
            ITextureRegion textureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(texture, comicViewerActivity, "autoplay_stop.png", 0, 0);
            texture.load();
            autoplayStopSprite = new PlayerControlButton(BUTTON_WIDTH * 1f, textureRegion, getVertexBufferObjectManager());
        }

        {
            //autoplay x1
            BitmapTextureAtlas texture = new BitmapTextureAtlas(comicViewerActivity.getTextureManager(), 104, 104, TextureOptions.DEFAULT);
            ITextureRegion textureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(texture, comicViewerActivity, "autoplay_x1.png", 0, 0);
            texture.load();
            autoplayX1Sprite = new PlayerControlButton(BUTTON_WIDTH * 3f, textureRegion, getVertexBufferObjectManager());
        }

        {
            //autoplay x2
            BitmapTextureAtlas texture = new BitmapTextureAtlas(comicViewerActivity.getTextureManager(), 104, 104, TextureOptions.DEFAULT);
            ITextureRegion textureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(texture, comicViewerActivity, "autoplay_x2.png", 0, 0);
            texture.load();
            autoplayX2Sprite = new PlayerControlButton(BUTTON_WIDTH * 3f, textureRegion, getVertexBufferObjectManager());
        }

        {
            //autoplay x3
            BitmapTextureAtlas texture = new BitmapTextureAtlas(comicViewerActivity.getTextureManager(), 104, 104, TextureOptions.DEFAULT);
            ITextureRegion textureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(texture, comicViewerActivity, "autoplay_x3.png", 0, 0);
            texture.load();
            autoplayX3Sprite = new PlayerControlButton(BUTTON_WIDTH * 3f, textureRegion, getVertexBufferObjectManager());
        }

        {
            //autoplay x4
            BitmapTextureAtlas texture = new BitmapTextureAtlas(comicViewerActivity.getTextureManager(), 104, 104, TextureOptions.DEFAULT);
            ITextureRegion textureRegion = BitmapTextureAtlasTextureRegionFactory.createFromAsset(texture, comicViewerActivity, "autoplay_x4.png", 0, 0);
            texture.load();
            autoplayX4Sprite = new PlayerControlButton(BUTTON_WIDTH * 3f, textureRegion, getVertexBufferObjectManager());
        }


        buttonContainer.attachChild(autoplayPlaySprite);
        buttonContainer.attachChild(autoplayStopSprite);
        buttonContainer.attachChild(autoplayBackSprite);
        buttonContainer.attachChild(autoplayPauseSprite);
        buttonContainer.attachChild(autoplayX1Sprite);
        buttonContainer.attachChild(autoplayX2Sprite);
        buttonContainer.attachChild(autoplayX3Sprite);
        buttonContainer.attachChild(autoplayX4Sprite);
        attachChild(buttonContainer);

        comicViewerActivity.scene.registerTouchArea(autoplayPauseSprite);
        comicViewerActivity.scene.registerTouchArea(autoplayPlaySprite);
        comicViewerActivity.scene.registerTouchArea(autoplayStopSprite);
        comicViewerActivity.scene.registerTouchArea(autoplayBackSprite);
        comicViewerActivity.scene.registerTouchArea(autoplayX1Sprite);
        comicViewerActivity.scene.registerTouchArea(autoplayX2Sprite);
        comicViewerActivity.scene.registerTouchArea(autoplayX3Sprite);
        comicViewerActivity.scene.registerTouchArea(autoplayX4Sprite);

        reconfigureButtons();

    }

    private void setButtonStatus(PlayerControlButton button, boolean enabled) {
        if(enabled) {
            button.show();
        }
        else {
            button.hide();
        }
    }

    @Override
    public void buttonTouched(PlayerControlButton button) {
        if(button == autoplayBackSprite) {
            comicViewerActivity.pageNavigatorViewer.movePreviousRectangle();
        }
        else if(button == autoplayPauseSprite) {
            comicViewerActivity.setAutoPlaybackPaused(true);
            Toast.makeText(comicViewerActivity, comicViewerActivity.getResources().getString(R.string.autoplay_paused), Toast.LENGTH_SHORT).show();
        }
        else if(button == autoplayPlaySprite) {
            comicViewerActivity.setAutoPlaybackEnabled(true);
            Toast.makeText(comicViewerActivity, comicViewerActivity.getResources().getString(R.string.autoplay_enabled), Toast.LENGTH_SHORT).show();
            comicViewerActivity.playerHUDView.setVisible(false);
        }
        else if(button == autoplayStopSprite) {
            comicViewerActivity.setAutoPlaybackEnabled(false);
            Toast.makeText(comicViewerActivity, comicViewerActivity.getResources().getString(R.string.autoplay_disabled), Toast.LENGTH_SHORT).show();
        }
        else if(
                button == autoplayX1Sprite ||
                button == autoplayX2Sprite ||
                button == autoplayX3Sprite ||
                button == autoplayX4Sprite) {

            AutoPlayTime newAutoPlayTime = LogicFacade.incrementAutoPlayTime();
            float autoPlayBacTimeSeconds = LogicFacade.getAutoPlayTimeSeconds(newAutoPlayTime);
            comicViewerActivity.setAutoPlaybackSeconds(autoPlayBacTimeSeconds);
            Toast.makeText(comicViewerActivity, ((int)autoPlayBacTimeSeconds) + " " + comicViewerActivity.getResources().getString(R.string.seconds), Toast.LENGTH_SHORT).show();
        }

        //reconfigure buttons state
        reconfigureButtons();

    }

    private void reconfigureButtons() {

        if(LogicFacade.isAutoPlaybackEnabled()) {
            if(comicViewerActivity.isAutoPlaybackPaused()) {
                setButtonStatus(autoplayPlaySprite, true);
                setButtonStatus(autoplayPauseSprite, false);
            }
            else {
                setButtonStatus(autoplayPlaySprite, false);
                setButtonStatus(autoplayPauseSprite, true);
            }
        }
        else {
            setButtonStatus(autoplayPlaySprite, true);
            setButtonStatus(autoplayPauseSprite, false);
        }

        //stop
        setButtonStatus(autoplayStopSprite, true);

        //back
        setButtonStatus(autoplayBackSprite, true);

        //autoplay speed
        setButtonStatus(autoplayX1Sprite, false);
        setButtonStatus(autoplayX2Sprite, false);
        setButtonStatus(autoplayX3Sprite, false);
        setButtonStatus(autoplayX4Sprite, false);

        AutoPlayTime autoPlayTime = LogicFacade.getAutoPlayBackTime();
        if(autoPlayTime == AutoPlayTime.X1) {
            setButtonStatus(autoplayX1Sprite, true);
        }
        else if(autoPlayTime == AutoPlayTime.X2) {
            setButtonStatus(autoplayX2Sprite, true);
        }
        else if(autoPlayTime == AutoPlayTime.X3) {
            setButtonStatus(autoplayX3Sprite, true);
        }
        else if(autoPlayTime == AutoPlayTime.X4) {
            setButtonStatus(autoplayX4Sprite, true);
        }
    }

    public class PlayerControlButton extends Sprite {

        private float BUTTON_SCALE = 0.7f;

        public PlayerControlButton(float x, ITextureRegion textureRegion, VertexBufferObjectManager pVertexBufferObjectManager) {
            super(x, 0, textureRegion, pVertexBufferObjectManager);
            setScale(BUTTON_SCALE);
        }

        public void show() {
            setVisible(true);
        }

        public void hide() {
            setVisible(false);
        }

        @Override
        public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
            if(isVisible()) {
                buttonTouched(this);
                return true;
            }
            else {
                return false;
            }
        }
    }
}