package com.alchemy.thereaderengine.ui.viewer.util;

import com.alchemy.thereaderengine.App;
import com.alchemy.thereaderengine.listener.CameraTransformerListener;

import org.andengine.engine.camera.SmoothCamera;


public class CameraTransformer {
	
	private SmoothCamera camera;
	
	private boolean animating;
	private long animateDuration;
	private float animateOrigX;
	private float animateOrigY;
	private float animateDestX;
	private float animateDestY;
	private float animateOrigZoom;
	private float animateDestZoom;
	
	private long currentAnimateTime;
	private CameraTransformerListener listener;
	
	public void update(long delay) {
		
		if(animating) {
			
			//tiempo transcurrido de la animacion
			currentAnimateTime += delay;
			
			//diferencia en px de cada eje
			float difX = animateDestX - animateOrigX;
			float difY = animateDestY - animateOrigY;
			float difZoom = animateDestZoom - animateOrigZoom;
			
			float currX = ((currentAnimateTime * difX) / animateDuration) + animateOrigX;
			float currY = ((currentAnimateTime * difY) / animateDuration) + animateOrigY;
			float currZoom = ((currentAnimateTime * difZoom) / animateDuration) + animateOrigZoom;
			
			camera.setCenterDirect(currX, currY);
			camera.setZoomFactorDirect(currZoom);
			
			if(currentAnimateTime >= animateDuration) {
				
				//reseteamos variables de animacion
				animating = false;
				currentAnimateTime = 0;
				
				camera.setCenterDirect(animateDestX, animateDestY);
				camera.setZoomFactorDirect(animateDestZoom);
				
				//llamamos al listener
				if(listener != null) {
					listener.animationFinished();
				}
			}
		}
	}
	
	public void animateMoveAndZoom(long duration, float destX, float destY, float zoom) {
		animateDuration = duration;
		animateOrigX = camera.getCenterX();
		animateDestX = destX;
		animateOrigY = camera.getCenterY();
		animateDestY = destY;
		animateOrigZoom = camera.getZoomFactor();
		animateDestZoom = zoom;
		animating = true;
		if(listener != null) {
			listener.animationStarted();
		}
	}
	
	/**
	 * Center the screen on the given frame
	 * by translating from current camera 
	 * position 
	 * @param frameX
	 * @param frameY
	 * @param frameWidth
	 * @param frameHeight
	 */
	public void animateGotoFrame(CameraTransformerListener listener, long duration, float frameX, float frameY, float frameWidth, float frameHeight) {
		
		this.listener = listener;
		
		float screenWidth = App.SCREEN_WIDTH;
		float screenHeight = App.SCREEN_HEIGHT;
		
		//nos quedamos con el zoom mas
		//chico entre el horizontal y el
		//vertical
		float zoomW = screenWidth / frameWidth;
		float zoomH = screenHeight / frameHeight;
		float zoom = zoomW >= zoomH ? zoomH : zoomW;
		
		//los destinos x,y corresponden a las coordenadas
		//del frame, mas la mitad del ancho/alto del mismo
		float destX = frameX + (frameWidth / 2f);
		float destY = frameY + (frameHeight / 2f);
		
		//ejecutamos la animacion
		animateMoveAndZoom(duration, destX, destY, zoom);
		
	}
	
	public void gotoFrame(float frameX, float frameY, float frameWidth, float frameHeight) {
		
		float screenWidth = App.SCREEN_WIDTH;
		float screenHeight = App.SCREEN_HEIGHT;
		
		//nos quedamos con el zoom mas
		//chico entre el horizontal y el
		//vertical
		float zoomW = screenWidth / frameWidth;
		float zoomH = screenHeight / frameHeight;
		float zoom = zoomW >= zoomH ? zoomH : zoomW;
		
		//los destinos x,y corresponden a las coordenadas
		//del frame, mas la mitad del ancho/alto del mismo
		float destX = frameX + (frameWidth / 2f);
		float destY = frameY + (frameHeight / 2f);
		
		camera.setCenterDirect(destX, destY);
		camera.setZoomFactorDirect(zoom);
		
	}

	public SmoothCamera getCamera() {
		return camera;
	}

	public void setCamera(SmoothCamera camera) {
		this.camera = camera;
	}
}