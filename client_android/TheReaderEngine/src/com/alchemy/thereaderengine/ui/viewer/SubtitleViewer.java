package com.alchemy.thereaderengine.ui.viewer;

import android.graphics.Typeface;

import com.alchemy.thereaderengine.App;
import com.alchemy.thereaderengine.ComicViewerActivity;
import com.alchemy.thereaderengine.database.UserDatabaseManager;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.apocalipta.comic.vo.v1.NavigationItemVO;
import com.apocalipta.comic.vo.v1.PageVO;
import com.apocalipta.comic.vo.v1.PublicationVO;
import com.apocalipta.comic.vo.v1.SubtitleVO;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.text.AutoWrap;
import org.andengine.entity.text.Text;
import org.andengine.entity.text.TextOptions;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.util.HorizontalAlign;
import org.andengine.util.color.Color;

import java.util.ArrayList;
import java.util.List;

public class SubtitleViewer  {
	
	private ComicViewerActivity comicViewerActivity;
	private HUD hud;
	
	private Rectangle blackBackground;
	private List<Text> texts = new ArrayList<Text>();
	
	private Font subtitleFont;
	private float SUBTITLE_TEXT_MARGIN = 15;
	private static float SUBTITLE_AUTOWRAP_WIDTH;
	
	public SubtitleViewer(ComicViewerActivity comicViewerActivity) {
		this.comicViewerActivity = comicViewerActivity;
	}
	
	public void init(PublicationVO publicationVO, HUD hud) {

		this.hud = hud;

		//init the contrast
		blackBackground = new Rectangle(
				0, App.SCREEN_HEIGHT, //x and y
                App.SCREEN_WIDTH, App.SCREEN_HEIGHT, //width and height
				comicViewerActivity.getVertexBufferObjectManager());
		
		//color
		blackBackground.setColor(0f, 0f, 0f, 0.65f);
		
		//add the background to the hud
		hud.attachChild(blackBackground);
		
		//create the subtitles font
		BitmapTextureAtlas mFontTexture = new BitmapTextureAtlas
				(comicViewerActivity.getTextureManager(), 512, 256, TextureOptions.BILINEAR);
		
		//font size 
		int fontSize = 18;

		//read from configuration
		UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
		String textSizeStr =  userDatabaseManager.getSettingsValue(UserDatabaseManager.TABLE_SETTINGS_COLUMN_TEXT_SIZE);
		if(textSizeStr.equals(App.SCREEN_CATEGORY_SMALL)) {
			fontSize = 22;
		}
		else if(textSizeStr.equals(App.SCREEN_CATEGORY_MEDIUM)) {
			fontSize = 30;
		}
		else if(textSizeStr.equals(App.SCREEN_CATEGORY_LARGE)) {
			fontSize = 36;
		}
		
		subtitleFont = new Font(
				comicViewerActivity.getFontManager(), 
				mFontTexture, 
				Typeface.create(Typeface.DEFAULT, Typeface.BOLD), 
				fontSize, 
				true, 
				Color.YELLOW);
		
		//load the font in memory
		subtitleFont.load();
		
		//left and right margins
		SUBTITLE_AUTOWRAP_WIDTH = App.SCREEN_WIDTH - SUBTITLE_TEXT_MARGIN - SUBTITLE_TEXT_MARGIN;

        //init the publication
		initPublication(publicationVO);
		
	}
	
	private void initPublication(PublicationVO publicationVO) {
		
		List<PageVO> pageVOs = publicationVO.getProjectVO().getPages();

		int maxLinesQnty = 0;
		for(PageVO pageVO : pageVOs) {
            if(pageVO.getNavigationItemVOList() != null) {
                for(NavigationItemVO navigationItemVO : pageVO.getNavigationItemVOList()) {
                    if(navigationItemVO.getSubtitleVOList() != null) {
                        for(SubtitleVO subtitleVO : navigationItemVO.getSubtitleVOList()) {
                            String[] subtitlesArray = subtitleVO.getSubtitle().split("\\n");
                            if(subtitlesArray.length > maxLinesQnty) {
                                maxLinesQnty = subtitlesArray.length;
                            }
                        }
                    }
                }
            }
		}
		
		//init text ui objects
		for(int i=0; i<maxLinesQnty; i++) {
			texts.add(new Text(
					SUBTITLE_TEXT_MARGIN, App.SCREEN_HEIGHT, subtitleFont, "", 500,
					new TextOptions(AutoWrap.WORDS, SUBTITLE_AUTOWRAP_WIDTH, HorizontalAlign.CENTER), 
					comicViewerActivity.getVertexBufferObjectManager())
			);
		}

		//add them to the ui
		for(Text currentText : texts) {
			hud.attachChild(currentText);
		}
	}

	public void hide() {
		for(Text currentText : texts) {
			currentText.setY(App.SCREEN_HEIGHT * 2);
		}
		blackBackground.setY(App.SCREEN_HEIGHT * 2);
	}

	public void show(String subtitles) {
		
		//reset ui text object positions
		for(Text currentText : texts) {
			currentText.setY(App.SCREEN_HEIGHT);
		}

        String[] subtitlesList = subtitles.split("\\n");

		int subtitlesSize = subtitlesList.length;
		float accumY = SUBTITLE_TEXT_MARGIN / 2f; //init with a small margin
		for(int i=0; i<subtitlesSize; i++) {
			//get the current subtitle line
			String subtitle = subtitlesList[i];
			
			//get the ui entity
			Text subtitleText = texts.get(i);
			
			//set the text
			subtitleText.setText(subtitle);
			subtitleText.setY(App.SCREEN_HEIGHT - subtitleText.getHeight() - accumY);
			accumY += subtitleText.getHeight();
			
		}
		
		blackBackground.setY(App.SCREEN_HEIGHT - accumY - (SUBTITLE_TEXT_MARGIN / 2f));
	}
}