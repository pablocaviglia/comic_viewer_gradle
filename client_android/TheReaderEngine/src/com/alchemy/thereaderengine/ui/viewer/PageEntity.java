package com.alchemy.thereaderengine.ui.viewer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.MotionEvent;

import com.alchemy.thereaderengine.App;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.apocalipta.comic.vo.v1.NavigationItemVO;
import com.apocalipta.comic.vo.v1.PageVO;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import org.andengine.engine.camera.SmoothCamera;
import org.andengine.entity.Entity;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.detector.PinchZoomDetector;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class PageEntity extends Entity {

    private static final Logger LOGGER = LoggerManager.getLogger();

    public Sprite sprite;
	public PageVO pageVO;
	public Bitmap bitmap;
	private PageNavigatorViewer pageNavigatorViewer;

	private int totalWidth;
	private int totalHeight;

	private float calcZoomFactor;
	private float calcZoomFactorAnterior;
	private float lastdif;
	private float totaldif;

	public boolean loaded;

	public List<NavigationItemVO> navigationItemVOs;

	public PageEntity(PageVO pageVO, PageNavigatorViewer pageNavigatorViewer) {
		navigationItemVOs = new ArrayList<>();
		this.pageVO = pageVO;
		this.pageNavigatorViewer = pageNavigatorViewer;
	}
	
	public void loadSprite(Sprite sprite) {
		//remove previous sprite
		if(this.sprite != null) {
			removeSprite();
		}
		//laod the new sprite
		this.sprite = sprite;
		attachChild(sprite);
	}
	
	public void removeSprite() {
        if(null != sprite) {
            sprite.getTextureRegion().getTexture().unload();
            detachChild(sprite);
            sprite = null;
            bitmap = null;
            loaded = false;
        }
    }
	
	public void loadImage() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				if(!loaded) {
                    boolean pageLoadedWithoutOutOfMemory = false;
                    do {
                        try {
                            //get the image data
                            byte[] pageData = LogicFacade.readPage(pageVO.getId());
                            //decode it
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inPreferredConfig = Bitmap.Config.RGB_565;
                            bitmap = BitmapFactory.decodeByteArray(pageData, 0, pageData.length, options);
                            pageLoadedWithoutOutOfMemory = true;
                        }
                        catch (OutOfMemoryError e) {
                            e.printStackTrace();
                            LOGGER.e("ERROR!!! Sin memoria para cargar pageVO " + PageEntity.this.pageVO.getPageIndex());
                            try { Thread.sleep(1000); } catch (InterruptedException e1) { LOGGER.e(e1.getMessage(), e1); }
                        }
                        catch (FileNotFoundException e) {
                            e.printStackTrace();
                            LOGGER.e("ERROR!!! Pagina no encontrada " + PageEntity.this.pageVO.getPageIndex());
                            //the image file cannot be found
                            //try to redownload the publication
                            //images
                            pageNavigatorViewer.resourcesLoaded(PageEntity.this, false, true, false);
                            return;
                        }
                        catch (Exception e) {
                            e.printStackTrace();
                            LOGGER.e("ERROR!!! Problema general cargando pagina " + PageEntity.this.pageVO.getPageIndex());
                            //the image file cannot be found
                            //try to redownload the publication
                            //images
                            pageNavigatorViewer.resourcesLoaded(PageEntity.this, false, true, false);
                            return;
                        }
                    }
                    while(!pageLoadedWithoutOutOfMemory);

                    //call the listener
                    pageNavigatorViewer.resourcesLoaded(PageEntity.this, false, false, false);
				}
				else {
					pageNavigatorViewer.resourcesLoaded(PageEntity.this, true, false, false);
				}
			}
		}).start();
	}
	
	public void onPinchZoom(MotionEvent onTouchEvent, SmoothCamera camera) {
        if(PinchZoomDetector.hasTwoOrMorePointers(onTouchEvent)) {
            float touch1X = onTouchEvent.getX(0);
            float touch2X = onTouchEvent.getX(1);
            float currdifX = touch2X > touch1X ? touch2X - touch1X : touch1X - touch2X;

            float touch1Y = onTouchEvent.getY(0);
            float touch2Y = onTouchEvent.getY(1);
            float currdifY = touch2Y > touch1Y ? touch2Y - touch1Y : touch1Y - touch2Y;

            //nos quedamos con la separacion de dedos
            //en el eje que mas haya cambiado
            float currdif = Math.abs(currdifX) > Math.abs(currdifY) ? currdifX : currdifY;

            //si estamos en el primer evento pinch
            //evitamos que el zoom se dispare debido
            //a la primer gran difrerencia de pixeles
            if(lastdif == 0) {
                lastdif = currdif;
                calcZoomFactorAnterior = calcZoomFactor;
            }

            float deltadif = lastdif - currdif;

            //para ajustar en caso de que sea muy
            //grande la diferencia y el zoom sea
            //muy repentino
            if(Math.abs(deltadif) > 20) {
                deltadif = deltadif > 0 ? 2 : -2;
            }

            totaldif -= deltadif;

            //maximo valor relacionado con
            //el maximo escalamiento
            float MAX_PX = App.SCREEN_WIDTH > App.SCREEN_HEIGHT ? App.SCREEN_WIDTH : App.SCREEN_HEIGHT;
            float MIN_ZOOM = pageNavigatorViewer.minPageZoom;;
            float MAX_ZOOM = 4f;

            calcZoomFactor = (totaldif * MAX_ZOOM) / MAX_PX;

            float difZoomFactor = calcZoomFactorAnterior - calcZoomFactor;
            float zoomFactor = camera.getZoomFactor() - difZoomFactor;

            if(zoomFactor < MIN_ZOOM) {
                zoomFactor = MIN_ZOOM;
            }
            else if(zoomFactor > MAX_ZOOM) {
                zoomFactor = MAX_ZOOM;
            }

            //ajustamos el zoom
            camera.setZoomFactorDirect(zoomFactor);

            //guardamos valor para proximo evento
            lastdif = currdif;
            calcZoomFactorAnterior = calcZoomFactor;
        }
	}
	
	public int getTotalWidth() {
		return totalWidth;
	}

	public void setTotalWidth(int totalWidth) {
		this.totalWidth = totalWidth;
	}

	public int getTotalHeight() {
		return totalHeight;
	}

	public void setTotalHeight(int totalHeight) {
		this.totalHeight = totalHeight;
	}

}