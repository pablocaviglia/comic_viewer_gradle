package com.alchemy.thereaderengine.ui.viewer;

import android.view.MotionEvent;
import android.widget.Toast;

import com.alchemy.thereaderengine.App;
import com.alchemy.thereaderengine.ComicViewerActivity;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.config.Config;
import com.alchemy.thereaderengine.database.UserDatabaseManager;
import com.alchemy.thereaderengine.listener.CameraTransformerListener;
import com.alchemy.thereaderengine.listener.PageEntityListener;
import com.alchemy.thereaderengine.listener.ThumbEntityListener;
import com.alchemy.thereaderengine.ui.viewer.threed.ApocaliptaBitmapTexture;
import com.alchemy.thereaderengine.ui.viewer.threed.BitmapLoaderListener;
import com.alchemy.thereaderengine.ui.viewer.util.CameraTransformer;
import com.apocalipta.comic.vo.v1.NavigationItemVO;
import com.apocalipta.comic.vo.v1.PageVO;
import com.apocalipta.comic.vo.v1.PublicationVO;
import com.apocalipta.comic.vo.v1.SubtitleVO;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.ITextureStateListener;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PageNavigatorViewer implements PageEntityListener, CameraTransformerListener, ITextureStateListener, ThumbEntityListener {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private boolean inicializado;

    private PublicationVO publicationVO;
	
	//listado de paginas
	public List<PageEntity> pages;
	public int currentPageIndex;
	
	//listado de rectangles
	private List<NavigationItemVO> rectangles;
	private int currentRectangleIndex;
	private NavigationItemVO currentRectangle;
	
	//rectangle animator
	private RectangleAnimationViewer rectangleAnimationViewer;
	
	//ui
	private Scene scene;
	private SmoothCamera camera;
	private HUD hud;
	public CameraTransformer cameraTransformer;

	//recursos de manejo grafico
	public ComicViewerActivity comicViewerActivity;
	
	//flags
	private boolean cargandoImagenPagina;
	private boolean mostrarPaginaCompleta;
	public boolean animating;
	
	//flag usado para descartar el
	//primer movimiento de scroll
	//esto se hace para corregir el primer
	//movimiento de scroll q mueve demasiados
	//pixeles abruptamente
	private boolean scrollFlag;
	
	//zoom maximo permitido para la pagina
	//para que se muestre fullscreen
	public float minPageZoom;

	private List<PageEntity> preloadPages = new ArrayList<>();

	public PageNavigatorViewer(ComicViewerActivity comicViewerActivity) {
		this.comicViewerActivity = comicViewerActivity;
		cameraTransformer = new CameraTransformer();
		pages = new ArrayList<>();
		rectangles = new ArrayList<>();
	}

	public void init(final PublicationVO publicationVO, Scene scene, SmoothCamera camera, HUD hud) {

		inicializado = false;
        this.publicationVO = publicationVO;
		this.scene = scene;
		this.camera = camera;
		this.hud = hud;
		cameraTransformer.setCamera(camera);
		comicViewerActivity.showLoadingAnimation(false);

		initPublication(publicationVO);
		inicializado = true;
	}
	
	private void initPublication(PublicationVO publicationVO) {
		
		//limpiamos las paginas y
		//recuadros anteriores
		finalizeResources();
		rectangles.clear();
		currentRectangleIndex = 0;
		currentPageIndex = 0;
		
		//posicion en el eje x de cada pagina
		//calculado tras la suma del ancho de 
		//cada pagina
		float posicionPaginaX = 0f;
		
		///si no es lectura occidental, o sea de
		//derecha a izquierda, la posicion inicial
		//de la pagina 0 en el eje x es a la derecha
		if(!Config.LECTURA_OCCIDENTAL) {
			for(PageVO pageVO : publicationVO.getProjectVO().getPages()) {
				posicionPaginaX += pageVO.getImageWidth() + App.SCREEN_WIDTH;
			}
		}

        long generatedRectangleId = 0;

		//recorremos las paginas y creamos las
		//entidades de ui asociadas al cargador
		//de paginas
		for(PageVO pageVO : publicationVO.getProjectVO().getPages()) {

            //creamos la entidad visual
			//contenedora de la pagina
			PageEntity pageEntity = new PageEntity(pageVO, this);
			pageEntity.setCullingEnabled(true);
			
			//posicion de la pagina en el eje x
			//en base a la suma de los anchos
			//de todas las paginas
			pageEntity.setX(posicionPaginaX);
			pageEntity.setY(0f);
			
			//creamos el primer rectangulo de la 
			//pagina que es el recuadro fullscreen
			float frameX = pageEntity.getX();
			float frameY = pageEntity.getY();
			float frameWidth = pageEntity.pageVO.getImageWidth();
			float frameHeight = pageEntity.pageVO.getImageHeight();
			NavigationItemVO navigationItemVO = new NavigationItemVO();
            navigationItemVO.setId(generatedRectangleId);
            navigationItemVO.setX((int) frameX);
            navigationItemVO.setY((int) frameY);
            navigationItemVO.setWidth((int) frameWidth);
            navigationItemVO.setHeight((int) frameHeight);
            navigationItemVO.setSynthetic(true);
			pageEntity.navigationItemVOs.add(navigationItemVO);

            //increment rectangle id
            generatedRectangleId++;

			UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
			boolean assistedReadingEnabled = Boolean.valueOf(userDatabaseManager.getSettingsValue(UserDatabaseManager.TABLE_SETTINGS_COLUMN_ASSISTED_READING));
			if(assistedReadingEnabled) {
				//add all the page navigation items
				if(pageVO.getNavigationItemVOList() != null && pageVO.getNavigationItemVOList().size() > 0) {
					for(int i=0; i<pageVO.getNavigationItemVOList().size(); i++) {
                        //get the navigation item
						NavigationItemVO navigationItemVOPage = pageVO.getNavigationItemVOList().get(i);

                        //adapt it
						NavigationItemVO rectanguloPaginaAdaptado = new NavigationItemVO();
						rectanguloPaginaAdaptado.setId(generatedRectangleId);
                        rectanguloPaginaAdaptado.setSubtitleVOList(navigationItemVOPage.getSubtitleVOList());
						rectanguloPaginaAdaptado.setX((int) (pageEntity.getX() + navigationItemVOPage.getX()));
						rectanguloPaginaAdaptado.setY(navigationItemVOPage.getY());
						rectanguloPaginaAdaptado.setWidth(navigationItemVOPage.getWidth());
						rectanguloPaginaAdaptado.setHeight(navigationItemVOPage.getHeight());
						
						pageEntity.navigationItemVOs.add(rectanguloPaginaAdaptado);

                        //increment rectangle id
                        generatedRectangleId++;

                    }
				}
			}
			
			//add all the navigation items
            //to a single collection
			rectangles.addAll(pageEntity.navigationItemVOs);
			
			if(!Config.LECTURA_OCCIDENTAL) {
				//decrementamos la posicion en x
				//para mover la proxima pagina hacia
				//la izquierda con respecto a la actual
				posicionPaginaX -= pageVO.getImageWidth() + App.SCREEN_WIDTH;
			}
			else {
				//incrementamos la posicion en x
				//para mover la proxima pagina hacia
				//la derecha con respecto a la actual
				posicionPaginaX += pageVO.getImageWidth() + App.SCREEN_WIDTH;
			}
			
			//dimensiones usadas para el bound por pagina
			pageEntity.setTotalWidth(pageVO.getImageWidth() + App.SCREEN_WIDTH);
			pageEntity.setTotalHeight(pageVO.getImageHeight());
			
			//agregamos la nueva entidad 
			//ui que define una pagina 
			pages.add(pageEntity);
			
			//agregamos la entidad visual a la escena
			scene.attachChild(pageEntity);
		}
		
		//verificamos si el usuario estaba
		//en una pagina y la aplicacion fue
		//reiniciada
		int savedPageMarker = getPageMarker();

		//init the rectangle animator
		rectangleAnimationViewer = new RectangleAnimationViewer(comicViewerActivity, pages, scene);

		//inicializamos la primer pagina
		currentPageIndex = savedPageMarker;
		
		//indicamos cual es el proximo
		//rectangulo al cual moverse
		currentRectangleIndex = obtenerIndicePrimerRectanguloPagina(currentPageIndex) + 1;

		//load the first visible page
		cargarPagina(pages.get(savedPageMarker - 1), true, true);

        //load the next page
        if((savedPageMarker+1) < pages.size()) {
            cargarPagina(pages.get(savedPageMarker), false, false);
        }
	}

    private PageEntity findPageEntityByNavigationItemVO(NavigationItemVO navigationItemVO) {
        for(PageEntity pageEntity : pages) {
            for(NavigationItemVO nav : pageEntity.navigationItemVOs) {
                if(nav == navigationItemVO) {
                    return pageEntity;
                }
            }
        }
        return null;
    }
	
	public void moveNextPage() {
		int nextPageIndex = currentPageIndex + 1;
		if(nextPageIndex <= pages.size()) {
			movePage(pages.get(nextPageIndex-1));
		}
        else {
            finishNavigation();
        }
	}
	
	public void movePreviousPage() {
		int anteriorPaginaIndex = currentPageIndex - 1;
		if(anteriorPaginaIndex > 0) {
			movePage(pages.get(anteriorPaginaIndex - 1));
		}
	}
	
	public void movePage(PageEntity pageEntity) {
		if(!animating) {
            //permitimos a la camara
            //moverse libremente
            deshabilitarBoundsCamaraPagina();

            //indica hacia donde debe
            //de moverse la animacion
            boolean flipAnimation = currentPageIndex > pageEntity.pageVO.getPageIndex();

            currentPageIndex = pageEntity.pageVO.getPageIndex();

			//indicamos la pagina a precargar
            //para cuando termine el movimiento
            preloadPages.add(pageEntity);

            //en caso de que haya una pagina siguiente
            //a la actual indicamos que se precargue la misma
            int proximaPaginaIndex = currentPageIndex + 1;
            if(proximaPaginaIndex <= pages.size()) {
                preloadPages.add(pages.get(proximaPaginaIndex - 1));
            }

            //mostramos la animacion de carga
            if(!pageEntity.loaded) {
                comicViewerActivity.showLoadingAnimation(flipAnimation);
            }

            //nos movemos a la pagina
            //actual en modo fullscreen
            showFullPage();
		}
	}
	
	private void cargarPagina(final PageEntity pageEntity, final boolean mostrarPaginaCompleta, final boolean cargaDirecta) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                //seteamos variables
                cargandoImagenPagina = true;
                PageNavigatorViewer.this.mostrarPaginaCompleta = mostrarPaginaCompleta;

                //si es carga directa se muestra
                //animacion de carga porque no es
                //una pagina a ser precargada
                if(cargaDirecta) {
                    comicViewerActivity.showLoadingAnimation(false);
                }

                if(cargaDirecta) {
                    //si la carga es directa nos
                    //movemos sin animacion hacia
                    //las coordenadas de la pagina
                    showPageDirect(pageEntity.pageVO.getPageIndex());
                }

                //cargamos la imagen
                pageEntity.loadImage();

                //no nos movemos mas alla de
                //los bounds de la pagina
                habilitarBoundsCamaraPagina(currentPageIndex);
            }
        });

        if(cargaDirecta) {
            try {
                t.start();
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        else {
            t.start();
        }
	}
	
	private void habilitarBoundsCamaraPagina(int pagina) {
        if(pages.size() > pagina) {
            PageEntity pageEntityActual = pages.get(pagina-1);

            float imageHeight = pageEntityActual.getTotalHeight();
            float imageWidth = pageEntityActual.getTotalWidth() - App.SCREEN_WIDTH;
            float halfScreenWidth = App.SCREEN_WIDTH / 2f;
            float halfScreenHeight = App.SCREEN_HEIGHT / 2f;

            float pBoundsXMin = pageEntityActual.getX() - halfScreenWidth;
            float pBoundsYMin = pageEntityActual.getY() - halfScreenHeight;
            float pBoundsXMax = pBoundsXMin + imageWidth + App.SCREEN_WIDTH;
            float pBoundsYMax = imageHeight + halfScreenHeight;

            camera.setBoundsEnabled(true);
            camera.setBounds(pBoundsXMin, pBoundsYMin, pBoundsXMax, pBoundsYMax);
        }
	}
	
	private void deshabilitarBoundsCamaraPagina() {
		camera.setBoundsEnabled(false);
	}
	
	public void update(long elapsed) {
		if(inicializado) {
			cameraTransformer.update(elapsed);
			rectangleAnimationViewer.update(elapsed);
		}
	}
	
	public void onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		if(!animating) {
			if(!scrollFlag) {
				camera.setCenterDirect(camera.getCenterX() + distanceX, camera.getCenterY() + distanceY);	
			}
			scrollFlag = false;
		}
	}
	
	public void onDown(MotionEvent evt) {
		scrollFlag = true;
	}
	
	public void onSingleTapUp(MotionEvent evt) {
		if(!cargandoImagenPagina && !animating) {
			moveNextRectangle();
		}
	}

    public void onDoubleTap(MotionEvent evt) {
        if(currentRectangle == null) {
            int rectangleIndex = getRectangleIndexByCoordinates(evt.getX(), evt.getY());
            gotoRectangle(rectangleIndex);
        }
        else {
            showFullPage();
        }
    }
	
	public void onScale(MotionEvent onTouchEvent) {
		if(!animating) {
			pages.get(currentPageIndex - 1).onPinchZoom(onTouchEvent, camera);
		}
	}
	
	@Override
	public void resourcesLoaded(final PageEntity pageEntity, final boolean cached, final boolean error, final boolean previousOutOfMemory) {
        if(error) {
            comicViewerActivity.showDownloadCorruptedImageMessageDialog();
        }
        else {
            if(!cached) {
                try {
                    ITexture texture = new ApocaliptaBitmapTexture(new BitmapLoaderListener() {
						@Override
						public void outOfMemory() {
                            if(!previousOutOfMemory) {
                                LOGGER.e("Out of memory loading page, trying to remove all pages and retrying...");

                                //try to remove all pages and load current page again
                                for(PageEntity currentPage : pages) {
                                    if(pageEntity != currentPage) {
                                        currentPage.removeSprite();
                                    }
                                }

                                //reload current page
                                resourcesLoaded(pageEntity, cached, false, true);
                            }
                            else {
                                comicViewerActivity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(comicViewerActivity, comicViewerActivity.getResources().getString(R.string.memory_error_msg), Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }
						}
					}, comicViewerActivity.getTextureManager(), this, pageEntity.bitmap);

                    texture.setTextureStateListener(this);
                    texture.load();
                    ITextureRegion faceTextureRegion = TextureRegionFactory.extractFromTexture(texture);
                    Sprite sprite = new Sprite(0, 0, faceTextureRegion, comicViewerActivity.getVertexBufferObjectManager());
                    pageEntity.loadSprite(sprite);
                    pageEntity.loaded = true;

                    //oculta animacion de carga
                    comicViewerActivity.hideLoadingAnimation();

                }
                catch (Exception e) {
                    LOGGER.e(e.getMessage(), e);
                }
            }
            else {
                cargandoImagenPagina = false;
            }
        }
	}

    private int getRectangleIndexByCoordinates(float x, float y) {
        int rectangleIndex = -1;

        //get the touch coordinates
        //relative to the real page
        //entity image size (jpeg size)
        float[] pageEntityCoordinates = comicViewerActivity.camera.getSceneCoordinatesFromCameraSceneCoordinates(x, y);

        //verify the collision with
        //the navigation items
        for(NavigationItemVO navigationItemVO : rectangles) {
            if(
                    !navigationItemVO.isSynthetic() &&
                    pageEntityCoordinates[0] >= navigationItemVO.getX() && pageEntityCoordinates[0] <= (navigationItemVO.getX() + navigationItemVO.getWidth()) &&
                    pageEntityCoordinates[1] >= navigationItemVO.getY() && pageEntityCoordinates[1] <= (navigationItemVO.getY() + navigationItemVO.getHeight()))
            {
                rectangleIndex = navigationItemVO.getId().intValue();
                break;
            }
        }

        return rectangleIndex;
    }

    public void gotoRectangle(int index) {
        if(index != -1) {
            for(NavigationItemVO navigationItemVO : rectangles) {
                if(navigationItemVO.getId() == index) {
                    currentRectangleIndex = index;
                    moveNextRectangle();
                }
            }
        }
    }

    public void movePreviousRectangle() {
		if(rectangleAnimationViewer != null) {
			if(currentRectangleIndex > 0) {

				//disable free navigation
				comicViewerActivity.freeNavigation = false;

				//-2 to make it zero based, and
				//decrement current index by one
				int previousRectangleIndex = currentRectangleIndex - 2;
				if(previousRectangleIndex >= 0) {

					//find current and previous pages
					int currentRectanglePage = findPageEntityByNavigationItemVO(rectangles.get(currentRectangleIndex-1)).pageVO.getPageIndex();
					int previousRectanglePage = findPageEntityByNavigationItemVO(rectangles.get(previousRectangleIndex)).pageVO.getPageIndex();

					LOGGER.i("---> CURRENT PAGE INDEX :: " + currentRectanglePage);
					LOGGER.i("---> CURRENT RECT INDEX :: " + currentPageIndex);

					//global vars setting
					currentPageIndex = previousRectanglePage;
					currentRectangle = rectangles.get(previousRectangleIndex);

					//move to rectangle
					moveRectangle(previousRectangleIndex);

					if(currentRectanglePage != previousRectanglePage) {
						PageEntity pageEntity = findPageEntityByNavigationItemVO(rectangles.get(previousRectangleIndex));
						preloadPages.add(pageEntity);
					}

					LOGGER.i("---> PREVIOUS PAGE INDEX :: " + previousRectanglePage);
					LOGGER.i("---> PREVIOUS RECT INDEX :: " + previousRectangleIndex);

					//decrement
					--currentRectangleIndex;
				}
			}
		}
	}
	
	public void moveNextRectangle() {
		if(rectangleAnimationViewer != null) {
			//solo nos movemos si actualmente no se
			//encuentra carga una imagen o estamos en
			//medio de una animacion
			if(!cargandoImagenPagina && !animating) {
				if(currentRectangleIndex >= rectangles.size()) {
					finishNavigation();
				}
				else {

					//disable free navigation
					comicViewerActivity.freeNavigation = false;

					//encontramos cual es la
					//pagina actual y la siguiente
					int pageIndex = findPageEntityByNavigationItemVO(rectangles.get(currentRectangleIndex)).pageVO.getPageIndex();

					long currentRectanglePage = pageIndex;
					long nextRectanglePage = -1;
					if((currentRectangleIndex +1) <= (rectangles.size()-1)) {
						nextRectanglePage = findPageEntityByNavigationItemVO(rectangles.get(currentRectangleIndex +1)).pageVO.getPageIndex();
					}

					currentPageIndex = pageIndex;
					currentRectangle = rectangles.get(currentRectangleIndex);

					//move to rectangle
					moveRectangle(currentRectangleIndex);

					if(currentRectanglePage != nextRectanglePage && nextRectanglePage != -1) {
						PageEntity pageEntity = findPageEntityByNavigationItemVO(rectangles.get(currentRectangleIndex + 1));
						preloadPages.add(pageEntity);
					}

					//increment
					++currentRectangleIndex;
				}
			}
		}
	}

	public void moveRectangle(int rectangleIndex) {
		if(rectangleAnimationViewer != null) {
			long animationTime = 400;
			NavigationItemVO navigationItemVO = rectangles.get(rectangleIndex);
			cameraTransformer.animateGotoFrame(
					this,
					animationTime,
					navigationItemVO.getX(),
					navigationItemVO.getY(),
					navigationItemVO.getWidth(),
					navigationItemVO.getHeight());

			//animate black rectangle
			rectangleAnimationViewer.show(currentRectangle.getX(), currentRectangle.getX() + currentRectangle.getWidth(), currentRectangle.getY(), currentRectangle.getY() + currentRectangle.getHeight(), animationTime);
		}
	}

	private void finishNavigation() {
        savePageMarker(1);
        comicViewerActivity.gotoMainActivity();

    }
	
	public void showFullPage() {
		if(!animating) {
			
			//deshabilitamos navegacion libre
			comicViewerActivity.freeNavigation = false;
			
			//ocultamos subtitles si se mostraban
			comicViewerActivity.hideSubtitle();
			
			//obtenos las dimensiones de la pagina
			//para mostrarla fullscreen
			PageEntity pageEntityActual = pages.get(currentPageIndex -1);
			float frameX = pageEntityActual.getX();
			float frameY = pageEntityActual.getY();
			float frameWidth = pageEntityActual.pageVO.getImageWidth();
			float frameHeight = pageEntityActual.pageVO.getImageHeight();
			
			long animationTime = 400;
			
			//animamos el rectangulo negro
			rectangleAnimationViewer.show(frameX, frameX + frameWidth, frameY, frameY + frameHeight, animationTime);
			
			//no es un rectangulo al que vamos
			//sino a la pagina completa
			currentRectangle = null;

			//indicamos que el proximo rectangulo a
			//moverse es el primero de la pagina actual
			currentRectangleIndex = obtenerIndicePrimerRectanguloPagina(pageEntityActual.pageVO.getPageIndex());
			
			//debido a que este metodo muestra la pagina
			//en fullscreen, no deseamos mostrar el primero que
			//es sintetico debido a que representa la pagina
			//en fullscreen tambien, de esta manera nos salteamos
			//el rectangulo sintetico
			++currentRectangleIndex;
			
			//fix para el caso rarisimo de
			//que no haya ni siquiera un 
			//rectangulo en toda la aplicacion
			if(currentRectangleIndex > rectangles.size()) {
				currentRectangleIndex = rectangles.size()-1;
			}
			
			//mostramos el frame 
			//del tamano de la imagen 
			cameraTransformer.animateGotoFrame(
					this, //animation listener
					animationTime, //animation time
					frameX, 
					frameY, 
					frameWidth, 
					frameHeight);
			
			//calculamos el zoom minimo
			//posible para la pagina
			calcularMinZoom();
			
			//persistimos el id de la pagina
			savePageMarker(currentPageIndex);
			
		}
	}
	
	private void calcularMinZoom() {
		PageEntity pageEntity = pages.get(currentPageIndex -1);
		float areaPaginaActual = pageEntity.pageVO.getImageWidth() + pageEntity.pageVO.getImageHeight();
		float areaPantalla = App.SCREEN_WIDTH + App.SCREEN_HEIGHT;
		minPageZoom = areaPantalla / areaPaginaActual;
	}
	
    private void savePageMarker(int indice) {
        App.saveSharedPreference(comicViewerActivity, App.NAVIGATION_PAGE_ID_SHARED_PREFERENCE_KEY + "_" + publicationVO.getId(), String.valueOf(indice));
    }

    private int getPageMarker() {
        String pageMarkerStr = App.getSharedPreference(comicViewerActivity, App.NAVIGATION_PAGE_ID_SHARED_PREFERENCE_KEY + "_" + publicationVO.getId());
        int pageNavigationId = 1;
        if(pageMarkerStr != null) {
            pageNavigationId = Integer.valueOf(pageMarkerStr);
        }
        return pageNavigationId;
    }

    private void showPageDirect(int pagina) {

		//hide subs
		comicViewerActivity.hideSubtitle();
		
		//obtenemos las dimensiones de la
		//pagina para mostrarla completa
		PageEntity pageEntityActual = pages.get(pagina-1);
		float frameX = pageEntityActual.getX();
		float frameY = pageEntityActual.getY();
		float frameWidth = pageEntityActual.pageVO.getImageWidth();
		float frameHeight = pageEntityActual.pageVO.getImageHeight();
		
		//mostramos el frame 
		//del tamano de la imagen 
		cameraTransformer.gotoFrame(frameX, frameY, frameWidth, frameHeight);
		
		//calculamos el zoom minimo
		//posible para la pagina
		calcularMinZoom();
		
		//persistimos el id de pagina
		//a la cual vamos asi si se 
		//reinicia la aplicacion volvemos
		//a la misma pagina
		savePageMarker(currentPageIndex);
		
	}
	
	public void finalizeResources() {
		for(PageEntity page : pages) {
			page.detachSelf();
		}
		pages.clear();
	}
	
	@Override
	public void onLoadedToHardware(ITexture pTexture) {
		cargandoImagenPagina = false;
		if(mostrarPaginaCompleta) {
			showFullPage();
		}
	}
	
	@Override
	public void onUnloadedFromHardware(ITexture pTexture) {

	}
	
	private int obtenerIndicePrimerRectanguloPagina(int pagina) {
		int indice = 0;
		for(PageEntity pageEntity : pages) {
			if(pageEntity.pageVO.getPageIndex() < pagina) {
				indice += pageEntity.navigationItemVOs.size();
			}
		}
		return indice;
	}
	
	@Override
	public void thumbTouched(ThumbEntity thumbEntity) {
		movePage(pages.get(thumbEntity.pageVO.getPageIndex() - 1));
	}
	
	@Override
	public void animationStarted() {
		//deshabilitamos los limites de la pagina
		deshabilitarBoundsCamaraPagina();
		//comenzamos la animacion
		animating = true;
	}
	
	@Override
	public void animationFinished() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				
				//asi no tenemos un glitch 
				//debido al ultimo movimiento
				//de la animacion
				try { Thread.sleep(100); } catch (InterruptedException e) { LOGGER.e(e.getMessage(), e); }
				
				//liberamos memoria
				if(currentPageIndex < (pages.size())) {
					for(PageEntity currentPage : pages) {
						if(currentPage.loaded && currentPage.pageVO.getPageIndex() != pages.get(currentPageIndex -1).pageVO.getPageIndex()) {
							//calculamos la distancia entre
							//la pagina actual y la que estamos 
							//evaluando para liberar memoria
							int pageDistance = Math.abs(currentPage.pageVO.getPageIndex() - pages.get(currentPageIndex -1).pageVO.getPageIndex());
							if(pageDistance > 1) {
								currentPage.removeSprite();
							}
						}
					}
				}

				//save page marker
				savePageMarker(currentPageIndex);
				
				//preload pages
				if(preloadPages.size() > 0) {
					Iterator<PageEntity> paginaEntityIt = preloadPages.iterator();
					while(paginaEntityIt.hasNext()) {
						PageEntity precargarPageEntity = paginaEntityIt.next();
						cargarPagina(precargarPageEntity, false, false);
						paginaEntityIt.remove();
					}
				}
				
				//set flag
				animating = false;
				
				if(currentRectangle != null) {
					//show subtitles
                    SubtitleVO subtitle = findSubtitleByLanguage(comicViewerActivity.getUserSelectedLanguage(), currentRectangle.getSubtitleVOList());
                    if(subtitle != null && subtitle.getSubtitle() != null && !subtitle.getSubtitle().trim().equalsIgnoreCase("")) {
						comicViewerActivity.showSubtitle(subtitle);
					}
					else {
						comicViewerActivity.hideSubtitle();
					}
				}
				
				//habilita los bounds 
				//para la pagina actual
				habilitarBoundsCamaraPagina(currentPageIndex);
				
			}
		}).start();
	}

    private SubtitleVO findSubtitleByLanguage(Long languageId, List<SubtitleVO> subtitleVOList) {
        if(languageId != null && subtitleVOList != null) {
            for(SubtitleVO subtitleVO : subtitleVOList) {
                if(subtitleVO.getLanguageId().equals(languageId)) {
                    return subtitleVO;
                }
            }
        }
        return null;
    }
}