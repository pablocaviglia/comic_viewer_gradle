package com.alchemy.thereaderengine.ui.viewer.util;

import com.alchemy.thereaderengine.App;
import com.facebook.crypto.Entity;
import com.facebook.crypto.exception.CryptoInitializationException;
import com.facebook.crypto.exception.KeyChainException;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import org.andengine.util.adt.io.in.IInputStreamOpener;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by pablo on 01/01/15.
 */
public class FileInputStreamOpener implements IInputStreamOpener {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private String file;
    private String cryptEntity;

    public FileInputStreamOpener(String file, String cryptEntity) {
        this.file = file;
        this.cryptEntity = cryptEntity;
    }

    @Override
    public InputStream open() throws IOException {
        if(cryptEntity != null) {
            try {
                return App.crypto.getCipherInputStream(new FileInputStream(file), new Entity(cryptEntity));
            }
            catch(Exception e) {
                LOGGER.e(e.getMessage(), e);
                return null;
            }
        }
        else {
            return new FileInputStream(file);
        }
    }
}
