package com.alchemy.thereaderengine.ui.viewer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.alchemy.thereaderengine.logic.LogicFacade;
import com.alchemy.thereaderengine.ui.viewer.util.FileInputStreamOpener;
import com.apocalipta.comic.vo.v1.PageVO;
import com.alchemy.thereaderengine.listener.ThumbEntityListener;
import com.facebook.crypto.exception.CryptoInitializationException;
import com.facebook.crypto.exception.KeyChainException;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.bitmap.BitmapTexture;
import org.andengine.opengl.texture.bitmap.BitmapTextureFormat;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TextureRegionFactory;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.adt.io.in.ByteArrayInputStreamOpener;
import org.andengine.util.color.Color;

import java.io.IOException;

public class ThumbEntity extends Rectangle {

    private static final Logger LOGGER = LoggerManager.getLogger();

    public PageVO pageVO;
	private ThumbEntityListener listener;
    private Sprite thumbSprite;

	public ThumbEntity(ThumbEntityListener listener, PageVO pageVO, float pX, float pY, VertexBufferObjectManager pVertexBufferObjectManager, TextureManager textureManager) throws IOException, KeyChainException, CryptoInitializationException {
        super(pX, pY, 0, 0, pVertexBufferObjectManager);

        setColor(Color.BLACK);

        byte[] pageThumbData = LogicFacade.readPageThumb(pageVO.getId());

        //decode image size
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        Bitmap bitmap = BitmapFactory.decodeByteArray(pageThumbData, 0, pageThumbData.length);
        setWidth(bitmap.getWidth());
        setHeight(bitmap.getHeight());
        bitmap.recycle();

        ITexture texture = new BitmapTexture(
                textureManager,
                new ByteArrayInputStreamOpener(pageThumbData),
                BitmapTextureFormat.RGB_565,
                TextureOptions.BILINEAR);

        texture.load();
        ITextureRegion thumbTextureRegion = TextureRegionFactory.extractFromTexture(texture);
        thumbSprite = new Sprite(0, 0, thumbTextureRegion, getVertexBufferObjectManager());

        this.pageVO = pageVO;
		this.listener = listener;
	}

    public void attachThumb() {
        thumbSprite.setWidth(getWidth());
        thumbSprite.setHeight(getHeight());
        attachChild(thumbSprite);
    }

	@Override
	public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
        LOGGER.i("--> THUMB TOUCHED!");
		listener.thumbTouched(this);
		return super.onAreaTouched(pSceneTouchEvent, pTouchAreaLocalX, pTouchAreaLocalY);
	}
}