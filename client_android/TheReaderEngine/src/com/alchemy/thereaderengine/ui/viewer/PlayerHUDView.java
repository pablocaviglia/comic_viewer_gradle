package com.alchemy.thereaderengine.ui.viewer;

import android.view.MotionEvent;

import com.alchemy.thereaderengine.App;
import com.alchemy.thereaderengine.ComicViewerActivity;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.config.Config;
import com.alchemy.thereaderengine.listener.ThumbEntityListener;
import com.apocalipta.comic.vo.v1.PageVO;
import com.apocalipta.comic.vo.v1.PublicationVO;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.modifier.AlphaModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.util.color.Color;

import java.util.ArrayList;
import java.util.List;

public class PlayerHUDView implements ThumbEntityListener {

    private static final Logger LOGGER = LoggerManager.getLogger();

    //constants
	private final int THUMBS_MARGIN = 5;
	private final float ANIMATION_TIME_SEC = 0.3f;

	public boolean visible;

	private ComicViewerActivity comicViewerActivity;
	private Scene scene;
    private PlayControlsEntity playControlsEntity;
	private List<ThumbEntity> thumbEntities;
	private ThumbBarEntity thumbBarEntity;
	private Rectangle backgroundEntity;

	private int maxThumbHeight;
	private float totalThumbsWidth;

	private HUD hud;
	private ThumbEntityListener listener;

    private ITextureRegion thumbLoadingTextureRegion;

	public PlayerHUDView(ComicViewerActivity comicViewerActivity, ThumbEntityListener listener) {
		this.comicViewerActivity = comicViewerActivity;
		this.listener = listener;
	}

	public void init(final PublicationVO publicationVO, Scene scene, HUD hud) {
		this.scene = scene;
		this.hud = hud;

        BitmapTextureAtlas thumbLoadingAtlas = new BitmapTextureAtlas(comicViewerActivity.getTextureManager(), 256, 256);
        thumbLoadingTextureRegion = BitmapTextureAtlasTextureRegionFactory.createFromResource(thumbLoadingAtlas, comicViewerActivity, R.drawable.tre_logo_small, 0, 0);
        thumbLoadingAtlas.load();

		backgroundEntity = new Rectangle(
				0, 0,
				App.SCREEN_WIDTH, App.SCREEN_HEIGHT,
				comicViewerActivity.getVertexBufferObjectManager());
		backgroundEntity.setColor(0f, 0f, 0f, 0f);

		initPublication(publicationVO);
	}

	private void initPublication(PublicationVO publicationVO) {
		try {
			//attach the background
			hud.attachChild(backgroundEntity);

            //play controls
            playControlsEntity = new PlayControlsEntity(comicViewerActivity);
            hud.attachChild(playControlsEntity);

			//calculamos el 30% de la altura del
			//dispositivo para cada thumbnail tenga
			//ese alto
			maxThumbHeight = (int)(((float)App.SCREEN_HEIGHT) * 0.30f);

            thumbBarEntity = new ThumbBarEntity(0, maxThumbHeight, 0, maxThumbHeight, comicViewerActivity.getVertexBufferObjectManager());
			thumbBarEntity.setColor(new Color(1f,1f,1f,0.35f));
			hud.attachChild(thumbBarEntity);

			int thumbPositionX = 0;
            int thumbBarEntityWidth = 0;

			//list of thumbs
			thumbEntities = new ArrayList<>();

			List<PageVO> pagesVO = publicationVO.getProjectVO().getPages();
			for(PageVO pageVO : pagesVO) {

                //create thumb entity
				ThumbEntity thumbEntity = new ThumbEntity(this, pageVO, 0, 0, comicViewerActivity.getVertexBufferObjectManager(), comicViewerActivity.getTextureManager());

				//calculamos el nuevo ancho en
				//relacion al nuevo alto 'altoThumb'
				int thumbWidth = maxThumbHeight * (int)thumbEntity.getWidth() / (int)thumbEntity.getHeight();
                thumbBarEntityWidth += thumbWidth;

				//indicamos alto y ancho
				thumbEntity.setHeight(maxThumbHeight);
				thumbEntity.setWidth(thumbWidth);
				thumbEntity.setPosition(thumbPositionX, 0);

				//listener touch para el thumb
				scene.registerTouchArea(thumbEntity);

				//incrementamos para indicar
				//el ancho total de los thumbs
				totalThumbsWidth += thumbEntity.getWidth() + THUMBS_MARGIN;

				//agregamos el thumb bar
				thumbBarEntity.attachChild(thumbEntity);

				//incrementamos la posicion en x
				//para la proxima imagen
				thumbPositionX += thumbEntity.getWidth() + THUMBS_MARGIN;

				//attach thumb to scene
                thumbEntity.attachThumb();

				//add to the collection
				thumbEntities.add(thumbEntity);

			}

			//set the total width of the thumb bar
            thumbBarEntity.setWidth(thumbBarEntityWidth);

			//recalculamos posicion de los thumbs
			//en caso de que la lectura no sea
			//de tipo occidental (de derecha a
			//izquierda)
			if(!Config.LECTURA_OCCIDENTAL) {
				//obtenemos las posiciones actuales
				List<Float> thumbPositions = new ArrayList<Float>();
				for(ThumbEntity thumbEntity : thumbEntities) {
					thumbPositions.add(thumbEntity.getX());
				}
				//recorremos la lista de thumbentities
				//al reves y le seteamos las posiciones
				for(int i=thumbEntities.size()-1,j=0; i>=0; i--,j++) {
					thumbEntities.get(i).setPosition(thumbPositions.get(j), 0);
				}
			}

			//si el ancho total de los thumbs no
			//llega a cubrir el ancho del dispositivo
			//centramos los thumbs
			if(totalThumbsWidth < App.SCREEN_WIDTH) {
				thumbBarEntity.setX((App.SCREEN_WIDTH - totalThumbsWidth) / 2f);
			}

			//hide the hud
			hide();
		}
		catch (Exception e) {
            LOGGER.e(e.getMessage(), e);
		}
	}

	public void onLongPress(MotionEvent evt) {
		setVisible(!visible);
	}

	public void onSingleTapUp(MotionEvent evt) {
		if(evt.getY() > playControlsEntity.getHeight() && evt.getY() < (App.SCREEN_HEIGHT - maxThumbHeight)) {
			setVisible(false);
		}
		else {
			TouchEvent touchEvent = new TouchEvent();
			touchEvent.set(evt.getX(), evt.getY());
			scene.onSceneTouchEvent(touchEvent);
		}
	}

	public void onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		if(totalThumbsWidth > App.SCREEN_WIDTH) {
			float rightLimit = -(totalThumbsWidth - App.SCREEN_WIDTH);
			thumbBarEntity.setX(thumbBarEntity.getX() - distanceX);
			if(thumbBarEntity.getX() > 0) {
				thumbBarEntity.setX(0f);
			}
			else if(thumbBarEntity.getX() < rightLimit) {
				thumbBarEntity.setX(rightLimit);
			}
		}
	}

	public void show() {
		thumbBarEntity.setY(App.SCREEN_HEIGHT - maxThumbHeight);
	}

	public void hide() {
		hidePlayBar();
		hideThumbBar();
		if(backgroundEntity != null) {
			backgroundEntity.setColor(0f, 0f, 0f, 0f);
		}
	}

	private void hideThumbBar() {
		thumbBarEntity.setY(App.SCREEN_HEIGHT);
	}

	private void hidePlayBar() {
		playControlsEntity.setY(-PlayControlsEntity.PLAY_BAR_HEIGHT);
	}

	public void finalizeResources() {
        if(thumbEntities != null) {
            for(ThumbEntity thumbEntity : thumbEntities) {
                thumbEntity.detachSelf();
            }
            thumbEntities.clear();
        }
	}

	public void setVisible(boolean visible) {

		this.visible = visible;

		if(visible) {
			comicViewerActivity.pageNavigatorViewer.animating = false;
		}

		//background alpha
		AlphaModifier backgroundAlphaModifier = new AlphaModifier(ANIMATION_TIME_SEC, backgroundEntity.getAlpha(), visible ? .5f : 0f);
		backgroundEntity.registerEntityModifier(backgroundAlphaModifier);
		backgroundAlphaModifier.setAutoUnregisterWhenFinished(true);

		//play controls
		MoveYModifier playControlsYModifier = new MoveYModifier(ANIMATION_TIME_SEC, playControlsEntity.getY(), visible ? 0 : -PlayControlsEntity.PLAY_BAR_HEIGHT);
		playControlsYModifier.setAutoUnregisterWhenFinished(true);
		playControlsEntity.registerEntityModifier(playControlsYModifier);

		//thumb list
		MoveYModifier thumbListYModifier = new MoveYModifier(ANIMATION_TIME_SEC, thumbBarEntity.getY(), visible ? App.SCREEN_HEIGHT - thumbBarEntity.getHeight() : App.SCREEN_HEIGHT);
		thumbBarEntity.registerEntityModifier(thumbListYModifier);
		thumbListYModifier.setAutoUnregisterWhenFinished(true);

		centerThumbs();
	}

	public void centerThumbs() {
		int currentPageIndex = comicViewerActivity.pageNavigatorViewer.currentPageIndex;
		if(currentPageIndex < thumbEntities.size()) {
			ThumbEntity currentPageThumb = thumbEntities.get(currentPageIndex);

			//calcula la posicion del
			//thumb de la pagina actual
			float currentThumbPosition = 0f;
			for(ThumbEntity thumbEntity : thumbEntities) {
				if(thumbEntity.pageVO.getPageIndex() == currentPageIndex) {
					currentThumbPosition = thumbEntity.getX();
					break;
				}
			}

			//centramos pagina actual
			currentThumbPosition -= (((float)App.SCREEN_WIDTH) / 2f) - (currentPageThumb.getWidth() / 2f);

			//seteamos pos
			thumbBarEntity.setX(-currentThumbPosition);

			//verificamos limite izq y der
			float rightLimit = -(totalThumbsWidth - App.SCREEN_WIDTH);
			if(thumbBarEntity.getX() > 0) {
				thumbBarEntity.setX(0f);
			}
			else if(thumbBarEntity.getX() < rightLimit) {
				thumbBarEntity.setX(rightLimit);
			}
		}
	}

	@Override
	public void thumbTouched(ThumbEntity thumbEntity) {
		listener.thumbTouched(thumbEntity);
	}
}