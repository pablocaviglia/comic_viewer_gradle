package com.alchemy.thereaderengine.ui.viewer;

import org.andengine.entity.primitive.Rectangle;
import org.andengine.opengl.vbo.VertexBufferObjectManager;

import java.util.List;

public class ThumbBarEntity extends Rectangle {
    public ThumbBarEntity(float pX, float pY, float pWidth, float pHeight, VertexBufferObjectManager pVertexBufferObjectManager) {
        super(pX, pY, pWidth, pHeight, pVertexBufferObjectManager);
    }
}
