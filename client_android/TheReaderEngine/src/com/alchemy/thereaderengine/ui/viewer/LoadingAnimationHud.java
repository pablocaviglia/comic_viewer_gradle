package com.alchemy.thereaderengine.ui.viewer;

import com.alchemy.thereaderengine.App;
import com.alchemy.thereaderengine.ComicViewerActivity;

import org.andengine.engine.camera.hud.HUD;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.region.TiledTextureRegion;

public class LoadingAnimationHud {

	private AnimatedSprite sprLoading;
	private float animX;
	private float animY;
	
	public void init(ComicViewerActivity comicViewerActivity, HUD hud) {
		
		//load the texture
		BitmapTextureAtlas texLoading = new BitmapTextureAtlas(comicViewerActivity.getTextureManager(), 1024, 512, TextureOptions.BILINEAR);
		TiledTextureRegion regLoading = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(texLoading, comicViewerActivity.getAssets(),"bookloading.png", 0, 0, 5, 2);
		texLoading.load();
		
		animX = App.SCREEN_WIDTH / 2f - regLoading.getWidth() / 2f;
		animY = App.SCREEN_HEIGHT / 2f - regLoading.getHeight() / 2f;
		
		sprLoading = new AnimatedSprite(0, 0, regLoading, comicViewerActivity.getVertexBufferObjectManager());
		sprLoading.animate(100);
		
		hud.attachChild(sprLoading);
	}
	
	public void show(boolean flipped) {
		sprLoading.setFlippedHorizontal(flipped);
		sprLoading.setPosition(animX, animY);
	}
	
	public void ocultar() {
		sprLoading.setPosition(0, App.SCREEN_HEIGHT);
	}
}