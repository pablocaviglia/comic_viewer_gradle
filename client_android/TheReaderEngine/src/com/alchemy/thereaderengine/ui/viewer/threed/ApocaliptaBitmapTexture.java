package com.alchemy.thereaderengine.ui.viewer.threed;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLUtils;
import android.widget.Toast;

import com.alchemy.thereaderengine.ComicViewerActivity;
import com.alchemy.thereaderengine.R;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import org.andengine.opengl.texture.ITextureStateListener;
import org.andengine.opengl.texture.PixelFormat;
import org.andengine.opengl.texture.Texture;
import org.andengine.opengl.texture.TextureManager;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.bitmap.BitmapTextureFormat;
import org.andengine.opengl.util.GLState;
import org.andengine.util.math.MathUtils;

import java.io.IOException;

public class ApocaliptaBitmapTexture extends Texture {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private final int mWidth;
	private final int mHeight;
	private Bitmap bitmap;
    private BitmapLoaderListener bitmapLoaderListener;
	
	public ApocaliptaBitmapTexture(BitmapLoaderListener bitmapLoaderListener, TextureManager pTextureManager, ITextureStateListener pTextureStateListener, Bitmap bitmap) throws IOException {
		super(pTextureManager, BitmapTextureFormat.RGBA_8888.getPixelFormat(), TextureOptions.BILINEAR, pTextureStateListener);
        this.bitmapLoaderListener = bitmapLoaderListener;
		this.bitmap = bitmap;
		this.mWidth = bitmap.getWidth();
		this.mHeight = bitmap.getHeight();
	}

	public int getWidth() {
		return this.mWidth;
	}

	@Override
	public int getHeight() {
		return this.mHeight;
	}

	@Override
	protected void writeTextureToHardware(final GLState pGLState) throws IOException {
		if(bitmap != null && !bitmap.isRecycled()) {
			final boolean useDefaultAlignment = MathUtils.isPowerOfTwo(bitmap.getWidth()) && MathUtils.isPowerOfTwo(bitmap.getHeight()) && (this.mPixelFormat == PixelFormat.RGBA_8888);
			if(!useDefaultAlignment) {
				GLES20.glPixelStorei(GLES20.GL_UNPACK_ALIGNMENT, 1);
			}

			try {
				final boolean preMultiplyAlpha = this.mTextureOptions.mPreMultiplyAlpha;
				if(preMultiplyAlpha) {
					GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
				} else {
					pGLState.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0, this.mPixelFormat);
				}

				if(!useDefaultAlignment) {
					GLES20.glPixelStorei(GLES20.GL_UNPACK_ALIGNMENT, GLState.GL_UNPACK_ALIGNMENT_DEFAULT);
				}
			}
			catch(OutOfMemoryError e) {
				LOGGER.e(e.getMessage(), e);
                bitmapLoaderListener.outOfMemory();
				throw e;
			}

			bitmap = null;
		}
	}
}