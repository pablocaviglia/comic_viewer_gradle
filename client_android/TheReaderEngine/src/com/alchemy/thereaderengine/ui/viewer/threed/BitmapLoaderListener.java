package com.alchemy.thereaderengine.ui.viewer.threed;

/**
 * Created by pablo on 25/06/15.
 */
public interface BitmapLoaderListener {

    void outOfMemory();

}
