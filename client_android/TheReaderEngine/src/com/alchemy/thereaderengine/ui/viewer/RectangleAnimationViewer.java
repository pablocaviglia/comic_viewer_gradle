package com.alchemy.thereaderengine.ui.viewer;

import com.alchemy.thereaderengine.ComicViewerActivity;

import org.andengine.entity.modifier.MoveXModifier;
import org.andengine.entity.modifier.MoveYModifier;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.util.color.Color;

import java.util.List;

public class RectangleAnimationViewer {
	
	private float totalPagesHeight;
	private float totalPagesWidth;
	
	//rectangles
	private Rectangle topRectangle;
	private Rectangle bottomRectangle;
	private Rectangle leftRectangle;
	private Rectangle rightRectangle;
	
	//left rectangle modifier
	private MoveYModifier topRectangleModifier;
	private MoveYModifier bottomRectangleModifier;
	private MoveXModifier leftRectangleModifier;
	private MoveXModifier rightRectangleModifier;
	
	public RectangleAnimationViewer(ComicViewerActivity comicViewerActivity, List<PageEntity> paginas, Scene scene) {

        if(paginas.size() > 0) {

            //calculate the biggest page height
            totalPagesHeight = 0;

            for(PageEntity pagina : paginas) {
                //max page height
                if(pagina.pageVO.getImageHeight() > totalPagesHeight) {
                    totalPagesHeight = pagina.pageVO.getImageHeight();
                }
            }

            //get the position of the
            //last page plus its width
            PageEntity lastPage = paginas.get(paginas.size()-1);
            totalPagesWidth = lastPage.getX() + lastPage.pageVO.getImageWidth();

            //create rectangles
            topRectangle = new Rectangle(0, -totalPagesHeight, totalPagesWidth, totalPagesHeight, comicViewerActivity.getVertexBufferObjectManager());
            bottomRectangle = new Rectangle(0, totalPagesHeight, totalPagesWidth, totalPagesHeight, comicViewerActivity.getVertexBufferObjectManager());
            leftRectangle = new Rectangle(-totalPagesWidth, 0, totalPagesWidth, totalPagesHeight, comicViewerActivity.getVertexBufferObjectManager());
            rightRectangle = new Rectangle(totalPagesWidth, 0, totalPagesWidth, totalPagesHeight, comicViewerActivity.getVertexBufferObjectManager());

            //set rectangles color
            topRectangle.setColor(Color.BLACK);
            bottomRectangle.setColor(Color.BLACK);
            leftRectangle.setColor(Color.BLACK);
            rightRectangle.setColor(Color.BLACK);

            //add rectangles to scene
            scene.attachChild(topRectangle);
            scene.attachChild(bottomRectangle);
            scene.attachChild(leftRectangle);
            scene.attachChild(rightRectangle);

        }
	}
	
	public void update(long delay) {
			
		if(topRectangleModifier != null && !topRectangleModifier.isFinished()) {
			topRectangleModifier.onUpdate(delay, topRectangle);
		}
		
		if(bottomRectangleModifier != null && !bottomRectangleModifier.isFinished()) {
			bottomRectangleModifier.onUpdate(delay, bottomRectangle);
		}
		
		if(leftRectangleModifier != null && !leftRectangleModifier.isFinished()) {
			leftRectangleModifier.onUpdate(delay, leftRectangle);
		}
		
		if(rightRectangleModifier != null && !rightRectangleModifier.isFinished()) {
			rightRectangleModifier.onUpdate(delay, rightRectangle);
		}
	}
	
	public void show(float leftMargin, float rightMargin, float topMargin, float bottomMargin, long animationTime) {
		
		//top
		topRectangleModifier = new MoveYModifier(animationTime, topRectangle.getY(), -totalPagesHeight + topMargin);
		
		//bottom
		bottomRectangleModifier = new MoveYModifier(animationTime, bottomRectangle.getY(), bottomMargin);
		
		//left
		leftRectangleModifier = new MoveXModifier(animationTime, leftRectangle.getX(), -totalPagesWidth + leftMargin);
		
		//right
		rightRectangleModifier = new MoveXModifier(animationTime, rightRectangle.getX(), rightMargin);
		
	}
}