package com.alchemy.thereaderengine.ui.viewer;

import org.andengine.entity.sprite.Sprite;

/**
 * Created by pablo on 11/03/15.
 */
public interface PlayerButtonListener {
    void buttonTouched(PlayControlsEntity.PlayerControlButton button);
}
