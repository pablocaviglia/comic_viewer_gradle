package com.alchemy.thereaderengine.ui.view.component;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.ui.util.ParallaxViewPager;
import com.alchemy.thereaderengine.ui.view.GenericView;

/**
 * Created by pablo on 10/03/15.
 */
public class CustomViewPager extends GenericView {

    public CustomViewPager(Context context) {
        super(context, R.layout.custom_pager_view);
    }

    @Override
    public void onCreateView(View parent) {
        RelativeLayout relativeLayout = (RelativeLayout) parent;
    }

    public void showLoadingLayer() {
        findViewById(R.id.loadingLayout).setVisibility(View.VISIBLE);
    }

    public void hideLoadingLayer() {
        findViewById(R.id.loadingLayout).setVisibility(View.GONE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void refreshView() {

    }

    public ParallaxViewPager getPager() {
        return (ParallaxViewPager) findViewById(R.id.customViewerPager);
    }
}