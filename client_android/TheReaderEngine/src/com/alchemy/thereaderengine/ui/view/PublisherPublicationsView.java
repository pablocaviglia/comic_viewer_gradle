package com.alchemy.thereaderengine.ui.view;

import android.app.Activity;

import com.alchemy.thereaderengine.logic.LogicFacade;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

/**
 * Created by pablo on 28/04/15.
 */
public class PublisherPublicationsView extends PublicationListGenericView {

    private static final Logger LOGGER = LoggerManager.getLogger();

    public PublisherPublicationsView(Activity activity, PublicationListLoaderListener publicationListLoaderListener) {
        super(activity, publicationListLoaderListener);
    }

    @Override
    public void getPublications(Object[] params) {
        LOGGER.d("-------> Getting publisher publications...");
        showLoadingCircle(com.alchemy.thereaderengine.R.id.publicationListProgressBar);
        LogicFacade.searchPublisherPublications((Long)params[0], getPublicationAdapter().getPublications().size(), this);
    }

    @Override
    public void refreshView() {

    }
}