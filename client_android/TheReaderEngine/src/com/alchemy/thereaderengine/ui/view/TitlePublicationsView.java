package com.alchemy.thereaderengine.ui.view;

import android.app.Activity;

import com.alchemy.thereaderengine.logic.LogicFacade;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

/**
 * Created by pablo on 28/04/15.
 */
public class TitlePublicationsView extends PublicationListGenericView {

    private static final Logger LOGGER = LoggerManager.getLogger();

    public TitlePublicationsView(Activity activity, PublicationListLoaderListener publicationListLoaderListener) {
        super(activity, publicationListLoaderListener);
    }

    @Override
    public void getPublications(Object[] params) {
        LOGGER.d("-------> Getting title publications...");
        showLoadingCircle(com.alchemy.thereaderengine.R.id.publicationListProgressBar);
        LogicFacade.searchTitlePublications((Long) params[0], getPublicationAdapter().getPublications().size(), this);
    }

    @Override
    public void refreshView() {

    }
}