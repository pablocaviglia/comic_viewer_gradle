package com.alchemy.thereaderengine.ui.view;

import android.app.Activity;

import com.alchemy.thereaderengine.logic.LogicFacade;
import com.alchemy.thereaderengine.R;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

/**
 * Created by pablo on 19/01/15.
 */
public class MyLibraryView extends PublicationListGenericView {

    private static final Logger LOGGER = LoggerManager.getLogger();

    public MyLibraryView(Activity activity, PublicationListLoaderListener publicationListLoaderListener) {
        super(activity, publicationListLoaderListener);
    }

    @Override
    protected void getPublications(Object[] params) {
        LOGGER.d("-------> Getting my downloaded publications...");
        showLoadingCircle(R.id.publicationListProgressBar);
        LogicFacade.searchMyLibrary(null, getPublicationAdapter().getPublications().size(), this);
    }

    @Override
    public void refreshView() {

    }
}