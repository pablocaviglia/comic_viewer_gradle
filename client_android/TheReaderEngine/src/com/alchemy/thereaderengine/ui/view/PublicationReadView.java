package com.alchemy.thereaderengine.ui.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.alchemy.thereaderengine.ui.util.ParallaxViewPager;
import com.alchemy.thereaderengine.ui.util.UILUtil;
import com.apocalipta.comic.vo.v1.PublicationVO;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.io.InputStream;

public class PublicationReadView extends GenericView implements View.OnClickListener {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private ImageView coverImageView;
    private TextView titleTextView;
    private TextView chapterTextView;
    private TextView actionTextView;
    private ImageView actionImageView;
    private LinearLayout publicationSocialContainer;
    private LinearLayout publicationActionContainer;

    private boolean readState;
    private PublicationVO publicationVO;

    public PublicationReadView(Context context) {
        super(context);
    }

    public PublicationReadView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onCreateView(View parent) {
        coverImageView = (ImageView) findViewById(R.id.coverImageView);
        titleTextView = (TextView) findViewById(R.id.titleTextView);
        chapterTextView = (TextView) findViewById(R.id.chapterTextView);
        actionTextView = (TextView) findViewById(R.id.action_txt);
        actionImageView = (ImageView) findViewById(R.id.action_icon);
        publicationSocialContainer = (LinearLayout) findViewById(R.id.publicationSocialContainer);
        publicationActionContainer = (LinearLayout) findViewById(R.id.publicationActionContainer);
        clearPublication();
    }

    public void clearPublication() {
        if(null != coverImageView) {
            coverImageView.setImageDrawable(null);
            titleTextView.setText("");
            chapterTextView.setText("");
            actionTextView.setText("");
            publicationSocialContainer.setVisibility(View.GONE);
            publicationActionContainer.setVisibility(View.GONE);
        }
    }

    public void loadPublication(final PublicationVO publicationVO) {

        publicationSocialContainer.setVisibility(View.VISIBLE);
        publicationActionContainer.setVisibility(View.VISIBLE);

        final ParallaxViewPager parallaxViewPager = ((MainMenuActivity)getContext()).getPublicationViewPager();
        parallaxViewPager.setBackgroundStream(getContext().getResources().openRawResource(R.raw.background_empty));

        //cover
        this.publicationVO = publicationVO;
        String coverURL = LogicFacade.getPublicationCoverURL(publicationVO.getId(), publicationVO.getVersion());
        UILUtil.downloadImage(coverURL, coverImageView, true);

        new Thread(new Runnable() {
            @Override
            public void run() {

                String backgroundURL = LogicFacade.getPublicationBackgroundURL(publicationVO.getId(), publicationVO.getVersion());
                InputStream backgroundBitmapInputStream = UILUtil.downloadImageStreamSync(backgroundURL, true);

                parallaxViewPager.setMaxPages(parallaxViewPager.getAdapter().getCount());
                if(backgroundBitmapInputStream != null) {
                    parallaxViewPager.setBackgroundStream(backgroundBitmapInputStream);
                }
            }
        }).start();

        //app name
        titleTextView.setText(LogicFacade.getPublicationName(publicationVO));
        titleTextView.setTypeface(((MainMenuActivity)getContext()).robotoTypeface);
        titleTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showPublicationDescription();
            }
        });

        //chapter name
        chapterTextView.setText(LogicFacade.getPublicationChapterName(publicationVO));
        chapterTextView.setTypeface(((MainMenuActivity)getContext()).robotoTypeface);
        chapterTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showPublicationDescription();
            }
        });
    }

    private void showPublicationDescription() {
        String publicationDescription = LogicFacade.getPublicationDescription(publicationVO);
        if(publicationDescription != null && !publicationDescription.trim().equalsIgnoreCase("")) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());

            dialog.setTitle(getContext().getString(R.string.description));
            dialog.setMessage(publicationDescription);

            dialog.setPositiveButton(" OK ", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.dismiss();

                }
            });
            dialog.show();
        }
    }

    public void setReadState(boolean enabled) {

        actionTextView = (TextView) findViewById(R.id.action_txt);
        actionImageView = (ImageView) findViewById(R.id.action_icon);
        LinearLayout downloadLinearLayout = (LinearLayout) findViewById(R.id.downloadLinearLayout);

        readState = enabled;
        if(readState) {
            actionTextView.setText(getResources().getString(R.string.read));
            actionImageView.setVisibility(GONE);

            OnClickListener readClickListener = new OnClickListener() {
                @Override
                public void onClick(View v) {
                    readPublication();
                }
            };

            actionTextView.setOnClickListener(readClickListener);
            actionImageView.setOnClickListener(readClickListener);
            downloadLinearLayout.setOnClickListener(null);
            coverImageView.setOnClickListener(readClickListener);
            actionTextView.setOnClickListener(readClickListener);
        }
        else {

            actionTextView.setText(getResources().getString(R.string.download));
            actionImageView.setImageDrawable(getResources().getDrawable(R.drawable.download_icon));
            actionImageView.setVisibility(VISIBLE);

            OnClickListener downloadClickListener = new OnClickListener() {
                @Override
                public void onClick(View v) {
                    downloadPublication();
                }
            };

            coverImageView.setOnClickListener(null);
            actionTextView.setOnClickListener(downloadClickListener);
            actionImageView.setOnClickListener(downloadClickListener);
            downloadLinearLayout.setOnClickListener(downloadClickListener);
            actionImageView.setOnClickListener(downloadClickListener);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void refreshView() {

    }

    @Override
    public void onClick(View view) {

    }

    private void downloadPublication() {
        ((MainMenuActivity) getContext()).downloadPublication();
    }

    private void readPublication() {
        ((MainMenuActivity) getContext()).readPublication();
    }
}