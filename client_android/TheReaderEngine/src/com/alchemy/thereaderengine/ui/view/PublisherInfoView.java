package com.alchemy.thereaderengine.ui.view;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.alchemy.thereaderengine.ui.util.UILUtil;
import com.apocalipta.comic.vo.v1.PublisherVO;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by pablo on 19/02/15.
 */
public class PublisherInfoView extends GenericView {

    private static final Logger LOGGER = LoggerManager.getLogger();
    private SimpleDateFormat sdf = new SimpleDateFormat("LLLL dd, yyyy");

    private Context context;
    private PublisherVO publisherVO;

    public PublisherInfoView(Context context) {
        super(context);
        this.context = context;
    }

    public PublisherInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public void loadPublisher(PublisherVO publisher) {
        this.publisherVO = publisher;

        //publisher name
        String publisherName = publisher.getName();
        TextView publisherNameText = ((TextView) findViewById(R.id.publisherNameText));
        publisherNameText.setText(publisherName);

        //info
        String description = LogicFacade.getPublisherInfo(publisherVO);
        TextView publisherInformationText = (TextView) findViewById(R.id.publisherInformation);
        publisherInformationText.setText(description);

        //creation date
        long creationDate = publisherVO.getCreationDate();
        TextView publisherCreationDateText = (TextView) findViewById(R.id.publisherCreationDate);
        publisherCreationDateText.setText(context.getString(R.string.member_since) + " " + sdf.format(new Date(creationDate)));

        //show publisher avatar
        String avatarUrl = LogicFacade.getPublisherAvatarURL(publisherVO.getId(), publisherVO.getVersion());
        UILUtil.downloadImage(avatarUrl, (ImageView) findViewById(R.id.publisherAvatarImage), true, new UILUtil.UILDownloadListener() {
            @Override
            public void loadingStarted() {

            }
            @Override
            public void downloadFinished(boolean success) {
                if (!success) {
                    ((ImageView) findViewById(R.id.publisherAvatarImage)).setImageResource(R.drawable.tre_logo);
                }
            }
        });

    }

    @Override
    public void onCreateView(View parent) {


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void refreshView() {

    }
}