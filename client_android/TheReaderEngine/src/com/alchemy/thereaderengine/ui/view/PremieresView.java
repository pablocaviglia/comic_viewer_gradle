package com.alchemy.thereaderengine.ui.view;

import android.app.Activity;

import com.alchemy.thereaderengine.R;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

/**
 * Created by pablo on 19/01/15.
 */
public class PremieresView extends PublicationListGenericView {

    private static final Logger LOGGER = LoggerManager.getLogger();

    public PremieresView(Activity activity, PublicationListLoaderListener publicationListLoaderListener) {
        super(activity, publicationListLoaderListener);
    }

    @Override
    public void getPublications(Object[] params) {
        LOGGER.d("-------> Getting premieres publications...");
        showLoadingCircle(R.id.publicationListProgressBar);
        //LogicFacade.search(null, getPublicationAdapter().getPublications().size(), this);

    }

    @Override
    public void refreshView() {

    }
}