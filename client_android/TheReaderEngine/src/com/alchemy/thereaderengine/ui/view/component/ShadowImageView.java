package com.alchemy.thereaderengine.ui.view.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by pablo on 23/02/15.
 */
public class ShadowImageView extends ImageView {

    private Paint mShadow;
    private Bitmap bmp;

    public ShadowImageView(Context context) {
        super(context);
        setup();
    }

    public ShadowImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public ShadowImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup();
    }

    private void setup() {

        mShadow = new Paint();
        mShadow.setShadowLayer(15.0f, 0.0f, 0.0f, Color.GRAY);
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB) {
            setLayerType(LAYER_TYPE_SOFTWARE, mShadow);
        }
    }

    @SuppressLint("DrawAllocation")
    @Override
    public void onDraw(Canvas canvas) {
        Rect rect = new Rect((int)(getWidth()*0.03), (int)(getWidth()*0.03),(int)(getWidth()*0.97), (int)(getHeight()*0.97));
        bmp = ((BitmapDrawable)getDrawable()).getBitmap();
        canvas.drawBitmap(bmp, null, rect, mShadow);
    }
}