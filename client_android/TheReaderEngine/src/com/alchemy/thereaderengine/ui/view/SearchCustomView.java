package com.alchemy.thereaderengine.ui.view;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

/**
 * Created by pablo on 04/05/15.
 */
public class SearchCustomView extends GenericView implements View.OnClickListener, PublicationListGenericView.PublicationListLoaderListener {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private EditText keywordsEditView;
    private SearchPublicationsByKeywordsView searchPublicationsByKeywordsView;

    public SearchCustomView(Context context) {
        super(context, R.layout.search_custom_view);
    }

    @Override
    public void onCreateView(View parent) {

        LinearLayout publicationListLinearLayout = (LinearLayout) findViewById(R.id.publicationListLinearLayout);

        searchPublicationsByKeywordsView = new SearchPublicationsByKeywordsView((MainMenuActivity)getContext(), this);
        publicationListLinearLayout.addView(searchPublicationsByKeywordsView);

        //request edit text focus
        keywordsEditView = (EditText) findViewById(R.id.keywordsEditView);
        keywordsEditView.requestFocus();
        keywordsEditView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_NULL && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    //hide virtual keyboard
                    InputMethodManager imm = (InputMethodManager)getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(keywordsEditView.getWindowToken(), 0);
                    //do the search
                    search();
                }
                return true;
            }
        });

        ImageView searchImage = (ImageView) findViewById(R.id.search_img);
        searchImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });
    }

    public void search() {
        showLoadingCircle(R.id.publicationListProgressBar);
        String keywords = keywordsEditView.getText().toString();
        searchPublicationsByKeywordsView.setParams(new String[] {keywords});
        searchPublicationsByKeywordsView.getPublicationAdapter().clearPublications();
        LogicFacade.searchPublications(keywords, searchPublicationsByKeywordsView.getPublicationAdapter().getPublications().size(), searchPublicationsByKeywordsView);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void refreshView() {
        searchPublicationsByKeywordsView.getPublicationAdapter().notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void finishPublicationListLoading(PublicationListGenericView view, byte operationType) {

    }
}
