package com.alchemy.thereaderengine.ui.view;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.alchemy.thereaderengine.App;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.database.UserDatabaseManager;
import com.alchemy.thereaderengine.enums.AutoPlayTime;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.apocalipta.comic.vo.v1.LanguageVO;
import com.apocalipta.comic.vo.v1.ViewerUserVO;

import java.util.List;

/**
 * Created by pablo on 20/08/14.
 */
public class ConfigView extends GenericView implements View.OnClickListener {

    private Button rotationYesButton;
    private Button rotationNoButton;

    private Button assistedReadingYesButton;
    private Button assistedReadingNoButton;

    private Button textSizeSButton;
    private Button textSizeMButton;
    private Button textSizeLButton;

    private Spinner languageSpinner;
    private Spinner autoPlaybackSpinner;

    public ViewerUserVO loggedUser;

    public ConfigView(Context context) {
        super(context, R.layout.config_view);
    }

    @Override
    public void onCreateView(View parent) {
        //get the last logged user
        loggedUser = LogicFacade.getLastLoggedUser();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void refreshView() {

    }

    @Override
    public void onClick(View view) {

        if(view == rotationYesButton) {
            setRotationConfig(true);
            rotationYesButton.setSelected(true);
            rotationNoButton.setSelected(false);
        }
        else if(view == rotationNoButton) {
            setRotationConfig(false);
            rotationYesButton.setSelected(false);
            rotationNoButton.setSelected(true);
        }
        else if(view == assistedReadingYesButton) {
            setAssistedReading(true);
            assistedReadingYesButton.setSelected(true);
            assistedReadingNoButton.setSelected(false);
        }
        else if(view == assistedReadingNoButton) {
            setAssistedReading(false);
            assistedReadingYesButton.setSelected(false);
            assistedReadingNoButton.setSelected(true);
        }
        else if(view == textSizeSButton) {
            setTextSize("S");
            textSizeSButton.setSelected(true);
            textSizeMButton.setSelected(false);
            textSizeLButton.setSelected(false);
        }
        else if(view == textSizeMButton) {
            setTextSize("M");
            textSizeSButton.setSelected(false);
            textSizeMButton.setSelected(true);
            textSizeLButton.setSelected(false);
        }
        else if(view == textSizeLButton) {
            setTextSize("L");
            textSizeSButton.setSelected(false);
            textSizeMButton.setSelected(false);
            textSizeLButton.setSelected(true);
        }
    }

    public boolean getRotationConfig() {
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        return Boolean.valueOf(userDatabaseManager.getSettingsValue(UserDatabaseManager.TABLE_SETTINGS_COLUMN_ROTATION));
    }

    public void setRotationConfig(boolean rotation) {
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        userDatabaseManager.setSettingsValue(
                UserDatabaseManager.TABLE_SETTINGS_COLUMN_ROTATION,
                Boolean.valueOf(rotation).toString());
    }

    public boolean getAssistedReading() {
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        return Boolean.valueOf(userDatabaseManager.getSettingsValue(UserDatabaseManager.TABLE_SETTINGS_COLUMN_ASSISTED_READING));
    }

    public void setAssistedReading(boolean assistedReading) {
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        userDatabaseManager.setSettingsValue(
                UserDatabaseManager.TABLE_SETTINGS_COLUMN_ASSISTED_READING,
                Boolean.valueOf(assistedReading).toString());
    }

    public String getTextSize() {
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        return userDatabaseManager.getSettingsValue(UserDatabaseManager.TABLE_SETTINGS_COLUMN_TEXT_SIZE);
    }

    public void setTextSize(String textSize) {
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        userDatabaseManager.setSettingsValue(
                UserDatabaseManager.TABLE_SETTINGS_COLUMN_TEXT_SIZE,
                textSize);
    }

    public void loadConfig() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                //get the rotation config value
                boolean rotationConfig = getRotationConfig();

                //yes
                rotationYesButton = (Button) findViewById(com.alchemy.thereaderengine.R.id.rotationYesButton);
                rotationYesButton.setSelected(rotationConfig);
                rotationYesButton.setOnClickListener(ConfigView.this);
                //no
                rotationNoButton = (Button) findViewById(com.alchemy.thereaderengine.R.id.rotationNoButton);
                rotationNoButton.setSelected(!rotationConfig);
                rotationNoButton.setOnClickListener(ConfigView.this);

                //get the assisted reading config value
                boolean assistedReadingConfig = getAssistedReading();

                //yes
                assistedReadingYesButton = (Button) findViewById(com.alchemy.thereaderengine.R.id.assistedReadingYesButton);
                assistedReadingYesButton.setSelected(assistedReadingConfig);
                assistedReadingYesButton.setOnClickListener(ConfigView.this);
                //no
                assistedReadingNoButton = (Button) findViewById(com.alchemy.thereaderengine.R.id.assistedReadingNoButton);
                assistedReadingNoButton.setSelected(!assistedReadingConfig);
                assistedReadingNoButton.setOnClickListener(ConfigView.this);

                //get the assisted reading config value
                String textSizeConfig = getTextSize();
                if(textSizeConfig == null || textSizeConfig.trim().equalsIgnoreCase("")) {
                    setTextSize("S");
                    textSizeConfig = getTextSize();
                }

                //s
                textSizeSButton = (Button) findViewById(com.alchemy.thereaderengine.R.id.textSizeSButton);
                textSizeSButton.setSelected(textSizeConfig.equalsIgnoreCase("s"));
                textSizeSButton.setOnClickListener(ConfigView.this);
                //m
                textSizeMButton = (Button) findViewById(com.alchemy.thereaderengine.R.id.textSizeMButton);
                textSizeMButton.setSelected(textSizeConfig.equalsIgnoreCase("m"));
                textSizeMButton.setOnClickListener(ConfigView.this);
                //l
                textSizeLButton = (Button) findViewById(com.alchemy.thereaderengine.R.id.textSizeLButton);
                textSizeLButton.setSelected(textSizeConfig.equalsIgnoreCase("l"));
                textSizeLButton.setOnClickListener(ConfigView.this);

                //autoplayback
                autoPlaybackSpinner = (Spinner) findViewById(R.id.autoPlaybackSpinner);

                final AutoPlaybackSpinnerItem[] autoPlaybackSpinnerItems = new AutoPlaybackSpinnerItem[5];
                autoPlaybackSpinnerItems[0] = new AutoPlaybackSpinnerItem(null);
                autoPlaybackSpinnerItems[1] = new AutoPlaybackSpinnerItem(AutoPlayTime.X4);
                autoPlaybackSpinnerItems[2] = new AutoPlaybackSpinnerItem(AutoPlayTime.X3);
                autoPlaybackSpinnerItems[3] = new AutoPlaybackSpinnerItem(AutoPlayTime.X2);
                autoPlaybackSpinnerItems[4] = new AutoPlaybackSpinnerItem(AutoPlayTime.X1);

                //load the languages
                ArrayAdapter autoPlaybackArrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, autoPlaybackSpinnerItems);
                autoPlaybackArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                autoPlaybackSpinner.setAdapter(autoPlaybackArrayAdapter);

                //set the user selected value
                boolean autoPlaybackEnabled = LogicFacade.isAutoPlaybackEnabled();
                if(autoPlaybackEnabled) {
                    AutoPlayTime autoPlaybackUserSelected = LogicFacade.getAutoPlayBackTime();
                    for(AutoPlaybackSpinnerItem autoPlaybackSpinnerItem : autoPlaybackSpinnerItems) {
                        if(autoPlaybackSpinnerItem.autoPlayTime == autoPlaybackUserSelected) {
                            for(int i=0; i<autoPlaybackSpinnerItems.length; i++) {
                                AutoPlaybackSpinnerItem autoPlaybackSpinnerItem1 = (AutoPlaybackSpinnerItem) autoPlaybackSpinner.getItemAtPosition(i);
                                if(autoPlaybackSpinnerItem1.autoPlayTime == autoPlaybackUserSelected) {
                                    autoPlaybackSpinner.setSelection(i);
                                    break;
                                }
                            }
                        }
                    }
                }


                //listener to autoplayback selection
                autoPlaybackSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        AutoPlaybackSpinnerItem autoPlaybackSpinnerItem = autoPlaybackSpinnerItems[i];
                        if(autoPlaybackSpinnerItem.autoPlayTime == null) {
                            LogicFacade.setAutoPlaybackEnabled(false);
                        }
                        else {
                            LogicFacade.setAutoPlaybackEnabled(true);
                            LogicFacade.setAutoPlayBackTime(autoPlaybackSpinnerItem.autoPlayTime);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });


                //language
                List<LanguageVO> languageVOList = LogicFacade.getLanguages();

                //language
                languageSpinner = (Spinner) findViewById(com.alchemy.thereaderengine.R.id.languageSpinner);

                //fill subtitles array
                int lenguajesSize = languageVOList.size();
                final LanguageSpinnerItem[] languageItems = new LanguageSpinnerItem[lenguajesSize + 1];
                languageItems[0] = new LanguageSpinnerItem(null);
                for(int i=1;i<lenguajesSize + 1; i++) {
                    languageItems[i] = new LanguageSpinnerItem(languageVOList.get(i-1));
                }

                //load the languages
                ArrayAdapter languageArrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, languageItems);
                languageArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                languageSpinner.setAdapter(languageArrayAdapter);

                //set the user selected value
                UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
                String languageUserSelectedLanguage = userDatabaseManager.getSettingsValue(UserDatabaseManager.TABLE_SETTINGS_COLUMN_LANGUAGE);
                if(languageUserSelectedLanguage != null && !languageUserSelectedLanguage.trim().equalsIgnoreCase("")) {
                    for(LanguageVO languageVO : languageVOList) {
                        if(languageVO.getId() == Long.parseLong(languageUserSelectedLanguage)) {
                            //qnty of elements in combo
                            int langElements = languageItems.length;
                            for(int i=0; i<langElements; i++) {
                                LanguageSpinnerItem languageCombo = (LanguageSpinnerItem) languageSpinner.getItemAtPosition(i+1);
                                if(languageCombo.languageVO.getId() == languageVO.getId()) {
                                    languageSpinner.setSelection(i+1);
                                    break;
                                }
                            }
                        }
                    }
                }

                //listener to language selection
                languageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        //get the selected language
                        LanguageSpinnerItem languageSelected = languageItems[i];
                        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
                        if (languageSelected.languageVO == null) {
                            //disable subtitles
                            userDatabaseManager.setSettingsValue(
                                    UserDatabaseManager.TABLE_SETTINGS_COLUMN_SUBTITLES,
                                    Boolean.toString(false));
                            //update the language
                            userDatabaseManager.setSettingsValue(
                                    UserDatabaseManager.TABLE_SETTINGS_COLUMN_LANGUAGE,
                                    "");
                        } else {
                            //enable subtitles
                            userDatabaseManager.setSettingsValue(
                                    UserDatabaseManager.TABLE_SETTINGS_COLUMN_SUBTITLES,
                                    Boolean.toString(true));
                            //update the language
                            userDatabaseManager.setSettingsValue(
                                    UserDatabaseManager.TABLE_SETTINGS_COLUMN_LANGUAGE,
                                    String.valueOf(languageSelected.languageVO.getId()));
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }
        });
   }

    private class LanguageSpinnerItem {
        public LanguageVO languageVO;
        private LanguageSpinnerItem(LanguageVO languageVO) {
            this.languageVO = languageVO;
        }
        @Override
        public String toString() {
            return languageVO != null ? languageVO.getDescription() : getContext().getString(R.string.disabled);
        }
    }

    private class AutoPlaybackSpinnerItem {
        public AutoPlayTime autoPlayTime;
        public float seconds;
        private AutoPlaybackSpinnerItem(AutoPlayTime autoPlayTime) {
            if(autoPlayTime != null) {
                this.autoPlayTime = autoPlayTime;
                seconds = LogicFacade.getAutoPlayTimeSeconds(autoPlayTime);
            }
        }
        @Override
        public String toString() {
            return autoPlayTime != null ? ((int)seconds) + " " + getContext().getString(R.string.seconds) : getContext().getString(R.string.disabled);
        }
    }
}