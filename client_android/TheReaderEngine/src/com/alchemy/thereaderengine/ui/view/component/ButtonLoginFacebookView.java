package com.alchemy.thereaderengine.ui.view.component;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;

import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.ui.view.GenericView;
import com.noveogroup.android.log.LoggerManager;

/**
 * Created by pablo on 23/06/15.
 */
public class ButtonLoginFacebookView extends GenericView {

    private static final com.noveogroup.android.log.Logger LOGGER = LoggerManager.getLogger();

    public ButtonLoginFacebookView(Context context, AttributeSet attrs) {
        super(context, R.layout.button_login_facebook_view);
        setTag(getClass().getName());
    }

    @Override
    public void onCreateView(View parent) {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void refreshView() {

    }
}