package com.alchemy.thereaderengine.ui.view;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.R;

/**
 * Created by pablo on 04/05/15.
 */
public class SearchByTitleView extends GenericView implements View.OnClickListener {

    private TitleListView titleListView;

    public SearchByTitleView(Context context) {
        super(context, R.layout.search_by_title_view);
    }

    @Override
    public void onCreateView(View parent) {
        LinearLayout titleListLinearLayout = (LinearLayout) findViewById(R.id.titleListLinearLayout);
        titleListView = new TitleListView((MainMenuActivity)getContext());
        titleListLinearLayout.addView(titleListView);
        titleListView.getTitles();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void refreshView() {
        titleListView.getTitleAdapter().notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {

    }
}