package com.alchemy.thereaderengine.ui.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.model.DownloadPageStatus;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

/**
 * Created by pablo on 19/01/15.
 */
public class DownloadPublicationView extends LinearLayout {

    private static final Logger LOGGER = LoggerManager.getLogger();
    private boolean thumbLoaded;
    private boolean viewReady;

    public DownloadPublicationView(Context context) {
        super(context);
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(getContext()).inflate(R.layout.download_publication, null, true);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.WRAP_CONTENT, 1.0f);

        addView(linearLayout, layoutParams);
        viewReady = true;

    }

    public void progress(DownloadPageStatus downloadPageStatus) {
        if (viewReady) {
            if (!downloadPageStatus.transferFinished) {

                String pageStr = getContext().getString(R.string.page);
                String ofStr = getContext().getString(R.string.of);

                TextView pageCountStatusTextView = (TextView) findViewById(R.id.pageCountStatusTextView);
                pageCountStatusTextView.setText(pageStr + " " + downloadPageStatus.page + " " + ofStr + " " + downloadPageStatus.qntyPages);

                //calculate page publicationDownloadProgress
                ProgressBar pageProgressBar = (ProgressBar) findViewById(R.id.pageProgressBar);
                pageProgressBar.setProgress((int) ((downloadPageStatus.thumbReadBytes + downloadPageStatus.pageReadBytes) * 100 / (downloadPageStatus.thumbTotalBytes + downloadPageStatus.pageTotalBytes)));

                //calculate total publicationDownloadProgress
                ProgressBar totalProgressBar = (ProgressBar) findViewById(R.id.totalProgressBar);
                totalProgressBar.setProgress((int) (downloadPageStatus.readBytes * 100 / downloadPageStatus.totalBytes));

                //show the thumb
                if (downloadPageStatus.thumbBitmap != null) {
                    ImageView thumbPreview = (ImageView) findViewById(R.id.thumbPreview);
                    thumbPreview.setImageBitmap(downloadPageStatus.thumbBitmap);
                }

                //page download finished
                if (downloadPageStatus.pageReadBytes == downloadPageStatus.pageTotalBytes) {
                    thumbLoaded = false;
                }
            }
            else {
                //set default icon back
                ImageView thumbPreview = (ImageView) findViewById(R.id.thumbPreview);
                thumbPreview.setImageResource(R.drawable.tre_logo);
                thumbLoaded = false;
                //reset old values
                TextView pageCountStatusTextView = (TextView) findViewById(R.id.pageCountStatusTextView);
                pageCountStatusTextView.setText("");
                ProgressBar pageProgressBar = (ProgressBar) findViewById(R.id.pageProgressBar);
                pageProgressBar.setProgress(0);
                ProgressBar totalProgressBar = (ProgressBar) findViewById(R.id.totalProgressBar);
                totalProgressBar.setProgress(0);
            }
        }
    }
}