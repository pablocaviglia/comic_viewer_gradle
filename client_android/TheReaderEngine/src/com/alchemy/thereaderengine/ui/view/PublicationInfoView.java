package com.alchemy.thereaderengine.ui.view;

import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.apocalipta.comic.constants.ProjectAgeRate;
import com.apocalipta.comic.vo.v1.AuthorVO;
import com.apocalipta.comic.vo.v1.PageVO;
import com.apocalipta.comic.vo.v1.PublicationVO;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.ui.util.UILUtil;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.util.List;

/**
 * Created by pablo on 19/02/15.
 */
public class PublicationInfoView extends GenericView {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private static final int MAX_THUMBS = 10;

    private Context context;
    private PublicationVO publicationVO;

    public PublicationInfoView(Context context) {
        super(context);
        this.context = context;
    }

    public PublicationInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public void loadPublication(PublicationVO publication) {
        this.publicationVO = publication;

        //publisher name
        TextView publisherNameText = (TextView) findViewById(R.id.publisherNameText);
        if(publicationVO.getPublisherVO() != null) {
            publisherNameText.setText(publicationVO.getPublisherVO().getName());
        }
        else {
            publisherNameText.setText("");
        }

        //clean thumb container content
        LinearLayout thumbContainerLayout = (LinearLayout) findViewById(R.id.thumbContainerLayout);
        thumbContainerLayout.removeAllViews();

        //get the pages
        List<PageVO> pageVOList = publication.getProjectVO().getPages();
        if(pageVOList != null && pageVOList.size() > 0) {
            int currentThumb = 0;
            //load thumbs
            for(PageVO pageVO : pageVOList) {
                if(currentThumb < MAX_THUMBS) {
                    String thumbURL = LogicFacade.getPublicationThumbURL(publication.getId(), pageVO.getId(), publication.getVersion());
                    ImageView thumbImageView = new ImageView(context);
                    LayoutParams newParams = new LayoutParams((int)getContext().getResources().getDimension(R.dimen.publicationThumbImageView), ViewGroup.LayoutParams.MATCH_PARENT);
                    thumbImageView.setLayoutParams(newParams);
                    thumbImageView.setAdjustViewBounds(true);
                    thumbContainerLayout.addView(thumbImageView);
                    UILUtil.downloadImage(thumbURL, thumbImageView, true);
                    currentThumb++;
                }
            }
        }
        else {
            //set thumb container height to zero
            HorizontalScrollView thumbHorizontalScroll = (HorizontalScrollView) findViewById(R.id.thumb_horizontal_scroll);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0,0);
            thumbHorizontalScroll.setLayoutParams(params);
        }

        //rate
        String ageRate = publication.getProjectVO().getAgeRate();
        ImageView ageRateImageView = (ImageView) findViewById(R.id.agerate_image);
        if(ageRate == ProjectAgeRate.PLUS_18.toString()) {
            ageRateImageView.setImageResource(R.drawable.rate_a);
        }
        else if(ageRate == ProjectAgeRate.PLUS_13.toString()) {
            ageRateImageView.setImageResource(R.drawable.rate_t);
        }
        else {
            ageRateImageView.setImageResource(R.drawable.rate_e);
        }

        //description
        String description = LogicFacade.getPublicationDescription(publicationVO);
        TextView descriptionText = (TextView) findViewById(R.id.descriptionText);
        descriptionText.setText(description);

        //authors
        List<AuthorVO> authors = publication.getProjectVO().getAuthors();
        if(null != authors && authors.size() > 0) {
            String authorsStr = "";
            int i = 0;
            for(AuthorVO authorVO : authors) {
                authorsStr += (i==0 ? "" : " / ") + authorVO.getName();
                i++;
            }
            ((TextView) findViewById(R.id.authorsText)).setText(authorsStr);
        }
        else {
            findViewById(R.id.authorsLinearLayout).setVisibility(View.GONE);
        }

        //genre
        if(null != publication.getProjectVO().getGenre()) {
            ((TextView) findViewById(R.id.genreText)).setText(publication.getProjectVO().getGenre().getDescription());
        }
        else {
            findViewById(R.id.genreLinearLayout).setVisibility(View.GONE);
        }

        //credits
        String creditsStr = "";
        String credits = LogicFacade.getPublicationCredits(publicationVO);
        if(credits != null && !"".equals(credits.trim())) {
            creditsStr += Html.fromHtml(credits);
        }

        //add authors to credits
        if(null != authors && authors.size() > 0) {
            if(creditsStr.length() > 0) {
                creditsStr += Html.fromHtml("<br><br>");
            }

            int i=0;
            for(AuthorVO authorVO : authors) {
                creditsStr += Html.fromHtml((i>0?"<br>":"") + authorVO.getType() + ": " + authorVO.getName());
                i++;
            }
        }

        //legal
        String legal = LogicFacade.getPublicationLegal(publicationVO);
        if(legal != null && !"".equals(legal.trim())) {
            creditsStr += Html.fromHtml("<br><br>" + legal);
        }

        //set full credits str
        TextView creditsText = (TextView) findViewById(R.id.creditsText);
        creditsText.setText(creditsStr);

        //publisher layout listener
        findViewById(R.id.publisherLinearLayout).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainMenuActivity)getContext()).showPublisher(publicationVO.getPublisherVO().getId(), true);
            }
        });
    }

    public void showPublication() {
//        //get the pages
//        List<PageVO> pageVOList = publicationVO.getProjectVO().getPages();
//        if(pageVOList != null && pageVOList.size() > 0) {
//            //animate thumb container
//            LinearLayout thumbContainerLayout = (LinearLayout) findViewById(R.id.thumbContainerLayout);
//            Animation slideDown = AnimationUtils.loadAnimation(context, R.anim.slide_down);
//            thumbContainerLayout.startAnimation(slideDown);
//        }
    }

    @Override
    public void onCreateView(View parent) {


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void refreshView() {

    }
}