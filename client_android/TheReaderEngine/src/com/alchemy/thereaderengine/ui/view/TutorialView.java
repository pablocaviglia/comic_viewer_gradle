package com.alchemy.thereaderengine.ui.view;

import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.View.OnTouchListener;

import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.ui.pager.NonSwipeableViewPager;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.ui.adapter.HelpPagerAdapter;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

public class TutorialView extends GenericView implements OnTouchListener, GestureDetector.OnGestureListener, ScaleGestureDetector.OnScaleGestureListener, OnDoubleTapListener {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private GestureDetectorCompat gestureDetector; 
    private ScaleGestureDetector scaleGestureDetector;

	public TutorialView(MainMenuActivity mainMenuActivity) {
		super(mainMenuActivity, R.layout.tutorial_view);
    }

    @Override
    public void onCreateView(View parent) {

        //instantiate the gesture detectors
        gestureDetector = new GestureDetectorCompat(getContext(), this);
        gestureDetector.setOnDoubleTapListener(this);
        scaleGestureDetector = new ScaleGestureDetector(getContext(), this);

        //create the help view adapter
        HelpPagerAdapter pagerAdapter = new HelpPagerAdapter((MainMenuActivity)getContext(), this, this);

        //get the pager
        NonSwipeableViewPager pager = (NonSwipeableViewPager) findViewById(R.id.tutorial_pager);

        //set the View Pager Adapter into ViewPager
        pager.setAdapter(pagerAdapter);

        resetHelp();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

	@Override
	public void refreshView() {

	}

	public void resetHelp() {
		NonSwipeableViewPager pager = (NonSwipeableViewPager) findViewById(R.id.tutorial_pager);
		pager.setCurrentItem(HelpPagerAdapter.HELP_IN_1, true);
	}

    public void exit() {
        resetHelp();
        ((MainMenuActivity)getContext()).showMainMenu(MainMenuActivity.TAB_MAIN_MENU_ALL, true);
    }

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		scaleGestureDetector.onTouchEvent(event);
		gestureDetector.onTouchEvent(event);
		v.onTouchEvent(event);
		return true;
	}

	@Override
	public boolean onDown(MotionEvent e) {
		return true;
	}

    public void setCurrentItem(int item) {
        NonSwipeableViewPager pager = (NonSwipeableViewPager) findViewById(R.id.tutorial_pager);
        pager.setCurrentItem(item, true);
    }

    @Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if(velocityX < 0) {
            moveNextPage();
        }
        else {
            movePreviousPage();
        }
        return true;
	}

	@Override
	public void onLongPress(MotionEvent e) {

    }

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		return true;
	}

	@Override
	public void onShowPress(MotionEvent e) {
		
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
        moveNextPage();
        return true;
	}

	@Override
	public boolean onScale(ScaleGestureDetector detector) {
		return true;
	}

	@Override
	public boolean onScaleBegin(ScaleGestureDetector detector) {
		return true;
	}

	@Override
	public void onScaleEnd(ScaleGestureDetector detector) {

    }

	@Override
	public boolean onDoubleTap(MotionEvent e) {
        moveNextPage();
		return true;
	}

    public void movePreviousPage() {
        NonSwipeableViewPager pager = (NonSwipeableViewPager) findViewById(R.id.tutorial_pager);
        if(pager.getCurrentItem() != HelpPagerAdapter.HELP_OUT_2) {
            setCurrentItem(pager.getCurrentItem()-1);
        }
    }

    public void moveNextPage() {
        NonSwipeableViewPager pager = (NonSwipeableViewPager) findViewById(R.id.tutorial_pager);
        if(pager.getCurrentItem() == HelpPagerAdapter.HELP_OUT_1 || pager.getCurrentItem() == HelpPagerAdapter.HELP_OUT_2) {
            exit();
        }
        else {
            setCurrentItem(pager.getCurrentItem()+1);
        }
    }

	@Override
	public boolean onDoubleTapEvent(MotionEvent e) {
		return true;
	}

	@Override
	public boolean onSingleTapConfirmed(MotionEvent e) {
		return true;
	}
}