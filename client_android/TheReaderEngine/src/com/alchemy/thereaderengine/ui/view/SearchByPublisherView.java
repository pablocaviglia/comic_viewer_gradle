package com.alchemy.thereaderengine.ui.view;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.R;

/**
 * Created by pablo on 04/05/15.
 */
public class SearchByPublisherView extends GenericView implements View.OnClickListener {

    private PublisherListView publisherListView;

    public SearchByPublisherView(Context context) {
        super(context, R.layout.search_by_publisher_view);
    }

    @Override
    public void onCreateView(View parent) {
        LinearLayout publisherListLinearLayout = (LinearLayout) findViewById(R.id.publisherListLinearLayout);
        publisherListView = new PublisherListView((MainMenuActivity)getContext());
        publisherListLinearLayout.addView(publisherListView);
        publisherListView.getPublishers();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void refreshView() {
        publisherListView.getPublisherAdapter().notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {

    }
}
