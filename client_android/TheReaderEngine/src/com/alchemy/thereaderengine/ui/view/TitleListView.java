package com.alchemy.thereaderengine.ui.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import com.alchemy.thereaderengine.App;
import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.listener.LogicFacadeListener;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.alchemy.thereaderengine.ui.adapter.TitleAdapter;
import com.alchemy.thereaderengine.ui.view.component.LoadMoreListView;
import com.apocalipta.comic.vo.v1.SearchResultTitleVO;
import com.apocalipta.comic.vo.v1.TitleVO;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.util.List;

/**
 * Created by pablo on 01/05/15.
 */
public class TitleListView extends GenericView implements LogicFacadeListener, LoadMoreListView.OnLoadMoreListener, View.OnClickListener {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private ImageView retryImageView;

    public TitleListView(Activity activity) {
        super(activity, R.layout.title_list_view);
    }

    @Override
    public void onCreateView(View parent) {

        //listview
        LoadMoreListView listView = (LoadMoreListView) findViewById(R.id.titleListView);

        //content adapter
        TitleAdapter titleAdapter = new TitleAdapter(getContext());

        //set the adapter
        listView.setAdapter(titleAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((MainMenuActivity)getContext()).showTitle(((TitleVO) view.getTag()).getId(), true);
            }
        });

        //scroll listener
        listView.setOnScrollListener(new PauseOnScrollListener(App.secureImageLoader, MainMenuActivity.PAUSE_ONSCROLL, MainMenuActivity.PAUSE_ONFLING));

        //finish scroll listener
        listView.setOnLoadMoreListener(this);

        //retry listener
        retryImageView = (ImageView) findViewById(R.id.retryImageView);
        retryImageView.setOnClickListener(this);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void refreshView() {

    }

    @Override
    public void operationFinished(Object result, byte operationType) {

        //hide loading circle
        hideLoadingCircle(R.id.titleListProgressBar);

        if(operationType == LogicFacadeListener.OPERATION_findTitles)
        {
            SearchResultTitleVO searchResultTitle = (SearchResultTitleVO)result;
            if(searchResultTitle != null && !searchResultTitle.isError()) {
                showTitles(searchResultTitle.getTotal(), searchResultTitle.getElements(), false);
            }
            else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.search_publications_error), Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
        else if(operationType == LogicFacadeListener.OPERATION_findPublishersError) {
            //show retry button
            showRetryImage(R.id.retryImageView);
        }
    }

    public void onLoadMore() {
        getTitles();
    }

    public void getTitles() {
        hideRetryImage(R.id.retryImageView);
        showLoadingCircle(R.id.titleListProgressBar);
        LogicFacade.searchTitles(getTitleAdapter().getTitles().size(), this);
    }

    public void showTitles(final long total, final List<TitleVO> titlesToAdd, final boolean clear) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //listview
                LoadMoreListView listView = (LoadMoreListView) findViewById(R.id.titleListView);
                listView.maxItemCount = total;
                //call onLoadMoreComplete when the LoadMore task, has finished
                listView.onLoadMoreComplete();
                if (clear) {
                    getTitleAdapter().setTitles(titlesToAdd);
                } else {
                    getTitleAdapter().addTitles(titlesToAdd);
                }
            }
        });
    }

    public TitleAdapter getTitleAdapter() {
        LoadMoreListView listView = (LoadMoreListView) findViewById(R.id.titleListView);
        TitleAdapter titleAdapter = (TitleAdapter) listView.getAdapter();
        return titleAdapter;
    }

    @Override
    public void onClick(View view) {
        if(view == retryImageView) {
            getTitles();
        }
    }
}