package com.alchemy.thereaderengine.ui.view;

import android.app.Activity;

import com.alchemy.thereaderengine.logic.LogicFacade;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

/**
 * Created by pablo on 04/05/15.
 */
public class SearchPublicationsByKeywordsView extends PublicationListGenericView {

    private static final Logger LOGGER = LoggerManager.getLogger();

    public SearchPublicationsByKeywordsView(Activity activity, PublicationListLoaderListener publicationListLoaderListener) {
        super(activity, publicationListLoaderListener);
    }

    @Override
    public void getPublications(Object[] params) {
        LOGGER.d("-------> Getting publications by keywords (" + params[0] + ")...");
        showLoadingCircle(com.alchemy.thereaderengine.R.id.publicationListProgressBar);
        LogicFacade.searchPublications(String.valueOf(params[0]), getPublicationAdapter().getPublications().size(), this);
    }

    @Override
    public void refreshView() {

    }
}