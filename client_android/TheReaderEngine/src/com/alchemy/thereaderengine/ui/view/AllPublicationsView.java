package com.alchemy.thereaderengine.ui.view;

import android.app.Activity;

import com.alchemy.thereaderengine.logic.LogicFacade;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

public class AllPublicationsView extends PublicationListGenericView {

    private static final Logger LOGGER = LoggerManager.getLogger();

    public AllPublicationsView(Activity activity, PublicationListLoaderListener publicationListLoaderListener) {
        super(activity, publicationListLoaderListener);
    }

    @Override
    public void getPublications(Object[] params) {
        LOGGER.d("-------> Getting all publications...");
        showLoadingCircle(com.alchemy.thereaderengine.R.id.publicationListProgressBar);
        LogicFacade.searchPublications(null, getPublicationAdapter().getPublications().size(), this);
    }

    @Override
    public void refreshView() {

    }
}