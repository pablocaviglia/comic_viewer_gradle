package com.alchemy.thereaderengine.ui.view;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import com.alchemy.thereaderengine.App;
import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.listener.LogicFacadeListener;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.alchemy.thereaderengine.ui.adapter.PublisherAdapter;
import com.alchemy.thereaderengine.ui.view.component.LoadMoreListView;
import com.apocalipta.comic.vo.v1.PublisherVO;
import com.apocalipta.comic.vo.v1.SearchResultPublisherVO;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.util.List;

/**
 * Created by pablo on 01/05/15.
 */
public class PublisherListView extends GenericView implements LogicFacadeListener, LoadMoreListView.OnLoadMoreListener, View.OnClickListener {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private ImageView retryImageView;

    public PublisherListView(Activity activity) {
        super(activity, R.layout.publisher_list_view);
    }

    @Override
    public void onCreateView(View parent) {

        //listview
        LoadMoreListView listView = (LoadMoreListView) findViewById(R.id.publisherListView);

        //content adapter
        PublisherAdapter publisherAdapter = new PublisherAdapter(getContext());

        //set the adapter
        listView.setAdapter(publisherAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((MainMenuActivity)getContext()).showPublisher(((PublisherVO) view.getTag()).getId(), true);
            }
        });

        //scroll listener
        listView.setOnScrollListener(new PauseOnScrollListener(App.secureImageLoader, MainMenuActivity.PAUSE_ONSCROLL, MainMenuActivity.PAUSE_ONFLING));

        //finish scroll listener
        listView.setOnLoadMoreListener(this);

        //retry listener
        retryImageView = (ImageView) findViewById(R.id.retryImageView);
        retryImageView.setOnClickListener(this);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    @Override
    public void refreshView() {

    }

    @Override
    public void operationFinished(Object result, byte operationType) {

        //hide loading circle
        hideLoadingCircle(R.id.publisherListProgressBar);

        if(operationType == LogicFacadeListener.OPERATION_findPublishers)
        {
            SearchResultPublisherVO searchResultPublisher = (SearchResultPublisherVO)result;
            if(searchResultPublisher != null && !searchResultPublisher.isError()) {
                showPublishers(searchResultPublisher.getTotal(), searchResultPublisher.getElements(), false);
            }
            else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(), getContext().getResources().getString(R.string.search_publications_error), Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
        else if(operationType == LogicFacadeListener.OPERATION_findPublishersError) {
            //show retry button
            showRetryImage(com.alchemy.thereaderengine.R.id.retryImageView);
        }
    }

    public void onLoadMore() {
        getPublishers();
    }

    public void getPublishers() {
        hideRetryImage(R.id.retryImageView);
        showLoadingCircle(R.id.publisherListProgressBar);
        LogicFacade.searchPublishers(null, getPublisherAdapter().getPublishers().size(), this);
    }

    public void showPublishers(final long total, final List<PublisherVO> publishersToAdd, final boolean clear) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //listview
                LoadMoreListView listView = (LoadMoreListView) findViewById(R.id.publisherListView);
                listView.maxItemCount = total;
                //call onLoadMoreComplete when the LoadMore task, has finished
                listView.onLoadMoreComplete();
                if (clear) {
                    getPublisherAdapter().setPublishers(publishersToAdd);
                } else {
                    getPublisherAdapter().addPublishers(publishersToAdd);
                }
            }
        });
    }

    public PublisherAdapter getPublisherAdapter() {
        LoadMoreListView listView = (LoadMoreListView) findViewById(R.id.publisherListView);
        PublisherAdapter publisherAdapter = (PublisherAdapter) listView.getAdapter();
        return publisherAdapter;
    }

    @Override
    public void onClick(View view) {
        if(view == retryImageView) {
            getPublishers();
        }
    }
}