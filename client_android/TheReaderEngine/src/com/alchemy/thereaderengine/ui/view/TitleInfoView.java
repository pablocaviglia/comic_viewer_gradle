package com.alchemy.thereaderengine.ui.view;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.alchemy.thereaderengine.ui.util.UILUtil;
import com.apocalipta.comic.vo.v1.TitleVO;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.text.SimpleDateFormat;

/**
 * Created by pablo on 19/02/15.
 */
public class TitleInfoView extends GenericView {

    private static final Logger LOGGER = LoggerManager.getLogger();
    private SimpleDateFormat sdf = new SimpleDateFormat("LLLL dd, yyyy");

    private Context context;
    private TitleVO titleVO;

    public TitleInfoView(Context context) {
        super(context);
        this.context = context;
    }

    public TitleInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public void loadTitle(TitleVO titleVO) {
        this.titleVO = titleVO;

        //publisher name
        String titleName = LogicFacade.getTitleName(this.titleVO);
        TextView publisherNameText = ((TextView) findViewById(R.id.titleNameText));
        publisherNameText.setText(titleName);

        //info
        String description = LogicFacade.getTitleInfo(this.titleVO);
        TextView publisherInformationText = (TextView) findViewById(R.id.titleInformation);
        publisherInformationText.setText(description);

        //show title icon
        String iconUrl = LogicFacade.getTitleIconURL(this.titleVO.getId(), this.titleVO.getVersion());
        UILUtil.downloadImage(iconUrl, (ImageView) findViewById(R.id.titleIconImage), true, new UILUtil.UILDownloadListener() {
            @Override
            public void loadingStarted() {

            }
            @Override
            public void downloadFinished(boolean success) {
                if (!success) {
                    ((ImageView) findViewById(R.id.titleIconImage)).setImageResource(R.drawable.tre_logo);
                }
            }
        });
    }

    @Override
    public void onCreateView(View parent) {


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void refreshView() {

    }
}