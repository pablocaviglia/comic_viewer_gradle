package com.alchemy.thereaderengine.ui.view;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public abstract class GenericView extends LinearLayout {

    public GenericView(Context context, int layoutId) {
        super(context);
        initComponent(context, layoutId);
    }

    public GenericView(Context context) {
        super(context);
    }

    public GenericView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void initComponent(Context context, int layoutId) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(layoutId, null, false);
        v.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        addView(v,0);
        onCreateView(v);
    }

	protected void runOnUiThread(Runnable runnable) {
		Handler mHandler = new Handler(Looper.getMainLooper());
		mHandler.post(runnable);
	}
	
    public void showLoadingCircle(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ProgressBar progressBar = (ProgressBar) findViewById(resourceId);
                progressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    public void hideLoadingCircle(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ProgressBar progressBar = (ProgressBar) findViewById(resourceId);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    protected void showRetryImage(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ImageView retryImage = (ImageView) findViewById(resourceId);
                retryImage.setVisibility(View.VISIBLE);
            }
        });
    }

    protected void hideRetryImage(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ImageView retryImage = (ImageView) findViewById(resourceId);
                retryImage.setVisibility(View.GONE);
            }
        });
    }

    public abstract void onCreateView(View parent);
    public abstract void onSaveInstanceState(Bundle outState);
    public abstract void refreshView();

}