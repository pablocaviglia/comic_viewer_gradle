package com.alchemy.thereaderengine.ui.view;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class CreditsView extends WebView {

    private String creditsTemplate;

    public CreditsView(Context context) {
        super(context);

        // settings
        getSettings().setJavaScriptEnabled(true);
        getSettings().setLoadWithOverviewMode(true);
        getSettings().setUseWideViewPort(true);

        // remove the white scrollbar at the right
        setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        //load credits html template
        try {
            BufferedInputStream bis = new BufferedInputStream(getResources().getAssets().open("credits.html"));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int i;
            while((i=bis.read(buffer)) != -1) {
                baos.write(buffer, 0, i);
            }
            baos.flush();
            baos.close();
            bis.close();
            creditsTemplate = new String(baos.toByteArray());
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        // disable text selection
        setOnLongClickListener(new OnLongClickListener() {
            public boolean onLongClick(View v) {
                return true;
            }
        });
	}

    public void setCredits(String credits) {
        credits = creditsTemplate.replaceAll("@credits_content", credits != null ? credits : "");
        loadData(credits, "text/html; charset=UTF-8", null);
    }
}