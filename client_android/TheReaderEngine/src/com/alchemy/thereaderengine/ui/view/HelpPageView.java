package com.alchemy.thereaderengine.ui.view;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.ui.adapter.HelpPagerAdapter;

/**
 * Created by pablo on 25/06/15.
 */
public class HelpPageView extends GenericView {

    private int backgroundResource;
    private boolean skip;
    private TutorialView tutorialView;

    public HelpPageView(Context context, OnTouchListener onTouchListener, TutorialView tutorialView, int backgroundResource, boolean skip) {
        super(context, null);
        this.skip = skip;
        setOnTouchListener(onTouchListener);
        this.backgroundResource = backgroundResource;
        this.tutorialView = tutorialView;
    }

    @Override
    public void onCreateView(View parent) {
        ImageView backgroundImage = (ImageView) findViewById(R.id.background_image);
        backgroundImage.setImageResource(backgroundResource);

        ImageView exitButtonImage = (ImageView) findViewById(R.id.exit_tutorial_image);
        if(skip) {
            exitButtonImage.setImageResource(R.drawable.skip_button);
            exitButtonImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tutorialView.setCurrentItem(HelpPagerAdapter.HELP_OUT_2);
                }
            });
        }
        else {
            exitButtonImage.setImageResource(R.drawable.exit_button);
            exitButtonImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tutorialView.exit();
                }
            });
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void refreshView() {

    }
}
