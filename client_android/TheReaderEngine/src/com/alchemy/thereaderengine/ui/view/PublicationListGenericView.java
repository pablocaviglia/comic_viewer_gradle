package com.alchemy.thereaderengine.ui.view;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import com.alchemy.thereaderengine.App;
import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.listener.LogicFacadeListener;
import com.alchemy.thereaderengine.ui.adapter.PublicationAdapter;
import com.alchemy.thereaderengine.ui.view.component.LoadMoreListView;
import com.apocalipta.comic.vo.v1.PublicationVO;
import com.apocalipta.comic.vo.v1.SearchResultPublicationVO;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;
import java.util.List;

/**
 * Created by pablo on 10/12/14.
 */
public abstract class PublicationListGenericView extends GenericView implements LogicFacadeListener, LoadMoreListView.OnLoadMoreListener, View.OnClickListener {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private ImageView retryImageView;
    private PublicationListLoaderListener publicationListLoaderListener;
    private Object[] params;

    public PublicationListGenericView(Activity activity, PublicationListLoaderListener publicationListLoaderListener) {
        super(activity, R.layout.publication_list_view);
        this.publicationListLoaderListener = publicationListLoaderListener;
    }

    @Override
    public void onCreateView(View parent) {

        //listview
        LoadMoreListView listView = (LoadMoreListView) findViewById(R.id.publicationListView);

        //content adapter
        PublicationAdapter publicationAdapter = new PublicationAdapter(getContext());

        //set the adapter
        listView.setAdapter(publicationAdapter);

        //listener que escucha cuando se
        //selecciona un elemento de la lista
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ((MainMenuActivity)getContext()).showPublication(((PublicationVO) view.getTag()).getId(), false, true);
            }
        });

        //listener que escucha el scroll
        listView.setOnScrollListener(new PauseOnScrollListener(App.secureImageLoader, MainMenuActivity.PAUSE_ONSCROLL, MainMenuActivity.PAUSE_ONFLING));

        //listener que escucha cuando el scroll
        //se aproxima al final para asi cargar
        //mas elementos
        listView.setOnLoadMoreListener(this);

        //retry listener
        retryImageView = (ImageView) findViewById(com.alchemy.thereaderengine.R.id.retryImageView);
        retryImageView.setOnClickListener(this);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

    }

    @Override
    public void refreshView() {

    }

    @Override
    public void operationFinished(Object result, byte operationType) {

        //hide loading circle
        hideLoadingCircle(com.alchemy.thereaderengine.R.id.publicationListProgressBar);

        if(operationType == LogicFacadeListener.OPERATION_findPublications ||
           operationType == LogicFacadeListener.OPERATION_findMyLibrary ||
           operationType == LogicFacadeListener.OPERATION_findPublisherPublications ||
           operationType == LogicFacadeListener.OPERATION_findTitlePublications)
        {
            SearchResultPublicationVO searchResultPublication = (SearchResultPublicationVO)result;
            if(searchResultPublication != null && !searchResultPublication.isError()) {
                showPublications(searchResultPublication.getTotal(), searchResultPublication.getElements(), false);
            }
            else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(), getContext().getResources().getString(com.alchemy.thereaderengine.R.string.search_publications_error), Toast.LENGTH_LONG).show();
                    }
                });
            }
        }
        else if(operationType == LogicFacadeListener.OPERATION_findPublicationsError || operationType == LogicFacadeListener.OPERATION_findMyLibraryError) {
            //show retry button
            showRetryImage(com.alchemy.thereaderengine.R.id.retryImageView);
        }

        if(publicationListLoaderListener != null) {
            publicationListLoaderListener.finishPublicationListLoading(this, operationType);
        }
    }

    public void onLoadMore() {
        getPublicationsGeneric();
    }

    public void getPublicationsGeneric() {
        hideRetryImage(com.alchemy.thereaderengine.R.id.retryImageView);
        showLoadingCircle(com.alchemy.thereaderengine.R.id.publicationListProgressBar);
        getPublications(params);
    }

    protected abstract void getPublications(Object[] params);

    public void search(Object[] params) {
        this.params = params;
        getPublicationAdapter().clearPublications();
        getPublicationsGeneric();
    }

    public void showPublications(final long total, final List<PublicationVO> publicationsToAdd, final boolean clear) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //listview
                LoadMoreListView listView = (LoadMoreListView) findViewById(com.alchemy.thereaderengine.R.id.publicationListView);
                listView.maxItemCount = total;
                //call onLoadMoreComplete when the LoadMore task, has finished
                listView.onLoadMoreComplete();
                if(clear) {
                    getPublicationAdapter().setPublications(publicationsToAdd);
                }
                else {
                    getPublicationAdapter().addPublications(publicationsToAdd);
                }
            }
        });
    }

    public PublicationAdapter getPublicationAdapter() {
        LoadMoreListView listView = (LoadMoreListView) findViewById(com.alchemy.thereaderengine.R.id.publicationListView);
        PublicationAdapter publicationAdapter = (PublicationAdapter) listView.getAdapter();
        return publicationAdapter;
    }

    @Override
    public void onClick(View view) {
        if(view == retryImageView) {
            getPublicationsGeneric();
        }
    }

    public interface PublicationListLoaderListener {
        void finishPublicationListLoading(PublicationListGenericView view, byte operationType);
    }

    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }
}