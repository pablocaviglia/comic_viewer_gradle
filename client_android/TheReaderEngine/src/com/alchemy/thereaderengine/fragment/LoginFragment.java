package com.alchemy.thereaderengine.fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.alchemy.thereaderengine.LoginActivity;
import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.database.UserDatabaseManager;
import com.alchemy.thereaderengine.listener.LogicFacadeListener;
import com.alchemy.thereaderengine.logic.AnalyticsFacade;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.alchemy.thereaderengine.ui.view.component.ButtonLoginFacebookView;
import com.alchemy.thereaderengine.ui.view.component.ButtonLoginGoogleView;
import com.apocalipta.comic.constants.Gender;
import com.apocalipta.comic.constants.UserPlatform;
import com.apocalipta.comic.vo.v1.ViewerUserVO;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import org.json.JSONObject;

/**
 * Created by pablo on 01/03/15.
 */
public class LoginFragment extends Fragment {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private RelativeLayout loadingLayout;

    //facebook button's'
    private LoginButton btnSignInFacebookHidden;
    private FacebookClient facebookClient;

    //google button
    private GooglePlusClient googlePlusClient;

    private ViewerUserVO lastLoggedUser;
    private boolean signout;
    private boolean googlePlayServicesAvailable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        googlePlayServicesAvailable = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getActivity()) == ConnectionResult.SUCCESS;

        //create clients
        facebookClient = new FacebookClient();
        googlePlusClient = new GooglePlusClient();

        facebookClient.onCreate(savedInstanceState);

        //google
        if(googlePlayServicesAvailable) {
            googlePlusClient.onCreate(savedInstanceState);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        facebookClient.onActivityResult(requestCode, resultCode, data);
        if(googlePlayServicesAvailable) {
            googlePlusClient.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login, container, false);
        loadingLayout = (RelativeLayout) view.findViewById(R.id.loadingLayout);
        facebookClient.onCreateView(view);
        googlePlusClient.onCreateView(view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        hideLoadingLayer();

        AnalyticsFacade.sendScreenEvent(AnalyticsFacade.Category.UX, AnalyticsFacade.Action.UX_ENTER_SCREEN, AnalyticsFacade.Screen.LOGIN, null);

        //get the last logged user
        lastLoggedUser = LogicFacade.getLastLoggedUser();

        Intent intent = getActivity().getIntent();
        if(intent != null && intent.getExtras() != null) {
            //get flag
            signout = intent.getBooleanExtra("signout", false);
            getActivity().getIntent().putExtra("signout", false); //reset extra
            //signout
            if(signout) {
                //finish database connection
                UserDatabaseManager.getDatabase().close();
                facebookClient.signout();
            }
        }

        if(!signout) {
            if(null != lastLoggedUser) {
                if(lastLoggedUser.getPlatform() == UserPlatform.FACEBOOK) {
                    userConnectedGeneric(lastLoggedUser);
                }
            }
        }

        if(googlePlayServicesAvailable) {
            googlePlusClient.onStart();
        }
    }

    private void showLoadingLayer() {
        loadingLayout.setVisibility(View.VISIBLE);
    }

    private void hideLoadingLayer() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingLayout.setVisibility(View.GONE);
            }
        });
    }

    private void initLoadingProcess() {
        ((LoginActivity)getActivity()).stopReceiveTouchEvents = true;
        showLoadingLayer();
    }

    private void userNotConnectedGeneric(UserPlatform platform) {
        Toast.makeText(getActivity(), "Cannot login on " + platform.toString().toLowerCase(), Toast.LENGTH_LONG).show();
        ((LoginActivity)getActivity()).stopReceiveTouchEvents = false;
        hideLoadingLayer();
    }

    private void userConnectedGeneric(final ViewerUserVO userVO) {

        showLoadingLayer();

        LogicFacade.login(userVO, new LogicFacadeListener() {
            @Override
            public void operationFinished(Object result, byte operationType) {
                if (operationType == LogicFacadeListener.OPERATION_login) {
                    if (getActivity() != null) {
                        Intent mainActivityIntent = new Intent(getActivity(), MainMenuActivity.class);
                        startActivity(mainActivityIntent);
                        getActivity().finish();
                        AnalyticsFacade.sendLogin(userVO.getPlatform(), userVO.getUsername(), true);
                    }
                } else if (operationType == LogicFacadeListener.OPERATION_loginError) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (userVO.getPlatform() == UserPlatform.GOOGLE_PLUS) {
                                googlePlusClient.mGoogleApiClient.disconnect();
                            }
                            AnalyticsFacade.sendLogin(userVO.getPlatform(), userVO.getUsername(), false);
                            Toast.makeText(getActivity(), getString(R.string.user_login_no_internet), Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });
    }

    private class FacebookClient {

        private CallbackManager callbackManager;

        public void onCreate(Bundle savedInstanceState) {
            callbackManager = CallbackManager.Factory.create();
        }

        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

        public void onCreateView(View view) {

            View button = view.findViewWithTag(ButtonLoginFacebookView.class.getName());
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    btnSignInFacebookHidden.performClick();
                }
            });

            btnSignInFacebookHidden = (LoginButton) view.findViewById(R.id.btn_sign_in_facebook_hidden);
            btnSignInFacebookHidden.setReadPermissions("email");
            btnSignInFacebookHidden.setFragment(LoginFragment.this);

            // Callback registration
            btnSignInFacebookHidden.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    initLoadingProcess();
                    getFacebookProfileInformation(loginResult.getAccessToken());
                }

                @Override
                public void onCancel() {
                    userNotConnectedGeneric(UserPlatform.FACEBOOK);
                }

                @Override
                public void onError(FacebookException exception) {
                    userNotConnectedGeneric(UserPlatform.FACEBOOK);
                }
            });

        }

        public void getFacebookProfileInformation(AccessToken accessToken) {

            GraphRequest request = GraphRequest.newMeRequest(
                    accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(
                                JSONObject object,
                                GraphResponse response) {

                            try {
                                JSONObject responseJSON = response.getJSONObject();
                                ViewerUserVO userVO = new ViewerUserVO();

                                //platform
                                userVO.setPlatform(UserPlatform.FACEBOOK);

                                //name, email
                                String firstName = responseJSON.getString("first_name");
                                String lastName = responseJSON.getString("last_name");
                                String email = responseJSON.getString("email");
                                String fullName = responseJSON.getString("name");

                                userVO.setFirstName(firstName);
                                userVO.setLastName(lastName);
                                userVO.setEmail(email);
                                userVO.setUsername(email);
                                userVO.setFullName(fullName);

                                try {
                                    //gender
                                    String gender = responseJSON.getString("gender");
                                    if (gender != null) {
                                        if (Gender.FEMALE.toString().equalsIgnoreCase(gender.trim())) {
                                            userVO.setGender(Gender.FEMALE);
                                        } else if (Gender.MALE.toString().equalsIgnoreCase(gender.trim())) {
                                            userVO.setGender(Gender.MALE);
                                        } else {
                                            userVO.setGender(Gender.UNKNOWN);
                                        }
                                    }

                                    //locale
                                    String locale = responseJSON.getString("locale");
                                    userVO.setLocale(locale);

                                    //photo url
                                    String photoURL = responseJSON.getJSONObject("picture").getJSONObject("data").getString("url");
                                    userVO.setPhotoUrl(photoURL);
                                } catch (Exception e) {
                                    //not so import fields, allow login if something fails here
                                    LOGGER.e(e.getMessage(), e);
                                }

                                userConnectedGeneric(userVO);
                            } catch (Exception e) {
                                LOGGER.e("Cannot login tofacebook", e);
                                userNotConnectedGeneric(UserPlatform.FACEBOOK);
                            }
                        }
                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "name, first_name, last_name, email, gender, locale, picture");
            request.setParameters(parameters);
            request.executeAsync();

        }

        public void signout() {
            if(null != lastLoggedUser) {

                if(lastLoggedUser.getPlatform() == UserPlatform.FACEBOOK) {
                    LogicFacade.setLastLoggedUser(null);
                    lastLoggedUser = null;
                    signout = false;
                }

                btnSignInFacebookHidden.clearPermissions();
                LoginManager.getInstance().logOut();
            }
        }
    }

    private class GooglePlusClient implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {


        private static final int REQUEST_CODE_RESOLVE_ERR = 9000;
        private static final int RC_SIGN_IN = 0;

        // Profile pic image size in pixels
        private static final int PROFILE_PIC_SIZE = 400;

        // Google client to interact with Google API
        private GoogleApiClient mGoogleApiClient;

        /**
         * A flag indicating that a PendingIntent is in progress and prevents us
         * from starting further intents.
         */
        private boolean mIntentInProgress;

        private boolean mSignInClicked;

        private ConnectionResult mConnectionResult;

        public void onCreate(Bundle savedInstanceState) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(Plus.API)
                    .addScope(Plus.SCOPE_PLUS_LOGIN)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();
        }

        public void onStart() {
            mGoogleApiClient.connect();
        }

        public void onCreateView(View view) {
            View button = view.findViewWithTag(ButtonLoginGoogleView.class.getName());
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(googlePlayServicesAvailable) {
                        mSignInClicked = true;
                        showLoadingLayer();
                        signin();
                    }
                    else {
                        GooglePlayServicesUtil.getErrorDialog(ConnectionResult.SERVICE_MISSING, getActivity(), 0).show();
                    }
                }
            });
        }

        @Override
        public void onConnected(Bundle connectionHint) {
            if(signout) {
                signout();
            }
            else {
                mSignInClicked = false;
                ViewerUserVO viewerUserVO = getGPlusProfileInformation(Plus.PeopleApi.getCurrentPerson(mGoogleApiClient));
                userConnectedGeneric(viewerUserVO);
            }
        }

        protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
            if (requestCode == RC_SIGN_IN) {
                if (responseCode != Activity.RESULT_OK) {
                    mSignInClicked = false;
                }

                hideLoadingLayer();
                mIntentInProgress = false;

                if(mSignInClicked) {
                    if (!mGoogleApiClient.isConnecting()) {
                        mGoogleApiClient.reconnect();
                    }
                }
            }
        }

        @Override
        public void onConnectionFailed(ConnectionResult result) {
            if (!result.hasResolution()) {
                GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), getActivity(), 0).show();
                userNotConnectedGeneric(UserPlatform.GOOGLE_PLUS);
                return;
            }

            if (!mIntentInProgress) {
                // Store the ConnectionResult for later usage
                mConnectionResult = result;

                if (mSignInClicked) {
                    // The user has already clicked 'sign-in' so we attempt to
                    // resolve all
                    // errors until the user is signed in, or they cancel.
                    resolveSignInError();
                }
            }
        }

        private void resolveSignInError() {
            if (mConnectionResult != null && mConnectionResult.hasResolution()) {
                try {
                    mIntentInProgress = true;
                    mConnectionResult.startResolutionForResult(getActivity(), RC_SIGN_IN);
                } catch (IntentSender.SendIntentException e) {
                    mIntentInProgress = false;
                    mGoogleApiClient.connect();
                }
            }
            else {
                mGoogleApiClient.connect();
            }
        }

        private void signin() {
            LOGGER.i("---> SIGNING IN");
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
        }

        private void signout() {
            if(null != lastLoggedUser) {
                if (mGoogleApiClient.isConnected()) {

                    if(lastLoggedUser.getPlatform() == UserPlatform.GOOGLE_PLUS) {
                        LogicFacade.setLastLoggedUser(null);
                        lastLoggedUser = null;
                        signout = false;
                    }

                    Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                }
            }
        }

        private ViewerUserVO getGPlusProfileInformation(Person person) {

            ViewerUserVO userVO = new ViewerUserVO();
            userVO.setPlatform(UserPlatform.GOOGLE_PLUS);

            if(person != null) {

                String personName = person.getDisplayName();
                String personPhotoUrl = person.getImage().getUrl();
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                int gender = person.getGender();

                // by default the profile url gives 50x50 px image only
                // we can replace the value with whatever dimension we want by
                // replacing sz=X
                personPhotoUrl = personPhotoUrl.substring(0,
                        personPhotoUrl.length() - 2)
                        + PROFILE_PIC_SIZE;

                userVO.setFullName(personName);
                userVO.setUsername(email);
                userVO.setEmail(email);
                userVO.setPhotoUrl(personPhotoUrl);

                if(Person.Gender.FEMALE == gender) {
                    userVO.setGender(Gender.FEMALE);
                }
                else if(Person.Gender.MALE == gender) {
                    userVO.setGender(Gender.MALE);
                }
                else {
                    userVO.setGender(Gender.UNKNOWN);
                }
            }

            return userVO;
        }

        @Override
        public void onConnectionSuspended(int cause) {
            mGoogleApiClient.connect();
        }
    }
}