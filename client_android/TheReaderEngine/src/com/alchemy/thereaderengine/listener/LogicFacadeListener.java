package com.alchemy.thereaderengine.listener;

public interface LogicFacadeListener {

    byte OPERATION_findPublications              = 1;
    byte OPERATION_findPublicationsError         = 2;
    byte OPERATION_downloadPublicationPages      = 3;
    byte OPERATION_getPublicationDescriptor      = 4;
    byte OPERATION_findMyLibrary                 = 5;
    byte OPERATION_findMyLibraryError            = 6;
    byte OPERATION_login                         = 7;
    byte OPERATION_loginError                    = 8;
    byte OPERATION_getPublisherDescriptor        = 9;
    byte OPERATION_findPublisherPublications     = 10;
    byte OPERATION_findPublishers                = 11;
    byte OPERATION_findPublishersError           = 12;
    byte OPERATION_findTitles                    = 13;
    byte OPERATION_findTitlesError               = 14;
    byte OPERATION_findTitlePublications         = 15;
    byte OPERATION_getTitleDescriptor            = 16;
    byte OPERATION_getPublicationDescriptorError = 17;

	public void operationFinished(Object result, byte operationType);
	
}
