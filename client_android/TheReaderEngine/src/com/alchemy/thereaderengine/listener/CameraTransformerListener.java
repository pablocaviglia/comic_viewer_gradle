package com.alchemy.thereaderengine.listener;

public interface CameraTransformerListener {

	public void animationStarted();
	public void animationFinished();
	
}
