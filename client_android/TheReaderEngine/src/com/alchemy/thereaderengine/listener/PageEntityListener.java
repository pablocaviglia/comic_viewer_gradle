package com.alchemy.thereaderengine.listener;

import com.alchemy.thereaderengine.ui.viewer.PageEntity;

public interface PageEntityListener {
    void resourcesLoaded(PageEntity pageEntity, boolean cacheado, boolean error, boolean recursosCargados);
}
