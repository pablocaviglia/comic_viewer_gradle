package com.alchemy.thereaderengine.listener;

import com.alchemy.thereaderengine.model.DownloadPageStatus;

/**
 * Created by pablo on 29/12/14.
 */
public interface PublicationDownloadListener {

    void publicationDownloadProgress(DownloadPageStatus downloadPageStatus);

}
