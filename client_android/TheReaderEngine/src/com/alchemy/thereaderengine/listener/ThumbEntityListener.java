package com.alchemy.thereaderengine.listener;

import com.alchemy.thereaderengine.ui.viewer.ThumbEntity;

public interface ThumbEntityListener {

	public void thumbTouched(ThumbEntity thumbEntity);

}
