package com.alchemy.thereaderengine;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.GestureDetector.OnDoubleTapListener;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.alchemy.thereaderengine.config.Config;
import com.alchemy.thereaderengine.database.UserDatabaseManager;
import com.alchemy.thereaderengine.listener.PublicationDownloadListener;
import com.alchemy.thereaderengine.listener.ThumbEntityListener;
import com.alchemy.thereaderengine.logic.AnalyticsFacade;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.alchemy.thereaderengine.model.DownloadPageStatus;
import com.alchemy.thereaderengine.ui.viewer.LoadingAnimationHud;
import com.alchemy.thereaderengine.ui.viewer.PageNavigatorViewer;
import com.alchemy.thereaderengine.ui.viewer.PlayerHUDView;
import com.alchemy.thereaderengine.ui.viewer.SubtitleViewer;
import com.alchemy.thereaderengine.ui.viewer.ThumbEntity;
import com.apocalipta.comic.vo.v1.PageVO;
import com.apocalipta.comic.vo.v1.PublicationVO;
import com.apocalipta.comic.vo.v1.SubtitleVO;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.camera.hud.HUD;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.resolutionpolicy.FixedResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.opengl.view.RenderSurfaceView;
import org.andengine.ui.activity.BaseGameActivity;
import org.andengine.ui.activity.SimpleBaseGameActivity;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class ComicViewerActivity extends SimpleBaseGameActivity implements OnTouchListener, OnGestureListener, OnDoubleTapListener, OnScaleGestureListener, ThumbEntityListener, PublicationDownloadListener {

    private static final Logger LOGGER = LoggerManager.getLogger();

    //publication being viewed
    private PublicationVO publicationVO;

    //last update method execution time
    private long lasttime;

    //camara de andengine
    public SmoothCamera camera;

    //hud
    private HUD hud;

    //scene de andengine
    public Scene scene;

    //player hud
    public PlayerHUDView playerHUDView;

    //clase encargada de mostrar
    //la animacion de carga
    private LoadingAnimationHud loadingAnimationHud;

    //clase encargada de la logica de
    //los subtitlos
    private SubtitleViewer subtitleViewer;

    //clase encargada de la logica
    //de navegacion entre las imagenes
    //y cargado de las mismas
    public PageNavigatorViewer pageNavigatorViewer;

    //detector de gestures
    private GestureDetector gestureDetector;
    private ScaleGestureDetector scaleGestureDetector;

    //motion event
    private MotionEvent onTouchEvent;

    //auto playback
    private float autoPlaybackSeconds;
    private boolean autoPlaybackEnabled;
    private boolean autoPlaybackPaused;
    private AutoPlaybackManager autoPlaybackManager;

    //flag to indicate if the user
    //is free to scroll the page
    public boolean freeNavigation;

    //flag seteado cuando se comienza
    //la actividad, leyendo su valor
    //desde la configuracion del usuario
    private boolean subtitlesEnabled;

    //dialogo de progreso
    private ProgressDialog progressDialog;

    //idioma de subtitulos
    //elegido por el usuario
    private Long userSelectedLanguage;

    private boolean showingDowloadImagesDialog;
    private long startReadingSessionTime;

    @Override
    public EngineOptions onCreateEngineOptions() {
        camera = new SmoothCamera(0, 0, App.SCREEN_WIDTH, App.SCREEN_HEIGHT, 0, 0, 0);
        camera.setCenter(0.5f * App.SCREEN_WIDTH, 0.5f * App.SCREEN_HEIGHT);
        EngineOptions engineOptions = new EngineOptions(true,
                ScreenOrientation.PORTRAIT_OR_LANDSCAPE_SENSOR,
                new FixedResolutionPolicy(App.SCREEN_WIDTH, App.SCREEN_HEIGHT), camera);
        engineOptions.getRenderOptions().setDithering(true);
        return engineOptions;
    }

    @Override
    protected synchronized void onResume() {
        super.onResume();
        init();
    }

    @Override
    protected void onSetContentView() {

        //set fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        mRenderSurfaceView = new RenderSurfaceView(this);
        mRenderSurfaceView.setEGLConfigChooser(5, 6, 5, 0, 16, 0);
        mRenderSurfaceView.setRenderer(mEngine, this);
        mRenderSurfaceView.getHolder().setFormat(PixelFormat.RGB_565);

        setContentView(mRenderSurfaceView, BaseGameActivity.createSurfaceViewLayoutParams());

        //seteamos listener touch
        mRenderSurfaceView.setOnTouchListener(this);

    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGB_565);
    }

    @Override
    protected void onCreateResources() {

    }

    @Override
    protected Scene onCreateScene() {
        return scene;
    }

    private void init() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //gesture detector
                gestureDetector = new GestureDetector(getApplicationContext(), ComicViewerActivity.this);
                gestureDetector.setOnDoubleTapListener(ComicViewerActivity.this);
                //pinch detector
                scaleGestureDetector = new ScaleGestureDetector(getApplicationContext(), ComicViewerActivity.this);
            }
        });

        //publication to load
        publicationVO = (PublicationVO) getIntent().getSerializableExtra("publication");

        //order the pages
        Collections.sort(publicationVO.getProjectVO().getPages(), new Comparator<PageVO>() {
            @Override
            public int compare(PageVO pageVO, PageVO pageVO2) {
                return pageVO.getPageIndex() - pageVO2.getPageIndex();
            }
        });

        //create the base scene objects
        scene = new Scene();
        hud = new HUD();

        playerHUDView = new PlayerHUDView(this, this);
        subtitleViewer = new SubtitleViewer(this);
        pageNavigatorViewer = new PageNavigatorViewer(this);
        loadingAnimationHud = new LoadingAnimationHud();
        autoPlaybackManager = new AutoPlaybackManager();

        scene.setBackground(new Background(0f, 0f, 0f));
        camera.setHUD(hud);
        loadingAnimationHud.init(this, hud);
        subtitleViewer.init(publicationVO, hud);
        playerHUDView.init(publicationVO, scene, hud);
        pageNavigatorViewer.init(publicationVO, scene, camera, hud);

        //enable touch listening to scene
        scene.setTouchAreaBindingOnActionDownEnabled(true);

        TimerHandler timerHandler = new TimerHandler(0.01f, true,
                new ITimerCallback() {
                    long elapsed = 0;
                    public void onTimePassed(TimerHandler pTimerHandler) {
                        elapsed = lasttime > 0 ? System.currentTimeMillis() - lasttime : 0;
                        update(elapsed);
                        lasttime = System.currentTimeMillis();
                    }
                });

        //register the timer handler to scene
        scene.registerUpdateHandler(timerHandler);

        //load user config
        loadUserConfiguration();

        startReadingSessionTime = System.currentTimeMillis();
        Map<String, String> extra = new HashMap<>();
        extra.put("PUBLICATION_ID", String.valueOf(publicationVO.getId()));
        extra.put("TIME", String.valueOf(startReadingSessionTime));
        AnalyticsFacade.sendScreenEvent(AnalyticsFacade.Category.UX, AnalyticsFacade.Action.UX_ENTER_SCREEN, AnalyticsFacade.Screen.READER, extra);

    }

    private void loadUserConfiguration() {

        //auto playback
        autoPlaybackSeconds = LogicFacade.getAutoPlayTimeSeconds(LogicFacade.getAutoPlayBackTime());
        autoPlaybackEnabled = LogicFacade.isAutoPlaybackEnabled();
        if(autoPlaybackEnabled) {
            if(autoPlaybackSeconds > 0) {
                autoPlaybackPaused = false;
                autoPlaybackManager.start();
                Toast.makeText(this, getResources().getString(R.string.autoplay_enabled), Toast.LENGTH_LONG).show();
            }
        }

        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();

        //get the rotation enabled flag
        boolean rotationEnabled = Boolean.valueOf(userDatabaseManager.getSettingsValue(UserDatabaseManager.TABLE_SETTINGS_COLUMN_ROTATION));
        if (rotationEnabled) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }

        //get the subtitles enabled flag
        subtitlesEnabled = Boolean.valueOf(userDatabaseManager.getSettingsValue(UserDatabaseManager.TABLE_SETTINGS_COLUMN_SUBTITLES));

        //get the user selected subtitles
        String userSelectedLanguageStr = userDatabaseManager.getSettingsValue(UserDatabaseManager.TABLE_SETTINGS_COLUMN_LANGUAGE);
        if(!"".equalsIgnoreCase(userSelectedLanguageStr.trim())) {
            userSelectedLanguage = Long.valueOf(userSelectedLanguageStr);
        }
    }

    public void update(long elapsed) {
        pageNavigatorViewer.update(elapsed);
    }

    @Override
    public void onBackPressed() {
        gotoMainActivity();
    }

    private void finishResources() {
        autoPlaybackManager.finish();
        pageNavigatorViewer.finalizeResources();
        playerHUDView.finalizeResources();
        camera.clearUpdateHandlers();
        scene.clearChildScene();
        scene.detachChildren();
        scene.reset();
        scene.detachSelf();
    }

    public void gotoMainActivity() {
        Intent loginActivityIntent = new Intent(this, MainMenuActivity.class);
        startActivity(loginActivityIntent);
        finish();
    }

    @Override
    public synchronized void onPauseGame() {
        super.onPauseGame();

        long finishReadingSessionTime = System.currentTimeMillis();
        Map<String, String> extra = new HashMap<>();
        extra.put("PUBLICATION_ID", String.valueOf(publicationVO.getId()));
        extra.put("TIME", String.valueOf(finishReadingSessionTime));
        extra.put("TIME_DIFF", String.valueOf(finishReadingSessionTime - startReadingSessionTime));
        AnalyticsFacade.sendScreenEvent(AnalyticsFacade.Category.UX, AnalyticsFacade.Action.UX_EXIT_SCREEN, AnalyticsFacade.Screen.READER, extra);

        finishResources();
    }

    @Override
    public boolean onTouch(View view, MotionEvent evt) {
        onTouchEvent = evt;
        scaleGestureDetector.onTouchEvent(evt);
        gestureDetector.onTouchEvent(evt);
        return true;
    }

    @Override
    public boolean onDown(MotionEvent evt) {
        pageNavigatorViewer.onDown(evt);
        return false;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (!freeNavigation && !playerHUDView.visible) {
            float flingX1 = e1.getX();
            float flingX2 = e2.getX();
            float flingDistance = Math.abs(flingX1 - flingX2);

            if (flingDistance > 50f) {
                if (e1.getX() > e2.getX()) {
                    if (Config.LECTURA_OCCIDENTAL) {
                        pageNavigatorViewer.moveNextPage();
                    } else {
                        pageNavigatorViewer.movePreviousPage();
                    }
                } else {
                    if (Config.LECTURA_OCCIDENTAL) {
                        pageNavigatorViewer.movePreviousPage();
                    } else {
                        pageNavigatorViewer.moveNextPage();
                    }
                }
            }
        }
        return true;
    }

    @Override
    public void onLongPress(MotionEvent evt) {
        playerHUDView.onLongPress(evt);
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        if (!playerHUDView.visible) {
            if (freeNavigation) {
                pageNavigatorViewer.onScroll(e1, e2, distanceX, distanceY);
            }
        } else {
            playerHUDView.onScroll(e1, e2, distanceX, distanceY);
        }
        return true;
    }

    public void showSubtitle(SubtitleVO subtitle) {
        if (subtitlesEnabled) {
            subtitleViewer.show(subtitle.getSubtitle());
        }
    }

    public void hideSubtitle() {
        subtitleViewer.hide();
    }

    @Override
    public void onShowPress(MotionEvent arg0) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent evt) {
        return true;
    }

    @Override
    public boolean onDoubleTap(MotionEvent evt) {
        if (!playerHUDView.visible) {
            pageNavigatorViewer.onDoubleTap(evt);
        }
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent evt) {
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent evt) {
        if (!playerHUDView.visible) {
            if (pageNavigatorViewer != null) {
                pageNavigatorViewer.onSingleTapUp(evt);
            }
        }
        else {
            playerHUDView.onSingleTapUp(evt);
        }
        return true;
    }

    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        if (!playerHUDView.visible) {
            pageNavigatorViewer.onScale(onTouchEvent);
        }
        return true;
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector detector) {
        freeNavigation = true;
        return true;
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector detector) {

    }

    @Override
    public void thumbTouched(ThumbEntity thumbEntity) {

        //hide the hud
        playerHUDView.setVisible(false);

        //indicamos al viewer que el thumb fue
        //tocado para que realice el cambio de
        //pagina correspondiente
        pageNavigatorViewer.thumbTouched(thumbEntity);

    }

    public Long getUserSelectedLanguage() {
        return userSelectedLanguage;
    }

    public void showLoadingAnimation(boolean flipped) {
        loadingAnimationHud.show(flipped && Config.LECTURA_OCCIDENTAL);
    }

    public void hideLoadingAnimation() {
        loadingAnimationHud.ocultar();
    }

    public void showDownloadCorruptedImageMessageDialog() {
        if(!showingDowloadImagesDialog) {
            showingDowloadImagesDialog = true;
            showAlertMessageDialog(R.string.publication_download, R.string.publication_download_retry_msg, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }, new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    returnToDownloadPublication();
                    showingDowloadImagesDialog = false;
                }
            });
        }
    }

    public void showAlertMessageDialog(final int title, final int message, final DialogInterface.OnClickListener listener, final DialogInterface.OnDismissListener dismissListener) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog alertDialog = new AlertDialog.Builder(ComicViewerActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle(ComicViewerActivity.this.getString(title));

                // Setting Dialog Message
                alertDialog.setMessage(ComicViewerActivity.this.getString(message));

                // Setting Icon to Dialog
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);

                // Setting OK Button
                alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "OK", listener);
                alertDialog.setOnDismissListener(dismissListener);

                // Showing Alert Message
                alertDialog.show();
            }
        });
    }

    @Override
    public void publicationDownloadProgress(DownloadPageStatus downloadPageStatus) {
        if(downloadPageStatus.page == -1) {
            LogicFacade.unregisterPublicationDownloadListener(this);
            init();
        }
    }

    private class AutoPlaybackManager implements Runnable {

        private Thread thread;
        private boolean finish;

        public void start() {
            finish = false;
            thread = new Thread(this);
            thread.start();
        }

        public void finish() {
            if(thread != null) {
                finish = true;
                thread.interrupt();
                thread = null;
            }
        }

        public void wakeup() {
            if(thread != null) {
                thread.interrupt();
            }
            else {
                start();
            }
        }

        @Override
        public void run() {
            while(!finish) {
                if(autoPlaybackEnabled && !autoPlaybackPaused) {
                    try {
                        Thread.sleep((long)(autoPlaybackSeconds * 1000f));
                        pageNavigatorViewer.moveNextRectangle();
                    }
                    catch (InterruptedException e)  {

                    }
                }
                else {
                    try { Thread.sleep(100); } catch (InterruptedException e) { }
                }
            }
        }
    }

    public void returnToDownloadPublication() {
        Intent mainMenuIntent = new Intent(ComicViewerActivity.this, MainMenuActivity.class);
        mainMenuIntent.putExtra("redownloadPublication", publicationVO);
        startActivity(mainMenuIntent);
    }

    public void setAutoPlaybackSeconds(float autoPlaybackSeconds) {
        this.autoPlaybackSeconds = autoPlaybackSeconds;
        autoPlaybackManager.wakeup();
    }

    public void setAutoPlaybackEnabled(boolean autoPlaybackEnabled) {
        LogicFacade.setAutoPlaybackEnabled(autoPlaybackEnabled);
        this.autoPlaybackPaused = false;
        this.autoPlaybackEnabled = autoPlaybackEnabled;
        autoPlaybackManager.wakeup();
    }

    public boolean isAutoPlaybackPaused() {
        return autoPlaybackPaused;
    }

    public void setAutoPlaybackPaused(boolean autoPlaybackPaused) {
        this.autoPlaybackPaused = autoPlaybackPaused;
        autoPlaybackManager.wakeup();
    }
}