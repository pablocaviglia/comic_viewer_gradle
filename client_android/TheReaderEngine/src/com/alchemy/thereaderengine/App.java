package com.alchemy.thereaderengine;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Base64;
import android.util.DisplayMetrics;

import com.alchemy.thereaderengine.database.SystemDatabaseManager;
import com.alchemy.thereaderengine.database.UserDatabaseManager;
import com.alchemy.thereaderengine.logic.AnalyticsFacade;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.facebook.android.crypto.keychain.SharedPrefsBackedKeyChain;
import com.facebook.crypto.Crypto;
import com.facebook.crypto.util.SystemNativeCryptoLibrary;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.MemoryCache;
import com.nostra13.universalimageloader.cache.memory.impl.LruMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FlushedInputStream;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.L;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import net.sqlcipher.database.SQLiteDatabase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class App extends MultiDexApplication {

    private static final Logger LOGGER = LoggerManager.getLogger();

    public static Context context;

    //constantes de categoria de
    //pantalla de dispositivo
    public static final String SCREEN_CATEGORY_SMALL = "S";
    public static final String SCREEN_CATEGORY_MEDIUM = "M";
    public static final String SCREEN_CATEGORY_LARGE = "L";

    public static int SCREEN_WIDTH;
    public static int SCREEN_HEIGHT;

    //cargador de imagenes
	public static ImageLoader secureImageLoader = ImageLoader.getInstance();
    public static ImageLoader normalImageLoader = ImageLoader.getInstance();
	
    //opciones para el cargador de imagenes
	public static DisplayImageOptions options;
	
	public static String GOOGLE_PLAY_APPLICATION_URL = "https://play.google.com/store/apps/details?id=";

    public static String ADMOB_BANNER_UNIT_ID = "ca-app-pub-9022829907836233/5377132509";
    public static String ADMOB_INTERSTITIAL_UNIT_ID = "ca-app-pub-9022829907836233/7763692500";

    //shared preferences keys
    public static String PUBLICATION_ID_SHARED_PREFERENCE_KEY = "publicationId";
    public static String NAVIGATION_PAGE_ID_SHARED_PREFERENCE_KEY = "navigationPageId";

    public static String DEFAULT_TEXT_SIZE;
	
	//persistence
    public static SystemDatabaseManager systemDatabaseManager;
	public UserDatabaseManager userDatabaseManager;
	
    //sdcard working folder
    public static String SDCARD_WORKING_FOLDER = Environment.getExternalStorageDirectory() + "/.apocalipta/";

    public static Crypto crypto;

    @Override
	public void onCreate() {
		super.onCreate();

        //calculate the screen dimensions
        calculateScreenDimensions();

        //completamos la url de esta
        //aplicacion en google play
        GOOGLE_PLAY_APPLICATION_URL += getApplicationContext().getPackageName();

        AnalyticsFacade.init(this);
//        printAppHash();

        this.context = this;

        //crypto database lib init
        SQLiteDatabase.loadLibs(this);

        //create working folder
        new File(SDCARD_WORKING_FOLDER).mkdirs();

		//init persistence
        systemDatabaseManager = new SystemDatabaseManager(getApplicationContext());

        //create the crypto object
        crypto = new Crypto(new SharedPrefsBackedKeyChain(this),new SystemNativeCryptoLibrary());

        //calculate the default text size
        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            DEFAULT_TEXT_SIZE = SCREEN_CATEGORY_LARGE;
        }
        else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_LARGE) {
            DEFAULT_TEXT_SIZE = SCREEN_CATEGORY_LARGE;
        }
        else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            DEFAULT_TEXT_SIZE = SCREEN_CATEGORY_MEDIUM;
        }
        else if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) ==
                Configuration.SCREENLAYOUT_SIZE_SMALL) {
            DEFAULT_TEXT_SIZE = SCREEN_CATEGORY_SMALL;
        }

        //create default settings
        systemDatabaseManager.createDefaultSettings();

        //init image loader
		initImageLoader(getApplicationContext());

	}

//    private void printAppHash() {
//        try {
//            PackageInfo info = getPackageManager().getPackageInfo("com.alchemy.thereaderengine", PackageManager.GET_SIGNATURES);
//            for (Signature signature : info.signatures) {
//                MessageDigest md = MessageDigest.getInstance("SHA");
//                md.update(signature.toByteArray());
//                LOGGER.i(Base64.encodeToString(md.digest(), Base64.DEFAULT));
//            }
//        }
//        catch (PackageManager.NameNotFoundException e) {
//            LOGGER.e(e.getMessage(), e);
//        }
//        catch (NoSuchAlgorithmException e) {
//            LOGGER.e(e.getMessage(), e);
//        }
//    }

    public static void saveSharedPreference(Context context, String key, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getSharedPreference(Context context, String key) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPreferences.getString(key, null);
    }

    public void initImageLoader(Context context) {

		// This configuration tuning is custom. You can tune every option, you may tune some of them,
		// or you can create default configuration by
		//  ImageLoaderConfiguration.createDefault(this);
		// method.
		ImageLoaderConfiguration secureConfig = new ImageLoaderConfiguration.Builder(context)
				.threadPriority(Thread.MAX_PRIORITY)
				.diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .memoryCache(new LruMemoryCache(5 * 1024 * 1024))
                .memoryCacheSize(5 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .imageDownloader(new SecureImageDownloader())
				.build();

		// Initialize ImageLoader with configuration
		secureImageLoader.init(secureConfig);

        ImageLoaderConfiguration normalConfig = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.MAX_PRIORITY)
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024)
                .diskCacheFileCount(100)
                .memoryCache(new LruMemoryCache(5 * 1024 * 1024))
                .memoryCacheSize(5 * 1024 * 1024)
                .memoryCacheSizePercentage(13) // default
                .tasksProcessingOrder(QueueProcessingType.FIFO)
                .imageDownloader(new AuthImageDownloader(context))
                .build();

        normalImageLoader.init(normalConfig);
		
		options = new DisplayImageOptions.Builder()
		.showImageOnLoading(R.drawable.tre_logo)
		.showImageForEmptyUri(R.drawable.tre_logo)
		.showImageOnFail(R.drawable.tre_logo)
		.cacheInMemory(true)
		.cacheOnDisk(true)
		.considerExifParams(true)
		.bitmapConfig(Bitmap.Config.RGB_565)
		.build();
		
		//disable imageloader logging
		L.writeLogs(false);
	}

    private void calculateScreenDimensions() {
        DisplayMetrics outMetrics = getResources().getDisplayMetrics();
        SCREEN_WIDTH = outMetrics.widthPixels;
        SCREEN_HEIGHT = outMetrics.heightPixels;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        calculateScreenDimensions();
    }

    public static Uri getDrawableUri(Context context, String name) {
        String uriStr = "drawable/"+ name;
        int imageResource = context.getResources().getIdentifier(uriStr, null, context.getPackageName());
        Uri uri = Uri.parse("android.resource://" + context.getPackageName() + "/" + imageResource);
        return uri;
    }

    class SecureImageDownloader extends BaseImageDownloader {

        public static final int DEFAULT_HTTP_CONNECT_TIMEOUT = 5 * 1000; // milliseconds
        public static final int DEFAULT_HTTP_READ_TIMEOUT = 20 * 1000; // milliseconds

        private int connectTimeout;
        private int readTimeout;

        public SecureImageDownloader() {
            this(DEFAULT_HTTP_CONNECT_TIMEOUT, DEFAULT_HTTP_READ_TIMEOUT);
        }

        public SecureImageDownloader(int connectTimeout, int readTimeout) {
            super(App.this.getApplicationContext());
            this.connectTimeout = connectTimeout;
            this.readTimeout = readTimeout;

            initSSLSocketFactory();
        }

        private void initSSLSocketFactory() {
            LogicFacade.getSSLContext().getSocketFactory();
        }

        @Override
        public InputStream getStreamFromNetwork(String imageUri, Object extra) throws IOException {

            URL url = new URL(imageUri);
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) url.openConnection();
            httpsURLConnection.setSSLSocketFactory(LogicFacade.getSSLContext().getSocketFactory());
            httpsURLConnection.setHostnameVerifier(LogicFacade.HOSTNAME_VERIFIER);
            httpsURLConnection.setConnectTimeout(connectTimeout);
            httpsURLConnection.setReadTimeout(readTimeout);

            return new BufferedInputStream(httpsURLConnection.getInputStream(), BUFFER_SIZE);
        }
    }

    class AuthImageDownloader extends BaseImageDownloader {

        public static final int DEFAULT_HTTP_CONNECT_TIMEOUT = 5 * 1000; // milliseconds
        public static final int DEFAULT_HTTP_READ_TIMEOUT = 20 * 1000; // milliseconds

        public AuthImageDownloader(Context context) {
            super(context, DEFAULT_HTTP_CONNECT_TIMEOUT, DEFAULT_HTTP_READ_TIMEOUT);
        }

        @Override
        protected InputStream getStreamFromNetwork(String imageUri, Object extra) throws IOException {

            URL url = null;
            try {
                url = new URL(imageUri);
            } catch (MalformedURLException e) {
                LOGGER.e(e.getMessage(), e);
            }
            HttpURLConnection http = null;

            if (Scheme.ofUri(imageUri) == Scheme.HTTPS) {
                trustAllHosts();
                HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
                https.setHostnameVerifier(DO_NOT_VERIFY);
                http = https;
                http.connect();
            } else {
                http = (HttpURLConnection) url.openConnection();
            }

            http.setConnectTimeout(connectTimeout);
            http.setReadTimeout(readTimeout);
            return new FlushedInputStream(new BufferedInputStream(
                    http.getInputStream()));
        }

        // always verify the host - dont check for certificate
        final HostnameVerifier DO_NOT_VERIFY = new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        /**
         * Trust every server - dont check for any certificate
         */
        private void trustAllHosts() {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] x509Certificates,
                        String s) throws java.security.cert.CertificateException {
                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] x509Certificates,
                        String s) throws java.security.cert.CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[] {};
                }
            } };

            // Install the all-trusting trust manager
            try {
                SSLContext sc = SSLContext.getInstance("TLS");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                HttpsURLConnection
                        .setDefaultSSLSocketFactory(sc.getSocketFactory());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}