package com.alchemy.thereaderengine;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.common.GooglePlayServicesUtil;

public class SplashScreenActivity extends Activity {

	static final int REQUEST_GOOGLE_PLAY_SERVICES = 0;

	// Splash screen timer
	private static int SPLASH_TIME_OUT = 500;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// set fullscreen
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// set the content view
		setContentView(R.layout.splash);
		
	}

	@Override
	protected void onResume() {
		super.onResume();
        gotoMainMenu();
	}

	private boolean verifyGooglePlayServices() {
		boolean avl = checkGooglePlayServicesAvailable(true);
		if (avl) {
			haveGooglePlayServices();
		}
		return avl;
	}

	/** Check that Google Play services APK is installed and up to date. */
	private boolean checkGooglePlayServicesAvailable(boolean showAlert) {
		final int connectionStatusCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
			if(showAlert) {
				showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);	
			}
			return false;
		}
		return true;
	}

	void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
		runOnUiThread(new Runnable() {
			public void run() {
				Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
						connectionStatusCode, SplashScreenActivity.this,
						REQUEST_GOOGLE_PLAY_SERVICES);

				dialog.setOnCancelListener(new OnCancelListener() {
					@Override
					public void onCancel(DialogInterface arg0) {
						verifyGooglePlayServices();
					}
				});

				dialog.show();
			}
		});
	}

	private void haveGooglePlayServices() {
        gotoMainMenu();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case REQUEST_GOOGLE_PLAY_SERVICES:
			if (resultCode == Activity.RESULT_OK) {
				haveGooglePlayServices();
			} else {
				checkGooglePlayServicesAvailable(true);
			}
			break;
		}
	}
	
	private void gotoMainMenu() {
		// go to the menu activity
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				// This method will be executed once the timer is over
				// Start your app main activity
				// Intent i = new Intent(SplashScreenActivity.this,
				// ComicViewerActivity.class);
				Intent i = new Intent(SplashScreenActivity.this, MainMenuActivity.class);
				startActivity(i);

				// close this activity
				finish();
			}
		}, SPLASH_TIME_OUT);
	}

    @Override
	protected void onDestroy() {
		super.onDestroy();
	}
}