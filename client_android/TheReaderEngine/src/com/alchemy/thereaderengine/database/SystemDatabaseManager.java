package com.alchemy.thereaderengine.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

/**
 * Created by pablo on 02/03/15.
 */
public class SystemDatabaseManager {

    private static final int DATABASE_VERSION = 1;
    public SQLiteDatabase db = null;

    /**
     * tables names
     */
    private static final String TABLE_NAME_SETTINGS = "SETTINGS";

    /**
     * columns names
     */
    public static final String TABLE_SETTINGS_LAST_LOGGED_USER = "LAST_LOGGED_USER_USERNAME";

    /**
     * statements for creating tables
     */
    private static final String CREATE_TABLE_SETTINGS =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_SETTINGS + " " +
                    "("+
                    TABLE_SETTINGS_LAST_LOGGED_USER + " VARCHAR(200)" +
                    ");";

    public SystemDatabaseManager(Context context) {
        SQLiteOpenHelper o = new PersistenceOpenHelper(context, "system" + context.getPackageName() + ".db");
        this.db = o.getWritableDatabase("0987654321abcdefghi");
    }

    private class PersistenceOpenHelper extends SQLiteOpenHelper {

        PersistenceOpenHelper(Context context, String databaseName) {
            super(context, databaseName, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_SETTINGS);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);
        }
    }

    public String getSettingsValue(String column) {

        String value = null;
        Cursor cursor = db.query(TABLE_NAME_SETTINGS,
                new String[] {column},
                null, null, null, null, null);

        if (cursor != null) {
            if(cursor.moveToFirst()) {
                value = cursor.getString(0);
            }
            cursor.close();
        }

        return value;
    }

    public void setSettingsValue(String column, String value) {
        ContentValues args = new ContentValues();
        args.put(column, value);
        db.update(TABLE_NAME_SETTINGS, args, null, null);
    }

    public void createDefaultSettings() {
        Cursor cursor = db.rawQuery(
                " select count(" + TABLE_SETTINGS_LAST_LOGGED_USER + ") " +
                        " from " + TABLE_NAME_SETTINGS, null);

        if (cursor != null) {
            cursor.moveToNext();
            int total = cursor.getInt(0);
            if(total == 0) {
                ContentValues args = new ContentValues();
                args.put(TABLE_SETTINGS_LAST_LOGGED_USER, "");
                db.insert(TABLE_NAME_SETTINGS, null, args);
            }
            cursor.close();
        }
    }
}