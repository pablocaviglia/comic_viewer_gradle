package com.alchemy.thereaderengine.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import net.sqlcipher.database.SQLiteDatabase;
import net.sqlcipher.database.SQLiteOpenHelper;

import com.alchemy.thereaderengine.enums.AutoPlayTime;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.apocalipta.comic.vo.v1.PageVO;
import com.alchemy.thereaderengine.App;
import com.alchemy.thereaderengine.enums.PublicationDownloadState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserDatabaseManager {

	private static final int DATABASE_VERSION = 180;
	public SQLiteDatabase db = null;
    private String username;

    private static Map<String, UserDatabaseManager> databases = new HashMap<String, UserDatabaseManager>();
	
    /**
     * tables names
     */
	private static final String TABLE_NAME_SETTINGS = "SETTINGS";
    private static final String TABLE_NAME_PUBLICATION = "PUBLICATION";
    private static final String TABLE_NAME_PUBLICATION_PAGES = "PUBLICATION_PAGES";

    /**
     * columns names
     */
    public static final String TABLE_SETTINGS_COLUMN_ROTATION = "ROTATION";
    public static final String TABLE_SETTINGS_COLUMN_ASSISTED_READING = "ASSISTED_READING";
    public static final String TABLE_SETTINGS_COLUMN_SUBTITLES = "SUBTITLES";
    public static final String TABLE_SETTINGS_COLUMN_FRAME_COLOR = "FRAME_COLOR";
    public static final String TABLE_SETTINGS_COLUMN_TEXT_SIZE = "TEXT_SIZE";
    public static final String TABLE_SETTINGS_COLUMN_LANGUAGE = "LANGUAGE";
    public static final String TABLE_SETTINGS_COLUMN_AUTO_PLAYBACK = "AUTO_PLAYBACK";
    public static final String TABLE_SETTINGS_COLUMN_AUTO_PLAYBACK_TIME = "AUTO_PLAYBACK_TIME";

    public static final String TABLE_PUBLICATION_COLUMN_PUBLICATION_ID = "PUBLICATION_ID";
    public static final String TABLE_PUBLICATION_COLUMN_DOWNLOAD_STATE = "DOWNLOAD_STATE";
    public static final String TABLE_PUBLICATION_COLUMN_POSTPONE_DOWNLOAD_TIMESTAMP = "POSTPONE_DOWNLOAD_TIMESTAMP";

    public static final String TABLE_PUBLICATION_PAGES_COLUMN_PUBLICATION_ID = "PUBLICATION_ID";
    public static final String TABLE_PUBLICATION_PAGES_COLUMN_PAGE_ID = "PAGE_ID";
    public static final String TABLE_PUBLICATION_PAGES_COLUMN_PAGE_VERSION = "PAGE_VERSION";

    private static final String CREATE_TABLE_SETTINGS =
                    "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_SETTINGS + " " +
                    "("+
                            TABLE_SETTINGS_COLUMN_ROTATION + " VARCHAR(10), " +
                            TABLE_SETTINGS_COLUMN_ASSISTED_READING + " VARCHAR(10)," +
                            TABLE_SETTINGS_COLUMN_SUBTITLES + " VARCHAR(10)," +
                            TABLE_SETTINGS_COLUMN_FRAME_COLOR + " VARCHAR(10)," +
                            TABLE_SETTINGS_COLUMN_TEXT_SIZE + " VARCHAR(10)," +
                            TABLE_SETTINGS_COLUMN_LANGUAGE + " VARCHAR(10)," +
                            TABLE_SETTINGS_COLUMN_AUTO_PLAYBACK + " VARCHAR(10)," +
                            TABLE_SETTINGS_COLUMN_AUTO_PLAYBACK_TIME + " VARCHAR(10)" +
                    ");";

    private static final String CREATE_TABLE_PUBLICATION =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_PUBLICATION + " " +
                    "(" +
                    TABLE_PUBLICATION_COLUMN_PUBLICATION_ID + " LONG NOT NULL, " +
                    TABLE_PUBLICATION_COLUMN_DOWNLOAD_STATE + " VARCHAR(40)," +
                    TABLE_PUBLICATION_COLUMN_POSTPONE_DOWNLOAD_TIMESTAMP + " LONG," +
                    "PRIMARY KEY (" + TABLE_PUBLICATION_COLUMN_PUBLICATION_ID + ")" +
                    ");";


    private static final String CREATE_TABLE_PUBLICATION_PAGES =
                    "CREATE TABLE IF NOT EXISTS " + TABLE_NAME_PUBLICATION_PAGES + " " +
                    "(" +
                            TABLE_PUBLICATION_PAGES_COLUMN_PUBLICATION_ID + " LONG NOT NULL, " +
                            TABLE_PUBLICATION_PAGES_COLUMN_PAGE_ID + " LONG NOT NULL," +
                            TABLE_PUBLICATION_PAGES_COLUMN_PAGE_VERSION + " INTEGER NOT NULL, " +
                        "PRIMARY KEY (" + TABLE_PUBLICATION_PAGES_COLUMN_PUBLICATION_ID + ", " + TABLE_PUBLICATION_PAGES_COLUMN_PAGE_ID + ")" +
                    ");";

    private static final String CREATE_INDEX_PUBLICATION_PAGES_PUBLICATION_ID =
            "CREATE INDEX IF NOT EXISTS PUBLICATION_PAGES_INDEX_PUBLICATION_ID_INDEX " +
                    "ON " + TABLE_NAME_PUBLICATION_PAGES + " ( " + TABLE_PUBLICATION_PAGES_COLUMN_PUBLICATION_ID + ");";

    private static final String CREATE_INDEX_PUBLICATION_PAGES_PAGE_ID =
            "CREATE INDEX IF NOT EXISTS PUBLICATION_PAGES_INDEX_PAGE_ID_INDEX " +
                    "ON " + TABLE_NAME_PUBLICATION_PAGES + " ( " + TABLE_PUBLICATION_PAGES_COLUMN_PAGE_ID + ");";

    public static UserDatabaseManager getDatabase() {
        String username = LogicFacade.getLastLoggedUser().getUsername();
        UserDatabaseManager userDatabaseManager = databases.get(username);
        if(null == userDatabaseManager) {
            userDatabaseManager = new UserDatabaseManager(App.context, username);
            //close all other databases
            for(String databaseUsername : databases.keySet()) {
                UserDatabaseManager userDatabaseManagerRemove = databases.get(databaseUsername);
                if(userDatabaseManagerRemove.db.isOpen()) {
                    userDatabaseManagerRemove.close();
                }
            }
            databases.clear();

            //insert the new database
            databases.put(username, userDatabaseManager);
        }

        return userDatabaseManager;
    }

    private UserDatabaseManager(Context context, String username) {
        this.username = username;
		SQLiteOpenHelper o = new PersistenceOpenHelper(context, "u_" + LogicFacade.md5(username) + ".db");
		db = o.getWritableDatabase(LogicFacade.md5("abcdefghi1234567890"));
        createDefaultSettings();
	}

    public void close() {
        db.close();
        databases.remove(username);
    }
    
	private class PersistenceOpenHelper extends net.sqlcipher.database.SQLiteOpenHelper {
		
		PersistenceOpenHelper(Context context, String databaseName) {
			super(context, databaseName, null, DATABASE_VERSION);
		}
		
		@Override
		public void onCreate(SQLiteDatabase db) {

            //tables creation
	        db.execSQL(CREATE_TABLE_SETTINGS);
            db.execSQL(CREATE_TABLE_PUBLICATION);
            db.execSQL(CREATE_TABLE_PUBLICATION_PAGES);

            //indexes creation
            db.execSQL(CREATE_INDEX_PUBLICATION_PAGES_PUBLICATION_ID);
            db.execSQL(CREATE_INDEX_PUBLICATION_PAGES_PAGE_ID);

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			onCreate(db);
		}
	}

	public String getSettingsValue(String column) {

		String value = null;
	    Cursor cursor = db.query(TABLE_NAME_SETTINGS,
				new String[] {column},
				null, null, null, null, null);

	    if (cursor != null) {
            if(cursor.moveToFirst()) {
                value = cursor.getString(0);
            }
            cursor.close();
	    }

		return value;
	}
	
	public void setSettingsValue(String column, String value) {
		ContentValues args = new ContentValues();
        args.put(column, value);
	    db.update(TABLE_NAME_SETTINGS, args, null, null);
	}

    public String getPublicationDownloadState(long publicationId) {

        String state = null;
        Cursor cursor = db.query(TABLE_NAME_PUBLICATION,
                new String[] {TABLE_PUBLICATION_COLUMN_DOWNLOAD_STATE},
                TABLE_PUBLICATION_COLUMN_PUBLICATION_ID + "=?",
                new String[] { String.valueOf(publicationId) }, null, null, null, null);

        if (cursor != null) {
            if(cursor.moveToNext()) {
                state = cursor.getString(0);
            }
            cursor.close();
        }

        return state;
    }

    public void createPublication(long publicationId) {
        if(!existsPublication(publicationId)) {
            ContentValues args = new ContentValues();
            args.put(TABLE_PUBLICATION_COLUMN_PUBLICATION_ID, publicationId);
            args.put(TABLE_PUBLICATION_COLUMN_DOWNLOAD_STATE, PublicationDownloadState.NOT_DOWNLOADED.toString());
            db.insert(TABLE_NAME_PUBLICATION, null, args);
        }
    }

    public boolean existsPublication(long publicationId) {
        boolean exists = false;
        Cursor cursor = db.rawQuery(
                " select count(" + TABLE_PUBLICATION_COLUMN_PUBLICATION_ID + ") " +
                        " from " + TABLE_NAME_PUBLICATION +
                        " where " + TABLE_PUBLICATION_COLUMN_PUBLICATION_ID + "=" + publicationId,
                null);

        if (cursor != null) {
            cursor.moveToNext();
            exists = cursor.getInt(0) > 0;
            cursor.close();
        }
        return exists;
    }

    public void setPublicationDownloadState(long publicationId, String state) {
        ContentValues args = new ContentValues();
        args.put(TABLE_PUBLICATION_COLUMN_DOWNLOAD_STATE, state);
        db.update(TABLE_NAME_PUBLICATION, args, TABLE_PUBLICATION_COLUMN_PUBLICATION_ID + "=?", new String[]{String.valueOf(publicationId)});
    }

    public long getPublicationPostponeDownload(long publicationId) {

        long postponedDownload = 0;
        Cursor cursor = db.query(TABLE_NAME_PUBLICATION,
                new String[] {TABLE_PUBLICATION_COLUMN_POSTPONE_DOWNLOAD_TIMESTAMP},
                TABLE_PUBLICATION_COLUMN_PUBLICATION_ID + "=?",
                new String[] { String.valueOf(publicationId) }, null, null, null, null);

        if (cursor != null) {
            if(cursor.moveToNext()) {
                postponedDownload = cursor.getLong(0);
            }
            cursor.close();
        }

        return postponedDownload;
    }

    public void setPublicationPostponeDownload(long publicationId, long postponeDate) {
        ContentValues args = new ContentValues();
        args.put(TABLE_PUBLICATION_COLUMN_POSTPONE_DOWNLOAD_TIMESTAMP, postponeDate);
        db.update(TABLE_NAME_PUBLICATION, args, TABLE_PUBLICATION_COLUMN_PUBLICATION_ID + "=?", new String[]{String.valueOf(publicationId)});
    }

    public void resetDownloadingPublications() {
        if(db.isOpen()) {
            ContentValues args = new ContentValues();
            args.put(TABLE_PUBLICATION_COLUMN_DOWNLOAD_STATE, PublicationDownloadState.NOT_DOWNLOADED.toString());
            db.update(TABLE_NAME_PUBLICATION, args, null, null);
        }
    }

    public void createDefaultSettings() {
        Cursor cursor = db.rawQuery(
                " select count(" + TABLE_SETTINGS_COLUMN_ROTATION + ") " +
                        " from " + TABLE_NAME_SETTINGS, null);

        if (cursor != null) {
            cursor.moveToNext();
            int total = cursor.getInt(0);
            if(total == 0) {
                ContentValues args = new ContentValues();

                args.put(TABLE_SETTINGS_COLUMN_ROTATION, "false");
                args.put(TABLE_SETTINGS_COLUMN_ASSISTED_READING, "true");
                args.put(TABLE_SETTINGS_COLUMN_SUBTITLES, "false");
                args.put(TABLE_SETTINGS_COLUMN_FRAME_COLOR, "false");
                args.put(TABLE_SETTINGS_COLUMN_TEXT_SIZE, App.DEFAULT_TEXT_SIZE);
                args.put(TABLE_SETTINGS_COLUMN_LANGUAGE, "");
                args.put(TABLE_SETTINGS_COLUMN_AUTO_PLAYBACK, "false");
                args.put(TABLE_SETTINGS_COLUMN_AUTO_PLAYBACK_TIME, AutoPlayTime.X2.name());

                db.insert(TABLE_NAME_SETTINGS, null, args);
            }

            cursor.close();
        }
    }

    public void savePublicationPage(long publicationId, long pageId, int pageVersion) {

        ContentValues args = new ContentValues();
        args.put(TABLE_PUBLICATION_PAGES_COLUMN_PUBLICATION_ID, publicationId);
        args.put(TABLE_PUBLICATION_PAGES_COLUMN_PAGE_ID, pageId);
        args.put(TABLE_PUBLICATION_PAGES_COLUMN_PAGE_VERSION, pageVersion);

        db.replace(TABLE_NAME_PUBLICATION_PAGES, null, args);
    }

    public List<PageVO> getPublicationPages(long publicationId) {
        List<PageVO> pages = new ArrayList<>();

        Cursor cursor = db.query(TABLE_NAME_PUBLICATION_PAGES,
                new String[] {TABLE_PUBLICATION_PAGES_COLUMN_PAGE_ID, TABLE_PUBLICATION_PAGES_COLUMN_PAGE_VERSION},
                TABLE_PUBLICATION_PAGES_COLUMN_PUBLICATION_ID + "=?",
                new String[] { String.valueOf(publicationId) }, null, null, null, null);

        if (cursor != null) {
            while(cursor.moveToNext()) {
                long pageId = cursor.getLong(0);
                int pageVersion = cursor.getInt(1);
                PageVO pageVO = new PageVO();
                pageVO.setId(pageId);
                pageVO.setVersion(pageVersion);
                pages.add(pageVO);
            }
            cursor.close();
        }

        return pages;
    }

    public List<Long> searchMyLibraryPublications(String keywords, int from) {

        List<Long> publicationIds = new ArrayList<>();
        Cursor cursor = db.rawQuery(
                " select " + TABLE_PUBLICATION_COLUMN_PUBLICATION_ID +
                " from " + TABLE_NAME_PUBLICATION +
                " LIMIT 10 " +
                " OFFSET " + from,
                null);

        if (cursor != null) {
            while(cursor.moveToNext()) {
                long publicationId = cursor.getLong(0);
                publicationIds.add(publicationId);
            }
            cursor.close();
        }
        return publicationIds;
    }

    public int countMyLibraryPublications(String keywords) {
        int total = 0;
        Cursor cursor = db.rawQuery(
                " select count(" + TABLE_PUBLICATION_COLUMN_PUBLICATION_ID + ") " +
                " from " + TABLE_NAME_PUBLICATION,
                null);

        if (cursor != null) {
            cursor.moveToNext();
            total = cursor.getInt(0);
            cursor.close();
        }
        return total;
    }
}