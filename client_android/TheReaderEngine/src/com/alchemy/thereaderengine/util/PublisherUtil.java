package com.alchemy.thereaderengine.util;

import com.apocalipta.comic.vo.v1.LanguageVO;
import com.apocalipta.comic.vo.v1.PublicationVO;
import com.apocalipta.comic.vo.v1.PublisherVO;

import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by pablo on 01/05/15.
 */
public class PublisherUtil {

    public static String getLocalizedText(PublisherVO publisherVO, Map<Long, String> texts) {

        String text = "";

        if(texts != null) {
            boolean defaultLanguage = false;
            String userLanguageCode = Locale.getDefault().getLanguage();
            LanguageVO selectedLanguage = null;

            //get available languages to match
            //the user device language
            List<LanguageVO> availableLanguages = publisherVO.getLanguages();
            for(LanguageVO availableLanguage : availableLanguages) {
                if(userLanguageCode.trim().equalsIgnoreCase(availableLanguage.getCode())) {
                    selectedLanguage = availableLanguage;
                    break;
                }
            }

            //if the user language is not
            //available get the publisher
            //default language
            if(selectedLanguage == null) {
                defaultLanguage = true;
                selectedLanguage = publisherVO.getDefaultLanguage();
            }

            if(defaultLanguage) {
                text = texts.get(selectedLanguage.getId());
            }
            else {
                //try to get text in the user
                //language, if its null get the
                //default language text
                String tmpText = texts.get(selectedLanguage.getId());
                if(null == tmpText || "".equalsIgnoreCase(tmpText.trim())) {
                    text = texts.get(publisherVO.getDefaultLanguage().getId());
                }
                else {
                    text = tmpText;
                }
            }
        }

        return text;
    }
}
