package com.alchemy.thereaderengine.util;

import com.apocalipta.comic.vo.v1.LanguageVO;
import com.apocalipta.comic.vo.v1.PublicationVO;
import com.apocalipta.comic.vo.v1.TitleVO;

import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by pablo on 19/04/15.
 */
public class TitleUtil {

    public static String getLocalizedText(TitleVO titleVO, Map<Long, String> texts) {

        String text = "";

        if(texts != null) {
            boolean defaultLanguage = false;
            String userLanguageCode = Locale.getDefault().getLanguage();
            LanguageVO selectedLanguage = null;

            //get available languages to match
            //the user device language
            List<LanguageVO> availableLanguages = titleVO.getLanguages();
            for(LanguageVO availableLanguage : availableLanguages) {
                if(userLanguageCode.trim().equalsIgnoreCase(availableLanguage.getCode())) {
                    selectedLanguage = availableLanguage;
                    break;
                }
            }

            //if the user language is not
            //available get the publication
            //default language
            if(selectedLanguage == null) {
                defaultLanguage = true;
                selectedLanguage = titleVO.getDefaultLanguage();
            }

            if(defaultLanguage) {
                text = texts.get(selectedLanguage.getId());
            }
            else {
                //try to get text in the user
                //language, if its null get the
                //default language text
                String tmpText = texts.get(selectedLanguage.getId());
                if(null == tmpText || "".equalsIgnoreCase(tmpText.trim())) {
                    text = texts.get(titleVO.getDefaultLanguage().getId());
                }
                else {
                    text = tmpText;
                }
            }
        }

        return text;
    }
}