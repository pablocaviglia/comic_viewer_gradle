package com.alchemy.thereaderengine;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alchemy.thereaderengine.database.UserDatabaseManager;
import com.alchemy.thereaderengine.downloader.PageDownloadService;
import com.alchemy.thereaderengine.enums.PublicationDescriptorDownloadEnum;
import com.alchemy.thereaderengine.enums.PublicationDownloadState;
import com.alchemy.thereaderengine.listener.LogicFacadeListener;
import com.alchemy.thereaderengine.listener.PublicationDownloadListener;
import com.alchemy.thereaderengine.logic.AnalyticsFacade;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.alchemy.thereaderengine.model.DownloadPageStatus;
import com.alchemy.thereaderengine.model.NavigationHistory;
import com.alchemy.thereaderengine.ui.adapter.MainMenuPagerAdapterHorizontal;
import com.alchemy.thereaderengine.ui.adapter.MainMenuPagerAdapterVertical;
import com.alchemy.thereaderengine.ui.adapter.PublicationMenuPagerAdapterHorizontal;
import com.alchemy.thereaderengine.ui.adapter.PublisherMenuPagerAdapterHorizontal;
import com.alchemy.thereaderengine.ui.adapter.SearchPagerAdapterHorizontal;
import com.alchemy.thereaderengine.ui.adapter.TitleMenuPagerAdapterHorizontal;
import com.alchemy.thereaderengine.ui.pager.NonSwipeableVerticalViewPager;
import com.alchemy.thereaderengine.ui.popup.DownloadPublicationPopupWindow;
import com.alchemy.thereaderengine.ui.util.ParallaxViewPager;
import com.alchemy.thereaderengine.ui.util.UILUtil;
import com.alchemy.thereaderengine.ui.view.ConfigView;
import com.alchemy.thereaderengine.ui.view.PublicationInfoView;
import com.alchemy.thereaderengine.ui.view.PublicationListGenericView;
import com.alchemy.thereaderengine.ui.view.PublicationReadView;
import com.alchemy.thereaderengine.ui.view.PublisherInfoView;
import com.alchemy.thereaderengine.ui.view.PublisherListView;
import com.alchemy.thereaderengine.ui.view.SearchCustomView;
import com.alchemy.thereaderengine.ui.view.TitleInfoView;
import com.alchemy.thereaderengine.ui.view.component.CustomViewPager;
import com.apocalipta.comic.vo.v1.PageVO;
import com.apocalipta.comic.vo.v1.PublicationVO;
import com.apocalipta.comic.vo.v1.PublisherVO;
import com.apocalipta.comic.vo.v1.TitleVO;
import com.apocalipta.comic.vo.v1.ViewerUserVO;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.makeramen.roundedimageview.RoundedImageView;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * Created by pablo on 09/12/14.
 */
public class MainMenuActivity extends ActionBarActivity implements PublicationListGenericView.PublicationListLoaderListener, LogicFacadeListener, PublicationDownloadListener, SearchView.OnQueryTextListener {

    private static final Logger LOGGER = LoggerManager.getLogger();

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private LinearLayout drawerLinearLayout;

    //prevent all views from receiving touch events
    private boolean stopReceiveTouchEvents;

    public ViewerUserVO loggedUser;

    //flag que indica si debemos de
    //salir de la aplicacion cuando
    //se presione el boton de back
    private boolean exitOnBack;

    //admob ads references
    private InterstitialAd interstitial;

    private final static byte ACCION_CIERRE_INTERSTITIAL_READ_COMIC         = 1;
    private final static byte ACCION_CIERRE_INTERSTITIAL_EXIT_APPLICATION   = 2;
    private byte accionCierreInterstitial;

    //contexto actual de la aplicacion
    private static final byte CONTEXT_MAIN_MENU     = 1;
    private static final byte CONTEXT_PUBLICATION   = 2;
    private static final byte CONTEXT_SETTINGS      = 3;
    private static final byte CONTEXT_PUBLISHER     = 4;
    private static final byte CONTEXT_SEARCH        = 5;
    private static final byte CONTEXT_TITLE         = 6;
    private static final byte CONTEXT_TUTORIAL      = 7;
    private byte currentContext;

    //main menu tabs
    public static final byte TAB_MAIN_MENU_ALL             = 0;
    public static final byte TAB_MAIN_MENU_MY_LIBRARY      = 1;
    public static final byte TAB_MAIN_MENU_PUBLISHERS      = 2;

    //publication tabs
    public static final byte TAB_PUBLICATION_READ          = 0;
    public static final byte TAB_PUBLICATION_INFO          = 1;
    public static final byte TAB_PUBLICATION_COMMUNITY     = 2;

    //publisher tabs
    public static final byte TAB_PUBLISHER_INFO            = 0;
    public static final byte TAB_PUBLISHER_PUBLICATIONS    = 1;

    //search tabs
    public static final byte TAB_SEARCH_CUSTOM             = 0;
    public static final byte TAB_SEARCH_BY_PUBLISHER       = 1;
    public static final byte TAB_SEARCH_BY_TITLE           = 2;

    //publisher tabs
    public static final byte TAB_TITLE_INFO                = 0;
    public static final byte TAB_TITLE_PUBLICATIONS        = 1;

    public byte currentTabMainMenu;
    public byte currentTabPublication;
    public byte currentTabSearch;

    public static final boolean PAUSE_ONSCROLL = false;
    public static final boolean PAUSE_ONFLING = true;

    //main vertical pager (switch between the horizontal pagers)
    private MainMenuPagerAdapterVertical mainMenuPagerAdapterVertical;

    //horizontal pagers
    private MainMenuPagerAdapterHorizontal mainMenuPagerAdapterHorizontal;
    private PublicationMenuPagerAdapterHorizontal publicationMenuPagerAdapterHorizontal;
    private PublisherMenuPagerAdapterHorizontal publisherMenuPagerAdapterHorizontal;
    private SearchPagerAdapterHorizontal searchPagerAdapterHorizontal;
    private TitleMenuPagerAdapterHorizontal titleMenuPagerAdapterHorizontal;

    private DownloadPublicationPopupWindow downloadPublicationPopupWindow;

    //font
    public Typeface robotoTypeface;

    //reference to publication
    //being viewed
    public PublicationVO publicationVO;

    //download service bind
    private PageDownloadService pageDownloadService;
    private boolean mBound = false;

    private boolean redownload;

    private boolean firstSearchInSession;

    //id of the publisher being viewed
    private long publisherId;

    //id of the title being viewed
    private long titleId;

    private Stack<NavigationHistory> navigationHistoryStack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        //important to call after 'remove title'
        super.onCreate(savedInstanceState);

        navigationHistoryStack = new Stack<>();

        //seteamos la vista de contenido
        setContentView(R.layout.mainmenu_activity);

        //init ui components
        initNavigationDrawer();
        initMainMenu();
    }

    private void initNavigationDrawer() {

        // Inflate the "decor.xml"
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        DrawerLayout drawer = (DrawerLayout) inflater.inflate(R.layout.navigation_drawer_layout, null); // "null" is important.

        // HACK: "steal" the first child of decor view
        ViewGroup decor = (ViewGroup) getWindow().getDecorView();
        View child = decor.getChildAt(0);
        decor.removeView(child);
        FrameLayout container = (FrameLayout) drawer.findViewById(R.id.container); // This is the container we defined just now.
        container.addView(child);

        // Make the drawer replace the first child
        decor.addView(drawer);

        drawerLinearLayout = (LinearLayout) findViewById(R.id.navigation_drawer_view);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        //move the navdrawer below the actionbar
        final TypedArray styledAttributes = getTheme().obtainStyledAttributes(new int[] { android.R.attr.actionBarSize });
        int mActionBarSize = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams)drawerLinearLayout.getLayoutParams();
        params.setMargins(0, mActionBarSize, 0, 0);
        drawerLinearLayout.setLayoutParams(params);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.applicationFullName, // nav drawer open - description for accessibility
                R.string.applicationFullName // nav drawer close - description for accessibility
        ) {
            @Override
            public void onDrawerClosed(View view) {
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        //enable action bar app icon and behaving it as toggle button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);


        //all
        findViewById(R.id.navigation_all_row).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawers();
                showMainMenu(TAB_MAIN_MENU_ALL, true);
            }
        });

        //my library
        findViewById(R.id.navigation_my_library_row).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawers();
                showMainMenu(TAB_MAIN_MENU_MY_LIBRARY, true);
            }
        });

        //publishers
        findViewById(R.id.navigation_publishers_row).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawers();
                showMainMenu(TAB_MAIN_MENU_PUBLISHERS, true);
            }
        });

        //settings
        findViewById(R.id.navigation_settings_row).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawers();
                showSettings(true);
            }
        });

        //tutorial
        findViewById(R.id.navigation_tutorial_row).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawers();
                showTutorial();
            }
        });

        //search
        findViewById(R.id.navigation_search_row).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawers();
                showSearch(true);
            }
        });

        //close session
        findViewById(R.id.navigation_closesession_row).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerLayout.closeDrawers();
                signout();
            }
        });
    }

    private void initMainMenu() {

        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setCustomView(R.layout.actionbar_custom_view_home);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);

        //font
        robotoTypeface = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/roboto.ttf");

        //flag
        firstSearchInSession = true;

        //create the vertical adapter
        mainMenuPagerAdapterVertical = new MainMenuPagerAdapterVertical(this);

        //publication adapter
        publicationMenuPagerAdapterHorizontal = new PublicationMenuPagerAdapterHorizontal(this);

        //set the adapter to the vertical pager
        NonSwipeableVerticalViewPager pagerVertical = (NonSwipeableVerticalViewPager) findViewById(R.id.mainmenu_pager_vertical);
        pagerVertical.setAdapter(mainMenuPagerAdapterVertical);

    }

    @Override
    protected void onStart() {
        super.onStart();
        //bind to download service
        Intent intent = new Intent(getApplicationContext(), PageDownloadService.class);
        bindService(intent, downloadServiceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            unbindService(downloadServiceConnection);
            mBound = false;
        }
    }

    private ServiceConnection downloadServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // We've bound to download service, cast the IBinder and get service instance
            PageDownloadService.PageDownloadServiceBinder binder = (PageDownloadService.PageDownloadServiceBinder) service;
            pageDownloadService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    /***
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
//        // if nav drawer is opened, hide the action items
//        boolean drawerOpen = mDrawerLayout.isDrawerOpen(drawerLinearLayout);
//        menu.findItem(R.id.overflow).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Pass the event to ActionBarDrawerToggle
        // If it returns true, then it has handled
        // the nav drawer indicator touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.search:
                showSearch(true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showMainMenu(byte selectedTab, boolean addHistory) {

        AnalyticsFacade.sendScreenEvent(AnalyticsFacade.Category.UX, AnalyticsFacade.Action.UX_ENTER_SCREEN, AnalyticsFacade.Screen.MAIN_MENU, null);

        mainMenuPagerAdapterHorizontal = new MainMenuPagerAdapterHorizontal(this, this);
        ActionBar actionBar = getSupportActionBar();

        if(currentContext != CONTEXT_MAIN_MENU) {

            if(addHistory) {
                navigationHistoryStack.push(new NavigationHistory(CONTEXT_MAIN_MENU, null));
            }

            currentContext = CONTEXT_MAIN_MENU;

            //swipe to publication view
            NonSwipeableVerticalViewPager pagerVertical = (NonSwipeableVerticalViewPager) findViewById(R.id.mainmenu_pager_vertical);
            pagerVertical.setCurrentItem(MainMenuPagerAdapterVertical.VIEW_MAINMENU, true);

            //get the horizontal pager
            CustomViewPager mainMenuPagerHorizontal = (CustomViewPager) mainMenuPagerAdapterVertical.getView(MainMenuPagerAdapterVertical.VIEW_MAINMENU);

            //capture tab button clicks
            MainMenuTabListener mainMenuTabListener = new MainMenuTabListener(mainMenuPagerHorizontal.getPager());

            //remove previous tabs
            actionBar.removeAllTabs();

            //create 'all' tab
            ActionBar.Tab allTab = actionBar.newTab().setText(getResources().getString(R.string.all)).setTabListener(mainMenuTabListener);
            allTab.setTag(TAB_MAIN_MENU_ALL);
            actionBar.addTab(allTab);

            //create 'my downloads' tab
            ActionBar.Tab myDownloadsTab = actionBar.newTab().setText(getResources().getString(R.string.my_library)).setTabListener(mainMenuTabListener);
            myDownloadsTab.setTag(TAB_MAIN_MENU_MY_LIBRARY);
            actionBar.addTab(myDownloadsTab);

            //create 'premieres' tab
            ActionBar.Tab publishersTab = actionBar.newTab().setText(getResources().getString(R.string.publishers)).setTabListener(mainMenuTabListener);
            publishersTab.setTag(TAB_MAIN_MENU_PUBLISHERS);
            actionBar.addTab(publishersTab);

            //show action bar
            actionBar.show();

            //indicamos el listener de cambio de pagina
            mainMenuPagerHorizontal.getPager().setOnPageChangeListener(new SimpleOnPageChangeListener(actionBar));

            // Set the View Pager Adapter into ViewPager
            mainMenuPagerHorizontal.getPager().setAdapter(mainMenuPagerAdapterHorizontal);

            //refresh main menu views
            mainMenuPagerAdapterHorizontal.refreshViews();

        }

        actionBar.setSelectedNavigationItem(selectedTab);

    }

    public void showPublication(final long publicationId, final boolean redownload, final boolean addHistory) {

        Map<String, String> extra = new HashMap<>();
        extra.put("PUBLICATION_ID", String.valueOf(publicationId));
        AnalyticsFacade.sendScreenEvent(AnalyticsFacade.Category.UX, AnalyticsFacade.Action.UX_ENTER_SCREEN, AnalyticsFacade.Screen.PUBLICATION, extra);

        new Thread(new Runnable() {
            @Override
            public void run() {

                //new context
                currentContext = CONTEXT_PUBLICATION;

                if(addHistory) {
                    navigationHistoryStack.push(new NavigationHistory(CONTEXT_PUBLICATION, new Object[]{publicationId, redownload}));
                }

                //wait until download service is bound
                while(!MainMenuActivity.this.mBound) {
                    try { Thread.sleep(100); } catch (InterruptedException e) { e.printStackTrace(); }
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //swipe to publication view
                        NonSwipeableVerticalViewPager pagerVertical = (NonSwipeableVerticalViewPager) findViewById(R.id.mainmenu_pager_vertical);
                        pagerVertical.setCurrentItem(MainMenuPagerAdapterVertical.VIEW_PUBLICATION, true);

                        //hide the action bar
                        ActionBar actionBar = getSupportActionBar();
                        //navigation tabs
                        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
                        //remove previous tabs
                        actionBar.removeAllTabs();

                        //get the horizontal pager
                        CustomViewPager publicationPagerHorizontal = (CustomViewPager) mainMenuPagerAdapterVertical.getView(MainMenuPagerAdapterVertical.VIEW_PUBLICATION);

                        //indicamos el listener de cambio de pagina
                        publicationPagerHorizontal.getPager().setOnPageChangeListener(new SimpleOnPageChangeListener(actionBar));

                        //capture tab button clicks
                        PublicationTabListener publicationMenuTabListener = new PublicationTabListener(publicationPagerHorizontal.getPager());

                        //set the adapter
                        publicationPagerHorizontal.getPager().setAdapter(publicationMenuPagerAdapterHorizontal);

                        //create 'read' tab
                        ActionBar.Tab readTab = actionBar.newTab().setText(getResources().getString(R.string.read)).setTabListener(publicationMenuTabListener);
                        readTab.setTag(TAB_PUBLICATION_READ);
                        actionBar.addTab(readTab);

                        //create 'info' tab
                        ActionBar.Tab infoTab = actionBar.newTab().setText(getResources().getString(R.string.info)).setTabListener(publicationMenuTabListener);
                        infoTab.setTag(TAB_PUBLICATION_INFO);
                        actionBar.addTab(infoTab);

                        //create 'community' tab
                        ActionBar.Tab communityTab = actionBar.newTab().setText(getResources().getString(R.string.community)).setTabListener(publicationMenuTabListener);
                        communityTab.setTag(TAB_PUBLICATION_COMMUNITY);
                        actionBar.addTab(communityTab);

                        //show action bar
                        actionBar.show();

                        //default tab
                        actionBar.selectTab(readTab);

                        //load the publication view
                        loadPublication(publicationId, redownload);

                        actionBar.setSelectedNavigationItem(currentTabPublication);

                    }
                });
            }
        }).start();
    }

    public void showPublisher(final long publisherId, final boolean addHistory) {

        publisherMenuPagerAdapterHorizontal = new PublisherMenuPagerAdapterHorizontal(this, this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //new context
                        currentContext = CONTEXT_PUBLISHER;

                        if(addHistory) {
                            navigationHistoryStack.push(new NavigationHistory(CONTEXT_PUBLISHER, new Object[]{publisherId}));
                        }

                        //save reference
                        MainMenuActivity.this.publisherId = publisherId;

                        //swipe to publication view
                        NonSwipeableVerticalViewPager pagerVertical = (NonSwipeableVerticalViewPager) findViewById(R.id.mainmenu_pager_vertical);
                        pagerVertical.setCurrentItem(MainMenuPagerAdapterVertical.VIEW_PUBLISHER, true);

                        //hide the action bar
                        ActionBar actionBar = getSupportActionBar();
                        //navigation tabs
                        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
                        //remove previous tabs
                        actionBar.removeAllTabs();

                        //get the horizontal pager
                        CustomViewPager publisherPagerHorizontal = (CustomViewPager) mainMenuPagerAdapterVertical.getView(MainMenuPagerAdapterVertical.VIEW_PUBLISHER);

                        //indicamos el listener de cambio de pagina
                        publisherPagerHorizontal.getPager().setOnPageChangeListener(new SimpleOnPageChangeListener(actionBar));

                        //capture tab button clicks
                        PublisherTabListener publisherMenuTabListener = new PublisherTabListener(publisherPagerHorizontal.getPager());

                        // Set the View Pager Adapter into ViewPager
                        publisherPagerHorizontal.getPager().setAdapter(publisherMenuPagerAdapterHorizontal);

                        //create 'info' tab
                        ActionBar.Tab infoTab = actionBar.newTab().setText(getResources().getString(R.string.info)).setTabListener(publisherMenuTabListener);
                        infoTab.setTag(TAB_PUBLISHER_INFO);
                        actionBar.addTab(infoTab);

                        //create 'publications' tab
                        ActionBar.Tab publicationsTab = actionBar.newTab().setText(getResources().getString(R.string.publications)).setTabListener(publisherMenuTabListener);
                        publicationsTab.setTag(TAB_PUBLISHER_PUBLICATIONS);
                        actionBar.addTab(publicationsTab);

                        //show action bar
                        actionBar.show();

                        //default tab
                        actionBar.selectTab(infoTab);

                        //load the publisher view
                        loadPublisher(publisherId);

                    }
                });
            }
        }).start();
    }

    public void showTitle(final long titleId, final boolean addHistory) {

        titleMenuPagerAdapterHorizontal = new TitleMenuPagerAdapterHorizontal(this, this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //new context
                        currentContext = CONTEXT_TITLE;

                        if(addHistory) {
                            navigationHistoryStack.push(new NavigationHistory(CONTEXT_TITLE, new Object[]{titleId}));
                        }

                        //save reference
                        MainMenuActivity.this.titleId = titleId;

                        //swipe to publication view
                        NonSwipeableVerticalViewPager pagerVertical = (NonSwipeableVerticalViewPager) findViewById(R.id.mainmenu_pager_vertical);
                        pagerVertical.setCurrentItem(MainMenuPagerAdapterVertical.VIEW_TITLE, true);

                        //hide the action bar
                        ActionBar actionBar = getSupportActionBar();
                        //navigation tabs
                        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
                        //remove previous tabs
                        actionBar.removeAllTabs();

                        //get the horizontal pager
                        CustomViewPager titlePagerHorizontal = (CustomViewPager) mainMenuPagerAdapterVertical.getView(MainMenuPagerAdapterVertical.VIEW_TITLE);

                        //indicamos el listener de cambio de pagina
                        titlePagerHorizontal.getPager().setOnPageChangeListener(new SimpleOnPageChangeListener(actionBar));

                        //capture tab button clicks
                        TitleTabListener titleMenuTabListener = new TitleTabListener(titlePagerHorizontal.getPager());

                        // Set the View Pager Adapter into ViewPager
                        titlePagerHorizontal.getPager().setAdapter(titleMenuPagerAdapterHorizontal);

                        //create 'info' tab
                        ActionBar.Tab infoTab = actionBar.newTab().setText(getResources().getString(R.string.info)).setTabListener(titleMenuTabListener);
                        infoTab.setTag(TAB_TITLE_INFO);
                        actionBar.addTab(infoTab);

                        //create 'publications' tab
                        ActionBar.Tab publicationsTab = actionBar.newTab().setText(getResources().getString(R.string.publications)).setTabListener(titleMenuTabListener);
                        publicationsTab.setTag(TAB_TITLE_PUBLICATIONS);
                        actionBar.addTab(publicationsTab);

                        //show action bar
                        actionBar.show();

                        //default tab
                        actionBar.selectTab(infoTab);

                        //load the title view
                        loadTitle(titleId);

                    }
                });
            }
        }).start();
    }

    public void showSettings(final boolean addHistory) {

        AnalyticsFacade.sendScreenEvent(AnalyticsFacade.Category.UX, AnalyticsFacade.Action.UX_ENTER_SCREEN, AnalyticsFacade.Screen.SETTINGS, null);

        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(currentContext != CONTEXT_SETTINGS) {

                            if(addHistory) {
                                navigationHistoryStack.push(new NavigationHistory(CONTEXT_SETTINGS, null));
                            }

                            //new context
                            currentContext = CONTEXT_SETTINGS;

                            ActionBar actionBar = getSupportActionBar();

                            //remove previous tabs
                            actionBar.removeAllTabs();

                            //create 'settings' tab
                            ActionBar.Tab settingsTab = actionBar.newTab().setText(getResources().getString(R.string.settings)).setTabListener(new ActionBar.TabListener() {
                                @Override
                                public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                                }
                                @Override
                                public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                                }
                                @Override
                                public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                                }
                            });
                            actionBar.addTab(settingsTab);

                            NonSwipeableVerticalViewPager pagerVertical = (NonSwipeableVerticalViewPager) findViewById(R.id.mainmenu_pager_vertical);
                            pagerVertical.setCurrentItem(MainMenuPagerAdapterVertical.VIEW_CONFIG, true);
                        }
                    }
                });
            }
        }).start();

    }

    public void showTutorial() {

        AnalyticsFacade.sendScreenEvent(AnalyticsFacade.Category.UX, AnalyticsFacade.Action.UX_ENTER_SCREEN, AnalyticsFacade.Screen.TUTORIAL, null);


        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(currentContext != CONTEXT_TUTORIAL) {

                            //new context
                            currentContext = CONTEXT_TUTORIAL;

                            ActionBar actionBar = getSupportActionBar();

                            //remove previous tabs
                            actionBar.removeAllTabs();

                            //create 'tutorial' tab
                            ActionBar.Tab settingsTab = actionBar.newTab().setText(getResources().getString(R.string.tutorial_help)).setTabListener(new ActionBar.TabListener() {
                                @Override
                                public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                                }
                                @Override
                                public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                                }
                                @Override
                                public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                                }
                            });
                            actionBar.addTab(settingsTab);

                            NonSwipeableVerticalViewPager pagerVertical = (NonSwipeableVerticalViewPager) findViewById(R.id.mainmenu_pager_vertical);
                            pagerVertical.setCurrentItem(MainMenuPagerAdapterVertical.VIEW_TUTORIAL, true);
                        }
                    }
                });
            }
        }).start();

    }

    public void showSearch(final boolean addHistory) {
        searchPagerAdapterHorizontal = new SearchPagerAdapterHorizontal(this);
        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ActionBar actionBar = getSupportActionBar();

                        if(currentContext != CONTEXT_SEARCH) {

                            byte lastCurrentTabSearch = currentTabSearch;

                            if(addHistory) {
                                navigationHistoryStack.push(new NavigationHistory(CONTEXT_SEARCH, null));
                            }

                            //new context
                            currentContext = CONTEXT_SEARCH;

                            //swipe to publication view
                            NonSwipeableVerticalViewPager pagerVertical = (NonSwipeableVerticalViewPager) findViewById(R.id.mainmenu_pager_vertical);
                            pagerVertical.setCurrentItem(MainMenuPagerAdapterVertical.VIEW_SEARCH, true);

                            //get the horizontal pager
                            CustomViewPager mainMenuPagerHorizontal = (CustomViewPager) mainMenuPagerAdapterVertical.getView(MainMenuPagerAdapterVertical.VIEW_SEARCH);

                            //capture tab button clicks
                            SearchTabListener searchTabListener = new SearchTabListener(mainMenuPagerHorizontal.getPager());

                            //remove previous tabs
                            actionBar.removeAllTabs();

                            //create 'custom' tab
                            ActionBar.Tab customTab = actionBar.newTab().setText(getResources().getString(R.string.search)).setTabListener(searchTabListener);
                            customTab.setTag(TAB_SEARCH_CUSTOM);
                            actionBar.addTab(customTab);

                            //create 'by publisher' tab
                            ActionBar.Tab byPublisherTab = actionBar.newTab().setText(getResources().getString(R.string.by_publisher)).setTabListener(searchTabListener);
                            byPublisherTab.setTag(TAB_SEARCH_BY_PUBLISHER);
                            actionBar.addTab(byPublisherTab);

                            //create 'by title' tab
                            ActionBar.Tab byTitleTab = actionBar.newTab().setText(getResources().getString(R.string.by_title)).setTabListener(searchTabListener);
                            byTitleTab.setTag(TAB_SEARCH_BY_TITLE);
                            actionBar.addTab(byTitleTab);

                            //show action bar
                            actionBar.show();

                            //indicamos el listener de cambio de pagina
                            mainMenuPagerHorizontal.getPager().setOnPageChangeListener(new SimpleOnPageChangeListener(actionBar));

                            // Set the View Pager Adapter into ViewPager
                            mainMenuPagerHorizontal.getPager().setAdapter(searchPagerAdapterHorizontal);

                            //refresh search views
                            searchPagerAdapterHorizontal.refreshViews();

                            //set selected navigation item
                            actionBar.setSelectedNavigationItem(lastCurrentTabSearch);

                        }
                    }
                });
            }
        }).start();
    }
    private void loadConfig() {
        ConfigView configView = (ConfigView) mainMenuPagerAdapterVertical.getView(MainMenuPagerAdapterVertical.VIEW_CONFIG);
        configView.loadConfig();
    }

    private void loadPublisher(long publisherId) {
        LogicFacade.getPublisherDescriptor(publisherId, this);
    }

    private void loadTitle(long titleId) {
        LogicFacade.getTitleDescriptor(titleId, this);
    }

    private void loadPublication(long publicationId, boolean redownload) {

        //set flag
        stopReceiveTouchEvents = true;

        //get the horizontal pager
        CustomViewPager publicationPagerHorizontal = (CustomViewPager) mainMenuPagerAdapterVertical.getView(MainMenuPagerAdapterVertical.VIEW_PUBLICATION);

        //get the read and info views
        PublicationReadView publicationReadView = (PublicationReadView) publicationMenuPagerAdapterHorizontal.getView(PublicationMenuPagerAdapterHorizontal.VIEW_READ);

        //clear previous values
        publicationReadView.clearPublication();

        //show the loading layer
        publicationPagerHorizontal.showLoadingLayer();

        //swipe to publication view
        publicationPagerHorizontal.getPager().setCurrentItem(PublicationMenuPagerAdapterHorizontal.VIEW_READ);

        //flag to indicate that the
        //pages must be redownloaded
        //after accessing this screen
        this.redownload = redownload;

        //load publication descriptor
        LogicFacade.getPublicationDescriptor(publicationId, this);

        //if the publication is being downloaded
        //show the progress popup
        if(LogicFacade.getPublicationDownloadState(publicationId) == PublicationDownloadState.DOWNLOADING) {
            showDownloadPublicationPopup();
        }
    }

    public void downloadPublication() {
        if(publicationVO != null) {

            //download listener
            LogicFacade.registerPublicationDownloadListener(this);

            //start download
            downloadPublication(publicationVO);

            //show download progress
            showDownloadPublicationPopup();
        }
    }

    public void downloadPublication(PublicationVO publicationVO) {
        pageDownloadService.downloadPublication(publicationVO);
    }

    public void downloadPublicationUpdatablePages(PublicationVO publicationVO, List<PageVO> updatablePages) {
        pageDownloadService.downloadPublicationUpdatedPages(publicationVO, updatablePages);
    }

    public boolean isDownloadingPublication(long publicationId) {
        return LogicFacade.getPublicationDownloadState(publicationId) == PublicationDownloadState.DOWNLOADING;
    }

    private void saveCurrentTab() {
        byte selectedTab = (byte) getSupportActionBar().getSelectedTab().getPosition();
        if(currentContext == CONTEXT_MAIN_MENU) {
            currentTabMainMenu = selectedTab;
        }
        else if(currentContext == CONTEXT_PUBLICATION) {
            currentTabPublication = selectedTab;
        }
        else if(currentContext == CONTEXT_SEARCH) {
            currentTabSearch = selectedTab;
        }
    }

    @Override
    public void finishPublicationListLoading(PublicationListGenericView view, byte operation) {
        if(!firstSearchInSession && operation == LogicFacadeListener.OPERATION_findPublicationsError) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainMenuActivity.this, getResources().getString(R.string.cannot_download_publications_list), Toast.LENGTH_LONG).show();
                }
            });
        }
        firstSearchInSession = false;
    }

    private class SimpleOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {
        private ActionBar actionBar;
        public SimpleOnPageChangeListener(ActionBar actionBar) {
            this.actionBar = actionBar;
        }
        @Override
        public void onPageSelected(int position) {
            super.onPageSelected(position);
            actionBar.setSelectedNavigationItem(position);
        }
    }

    private class MainMenuTabListener implements ActionBar.TabListener {

        private boolean cachedAll;
        private boolean cachedPublishers;

        private ViewPager viewPager;
        public MainMenuTabListener(ViewPager viewPager) {
            this.viewPager = viewPager;
        }
        @Override
        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {

            viewPager.setCurrentItem(tab.getPosition());
            if(tab.getTag() != null && tab.getTag().equals(TAB_MAIN_MENU_ALL)) {
                if(!cachedAll) {
                    cachedAll = true;
                    PublicationListGenericView publicationListGenericView = (PublicationListGenericView) mainMenuPagerAdapterHorizontal.getViews().get(MainMenuPagerAdapterHorizontal.VIEW_ALL);
                    publicationListGenericView.search(null);
                }
            }
            else if(tab.getTag() != null && tab.getTag().equals(TAB_MAIN_MENU_MY_LIBRARY)) {
                PublicationListGenericView publicationListGenericView = (PublicationListGenericView) mainMenuPagerAdapterHorizontal.getViews().get(MainMenuPagerAdapterHorizontal.VIEW_MY_LIBRARY);
                publicationListGenericView.search(null);
            }
            else if(tab.getTag() != null && tab.getTag().equals(TAB_MAIN_MENU_PUBLISHERS)) {
                if(!cachedPublishers) {
                    cachedPublishers = true;
                    PublisherListView publisherListView = (PublisherListView) mainMenuPagerAdapterHorizontal.getViews().get(MainMenuPagerAdapterHorizontal.VIEW_PUBLISHERS);
                    publisherListView.getPublisherAdapter().clearPublishers();
                    publisherListView.getPublishers();
                }
            }
            saveCurrentTab();
        }
        @Override
        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        }
        @Override
        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
        }
    }

    private class SearchTabListener implements ActionBar.TabListener {

        private boolean cachedSearchCustom;

        private ViewPager viewPager;
        public SearchTabListener(ViewPager viewPager) {
            this.viewPager = viewPager;
        }
        @Override
        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
            viewPager.setCurrentItem(tab.getPosition());
            if(tab.getTag() != null && tab.getTag().equals(TAB_SEARCH_CUSTOM)) {
                if(!cachedSearchCustom) {
                    cachedSearchCustom = true;
                    SearchCustomView searchCustomView = (SearchCustomView) searchPagerAdapterHorizontal.getViews().get(SearchPagerAdapterHorizontal.VIEW_CUSTOM);
                    searchCustomView.search();
                }
            }
            else if(tab.getTag() != null && tab.getTag().equals(TAB_SEARCH_BY_PUBLISHER)) {

            }
            else if(tab.getTag() != null && tab.getTag().equals(TAB_SEARCH_BY_TITLE)) {

            }
            saveCurrentTab();
        }
        @Override
        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        }
        @Override
        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
        }
    }

    private class PublicationTabListener implements ActionBar.TabListener {
        private ViewPager viewPager;
        public PublicationTabListener(ViewPager viewPager) {
            this.viewPager = viewPager;
        }
        @Override
        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {

            viewPager.setCurrentItem(tab.getPosition());
            saveCurrentTab();

            if(tab.getTag().equals(TAB_PUBLICATION_READ)) {
            }
            else if(tab.getTag().equals(TAB_PUBLICATION_INFO)) {
                PublicationInfoView publicationInfoView = (PublicationInfoView) publicationMenuPagerAdapterHorizontal.getView(PublicationMenuPagerAdapterHorizontal.VIEW_INFO);
                publicationInfoView.showPublication();
            }
            else if(tab.getTag().equals(TAB_PUBLICATION_COMMUNITY)) {
                showAlertMessageDialog(R.string.still_working, R.string.still_working_desc, null);
            }
        }
        @Override
        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        }
        @Override
        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
        }
    }

    private class PublisherTabListener implements ActionBar.TabListener {

        private ViewPager viewPager;
        private boolean cachedPublications;

        public PublisherTabListener(ViewPager viewPager) {
            this.viewPager = viewPager;
        }
        @Override
        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
            viewPager.setCurrentItem(tab.getPosition());
            if(tab.getTag().equals(TAB_PUBLISHER_INFO)) {
                LOGGER.i("Publisher info tab");
            }
            else if(tab.getTag().equals(TAB_PUBLISHER_PUBLICATIONS)) {
                if(!cachedPublications) {
                    cachedPublications = true;
                    LOGGER.i("Publisher publications tab");
                    PublicationListGenericView publicationListGenericView = (PublicationListGenericView) publisherMenuPagerAdapterHorizontal.getView(PublisherMenuPagerAdapterHorizontal.VIEW_PUBLICATIONS);
                    publicationListGenericView.search(new Object[]{publisherId});
                }
            }
        }
        @Override
        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        }
        @Override
        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
        }
    }

    private class TitleTabListener implements ActionBar.TabListener {
        private ViewPager viewPager;
        public TitleTabListener(ViewPager viewPager) {
            this.viewPager = viewPager;
        }
        @Override
        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
            viewPager.setCurrentItem(tab.getPosition());
            if(tab.getTag().equals(TAB_PUBLISHER_INFO)) {
                LOGGER.i("Title info tab");
            }
            else if(tab.getTag().equals(TAB_PUBLISHER_PUBLICATIONS)) {
                LOGGER.i("Title publications tab");
                PublicationListGenericView publicationListGenericView = (PublicationListGenericView) titleMenuPagerAdapterHorizontal.getView(TitleMenuPagerAdapterHorizontal.VIEW_PUBLICATIONS);
                publicationListGenericView.search(new Object[]{titleId});
            }
        }
        @Override
        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        }
        @Override
        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
        }
    }

    public void showAlertMessageDialog(final int title, final int message, final DialogInterface.OnClickListener listener) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog alertDialog = new AlertDialog.Builder(MainMenuActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle(MainMenuActivity.this.getString(title));

                // Setting Dialog Message
                alertDialog.setMessage(MainMenuActivity.this.getString(message));

                // Setting Icon to Dialog
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);

                // Setting OK Button
                alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "OK", listener);

                // Showing Alert Message
                alertDialog.show();
            }
        });
    }

    public void readPublication() {
        if(LogicFacade.publicationPagesDownloaded(publicationVO)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //save the id of the publication
                    //that will be viewed
                    App.saveSharedPreference(MainMenuActivity.this, App.PUBLICATION_ID_SHARED_PREFERENCE_KEY, publicationVO.getId().toString());
                    //start the viewer
                    Intent comicViewerIntent = new Intent(MainMenuActivity.this, ComicViewerActivity.class);
                    comicViewerIntent.putExtra("publication", publicationVO);
                    startActivity(comicViewerIntent);
                }
            });
        }
    }

    @Override
    protected void onPause() {
        AdView admobBanner = obtenerAdView();
        if(admobBanner != null) {
            admobBanner.pause();
        }
        super.onPause();
    }

    @Override
    public void onBackPressed() {

        //remove the current navigation item
        if(navigationHistoryStack.size() > 0) {
            navigationHistoryStack.pop();
        }

        if(navigationHistoryStack.size() == 0) {

            //creamos un handler para que espere
            //un tiempo antes de cambiar el flag
            //que indica si la proxima vez que se
            //presione el boton back se sale de la
            //aplicacion
            Handler backButtonTimerHandler = new Handler();
            backButtonTimerHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    exitOnBack = false;
                }
            }, 4000);

            if(!exitOnBack) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.back_to_exit), Toast.LENGTH_SHORT).show();
                exitOnBack = true;
                return;
            }
            else {
                if(interstitial != null && interstitial.isLoaded()) {
                    displayInterstitial(ACCION_CIERRE_INTERSTITIAL_EXIT_APPLICATION);
                }
                else {
                    signout();
                }
            }
        }
        else {

            //peek the next
            NavigationHistory navigationHistory = navigationHistoryStack.peek();

            //go to upper if
            if(navigationHistoryStack.size() == 0) {
                onBackPressed();
                return;
            }

            byte navigationHistoryContext = navigationHistory.context;
            if(navigationHistoryContext == CONTEXT_MAIN_MENU) {
                showMainMenu(currentTabMainMenu, false);
            }
            else if(navigationHistoryContext == CONTEXT_PUBLICATION) {
                showPublication((long)navigationHistory.metadata[0], (boolean)navigationHistory.metadata[1], false);
            }
            else if(navigationHistoryContext == CONTEXT_PUBLISHER) {
                showPublisher((long)navigationHistory.metadata[0], false);
            }
            else if(navigationHistoryContext == CONTEXT_SEARCH) {
                showSearch(false);
            }
        }
    }

    public void loadInterstitial() {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                interstitial = new InterstitialAd(MainMenuActivity.this);
//                interstitial.setAdUnitId(App.ADMOB_INTERSTITIAL_UNIT_ID);
//                interstitial.setAdListener(new AdListener() {
//                    public void onAdClosed() {
//                        if(accionCierreInterstitial == ACCION_CIERRE_INTERSTITIAL_READ_COMIC) {
//                            readPublication();
//                        }
//                        else if(accionCierreInterstitial == ACCION_CIERRE_INTERSTITIAL_EXIT_APPLICATION) {
//                            exitApplication();
//                        }
//                    }
//                });
//                interstitial.loadAd(new AdRequest.Builder().build());
//            }
//        });
    }

    public void displayInterstitial(byte accionCierreInterstitial) {
        if(interstitial != null) {
            this.accionCierreInterstitial = accionCierreInterstitial;
            interstitial.show();
        }
    }

    public void createAdmodBanner() {

//        final AdView admobBanner = new AdView(this);
//        admobBanner.setAdUnitId(App.ADMOB_BANNER_UNIT_ID);
//        admobBanner.setAdSize(AdSize.SMART_BANNER);
//        admobBanner.setVisibility(View.GONE);
//
//        //get the adview layout
//        LinearLayout comicMenuWebLayout = (LinearLayout) findViewById(R.id.adMainContainer);
//
//        //remove possible old views
//        comicMenuWebLayout.removeAllViews();
//
//        //show the adview on the UI
//        //only if its loading process
//        //is correctly finished
//        comicMenuWebLayout.addView(admobBanner);
//
//        //add the ad listener
//        admobBanner.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                admobBanner.setVisibility(View.VISIBLE);
//            }
//            @Override
//            public void onAdFailedToLoad(int errorCode) {
//                super.onAdFailedToLoad(errorCode);
//                //retry load
//                admobBanner.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        LOGGER.d("------ RECARGANDO AD DEBIDO A ERROR EN CARGA");
//                        loadBanner();
//                    }
//                }, 5000);
//            }
//        });
    }

    private AdView obtenerAdView() {
        LinearLayout comicMenuWebLayout = (LinearLayout) findViewById(R.id.admain_container);
        return (AdView)comicMenuWebLayout.getChildAt(0);
    }

    public void loadBanner() {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                createAdmodBanner();
//                LinearLayout comicMenuWebLayout = (LinearLayout) findViewById(R.id.adMainContainer);
//                AdView bannerAdView = (AdView) comicMenuWebLayout.getChildAt(0);
//                bannerAdView.loadAd(new AdRequest.Builder().build());
//            }
//        });
    }

    private void removeAds() {
        AdView bannerAdView = obtenerAdView();
        if(bannerAdView != null) {
            bannerAdView.setVisibility(View.GONE);
        }
        interstitial = null;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //get the last logged user
        loggedUser = LogicFacade.getLastLoggedUser();

        if(null != loggedUser) {
            //load user config
            loadConfig();

            setUsername(loggedUser.getUsername());
            setFullName(loggedUser.getFullName(), loggedUser.getFirstName(), loggedUser.getLastName());
            setAvatar(loggedUser.getUsername(), loggedUser.getPhotoUrl());

            if(publicationVO != null) {
                showPublication(publicationVO.getId(), false, true);
            }
            else {

                //verify network availability
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        boolean networkAvailable = isNetworkAvailable();
                        if(!networkAvailable) {
                            //switch to my library
                            showMainMenu(TAB_MAIN_MENU_MY_LIBRARY, true);
                        }
                        else {
                            //switch to all (online)
                            showMainMenu(TAB_MAIN_MENU_ALL, true);
                        }
                    }
                });
            }

            //load the ads
            loadBanner();
            loadInterstitial();

            //get intent
            Intent intent = getIntent();
            if(intent != null) {
                Long notificationDownloadPublicationId = intent.getLongExtra("notificationDownloadPublicationId", -1);
                PublicationVO redownloadPublication = (PublicationVO) intent.getSerializableExtra("redownloadPublication");
                if(redownloadPublication != null) {
                    intent.removeExtra("redownloadPublication");
                    showPublication(redownloadPublication.getId(), true, true);
                }
                else if(notificationDownloadPublicationId != null && notificationDownloadPublicationId != -1) {
                    intent.removeExtra("notificationDownloadPublicationId");
                    showPublication(notificationDownloadPublicationId, false, true);
                }
            }
        }
        else {
            //try to recover last login
            Intent loginActivityIntent = new Intent(this, LoginActivity.class);
            loginActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(loginActivityIntent);
            finish();
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void showDownloadPublicationPopup() {
        //create the download popup
        downloadPublicationPopupWindow = new DownloadPublicationPopupWindow(this);
        downloadPublicationPopupWindow.show();
    }

    @Override
    public void operationFinished(final Object result, byte operationType) {
        if(operationType == OPERATION_getPublicationDescriptor) {

            final Object[] resultArray = (Object[]) result;

            this.publicationVO = (PublicationVO) resultArray[0];
            final PublicationDescriptorDownloadEnum publicationDescriptorDownloadEnum = (PublicationDescriptorDownloadEnum) resultArray[1];

            //load the publisher descriptor
            loadPublisher(publicationVO.getPublisherVO().getId());

            //load ui
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    CustomViewPager publicationPagerHorizontal = (CustomViewPager) mainMenuPagerAdapterVertical.getView(MainMenuPagerAdapterVertical.VIEW_PUBLICATION);

                    //get the read and info views
                    PublicationReadView publicationReadView = (PublicationReadView) publicationMenuPagerAdapterHorizontal.getView(PublicationMenuPagerAdapterHorizontal.VIEW_READ);
                    PublicationInfoView publicationInfoView = (PublicationInfoView) publicationMenuPagerAdapterHorizontal.getView(PublicationMenuPagerAdapterHorizontal.VIEW_INFO);

                    publicationReadView.loadPublication(publicationVO);
                    publicationInfoView.loadPublication(publicationVO);

//                    //show the icon
//                    String iconURL = LogicFacade.getPublicationIconURL(publicationVO.getId());
//                    if(publicationDescriptorUpdated) {
//                        //remove old icon image
//                        UILUtil.removeFromCache(iconURL);
//                    }
//                    //show icon image
//                    UILUtil.downloadImage(iconURL, (ImageView) findViewById(R.id.iconImageView));

                    //start re downloading the pages
                    if(redownload) {
                        downloadPublication();
                    }

                    //evalute the publication status
                    //to configure the buttons bar
                    evaluatePublicationStatus();

                    //hide the loading layer
                    publicationPagerHorizontal.hideLoadingLayer();

                    //set flag
                    stopReceiveTouchEvents = false;

                    //if the pages were updated on the
                    //server ask the user to download them
                    if(publicationDescriptorDownloadEnum == PublicationDescriptorDownloadEnum.PENDING_PAGE_IMAGES_UPDATE) {
                        if(!LogicFacade.isDownloadPublicationPostponed(publicationVO.getId())) {
                            //get the list of pages to update
                            List<PageVO> updatedPages = (List<PageVO>) resultArray[2];
                            //show the download alert
                            showDownloadPagesAlert(updatedPages);
                        }
                    }
                }
            });
        }
        else if(operationType == OPERATION_getPublisherDescriptor) {
            final PublisherVO publisherVO = (PublisherVO) result;

            if(currentContext == MainMenuActivity.CONTEXT_PUBLICATION) {
                //get the info view
                final PublicationInfoView publicationInfoView = (PublicationInfoView) publicationMenuPagerAdapterHorizontal.getView(PublicationMenuPagerAdapterHorizontal.VIEW_INFO);

                if(publisherVO != null) {
                    //load ui
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //show publisher avatar
                            String avatarUrl = LogicFacade.getPublisherAvatarURL(publisherVO.getId(), publisherVO.getVersion());
                            UILUtil.downloadImage(avatarUrl, (ImageView) publicationInfoView.findViewById(R.id.publisherAvatarImage), true, new UILUtil.UILDownloadListener() {
                                @Override
                                public void loadingStarted() {

                                }
                                @Override
                                public void downloadFinished(boolean success) {
                                    if(!success) {
                                        ((ImageView) publicationInfoView.findViewById(R.id.publisherAvatarImage)).setImageResource(R.drawable.tre_logo);
                                    }
                                }
                            });
                        }
                    });
                }
            }
            else if(currentContext == MainMenuActivity.CONTEXT_PUBLISHER) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //get the horizontal pager
                        CustomViewPager publisherPagerHorizontal = (CustomViewPager) mainMenuPagerAdapterVertical.getView(MainMenuPagerAdapterVertical.VIEW_PUBLISHER);

                        //get the read and info views
                        PublisherInfoView publisherInfoView = (PublisherInfoView) publisherMenuPagerAdapterHorizontal.getView(PublisherMenuPagerAdapterHorizontal.VIEW_INFO);
                        if(null != publisherVO) {
                            //swipe to publication view
                            publisherInfoView.loadPublisher(publisherVO);
                            publisherPagerHorizontal.getPager().setCurrentItem(PublisherMenuPagerAdapterHorizontal.VIEW_INFO);
                        }
                        else {
                            //this should not happen, but it could happen if
                            //the server does not return any result for the
                            //publisher descriptor
                            Toast.makeText(MainMenuActivity.this, getResources().getString(R.string.cannot_load_publisher_info), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        }
        else if(operationType == OPERATION_getTitleDescriptor) {
            final TitleVO titleVO = (TitleVO) result;

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //get the horizontal pager
                    CustomViewPager titlePagerHorizontal = (CustomViewPager) mainMenuPagerAdapterVertical.getView(MainMenuPagerAdapterVertical.VIEW_TITLE);

                    //get the read and info views
                    TitleInfoView titleInfoView = (TitleInfoView) titleMenuPagerAdapterHorizontal.getView(TitleMenuPagerAdapterHorizontal.VIEW_INFO);
                    if(null != titleVO) {
                        //swipe to title view
                        titleInfoView.loadTitle(titleVO);
                        titlePagerHorizontal.getPager().setCurrentItem(TitleMenuPagerAdapterHorizontal.VIEW_INFO);
                    }
                    else {
                        //this should not happen, but it could happen if
                        //the server does not return any result for the
                        //title descriptor
                        Toast.makeText(MainMenuActivity.this, getResources().getString(R.string.cannot_load_publisher_info), Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }

    @Override
    public void publicationDownloadProgress(DownloadPageStatus downloadPageStatus) {
        if(downloadPageStatus.publicationDownloadState != PublicationDownloadState.DOWNLOADING) {
            LogicFacade.unregisterPublicationDownloadListener(this);

            if(downloadPageStatus.publicationDownloadState == PublicationDownloadState.DOWNLOADED) {
                //save the publication on user library
                UserDatabaseManager.getDatabase().createPublication(downloadPageStatus.publicationId);
                //refresh 'my library'
                mainMenuPagerAdapterHorizontal.refreshMyLibrary();
            }

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    evaluatePublicationStatus();
                }
            });
        }
    }

    private void evaluatePublicationStatus() {
        PublicationReadView publicationReadView = (PublicationReadView) publicationMenuPagerAdapterHorizontal.getView(PublicationMenuPagerAdapterHorizontal.VIEW_READ);
        if(LogicFacade.publicationPagesDownloaded(publicationVO)) {
            publicationReadView.setReadState(true);
        }
        else {
            publicationReadView.setReadState(false);
        }
    }

    private void showDownloadPagesAlert(final List<PageVO> updatablePages) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);

        dialog.setTitle(getString(R.string.publication_update));
        dialog.setMessage(getString(R.string.publication_update_desc));

        dialog.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                downloadPublicationUpdatablePages(publicationVO, updatablePages);
                dialog.dismiss();
            }
        });

        dialog.setNegativeButton(getString(R.string.postpone), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                LogicFacade.postponePublicationDownload(publicationVO.getId());
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void showLoadingCircle() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressbar);
                progressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    public void hideLoadingCircle() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressbar);
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void signout() {
        Intent loginActivityIntent = new Intent(this, LoginActivity.class);
        loginActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        loginActivityIntent.putExtra("signout", true);
        startActivity(loginActivityIntent);
        finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if(!stopReceiveTouchEvents) {
            return super.dispatchTouchEvent(ev);
        }
        else {
            return false;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return true;
    }

    private Intent getShareAppIntent() {

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.applicationFullName));
        intent.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.share_app_text) + " " + App.GOOGLE_PLAY_APPLICATION_URL);
        intent.setType("text/plain");

        return intent;
    }

    public ParallaxViewPager getPublicationViewPager() {
        CustomViewPager publicationPagerHorizontal = (CustomViewPager) mainMenuPagerAdapterVertical.getView(MainMenuPagerAdapterVertical.VIEW_PUBLICATION);
        return publicationPagerHorizontal.getPager();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public void setAvatar(final String username, final String url) {
        final RoundedImageView avatarView = (RoundedImageView) findViewById(R.id.profile_image);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Bitmap bitmap = null;
                try {
                    URLConnection urlConnection = new URL(url).openConnection();
                    BufferedInputStream bis = new BufferedInputStream(urlConnection.getInputStream());
                    byte[] imageData = LogicFacade.readInputStream(bis);
                    LogicFacade.saveUserAvatar(username, imageData);
                    bitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
                }
                catch (Exception e) {
                    byte[] imageData = LogicFacade.readUserAvatar(username);
                    if(imageData != null) {
                        bitmap = BitmapFactory.decodeByteArray(imageData, 0, imageData.length);
                    }
                }

                if(bitmap != null) {
                    final Bitmap finalBitmap = bitmap;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            avatarView.setImageBitmap(finalBitmap);
                        }
                    });
                }
            }
        }).start();
    }

    public void setFullName(String fullName, String firstName, String lastName) {
        TextView usernameText = (TextView) findViewById(R.id.profile_name);
        String name = "";
        if(null == fullName || "".equalsIgnoreCase(fullName.trim())) {
            if(null == firstName) {
                firstName = "";
            }
            if(null == lastName) {
                lastName = "";
            }
            name = firstName + " " + lastName;
        }
        else {
            name = fullName;
        }
        usernameText.setText(name);
    }

    public void setUsername(String val) {
        TextView userloginText = (TextView) findViewById(R.id.profile_email);
        userloginText.setText(val);
    }
}