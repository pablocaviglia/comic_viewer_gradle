package com.alchemy.thereaderengine.downloader;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.view.View;
import android.widget.RemoteViews;

import com.alchemy.thereaderengine.enums.PublicationDownloadState;
import com.alchemy.thereaderengine.logic.LogicFacade;
import com.apocalipta.comic.vo.v1.PageVO;
import com.apocalipta.comic.vo.v1.PublicationVO;
import com.alchemy.thereaderengine.MainMenuActivity;
import com.alchemy.thereaderengine.R;
import com.alchemy.thereaderengine.listener.LogicFacadeListener;
import com.alchemy.thereaderengine.listener.PublicationDownloadListener;
import com.alchemy.thereaderengine.model.DownloadPageStatus;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import java.util.List;

/**
 * Created by pablo on 29/12/14.
 */
public class PageDownloadService extends Service implements PublicationDownloadListener {

    private static final Logger LOGGER = LoggerManager.getLogger();

    public static final int SERVICE_ID = 0x101104;

    private NotificationManager notificationManager;
    private AsyncDownloadTask task = null;

    protected static boolean isRunning = false;

    private static final long MIN_UPDATE_INTERVAL = 10000000;
    private long lastUpdateTime = System.nanoTime();

    @Override
    public void onCreate() {
        if (isRunning)
            return;
        else
            isRunning = true;

        //reset failed downloading publications
        LogicFacade.resetDownloadingPublications();

        //register listener
        LogicFacade.registerPublicationDownloadListener(this);

        //get the notification manager
        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    public void downloadPublication(PublicationVO publicationVO) {
        long publicationId = publicationVO.getId();
        if(LogicFacade.getPublicationDownloadState(publicationId) != PublicationDownloadState.DOWNLOADING) {

            //save the new downloading state
            LogicFacade.setPublicationDownloadState(publicationId, PublicationDownloadState.DOWNLOADING);

            //get the publication name
            String publicationName = LogicFacade.getPublicationName(publicationVO);

            //get pages to download
            List<PageVO> pages = publicationVO.getProjectVO().getPages();

            //create the download request
            DownloadRequest downloadRequest = new DownloadRequest(publicationId, publicationName, pages);

            //start downloading immediately
            task = new AsyncDownloadTask();
            task.execute(downloadRequest);
        }
    }

    public void downloadPublicationUpdatedPages(PublicationVO publicationVO, List<PageVO> updatablePages) {

        long publicationId = publicationVO.getId();
        if(LogicFacade.getPublicationDownloadState(publicationId) != PublicationDownloadState.DOWNLOADING) {

            //save the new downloading state
            LogicFacade.setPublicationDownloadState(publicationId, PublicationDownloadState.DOWNLOADING);

            //get the publication name
            String publicationName = LogicFacade.getPublicationName(publicationVO);

            //create the download request
            DownloadRequest downloadRequest = new DownloadRequest(publicationId, publicationName, updatablePages);

            //start downloading immediately
            task = new AsyncDownloadTask();
            task.execute(downloadRequest);
        }
    }

    @Override
    public void onDestroy() {
//        if (task != null) {
//            if (!task.isCancelled())
//                task.cancel(true);
//        }
//        LogicFacade.unregisterPublicationDownloadListener(this);
//        isRunning = false;
//        LOGGER.d("service destroyed");
    }

    /**
     * implement this function to customize notification flag
     * <br>
     * <br>
     * ex)
     * <br>
     * <pre>
     * 	protected int getNotificationFlag()
     *    {
     * 		return Notification.FLAG_AUTO_CANCEL | Notification.DEFAULT_LIGHTS;
     *    }
     * </pre>
     *
     * @return
     */
    private int getNotificationFlag() {
        return Notification.DEFAULT_LIGHTS;
    }

    private int getNotificationIcon() {
        return R.drawable.tre_logo;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    protected void showDownloadNotification(String ticker, String title, String content, DownloadPageStatus downloadPageStatus) {

        RemoteViews remoteView = new RemoteViews(getPackageName(), R.layout.download_publication);
        remoteView.setTextViewText(R.id.publicationName, title);
        remoteView.setTextViewText(R.id.pageCountStatusTextView, content);
        remoteView.setImageViewResource(R.id.thumbPreview, R.drawable.tre_logo);

        if(downloadPageStatus.publicationDownloadState == PublicationDownloadState.DOWNLOADING) {
            int progress = (int)(downloadPageStatus.readBytes * 100 / downloadPageStatus.totalBytes);
            remoteView.setProgressBar(R.id.pageProgressBar, 100, progress, false);
            remoteView.setImageViewBitmap(R.id.thumbPreview, downloadPageStatus.thumbBitmap);
            remoteView.setViewVisibility(R.id.pageProgressBar, View.VISIBLE);
        }

        if(downloadPageStatus.publicationDownloadState == PublicationDownloadState.ERROR ||
           downloadPageStatus.publicationDownloadState == PublicationDownloadState.CANCELLED ||
           downloadPageStatus.publicationDownloadState == PublicationDownloadState.DOWNLOADED ||
           downloadPageStatus.publicationDownloadState == PublicationDownloadState.NOT_DOWNLOADED) {
            remoteView.setViewVisibility(R.id.pageProgressBar, View.INVISIBLE);
        }

        //create the intent
        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.putExtra("notificationDownloadPublicationId", downloadPageStatus.publicationId);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        //create the notification
        Notification.Builder builder = new Notification.Builder(getApplicationContext());
        builder.setSmallIcon(getNotificationIcon());
        builder.setTicker(ticker);
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setWhen(System.currentTimeMillis());
        builder.setContentIntent(contentIntent);

        //create the notification bar view
        builder.setContent(remoteView);

        //build the notification
        Notification notification = null;
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            notification = builder.getNotification();
        }
        else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notification = builder.build();
        }

        if(notificationManager != null) {
            notification.flags = getNotificationFlag();
            notificationManager.notify(SERVICE_ID, notification);
        }
    }

    private class AsyncDownloadTask extends AsyncTask<DownloadRequest, Void, Void> {

        private DownloadPageStatus downloadPageStatus = new DownloadPageStatus();

        @Override
        protected Void doInBackground(DownloadRequest... params) {

            //set state
            downloadPageStatus.publicationDownloadState = PublicationDownloadState.DOWNLOADING;

            DownloadRequest downloadRequest = params[0];
            long publicationId = downloadRequest.publicationId;
            String publicationName = downloadRequest.publicationName;

            try {
                Thread downloadThread = LogicFacade.downloadPages(downloadPageStatus, downloadRequest.publicationId, publicationName, downloadRequest.pages,
                        new LogicFacadeListener() {
                            @Override
                            public void operationFinished(Object result, byte operationType) {
                                if(operationType == LogicFacadeListener.OPERATION_downloadPublicationPages) {
                                    //finished download
                                }
                            }
                        }
                );

                //wait for download
                downloadThread.join();
            }
            catch (InterruptedException e) {
                LOGGER.e(e.getMessage(), e);
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            LOGGER.d("download task cancelled");
            downloadPageStatus.publicationDownloadState = PublicationDownloadState.CANCELLED;
            showDownloadNotification(getString(R.string.download_cancelled), getString(R.string.download_progress), getString(R.string.cancelled), downloadPageStatus);
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            String finishedMsg = "";
            if (downloadPageStatus.publicationDownloadState == PublicationDownloadState.DOWNLOADED) {
                finishedMsg = getString(R.string.download_completed);
            }
            else if (downloadPageStatus.publicationDownloadState == PublicationDownloadState.CANCELLED) {
                finishedMsg = getString(R.string.download_cancelled_by_user);
            }
            else if (downloadPageStatus.publicationDownloadState == PublicationDownloadState.ERROR) {
                finishedMsg = getString(R.string.download_finished_failures);
            }

            showDownloadNotification(getString(R.string.download_completed), downloadPageStatus.publicationName, finishedMsg, downloadPageStatus);
            LOGGER.d("download task finished");
        }
    }

    private class DownloadRequest {
        public long publicationId;
        public String publicationName;
        public List<PageVO> pages;

        private DownloadRequest(long publicationId, String publicationName, List<PageVO> pages) {
            this.publicationId = publicationId;
            this.publicationName = publicationName;
            this.pages = pages;
        }
    }

    @Override
    public void publicationDownloadProgress(DownloadPageStatus downloadPageStatus) {
        if(downloadPageStatus.page != -1) {
            // Don't update too often
            if ((System.nanoTime() - lastUpdateTime) < MIN_UPDATE_INTERVAL) return;

            String ticker = downloadPageStatus.publicationName;
            String title = downloadPageStatus.publicationName;

            String downloadingStr = getString(R.string.downloading);
            String pageStr = getString(R.string.page);
            String ofStr = getString(R.string.of);

            String content = downloadingStr + " " + pageStr + " " + downloadPageStatus.page + " " + ofStr + " " + downloadPageStatus.qntyPages;

            showDownloadNotification(ticker, title, content, downloadPageStatus);
        }
    }

    //binder given to clients
    private final IBinder mBinder = new PageDownloadServiceBinder();

    public class PageDownloadServiceBinder extends Binder {
        public PageDownloadService getService() {
            // Return this instance of LocalService so clients can call public methods
            return PageDownloadService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }
}