package com.alchemy.thereaderengine.logic;

import android.content.Context;

import com.alchemy.thereaderengine.App;
import com.alchemy.thereaderengine.R;
import com.apocalipta.comic.constants.UserPlatform;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pablo on 01/07/15.
 */
public class AnalyticsFacade {

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    public enum Category {
        UX,
        LOGIC,
    }

    public enum Action {
        UX_CLICK,
        UX_ENTER_SCREEN,
        UX_EXIT_SCREEN,
        LOGIC_DOWNLOAD_PUBLICATION_START,
        LOGIC_DOWNLOAD_PUBLICATION_FAILED,
        LOGIC_DOWNLOAD_PUBLICATION_OK,
        LOGIC_LOGIN,
    }

    public enum Screen {
        LOGIN,
        MAIN_MENU,
        READER,
        SEARCH,
        SETTINGS,
        PUBLICATION,
        TUTORIAL,
    }

    public static void init(Context context) {
        analytics = GoogleAnalytics.getInstance(context);
        analytics.setLocalDispatchPeriod(1800);

        tracker = analytics.newTracker("UA-64709317-1");
        tracker.setAppId(App.GOOGLE_PLAY_APPLICATION_URL);
        tracker.setAppName(context.getResources().getString(R.string.applicationFullName));
        tracker.setScreenResolution(App.SCREEN_WIDTH, App.SCREEN_HEIGHT);
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);
    }

    public static void sendScreenEvent(final Category category, final Action action, final Screen screen, final Map<String, String> extra) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                tracker.setScreenName(screen.name());
                tracker.send(new HitBuilders.EventBuilder()
                        .setCategory(category.name())
                        .setAction(action.name())
                        .setAll(extra)
                        .build());
            }
        }).start();
    }

    public static void sendLogicEvent(final Category category, final Action action, final Map<String, String> extra) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                tracker.setScreenName(null);
                tracker.send(new HitBuilders.EventBuilder()
                        .setCategory(category.name())
                        .setAction(action.name())
                        .setAll(extra)
                        .build());
            }
        }).start();
    }

    public static void sendLogin(final UserPlatform platform, final String username, final boolean success) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Map<String, String> extra  = new HashMap<>();
                extra.put("PLATFORM", platform.name());
                extra.put("USERNAME", username);
                extra.put("SUCCESS", String.valueOf(success));
                sendLogicEvent(Category.LOGIC, Action.LOGIC_LOGIN, extra);
            }
        }).start();
    }
}