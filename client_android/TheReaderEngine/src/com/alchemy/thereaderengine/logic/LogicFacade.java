package com.alchemy.thereaderengine.logic;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.alchemy.thereaderengine.App;
import com.alchemy.thereaderengine.database.SystemDatabaseManager;
import com.alchemy.thereaderengine.database.UserDatabaseManager;
import com.alchemy.thereaderengine.enums.AutoPlayTime;
import com.alchemy.thereaderengine.enums.PublicationDescriptorDownloadEnum;
import com.alchemy.thereaderengine.enums.PublicationDownloadState;
import com.alchemy.thereaderengine.listener.LogicFacadeListener;
import com.alchemy.thereaderengine.listener.PublicationDownloadListener;
import com.alchemy.thereaderengine.model.DownloadPageStatus;
import com.apocalipta.comic.constants.JSONApiVersion;
import com.apocalipta.comic.vo.v1.LanguageVO;
import com.apocalipta.comic.vo.v1.PageVO;
import com.apocalipta.comic.vo.v1.PublicationVO;
import com.apocalipta.comic.vo.v1.PublisherVO;
import com.apocalipta.comic.vo.v1.ResultVO;
import com.apocalipta.comic.vo.v1.SearchResultPublicationVO;
import com.apocalipta.comic.vo.v1.SearchResultPublisherVO;
import com.apocalipta.comic.vo.v1.SearchResultTitleVO;
import com.apocalipta.comic.vo.v1.TitleVO;
import com.apocalipta.comic.vo.v1.ViewerUserVO;
import com.facebook.crypto.Entity;
import com.facebook.crypto.exception.CryptoInitializationException;
import com.facebook.crypto.exception.KeyChainException;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import static com.alchemy.thereaderengine.enums.AutoPlayTime.X1;
import static com.alchemy.thereaderengine.enums.AutoPlayTime.X2;
import static com.alchemy.thereaderengine.enums.AutoPlayTime.X3;
import static com.alchemy.thereaderengine.enums.AutoPlayTime.X4;

/**
 * Created by pablo on 10/12/14.
 */
public class LogicFacade {

    private static final Logger LOGGER = LoggerManager.getLogger();

//    private final static String SERVER_URL = "10.181.10.193";

    //dev
//    private final static String SERVER_URL = "192.168.56.1";
//    private final static String SERVER_PORT = "9443";

    //prod
    private final static String SERVER_URL = "thereaderengine.com";
    private final static String SERVER_PORT = "9443";

    private final static String KEYSTORE_PASSWORD = "pablo!!84";
    private static SSLContext sslContext;

    private static final String PUBLISHER_DESCRIPTOR_ENTITY             = "publisherDescriptor";
    private static final String PUBLICATION_DESCRIPTOR_ENTITY           = "publicationDescriptor";
    private static final String PUBLICATION_DESCRIPTOR_PREVIEW_ENTITY   = "publicationDescriptorPreview";
    private static final String TITLE_DESCRIPTOR_ENTITY                 = "titleDescriptor";
    private static final String USER_ENTITY                             = "userDescriptor";
    public static final String PAGE_ENTITY                              = "page";
    public static final String PAGE_THUMB_ENTITY                        = "pageThumb";

    private static List<PublicationDownloadListener> publicationDownloadListenerList = new ArrayList<>();

    //json mapper
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private static final String PUBLISHER_DESCRIPTOR_PREFIX             = "pubd_";
    private static final String PUBLICATION_DESCRIPTOR_PREFIX           = "pd_";
    private static final String PUBLICATION_DESCRIPTOR_PREVIEW_PREFIX   = "pdp_";
    private static final String TITLE_DESCRIPTOR_PREFIX                 = "t_";
    private static final String USER_PREFIX                             = "u_";
    private static final String USER_AVATAR_PREFIX                      = "ua_";
    private static final String PAGE_IMAGE_PREFIX                       = "p_";
    private static final String THUMB_IMAGE_PREFIX                      = "pt_";

    public final static byte[] data = {0,0,0,1,0,0,0,20,116,-92,31,-45,123,-20,-123,14,-86,47,-56,-54,73,36,82,29,-11,21,-15,85,0,0,7,61,4,0,13,99,111,109,105,99,115,95,99,108,105,101,110,116,0,0,1,74,82,61,110,63,0,0,0,1,0,5,88,46,53,48,57,0,0,3,-77,48,-126,3,-81,48,-126,2,-105,2,4,84,-115,-40,4,48,13,6,9,42,-122,72,-122,-9,13,1,1,5,5,0,48,-127,-101,49,39,48,37,6,9,42,-122,72,-122,-9,13,1,9,1,22,24,112,97,98,108,111,46,99,97,118,105,103,108,105,97,64,103,109,97,105,108,46,99,111,109,49,11,48,9,6,3,85,4,6,19,2,85,89,49,19,48,17,6,3,85,4,8,12,10,109,111,110,116,101,118,105,100,101,111,49,19,48,17,6,3,85,4,7,12,10,109,111,110,116,101,118,105,100,101,111,49,12,48,10,6,3,85,4,10,12,3,103,109,105,49,19,48,17,6,3,85,4,11,12,10,97,112,111,99,97,108,105,112,116,97,49,22,48,20,6,3,85,4,3,12,13,99,111,109,105,99,115,95,99,108,105,101,110,116,48,30,23,13,49,52,49,50,49,52,49,56,51,51,52,48,90,23,13,50,52,49,50,49,49,49,56,51,51,52,48,90,48,-127,-101,49,39,48,37,6,9,42,-122,72,-122,-9,13,1,9,1,22,24,112,97,98,108,111,46,99,97,118,105,103,108,105,97,64,103,109,97,105,108,46,99,111,109,49,11,48,9,6,3,85,4,6,19,2,85,89,49,19,48,17,6,3,85,4,8,12,10,109,111,110,116,101,118,105,100,101,111,49,19,48,17,6,3,85,4,7,12,10,109,111,110,116,101,118,105,100,101,111,49,12,48,10,6,3,85,4,10,12,3,103,109,105,49,19,48,17,6,3,85,4,11,12,10,97,112,111,99,97,108,105,112,116,97,49,22,48,20,6,3,85,4,3,12,13,99,111,109,105,99,115,95,99,108,105,101,110,116,48,-126,1,34,48,13,6,9,42,-122,72,-122,-9,13,1,1,1,5,0,3,-126,1,15,0,48,-126,1,10,2,-126,1,1,0,-126,-118,105,-44,-21,4,36,28,50,120,85,-78,-58,72,-26,-58,-51,-48,-91,91,9,74,126,-110,38,106,-31,-106,98,-71,72,-119,27,95,121,110,42,-120,-113,15,-37,-52,-94,-88,-51,-54,30,-81,-109,125,-59,-69,77,57,10,-102,11,61,-82,79,31,123,-88,-5,-82,13,-58,-78,-34,37,22,8,110,25,32,99,-41,122,50,-115,24,50,120,-92,-90,-55,104,-49,-73,-106,52,-83,-97,7,-40,-126,3,-78,-92,92,-112,23,-70,31,-62,-117,-107,124,-12,-46,80,10,-27,96,23,-55,3,126,-86,-3,-20,6,93,-80,-100,60,3,25,61,79,-75,-116,-5,66,84,-31,-98,41,-43,42,-70,5,-46,92,-120,-63,89,88,77,66,-101,-47,122,-31,-52,87,18,94,-39,6,90,89,-61,45,-120,-23,-75,-117,125,55,-30,56,-66,-66,98,82,0,97,-57,48,-14,32,-80,84,99,76,-12,-63,56,-126,93,-20,56,46,-103,5,116,-32,-15,-43,56,64,89,-24,54,116,-62,-45,-3,-4,-52,-128,-87,-32,-34,74,63,-14,24,75,66,-44,-125,-50,-105,127,-101,56,-16,71,-115,60,127,47,25,34,-59,1,118,-62,-68,-81,-51,36,-93,-25,61,79,74,107,73,118,21,-66,92,-53,2,3,1,0,1,48,13,6,9,42,-122,72,-122,-9,13,1,1,5,5,0,3,-126,1,1,0,28,-37,-55,61,-5,-53,-11,-11,-69,45,117,-108,52,-83,38,105,-11,93,10,-64,55,-73,-120,54,-34,98,-87,119,-83,31,116,-80,-79,115,-49,-11,-62,62,34,55,42,-8,108,68,69,-106,-38,98,-58,98,-96,16,-79,26,65,-84,123,69,-9,-63,91,-52,55,-116,11,32,115,-7,-111,-1,45,-40,73,76,-58,57,5,41,-78,8,-75,-86,57,105,-40,73,-30,-65,113,121,22,-112,60,-9,44,9,-89,51,3,-105,112,-20,-2,66,26,-92,8,12,57,32,41,-47,47,122,-37,-69,-31,80,-91,-117,-57,-114,-1,44,-122,54,56,-102,27,-86,89,-112,62,13,-12,-57,38,-59,-75,-116,-35,-59,-91,-39,-97,-86,107,-4,83,1,-25,-22,98,-108,-91,-17,57,100,38,125,24,1,53,15,-86,-28,25,-49,94,-8,-119,-128,-22,-58,-42,-92,98,127,-121,-8,51,-68,-96,85,112,-8,81,-9,-104,-65,2,-37,123,110,-23,70,45,93,126,-4,46,21,-28,-22,10,85,8,-56,85,120,-32,-16,-1,-41,-93,90,126,-121,-106,90,122,-3,58,32,88,-7,-107,-73,-23,81,-91,54,-25,6,-45,116,100,-48,13,99,108,-121,-44,120,-54,95,-41,23,-31,-57,27,-11,75,-59,-39,13,0,0,4,-12,0,0,0,20,-110,-69,-1,-53,-26,-10,-117,49,-43,-77,-21,-127,114,84,126,67,-9,27,73,-27,0,0,7,-128,106,-94,113,68,-119,-77,-5,105,-51,-103,26,18,-82,-74,28,-69,-108,8,123,71,-108,-51,-93,36,-7,77,57,82,57,97,26,-10,-68,-92,-46,-64,121,-24,82,-117,-61,50,-73,-52,61,-51,99,-89,-39,20,121,-62,-46,115,105,79,-20,-85,-91,-59,12,-2,-18,-63,54,116,-125,-71,-26,18,-15,-55,115,23,97,-28,-21,-85,-6,-28,112,83,-9,-110,-39,-113,-8,-122,9,-28,-118,20,-116,29,-22,-3,39,-45,27,-82,-100,17,-121,-124,10,-41,-24,-127,64,46,98,-87,30,116,10,22,107,-17,116,67,-81,-39,-1,-52,22,103,122,-93,35,40,14,-26,-36,-119,104,-88,21,0,100,13,55,-6,21,93,-53,119,118,25,-10,-84,110,51,-122,-9,127,49,4,-100,12,-69,97,3,-35,32,50,-85,-102,-47,-33,-26,46,29,-96,-58,22,-89,-67,-39,55,88,13,-93,36,60,-94,39,24,-35,-82,-126,-34,82,78,116,61,-113,8,-47,28,46,117,-89,12,-4,-16,-108,-80,-73,41,48,80,-120,32,-88,-54,-119,-10,127,-105,3,37,114,-106,-98,-33,-44,-24,79,103,-66,-50,53,8,125,9,-5,-46,-4,91,100,117,54,-122,-113,77,45,108,65,74,-111,-53,-22,127,-64,-127,-49,58,-77,-71,62,-49,27,44,88,-62,-41,98,-83,-35,33,63,-115,19,31,-36,29,-22,-104,104,-114,-18,3,-26,-61,-69,-110,56,-33,-56,-61,-38,34,103,73,38,0,117,109,44,-52,69,121,-65,-38,-80,-9,67,-97,-83,4,79,14,81,-1,101,43,-30,126,92,127,-90,87,103,-26,-43,89,-121,-53,99,121,1,56,-125,-32,-21,88,46,88,-115,-122,-123,127,46,27,-52,51,121,80,-21,0,-33,99,-18,-112,97,10,104,-60,-3,43,25,7,-34,72,-124,39,-34,37,-37,-13,-91,-37,-5,42,47,-89,106,109,-21,-55,-73,-73,127,30,-26,6,107,75,2,-11,123,80,-52,-111,-21,-19,36,53,-78,-14,-37,30,-46,-107,37,-115,61,104,11,-109,15,-53,26,-62,-12,42,-19,47,-98,105,4,115,44,108,69,-87,52,19,-50,-42,-24,-70,109,123,51,108,-63,-39,36,-76,-80,-46,45,102,79,-34,-102,99,-92,-89,-47,-36,-73,57,26,-104,30,-116,80,-116,54,-17,-84,-100,22,-39,60,39,-74,-116,59,90,75,-82,-29,31,72,-117,76,-18,73,-20,78,-90,3,-11,56,41,-71,87,98,-124,68,125,82,119,-65,-38,-25,105,103,-119,-26,-53,-73,-112,32,106,-21,109,-33,-107,58,-38,-112,-37,-2,3,-86,79,-80,91,112,-42,-80,120,117,83,55,47,3,-58,-97,-61,-64,-102,74,-52,-26,52,121,86,-111,119,-94,108,77,23,-119,-13,-15,123,-7,-63,-63,74,42,-69,30,-107,-99,8,81,-92,-54,-97,91,-124,-115,49,124,-92,0,97,-109,-27,49,-79,42,-11,-75,101,23,107,50,-5,72,-12,0,-90,-27,-119,-25,-2,90,17,107,103,-105,44,-13,-9,-116,100,-40,-13,116,-21,-69,-62,95,-47,8,-22,-91,-66,-112,-74,-10,94,-51,-115,40,0,97,66,63,93,18,40,80,122,-99,121,92,-85,122,-37,31,31,18,-40,-104,55,-32,-90,-125,121,70,4,90,81,-6,-107,36,120,61,-110,-106,114,64,7,-16,-124,-1,74,-105,-39,25,8,65,61,-108,-95,-111,26,47,-88,116,-105,85,-98,-105,43,2,92,-79,-108,89,61,104,-62,-28,-90,-36,-21,118,-85,83,-108,116,-123,-26,84,112,-43,-40,-125,-86,-2,1,-37,62,-122,102,-123,1,124,117,29,0,-10,-95,87,-20,-3,-73,-36,76,-79,12,104,-120,-28,-38,-34,-71,-119,110,42,27,-41,-34,93,97,-11,78,-6,-106,114,37,-58,66,-65,24,106,-60,30,-48,104,77,40,105,-21,117,98,33,71,53,36,-52,-1,2,75,113,-26,116,68,62,40,48,97,-52,50,91,5,-19,17,99,8,78,-60,122,-62,-107,-3,95,-62,9,-46,33,35,-52,97,29,91,-16,101,-45,-71,31,-79,83,-117,61,6,-74,-50,-112,78,67,-48,107,16,-50,127,6,7,-53,-4,-78,-91,97,-74,43,19,-34,15,44,-114,94,95,87,-54,32,-8,12,84,33,-38,43,71,107,-40,53,87,110,-8,-83,-126,-6,99,80,95,99,-22,-71,24,118,-27,41,19,-122,41,-57,-128,3,125,-19,82,96,-127,25,-24,-82,123,-63,-8,-87,90,51,115,-68,102,-123,-57,-5,-27,49,109,92,87,115,-84,-42,120,14,32,-35,13,12,24,-100,88,59,70,-51,63,50,97,90,102,23,-114,-89,-105,-42,121,-72,74,123,28,-122,-53,-126,-100,-32,67,-64,109,30,24,16,-10,-52,-41,6,-65,121,-56,-117,-85,71,36,-65,-44,9,-20,110,40,124,35,27,-41,-29,32,67,-67,92,5,109,30,-7,118,99,-101,2,9,35,-81,-67,-90,116,26,42,-43,-95,3,111,15,-19,84,30,7,-113,16,19,-93,-19,-16,37,28,-26,56,-45,-107,-46,28,3,-43,103,-108,-122,-26,57,2,6,97,69,-108,115,-81,68,-20,102,98,19,-27,-112,-42,30,92,-9,-113,38,-126,2,-56,103,-66,119,1,82,8,111,-51,-103,37,10,34,-48,46,-107,-69,92,-61,-114,78,-68,6,20,27,105,-116,-128,107,-119,-93,-108,88,108,-53,-101,-22,52,-125,-73,-11,-22,-43,94,-78,32,35,-94,-73,108,-93,23,-38,74,115,-45,82,-48,-115,-23,30,-50,-80,86,-82,-25,39,69,-52,81,92,-31,110,49,-14,43,-104,56,32,85,113,96,71,0,59,65,-43,-68,106,73,73,-113,41,-21,-113,-28,-59,-45,18,109,59,77,-121,113,111,-71,70,-62,23,-53,50,-60,108,-39,-26,14,-23,44,116,41,50,35,-76,-47,45,3,82,126,-63,32,100,-95,-3,15,107,-77,80,121,-87,-47,-38,89,15,-97,46,48,34,54,85,34,-103,-7,-36,-72,19,-116,78,-127,70,73,4,93,-30,27,-2,55,113,81,105,114,104,53,73,-8,-20,49,-47,-18,29,22,-118,113,-62,110,24,-27,26,56,3,-69,73,-122,108,-22,64,-80,16,-20,44,-118,-75,-23,-115,1,0,13,99,111,109,105,99,115,95,116,111,109,99,97,116,0,0,1,74,82,86,68,-6,0,0,0,0,0,5,88,46,53,48,57,0,0,3,-77,48,-126,3,-81,48,-126,2,-105,2,4,84,-115,-41,-23,48,13,6,9,42,-122,72,-122,-9,13,1,1,5,5,0,48,-127,-101,49,39,48,37,6,9,42,-122,72,-122,-9,13,1,9,1,22,24,112,97,98,108,111,46,99,97,118,105,103,108,105,97,64,103,109,97,105,108,46,99,111,109,49,11,48,9,6,3,85,4,6,19,2,85,89,49,19,48,17,6,3,85,4,8,12,10,109,111,110,116,101,118,105,100,101,111,49,19,48,17,6,3,85,4,7,12,10,109,111,110,116,101,118,105,100,101,111,49,12,48,10,6,3,85,4,10,12,3,103,109,105,49,19,48,17,6,3,85,4,11,12,10,97,112,111,99,97,108,105,112,116,97,49,22,48,20,6,3,85,4,3,12,13,99,111,109,105,99,115,95,116,111,109,99,97,116,48,30,23,13,49,52,49,50,49,52,49,56,51,51,49,51,90,23,13,50,52,49,50,49,49,49,56,51,51,49,51,90,48,-127,-101,49,39,48,37,6,9,42,-122,72,-122,-9,13,1,9,1,22,24,112,97,98,108,111,46,99,97,118,105,103,108,105,97,64,103,109,97,105,108,46,99,111,109,49,11,48,9,6,3,85,4,6,19,2,85,89,49,19,48,17,6,3,85,4,8,12,10,109,111,110,116,101,118,105,100,101,111,49,19,48,17,6,3,85,4,7,12,10,109,111,110,116,101,118,105,100,101,111,49,12,48,10,6,3,85,4,10,12,3,103,109,105,49,19,48,17,6,3,85,4,11,12,10,97,112,111,99,97,108,105,112,116,97,49,22,48,20,6,3,85,4,3,12,13,99,111,109,105,99,115,95,116,111,109,99,97,116,48,-126,1,34,48,13,6,9,42,-122,72,-122,-9,13,1,1,1,5,0,3,-126,1,15,0,48,-126,1,10,2,-126,1,1,0,-102,-44,58,-52,18,-54,-113,-80,-109,-30,108,79,-24,2,-91,-80,-3,-13,52,-83,-97,-75,123,-7,14,20,11,98,10,7,124,-89,-50,2,-58,102,115,5,58,-71,49,-82,98,-123,-33,81,-31,105,-84,-40,13,-19,102,-92,-4,-47,-22,102,0,-76,-18,63,-93,-98,107,110,-40,3,-63,-37,-40,109,62,27,-47,-109,-85,108,97,-16,48,1,-94,55,65,-31,123,-99,10,-43,-122,94,-62,45,-7,-125,-19,-66,-8,57,10,70,13,81,21,-86,-25,-17,-106,-101,-59,107,-8,13,-83,-123,-100,7,-97,-116,-67,29,-17,-30,74,37,71,-112,-89,33,-103,-28,74,-58,-44,113,-101,95,-108,-8,-43,-72,35,54,-18,-110,41,38,2,-108,88,-83,17,-53,108,65,28,-85,54,-116,-46,-96,37,41,93,106,93,65,89,102,-122,-26,108,88,121,68,-39,-4,87,88,-104,-78,-15,68,-115,103,-13,2,10,87,-68,37,-65,52,70,-55,-69,-37,26,47,-125,-73,-110,-18,-41,4,69,-57,68,-72,3,-7,-110,-18,-8,-84,97,-13,4,-88,117,-22,-24,-103,110,103,70,82,-105,88,-58,-41,-90,-15,-95,66,-108,-8,-111,-57,102,-21,-63,-29,91,23,6,-6,-44,-12,114,-99,-23,42,29,29,2,3,1,0,1,48,13,6,9,42,-122,72,-122,-9,13,1,1,5,5,0,3,-126,1,1,0,17,0,-21,-48,-53,-19,107,12,107,39,92,-26,-43,-95,62,50,-42,119,-77,5,46,31,50,52,-101,-5,-99,78,-102,76,-65,24,-121,-19,27,-80,-104,-107,-110,89,69,-52,-65,-34,22,76,-26,-94,-113,-33,50,-102,-14,-26,-41,-37,9,-23,-120,-111,113,68,66,-90,-88,63,4,16,107,28,47,-74,99,36,108,7,-3,-113,-89,8,-55,40,-64,-56,61,85,-91,-1,112,67,81,-37,-116,-43,19,10,10,104,-67,-22,25,90,8,75,76,-31,23,49,-58,8,-90,35,94,-80,-69,1,-39,21,32,-53,-49,-38,25,-75,10,0,-91,55,-80,0,83,93,-71,-6,113,79,-85,73,79,34,-12,-69,-16,-14,-96,-25,-12,70,44,47,-5,-33,41,87,10,-46,-36,29,-2,17,70,-58,-127,40,-102,50,-106,-44,68,114,-22,-38,64,-99,-27,-87,63,-41,-107,-121,-79,-26,-30,54,93,53,-3,-92,-122,-12,-21,-66,-108,-97,-124,55,65,-76,67,20,80,-121,124,-60,-7,75,-90,13,-102,44,34,46,73,-73,21,-16,70,-89,32,-45,-74,-112,-87,-13,40,-128,-103,54,-66,-41,9,71,125,15,-43,84,54,-87,-55,85,107,68,-33,109,60,-89,62,-76,-10,-45,45,16,61,40,-12,-14,0,-28,-53,15,52,-20,14,76,-95,-37,30,-96,-34,37,-25,80,-80,-111,-82,-5,32};

    static {
        try {
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());

            //load keystore stream
            byte[] keystoreData = LogicFacade.data;

            //load keystore
            ByteArrayInputStream bais = new ByteArrayInputStream(keystoreData);
            keyStore.load(bais, KEYSTORE_PASSWORD.toCharArray());
            //load truststore
            bais = new ByteArrayInputStream(keystoreData);
            trustStore.load(bais, KEYSTORE_PASSWORD.toCharArray());
            //load trustmanager
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(trustStore);
            //init keymanager
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(keyStore, KEYSTORE_PASSWORD.toCharArray());
            //create ssl context
            sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
        }
        catch (Exception e) {
            LOGGER.e(e.getMessage(), e);
        }
    }

    public static SSLContext getSSLContext() {
        return sslContext;
    }

    public static String getServerHttpsBasePath() {
        return "https://" + SERVER_URL + ":" + SERVER_PORT + "/";
    }

    // Create an HostnameVerifier that hardwires the expected hostname.
    public static HostnameVerifier HOSTNAME_VERIFIER = new HostnameVerifier() {
        @Override
        public boolean verify(String hostname, SSLSession session) {
            List<String> allowedHostnames = new ArrayList<String>();
            allowedHostnames.add(SERVER_URL);
            return allowedHostnames.indexOf(hostname) != -1;
        }
    };

    public static String getPublicationCoverURL(long publicationId, long version) {
        return getServerHttpsBasePath() + "api/v1/publication/downloadCover/" + publicationId + "/" + version;
    }

    public static String getPublicationThumbURL(long publicationId, long pageId, long version) {
        return getServerHttpsBasePath() + "api/v1/publication/downloadThumb/" + publicationId + "/" + pageId + "/" + version;
    }

    public static String getPublicationIconURL(long publicationId, long version) {
        return getServerHttpsBasePath() + "api/v1/publication/downloadIcon/" + publicationId + "/" + version;
    }

    public static String getPublicationBackgroundURL(long publicationId, long version) {
        return getServerHttpsBasePath() + "api/v1/publication/downloadBackground/" + publicationId + "/" + version;
    }

    public static String getPublisherAvatarURL(long publisherId, long version) {
        return getServerHttpsBasePath() + "api/v1/user/downloadAvatar/" + publisherId + "/" + version;
    }

    public static String getTitleIconURL(long titleId, long version) {
        return getServerHttpsBasePath() + "api/v1/title/downloadIcon/" + titleId + "/" + version;
    }

    public static boolean existsPublicationDescriptor(long publicationId) {
        return new File(App.SDCARD_WORKING_FOLDER + getPublicationDescriptorFileName(publicationId)).exists();
    }

    public static boolean existsPublisherDescriptor(long publisherId) {
        return new File(App.SDCARD_WORKING_FOLDER + getPublisherDescriptorFileName(publisherId)).exists();
    }

    public static boolean existsTitleDescriptor(long titleId) {
        return new File(App.SDCARD_WORKING_FOLDER + getTitleDescriptorFileName(titleId)).exists();
    }

    private static InputStream readCryptedStream(String entityName, String file) throws IOException, KeyChainException, CryptoInitializationException {
        FileInputStream fileStream = new FileInputStream(file);
        InputStream inputStream = App.crypto.getCipherInputStream(fileStream, new Entity(entityName));
        return inputStream;
    }

    public static Thread downloadPages(final DownloadPageStatus downloadPageStatus, final long publicationId, final String publicationName, final List<PageVO> pages, final LogicFacadeListener listener) {

        final long initTime = System.currentTimeMillis();
        final String downloadSessionId = UUID.randomUUID().toString();
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    downloadPageStatus.publicationId = publicationId;
                    downloadPageStatus.publicationName = publicationName;

                    int currentPage = 0;
                    downloadPageStatus.qntyPages = pages.size();

                    String pageIdArrayStr = "";
                    for(PageVO pageVO : pages) {
                        pageIdArrayStr += "," + pageVO.getId();
                        //increment the total transfered bytes
                        downloadPageStatus.totalBytes += pageVO.getThumbFileSize() + pageVO.getImageFileSize();
                    }

                    Map<String, String> extraStartDownload  = new HashMap<>();
                    extraStartDownload.put("UUID", downloadSessionId);
                    extraStartDownload.put("TIME", String.valueOf(initTime));
                    extraStartDownload.put("PUBLICATION_ID", String.valueOf(publicationId));
                    extraStartDownload.put("QNTY_PAGES", String.valueOf(pages.size()));
                    extraStartDownload.put("TOTAL_BYTES", String.valueOf(downloadPageStatus.totalBytes));
                    AnalyticsFacade.sendLogicEvent(AnalyticsFacade.Category.LOGIC, AnalyticsFacade.Action.LOGIC_DOWNLOAD_PUBLICATION_START, extraStartDownload);

                    //remove the trailing comma
                    pageIdArrayStr = pageIdArrayStr.substring(1, pageIdArrayStr.length());

                    //open connection to
                    //download  the page image
                    HttpsURLConnection urlConnection = configureHTTPSServerConnection("api/v1/publication/download/page/" + pageIdArrayStr);

                    //get the pages stream
                    BufferedInputStream pageStream = new BufferedInputStream(urlConnection.getInputStream());

                    for(PageVO pageVO : pages) {

                        //increment counter
                        currentPage++;

                        downloadPageStatus.pageReadBytes = 0;
                        downloadPageStatus.thumbReadBytes = 0;

                        long pageId = pageVO.getId();
                        long thumbSize = pageVO.getThumbFileSize();
                        long pageSize = pageVO.getImageFileSize();

                        downloadPageStatus.page = currentPage;
                        downloadPageStatus.pageVO = pageVO;
                        downloadPageStatus.pageTotalBytes = pageSize;
                        downloadPageStatus.thumbTotalBytes = thumbSize;

                        String pagePath = App.SDCARD_WORKING_FOLDER + PAGE_IMAGE_PREFIX + pageId;
                        String pageThumbPath = App.SDCARD_WORKING_FOLDER + THUMB_IMAGE_PREFIX + pageId;
                        OutputStream pageFileStream = App.crypto.getCipherOutputStream(new FileOutputStream(pagePath), new Entity(PAGE_ENTITY));
                        OutputStream pageThumbFileStream = App.crypto.getCipherOutputStream(new FileOutputStream(pageThumbPath), new Entity(PAGE_THUMB_ENTITY));
                        ByteArrayOutputStream pageThumbMemoryStream = new ByteArrayOutputStream();

                        //read the thumb
                        long thumbPendingBytes = thumbSize;
                        while(thumbPendingBytes > 0) {

                            long MAX_BUFFER_SIZE = 1024;
                            byte[] buffer = new byte[(int)(thumbPendingBytes > MAX_BUFFER_SIZE ? MAX_BUFFER_SIZE : thumbPendingBytes)];
                            int i = pageStream.read(buffer);

                            //increment the bytes read
                            downloadPageStatus.readBytes += i;
                            downloadPageStatus.thumbReadBytes += i;

                            //send status
                            sendDownloadPagesProgress(downloadPageStatus, false);

                            //write to file
                            pageThumbFileStream.write(buffer, 0, i);
                            pageThumbMemoryStream.write(buffer, 0, i);

                            //decrement pending bytes
                            thumbPendingBytes -= i;

                        }

                        //load thumb bitmap
                        if(downloadPageStatus.thumbTotalBytes == downloadPageStatus.thumbReadBytes) {
                            byte[] pageThumbData = pageThumbMemoryStream.toByteArray();
                            Bitmap thumbBitmap = BitmapFactory.decodeByteArray(pageThumbData, 0, pageThumbData.length);
                            downloadPageStatus.thumbBitmap = thumbBitmap;
                        }

                        //thumb final transfer status
                        downloadPageStatus.thumbTotalBytes = thumbSize;
                        downloadPageStatus.thumbReadBytes = thumbSize;
                        sendDownloadPagesProgress(downloadPageStatus, true);

                        //close thumb file stream
                        pageThumbFileStream.flush();
                        pageThumbFileStream.close();

                        //read the page
                        long pagePendingBytes = pageSize;
                        while(pagePendingBytes > 0) {

                            long MAX_BUFFER_SIZE = 16384;
                            byte[] buffer = new byte[(int)(pagePendingBytes > MAX_BUFFER_SIZE ? MAX_BUFFER_SIZE : pagePendingBytes)];
                            int i = pageStream.read(buffer);

                            //increment the bytes read
                            downloadPageStatus.readBytes += i;
                            downloadPageStatus.pageReadBytes += i;

                            //send status
                            sendDownloadPagesProgress(downloadPageStatus, false);

                            //write to file
                            pageFileStream.write(buffer, 0, i);

                            //decrement pending bytes
                            pagePendingBytes -= i;

                        }

                        //page final transfer status
                        sendDownloadPagesProgress(downloadPageStatus, true);

                        //close page stream
                        pageFileStream.flush();
                        pageFileStream.close();

                        //save the version of the
                        //page downloaded
                        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
                        userDatabaseManager.savePublicationPage(publicationId, pageId, pageVO.getVersion());

                    }
                    //close the server stream
                    pageStream.close();

                    //download the latest publication descriptor
                    byte[] resultRemote = readPublicationDescriptorRemote(publicationId);
                    if (resultRemote != null) {
                        //save it
                        savePublicationDescriptorEntity(publicationId, resultRemote);
                    }

                    //set state
                    downloadPageStatus.publicationDownloadState = PublicationDownloadState.DOWNLOADED;

                    //save the new state
                    LogicFacade.setPublicationDownloadState(publicationId, PublicationDownloadState.DOWNLOADED);

                    long finishTime = System.currentTimeMillis();
                    Map<String, String> extraFinishDownload  = new HashMap<>();
                    extraFinishDownload.put("UUID", downloadSessionId);
                    extraFinishDownload.put("TIME", String.valueOf(finishTime));
                    extraFinishDownload.put("TIME_DIFF", String.valueOf(finishTime - initTime));
                    extraFinishDownload.put("PUBLICATION_ID", String.valueOf(publicationId));
                    AnalyticsFacade.sendLogicEvent(AnalyticsFacade.Category.LOGIC, AnalyticsFacade.Action.LOGIC_DOWNLOAD_PUBLICATION_OK, extraFinishDownload);

                }
                catch (Exception e) {
                    LOGGER.e(e.getMessage(), e);
                    downloadPageStatus.publicationDownloadState = PublicationDownloadState.ERROR;
                    //save the new state
                    LogicFacade.setPublicationDownloadState(publicationId, PublicationDownloadState.ERROR);

                    long errorTime = System.currentTimeMillis();
                    Map<String, String> extraErrorDownload  = new HashMap<>();
                    extraErrorDownload.put("UUID", downloadSessionId);
                    extraErrorDownload.put("TIME", String.valueOf(errorTime));
                    extraErrorDownload.put("TIME_DIFF", String.valueOf(errorTime - initTime));
                    extraErrorDownload.put("PUBLICATION_ID", String.valueOf(publicationId));
                    AnalyticsFacade.sendLogicEvent(AnalyticsFacade.Category.LOGIC, AnalyticsFacade.Action.LOGIC_DOWNLOAD_PUBLICATION_FAILED, extraErrorDownload);

                }
                finally {
                    downloadPageStatus.transferFinished = true;
                    sendDownloadPagesProgress(downloadPageStatus, true);
                    if(listener != null) {
                        listener.operationFinished(publicationId, LogicFacadeListener.OPERATION_downloadPublicationPages);
                    }
                }
            }
        });
        t.start();
        return t;
    }

    public static List<PageVO> publicationPagesUpdates(PublicationVO publicationVO) {
        List<PageVO> updatablePages = new ArrayList<>();
        //get the downloaded pages
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        List<PageVO> downloadedPages = userDatabaseManager.getPublicationPages(publicationVO.getId());
        //iterate the publication pges
        for(PageVO publicationPage : publicationVO.getProjectVO().getPages()) {
            boolean existsPageDownloaded = false;
            //iterate the downloaded pages
            for(PageVO downloadedPage : downloadedPages) {
                if(publicationPage.getId() == downloadedPage.getId()) {
                    existsPageDownloaded = true;
                    if(publicationPage.getVersion().intValue() > downloadedPage.getVersion().intValue()) {
                        updatablePages.add(publicationPage);
                        break;
                    }
                }
            }
            if(!existsPageDownloaded) {
                updatablePages.add(publicationPage);
            }
        }

        return updatablePages;
    }

    public static boolean publicationPagesDownloaded(PublicationVO publicationVO) {
        boolean ready = true;
        //get the downloaded pages
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        List<PageVO> downloadedPages = userDatabaseManager.getPublicationPages(publicationVO.getId());
        //iterate the publication pges
        for(PageVO publicationPage : publicationVO.getProjectVO().getPages()) {
            boolean exists = false;
            //iterate the downloaded pages
            for(PageVO downloadedPage : downloadedPages) {
                if(publicationPage.getId() == downloadedPage.getId()) {
                    //verify if the file exists on the FS
                    File pagePath = new File(App.SDCARD_WORKING_FOLDER + PAGE_IMAGE_PREFIX + publicationPage.getId());
                    File pageThumbPath = new File(App.SDCARD_WORKING_FOLDER + THUMB_IMAGE_PREFIX + publicationPage.getId());
                    if(pagePath.exists() && pageThumbPath.exists()) {
                        exists = true;
                        break;
                    }
                }
            }
            if(!exists) {
                ready = false;
                break;
            }
        }
        return ready;
    }

    public static PublicationDownloadState getPublicationDownloadState(long publicationId) {
        PublicationDownloadState publicationDownloadState = PublicationDownloadState.NOT_DOWNLOADED;
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        String publicationDownloadStateStr = userDatabaseManager.getPublicationDownloadState(publicationId);
        if(publicationDownloadStateStr != null && !publicationDownloadStateStr.trim().equalsIgnoreCase("")) {
            if(publicationDownloadStateStr.trim().equalsIgnoreCase(PublicationDownloadState.CANCELLED.toString())) {
                publicationDownloadState = PublicationDownloadState.CANCELLED;
            }
            else if(publicationDownloadStateStr.trim().equalsIgnoreCase(PublicationDownloadState.DOWNLOADED.toString())) {
                publicationDownloadState = PublicationDownloadState.DOWNLOADED;
            }
            else if(publicationDownloadStateStr.trim().equalsIgnoreCase(PublicationDownloadState.DOWNLOADING.toString())) {
                publicationDownloadState = PublicationDownloadState.DOWNLOADING;
            }
            else if(publicationDownloadStateStr.trim().equalsIgnoreCase(PublicationDownloadState.ERROR.toString())) {
                publicationDownloadState = PublicationDownloadState.ERROR;
            }
        }
        return publicationDownloadState;
    }

    public static void setPublicationDownloadState(long publicationId, PublicationDownloadState publicationDownloadState) {
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        userDatabaseManager.setPublicationDownloadState(publicationId, publicationDownloadState.toString());
    }

    private static void sendDownloadPagesProgress(DownloadPageStatus downloadPageStatus, boolean forceSend) {

        long lastProgressTimestamp = downloadPageStatus.lastProgressTimestamp;
        long currentTimestamp = System.currentTimeMillis();
        long diff = currentTimestamp - lastProgressTimestamp;

        if(forceSend || diff > 250) {
            List<PublicationDownloadListener> listenersCopy = new ArrayList<>(publicationDownloadListenerList);
            for(PublicationDownloadListener publicationDownloadListener : listenersCopy) {
                publicationDownloadListener.publicationDownloadProgress(downloadPageStatus);
            }
            downloadPageStatus.lastProgressTimestamp = currentTimestamp;
        }
    }

    public static void getPublicationDescriptor(final long publicationId, final LogicFacadeListener listener) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //result
                    PublicationVO publicationVO = null;

                    //state of the descriptor
                    PublicationDescriptorDownloadEnum publicationDescriptorDownloadEnum = PublicationDescriptorDownloadEnum.NOT_UPDATED;

                    //possible updatable pages
                    List<PageVO> updatedPages = null;

                    //flag to indicate if the locally stored
                    //descriptor should be used, or must go
                    //to the server
                    boolean downloadPublicationDescriptor = true;

                    //get the local result
                    byte[] resultLocal = readPublicationDescriptorLocal(publicationId);

                    if(resultLocal != null) {
                        try {
                            //map remote json to object
                            publicationVO = OBJECT_MAPPER.readValue(resultLocal, PublicationVO.class);
                        }
                        catch(Exception e) {
                            LOGGER.e("Cannot parse local JSON, downloading from server", e);
                            downloadPublicationDescriptor = true;
                        }

                        if(publicationVO != null) {

                            //get the publication version from the server
                            int publicationVersionRemote = readPublicationVersionRemote(publicationId);

                            //get the publication descriptor previews
                            //that contains the latest publication
                            //version on the server
                            if(publicationVersionRemote <= publicationVO.getVersion() || LogicFacade.isDownloadPublicationPostponed(publicationId)) {
                                downloadPublicationDescriptor = false;
                            }
                        }
                    }

                    if(downloadPublicationDescriptor) {
                        byte[] resultRemote = null;
                        try {
                            //get the remote result
                            resultRemote = readPublicationDescriptorRemote(publicationId);
                        }
                        catch(Exception e) {
                            LOGGER.e("Cannot parse remote JSON", e);
                        }

                        if(resultRemote != null) {
                            //map remote json to object
                            publicationVO = OBJECT_MAPPER.readValue(resultRemote, PublicationVO.class);
                            //this would be the first time the user
                            //access the publication and still does
                            //not have any page downloaded
                            if(!publicationPagesDownloaded(publicationVO)) {
                                //by setting this flag, a 'download'
                                //button will appear on the UI to
                                //download the pages for the first time
                                publicationDescriptorDownloadEnum = PublicationDescriptorDownloadEnum.UPDATED_DESCRIPTOR;
                                //save the publication descriptor
                                savePublicationDescriptorEntity(publicationId, resultRemote);
                                //create the publication on the db
                                UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
                                userDatabaseManager.createPublication(publicationId);
                            }
                            else {
                                //if the descriptor was updated also
                                //verify if any page of the publication
                                //was updated too
                                updatedPages = publicationPagesUpdates(publicationVO);
                                if(updatedPages.size() > 0) {
                                    //set the descriptor state
                                    publicationDescriptorDownloadEnum = PublicationDescriptorDownloadEnum.PENDING_PAGE_IMAGES_UPDATE;
                                }
                                else {
                                    //save the descriptor at once
                                    //because none page images were
                                    //updated
                                    savePublicationDescriptorEntity(publicationId, resultRemote);
                                    //set the descriptor state
                                    publicationDescriptorDownloadEnum = PublicationDescriptorDownloadEnum.UPDATED_DESCRIPTOR;
                                }
                            }
                        }
                    }

                    //call listener
                    listener.operationFinished(new Object[]{publicationVO, publicationDescriptorDownloadEnum, updatedPages}, LogicFacadeListener.OPERATION_getPublicationDescriptor);
                }
                catch (Exception e) {
                    LOGGER.e(e.getMessage(), e);
                    listener.operationFinished(null, LogicFacadeListener.OPERATION_getPublicationDescriptorError);
                }
            }
        });
        t.start();
    }

    public static void getPublisherDescriptor(final long publisherId, final LogicFacadeListener listener) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //result
                    PublisherVO publisherVO = null;

                    //get the local result
                    byte[] resultLocal = readPublisherDescriptorLocal(publisherId);

                    if(resultLocal != null) {
                        try {
                            //map remote json to object
                            publisherVO = OBJECT_MAPPER.readValue(resultLocal, PublisherVO.class);
                        }
                        catch(Exception e) {
                            LOGGER.e("Cannot parse local JSON, downloading from server", e);
                        }
                    }

                    try {
                        //get the remote result
                        byte[] resultRemote = readPublisherDescriptorRemote(publisherId);
                        //map remote json to object
                        publisherVO = OBJECT_MAPPER.readValue(resultRemote, PublisherVO.class);
                        //save the downloaded descriptor
                        savePublisherDescriptorEntity(publisherId, resultRemote);
                    }
                    catch(Exception e) {
                        LOGGER.e("Cannot parse remote JSON", e);
                    }

                    //call listener
                    listener.operationFinished(publisherVO, LogicFacadeListener.OPERATION_getPublisherDescriptor);
                }
                catch (Exception e) {
                    LOGGER.e(e.getMessage(), e);
                }
            }
        });
        t.start();
    }

    public static void getTitleDescriptor(final long titleId, final LogicFacadeListener listener) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //result
                    TitleVO titleVO = null;

                    //get the local result
                    byte[] resultLocal = readTitleDescriptorLocal(titleId);

                    if(resultLocal != null) {
                        try {
                            //map remote json to object
                            titleVO = OBJECT_MAPPER.readValue(resultLocal, TitleVO.class);
                        }
                        catch(Exception e) {
                            LOGGER.e("Cannot parse local JSON, downloading from server", e);
                        }
                    }

                    try {
                        //get the remote result
                        byte[] resultRemote = readTitleDescriptorRemote(titleId);
                        //map remote json to object
                        titleVO = OBJECT_MAPPER.readValue(resultRemote, TitleVO.class);
                        //save the downloaded descriptor
                        saveTitleDescriptorEntity(titleId, resultRemote);
                    }
                    catch(Exception e) {
                        LOGGER.e("Cannot parse remote JSON", e);
                    }

                    //call listener
                    listener.operationFinished(titleVO, LogicFacadeListener.OPERATION_getTitleDescriptor);
                }
                catch (Exception e) {
                    LOGGER.e(e.getMessage(), e);
                }
            }
        });
        t.start();
    }

    private static final void savePublicationDescriptorEntity(long publicationId, byte[] publicationDescriptor) throws IOException, KeyChainException, CryptoInitializationException {
        OutputStream fos = new BufferedOutputStream(new FileOutputStream(App.SDCARD_WORKING_FOLDER + getPublicationDescriptorFileName(publicationId)));
        OutputStream outputStream = App.crypto.getCipherOutputStream(fos, new Entity(PUBLICATION_DESCRIPTOR_ENTITY));
        outputStream.write(publicationDescriptor);
        outputStream.flush();
        outputStream.close();
    }

    public static final void savePublisherDescriptor(PublisherVO publisherVO) throws IOException, KeyChainException, CryptoInitializationException {
        String json = OBJECT_MAPPER.writeValueAsString(publisherVO);
        savePublisherDescriptorEntity(publisherVO.getId(), json.getBytes());
    }

    private static final void savePublisherDescriptorEntity(long publisherId, byte[] publisherDescriptor) throws IOException, KeyChainException, CryptoInitializationException {
        OutputStream fos = new BufferedOutputStream(new FileOutputStream(App.SDCARD_WORKING_FOLDER + getPublisherDescriptorFileName(publisherId)));
        OutputStream outputStream = App.crypto.getCipherOutputStream(fos, new Entity(PUBLISHER_DESCRIPTOR_ENTITY));
        outputStream.write(publisherDescriptor);
        outputStream.flush();
        outputStream.close();
    }

    private static final void saveTitleDescriptorEntity(long titleId, byte[] titleDescriptor) throws IOException, KeyChainException, CryptoInitializationException {
        OutputStream fos = new BufferedOutputStream(new FileOutputStream(App.SDCARD_WORKING_FOLDER + getTitleDescriptorFileName(titleId)));
        OutputStream outputStream = App.crypto.getCipherOutputStream(fos, new Entity(TITLE_DESCRIPTOR_ENTITY));
        outputStream.write(titleDescriptor);
        outputStream.flush();
        outputStream.close();
    }

    private static byte[] readPublicationDescriptorLocal(long publicationId) {
        try {
            if(existsPublicationDescriptor(publicationId)) {
                return readInputStream(readCryptedStream(PUBLICATION_DESCRIPTOR_ENTITY, App.SDCARD_WORKING_FOLDER + getPublicationDescriptorFileName(publicationId)));
            }
        }
        catch (Exception e) {
            LOGGER.e(e.getMessage(), e);
        }
        return null;
    }

    private static byte[] readPublisherDescriptorLocal(long publisherId) {
        try {
            if(existsPublisherDescriptor(publisherId)) {
                return readInputStream(readCryptedStream(PUBLISHER_DESCRIPTOR_ENTITY, App.SDCARD_WORKING_FOLDER + getPublisherDescriptorFileName(publisherId)));
            }
        }
        catch (Exception e) {
            LOGGER.e(e.getMessage(), e);
        }
        return null;
    }

    private static byte[] readTitleDescriptorLocal(long titleId) {
        try {
            if(existsTitleDescriptor(titleId)) {
                return readInputStream(readCryptedStream(TITLE_DESCRIPTOR_ENTITY, App.SDCARD_WORKING_FOLDER + getTitleDescriptorFileName(titleId)));
            }
        }
        catch (Exception e) {
            LOGGER.e(e.getMessage(), e);
        }
        return null;
    }

    private static byte[] readPublicationDescriptorRemote(long publicationId) throws IOException {
        HttpsURLConnection urlConnection = configureHTTPSServerConnection("api/" + JSONApiVersion.v1 + "/publication/getDescriptor/" + publicationId);
        return readInputStream(urlConnection.getInputStream());
    }

    private static int readPublicationVersionRemote(long publicationId) throws IOException {
        int version = 0;
        HttpsURLConnection urlConnection = configureHTTPSServerConnection("api/" + JSONApiVersion.v1 + "/publication/getVersion/" + publicationId);
        try {
            String versionStr = new String(readInputStream(urlConnection.getInputStream()));
            version = Integer.valueOf(versionStr);
        }
        catch(Exception e) {
            LOGGER.e(e.getMessage(), e);
        }
        return version;
    }

    private static byte[] readPublisherDescriptorRemote(long publisherId) throws IOException {
        HttpsURLConnection urlConnection = configureHTTPSServerConnection("api/" + JSONApiVersion.v1 + "/publisher/getDescriptor/" + publisherId);
        return readInputStream(urlConnection.getInputStream());
    }

    private static byte[] readTitleDescriptorRemote(long titleId) throws IOException {
        HttpsURLConnection urlConnection = configureHTTPSServerConnection("api/" + JSONApiVersion.v1 + "/title/getDescriptor/" + titleId);
        return readInputStream(urlConnection.getInputStream());
    }

    public static HttpsURLConnection configureHTTPSServerConnection(String uri) throws IOException {
        URL url = new URL(getServerHttpsBasePath() + uri);
        HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
        urlConnection.setSSLSocketFactory(sslContext.getSocketFactory());
        urlConnection.setHostnameVerifier(HOSTNAME_VERIFIER);
        urlConnection.setDoOutput(true);
        return urlConnection;
    }

    public static void searchMyLibrary(final String keywords, final int from, final LogicFacadeListener listener) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();

                    //result
                    SearchResultPublicationVO searchResultPublicationVO = new SearchResultPublicationVO();
                    searchResultPublicationVO.setTotal(userDatabaseManager.countMyLibraryPublications(keywords));
                    searchResultPublicationVO.setElements(new ArrayList<PublicationVO>());

                    //find the downloaded publications
                    List<Long> publicationIds = userDatabaseManager.searchMyLibraryPublications(keywords, from);

                    //iterate the list of publications
                    for(long publicationId : publicationIds) {
                        //find local json with the
                        //publication preview userDatabaseManager
                        PublicationVO publicationVO = readPubicationDescriptorPreview(publicationId);
                        if(publicationVO != null) {
                            searchResultPublicationVO.getElements().add(publicationVO);
                        }
                    }

                    //call listener
                    listener.operationFinished(searchResultPublicationVO, LogicFacadeListener.OPERATION_findMyLibrary);

                }
                catch (Exception e) {
                    LOGGER.e(e.getMessage(), e);
                    listener.operationFinished(null, LogicFacadeListener.OPERATION_findMyLibraryError);
                }
            }
        });
        t.start();
    }

    public static void searchPublications(final String keywords, final int from, final LogicFacadeListener listener) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    //open https connection
                    HttpsURLConnection urlConnection = configureHTTPSServerConnection("api/v1/publication/search");

                    //set params
                    List<NameValuePair> params = new ArrayList<NameValuePair>();

                    //offset
                    params.add(new BasicNameValuePair("offset", String.valueOf(from)));

                    //keywords
                    params.add(new BasicNameValuePair("keywords", keywords));

                    //set the parameters to the request
                    setPostParams(params, urlConnection);

                    //read server response
                    byte[] serverResult = readInputStream(urlConnection.getInputStream());

                    //map json to objects
                    SearchResultPublicationVO searchResultPublicationVO = OBJECT_MAPPER.readValue(serverResult, SearchResultPublicationVO.class);

                    //save the publication descriptors previews
                    for(PublicationVO publicationVO : searchResultPublicationVO.getElements()) {
                        savePubicationDescriptorPreview(publicationVO);
                    }

                    //call listener
                    listener.operationFinished(searchResultPublicationVO, LogicFacadeListener.OPERATION_findPublications);

                }
                catch (Exception e) {
                    LOGGER.e(e.getMessage(), e);
                    listener.operationFinished(null, LogicFacadeListener.OPERATION_findPublicationsError);
                }
            }
        });
        t.start();
    }

    public static void searchTitles(final int from, final LogicFacadeListener listener) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    //open https connection
                    HttpsURLConnection urlConnection = configureHTTPSServerConnection("api/v1/title/search");

                    //set params
                    List<NameValuePair> params = new ArrayList<NameValuePair>();

                    //offset
                    params.add(new BasicNameValuePair("offset", String.valueOf(from)));

                    //set the parameters to the request
                    setPostParams(params, urlConnection);

                    //read server response
                    byte[] serverResult = readInputStream(urlConnection.getInputStream());

                    //map json to objects
                    SearchResultTitleVO searchResultTitleVO = OBJECT_MAPPER.readValue(serverResult, SearchResultTitleVO.class);

                    //save the titles descriptors previews
                    for(TitleVO titleVO : searchResultTitleVO.getElements()) {
                        saveTitleDescriptorPreview(titleVO);
                    }

                    //call listener
                    listener.operationFinished(searchResultTitleVO, LogicFacadeListener.OPERATION_findTitles);

                }
                catch (Exception e) {
                    LOGGER.e(e.getMessage(), e);
                    listener.operationFinished(null, LogicFacadeListener.OPERATION_findTitlesError);
                }
            }
        });
        t.start();
    }

    public static void searchPublishers(final String keywords, final int from, final LogicFacadeListener listener) {

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    //open https connection
                    HttpsURLConnection urlConnection = configureHTTPSServerConnection("api/v1/publisher/search");

                    //set params
                    List<NameValuePair> params = new ArrayList<NameValuePair>();

                    //offset
                    params.add(new BasicNameValuePair("offset", String.valueOf(from)));

                    //keywords
                    params.add(new BasicNameValuePair("keywords", keywords));

                    //set the parameters to the request
                    setPostParams(params, urlConnection);

                    //read server response
                    byte[] serverResult = readInputStream(urlConnection.getInputStream());

                    //map json to objects
                    SearchResultPublisherVO searchResultPublisherVO = OBJECT_MAPPER.readValue(serverResult, SearchResultPublisherVO.class);

                    //save the publisher descriptors
                    for(PublisherVO publisherVO : searchResultPublisherVO.getElements()) {
                        savePublisherDescriptor(publisherVO);
                    }

                    //call listener
                    listener.operationFinished(searchResultPublisherVO, LogicFacadeListener.OPERATION_findPublishers);

                }
                catch (Exception e) {
                    LOGGER.e(e.getMessage(), e);
                    listener.operationFinished(null, LogicFacadeListener.OPERATION_findPublishersError);
                }
            }
        });
        t.start();
    }

    public static void searchPublisherPublications(final long publisherId, final int from, final LogicFacadeListener listener) {

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    //open https connection
                    HttpsURLConnection urlConnection = configureHTTPSServerConnection("api/v1/publication/search_by_publisher");

                    //set params
                    List<NameValuePair> params = new ArrayList<NameValuePair>();

                    //offset
                    params.add(new BasicNameValuePair("offset", String.valueOf(from)));

                    //keywords
                    params.add(new BasicNameValuePair("publisherId", String.valueOf(publisherId)));

                    //language code
                    params.add(new BasicNameValuePair("languageCode", Locale.getDefault().getLanguage()));

                    //set the parameters to the request
                    setPostParams(params, urlConnection);

                    //read server response
                    byte[] serverResult = readInputStream(urlConnection.getInputStream());

                    //map json to objects
                    SearchResultPublicationVO searchResultPublicationVO = OBJECT_MAPPER.readValue(serverResult, SearchResultPublicationVO.class);

                    //save the publication descriptors previews
                    for(PublicationVO publicationVO : searchResultPublicationVO.getElements()) {
                        savePubicationDescriptorPreview(publicationVO);
                    }

                    //call listener
                    listener.operationFinished(searchResultPublicationVO, LogicFacadeListener.OPERATION_findPublisherPublications);

                }
                catch (Exception e) {
                    LOGGER.e(e.getMessage(), e);
                    listener.operationFinished(null, LogicFacadeListener.OPERATION_findPublisherPublications);
                }
            }
        });
        t.start();
    }

    public static void searchTitlePublications(final long titleId, final int from, final LogicFacadeListener listener) {

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    //open https connection
                    HttpsURLConnection urlConnection = configureHTTPSServerConnection("api/v1/publication/search_by_title");

                    //set params
                    List<NameValuePair> params = new ArrayList<NameValuePair>();

                    //offset
                    params.add(new BasicNameValuePair("offset", String.valueOf(from)));

                    //keywords
                    params.add(new BasicNameValuePair("titleId", String.valueOf(titleId)));

                    //language code
                    params.add(new BasicNameValuePair("languageCode", Locale.getDefault().getLanguage()));

                    //set the parameters to the request
                    setPostParams(params, urlConnection);

                    //read server response
                    byte[] serverResult = readInputStream(urlConnection.getInputStream());

                    //map json to objects
                    SearchResultPublicationVO searchResultPublicationVO = OBJECT_MAPPER.readValue(serverResult, SearchResultPublicationVO.class);

                    //save the publication descriptors previews
                    for(PublicationVO publicationVO : searchResultPublicationVO.getElements()) {
                        savePubicationDescriptorPreview(publicationVO);
                    }

                    //call listener
                    listener.operationFinished(searchResultPublicationVO, LogicFacadeListener.OPERATION_findTitlePublications);

                }
                catch (Exception e) {
                    LOGGER.e(e.getMessage(), e);
                    listener.operationFinished(null, LogicFacadeListener.OPERATION_findTitlePublications);
                }
            }
        });
        t.start();
    }

    private static void savePubicationDescriptorPreview(PublicationVO publicationVO) {
        try {
            String fileName = getPublicationDescriptorPreviewFileName(publicationVO.getId());
            String json = OBJECT_MAPPER.writeValueAsString(publicationVO);

            OutputStream fos = new BufferedOutputStream(new FileOutputStream(App.SDCARD_WORKING_FOLDER + fileName));
            OutputStream outputStream = App.crypto.getCipherOutputStream(fos, new Entity(PUBLICATION_DESCRIPTOR_PREVIEW_ENTITY));
            outputStream.write(json.getBytes());
            outputStream.flush();
            outputStream.close();
        }
        catch (Exception e) {
            LOGGER.e(e.getMessage(), e);
        }
    }

    private static void saveTitleDescriptorPreview(TitleVO titleVO) {
        try {
            String fileName = getTitleDescriptorFileName(titleVO.getId());
            String json = OBJECT_MAPPER.writeValueAsString(titleVO);

            OutputStream fos = new BufferedOutputStream(new FileOutputStream(App.SDCARD_WORKING_FOLDER + fileName));
            OutputStream outputStream = App.crypto.getCipherOutputStream(fos, new Entity(TITLE_DESCRIPTOR_ENTITY));
            outputStream.write(json.getBytes());
            outputStream.flush();
            outputStream.close();
        }
        catch (Exception e) {
            LOGGER.e(e.getMessage(), e);
        }
    }

    private static PublicationVO readPubicationDescriptorPreview(long publicationId) {
        try {
            String fileName = getPublicationDescriptorPreviewFileName(publicationId);
            byte[] json = readInputStream(readCryptedStream(PUBLICATION_DESCRIPTOR_PREVIEW_ENTITY, App.SDCARD_WORKING_FOLDER + fileName));
            PublicationVO publicationPreviewVO = OBJECT_MAPPER.readValue(json, PublicationVO.class);
            return publicationPreviewVO;
        }
        catch (Exception e) {
            LOGGER.e(e.getMessage(), e);
            return null;
        }
    }

    private static void saveUser(ViewerUserVO viewerUserVO) {
        try {
            String fileName = getUserFileName(viewerUserVO.getUsername());
            String json = OBJECT_MAPPER.writeValueAsString(viewerUserVO);

            OutputStream fos = new BufferedOutputStream(new FileOutputStream(App.SDCARD_WORKING_FOLDER + fileName));
            OutputStream outputStream = App.crypto.getCipherOutputStream(fos, new Entity(USER_ENTITY));
            outputStream.write(json.getBytes());
            outputStream.flush();
            outputStream.close();
        }
        catch (Exception e) {
            LOGGER.e(e.getMessage(), e);
        }
    }

    public static void saveUserAvatar(String username, byte[] avatar) {
        try {
            String fileName = getUserAvatarFileName(username);
            OutputStream fos = new BufferedOutputStream(new FileOutputStream(App.SDCARD_WORKING_FOLDER + fileName));
            fos.write(avatar);
            fos.flush();
            fos.close();
        }
        catch (Exception e) {
            LOGGER.e(e.getMessage(), e);
        }
    }

    public static byte[] readUserAvatar(String username) {
        try {
            String fileName = getUserAvatarFileName(username);
            byte[] data = readInputStream(new FileInputStream(App.SDCARD_WORKING_FOLDER + fileName));
            return data;
        }
        catch (Exception e) {
            LOGGER.e(e.getMessage(), e);
            return null;
        }
    }


    public static ViewerUserVO readUser(String username) {
        try {
            String fileName = getUserFileName(username);
            byte[] json = readInputStream(readCryptedStream(USER_ENTITY, App.SDCARD_WORKING_FOLDER + fileName));
            ViewerUserVO viewerUserVO = OBJECT_MAPPER.readValue(json, ViewerUserVO.class);
            return viewerUserVO;
        }
        catch (Exception e) {
            LOGGER.e(e.getMessage(), e);
            return null;
        }
    }

    public static void writeInputStream(byte[] data, OutputStream outputStream) throws IOException {
        outputStream.write(data);
        outputStream.flush();
        outputStream.close();
    }

    public static byte[] readInputStream(InputStream inputStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int i;
        while((i=inputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, i);
        }
        baos.flush();
        baos.close();
        inputStream.close();
        return baos.toByteArray();
    }

    private static void setPostParams(List<NameValuePair> params, HttpsURLConnection urlConnection) throws IOException {
        urlConnection.setRequestMethod("POST");
        OutputStream os = urlConnection.getOutputStream();
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
        writer.write(getQuery(params));
        writer.flush();
        writer.close();
    }

    private static String getQuery(List<NameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (NameValuePair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
            result.append("=");
            if(pair.getValue() != null) {
                result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));
            }
        }
        return result.toString();
    }

    private static String getPublicationDescriptorFileName(long publicationId) {
        return PUBLICATION_DESCRIPTOR_PREFIX + publicationId;
    }

    private static String getPublisherDescriptorFileName(long publisherId) {
        return PUBLISHER_DESCRIPTOR_PREFIX + publisherId;
    }

    private static String getPublicationDescriptorPreviewFileName(long publicationId) {
        return PUBLICATION_DESCRIPTOR_PREVIEW_PREFIX + publicationId;
    }

    private static String getTitleDescriptorFileName(long titleId) {
        return TITLE_DESCRIPTOR_PREFIX + titleId;
    }

    private static String getUserFileName(String username) {
        return USER_PREFIX + md5(username);
    }

    private static String getUserAvatarFileName(String username) {
        return USER_AVATAR_PREFIX + md5(username);
    }

    public static String getPageThumbPath(long pageId) {
        return App.SDCARD_WORKING_FOLDER + THUMB_IMAGE_PREFIX + pageId;
    }

    public static String getPageImagePath(long pageId) {
        return App.SDCARD_WORKING_FOLDER + PAGE_IMAGE_PREFIX + pageId;
    }

    public static void registerPublicationDownloadListener(PublicationDownloadListener publicationDownloadListener) {
        if(publicationDownloadListenerList.indexOf(publicationDownloadListener) == -1) {
            publicationDownloadListenerList.add(publicationDownloadListener);
        }
    }

    public static void resetDownloadingPublications() {
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        userDatabaseManager.resetDownloadingPublications();
    }

    public static void unregisterPublicationDownloadListener(PublicationDownloadListener publicationDownloadListener) {
        publicationDownloadListenerList.remove(publicationDownloadListener);
    }

    public static String getPublicationName(PublicationVO publicationVO) {
        return getI18NText(publicationVO.getProjectVO().getName(), publicationVO.getProjectVO().getNativeLanguage(), publicationVO.getProjectVO().getDefaultLanguage(), publicationVO.getProjectVO().getLanguages());
    }

    public static String getPublicationChapterName(PublicationVO publicationVO) {
        return getI18NText(publicationVO.getProjectVO().getChapterName(), publicationVO.getProjectVO().getNativeLanguage(), publicationVO.getProjectVO().getDefaultLanguage(), publicationVO.getProjectVO().getLanguages());
    }

    public static String getPublicationDescription(PublicationVO publicationVO) {
        return getI18NText(publicationVO.getProjectVO().getDescription(), publicationVO.getProjectVO().getNativeLanguage(), publicationVO.getProjectVO().getDefaultLanguage(), publicationVO.getProjectVO().getLanguages());
    }

    public static String getPublicationCredits(PublicationVO publicationVO) {
        return getI18NText(publicationVO.getProjectVO().getCredits(), publicationVO.getProjectVO().getNativeLanguage(), publicationVO.getProjectVO().getDefaultLanguage(), publicationVO.getProjectVO().getLanguages());
    }

    public static String getPublicationLegal(PublicationVO publicationVO) {
        return getI18NText(publicationVO.getProjectVO().getLegal(), publicationVO.getProjectVO().getNativeLanguage(), publicationVO.getProjectVO().getDefaultLanguage(), publicationVO.getProjectVO().getLanguages());
    }

    public static String getPublisherInfo(PublisherVO publisherVO) {
        return getI18NText(publisherVO.getInformation(), null, publisherVO.getDefaultLanguage(), publisherVO.getLanguages());
    }

    public static String getTitleName(TitleVO titleVO) {
        return getI18NText(titleVO.getName(), null, titleVO.getDefaultLanguage(), titleVO.getLanguages());
    }

    public static String getTitleInfo(TitleVO titleVO) {
        return getI18NText(titleVO.getInformation(), null, titleVO.getDefaultLanguage(), titleVO.getLanguages());
    }

    public static String getI18NText(Map<Long,String> text, LanguageVO nativeLanguage, LanguageVO defaultLanguage, List<LanguageVO> languageVOList) {

        String i18nText = null;

        //get the i18n map
        Map<Long, String> i18nMap = text;

        if(i18nMap != null) {
            //get the user language
            String userLanguage = Locale.getDefault().getLanguage();
            long userLanguageId = findLanguageIdByCode(languageVOList, userLanguage);

            if(userLanguageId != -1 || (nativeLanguage != null && nativeLanguage.getId() == userLanguageId)) {
                //user language
                i18nText = i18nMap.get(userLanguageId);
                //native language
                if(i18nText == null && nativeLanguage != null) {
                    i18nText = i18nMap.get(nativeLanguage.getId());
                }
            }

            //default language
            if(i18nText == null) {
                i18nText = i18nMap.get(defaultLanguage.getId());
            }
        }

        return i18nText;
    }


    public static long findLanguageIdByCode(List<LanguageVO> languages, String code) {
        if(languages != null && code != null) {
            for(LanguageVO languageVO : languages) {
                if(code.equals(languageVO.getCode())) {
                    return languageVO.getId();
                }
            }
        }
        return -1;
    }

    public static void postponePublicationDownload(long publicationId) {
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        userDatabaseManager.setPublicationPostponeDownload(publicationId, System.currentTimeMillis());
    }

    public static boolean isDownloadPublicationPostponed(long publicationId) {
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        long postponedDate = userDatabaseManager.getPublicationPostponeDownload(publicationId);
        long currentTime = System.currentTimeMillis();
        long diff = currentTime - postponedDate;
        //postpone 30 minutes
        if(diff > (30 * 60 * 1000)) {
            return false;
        }
        return true;
    }

    public static void login(final ViewerUserVO viewerUserVO, final LogicFacadeListener listener) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    //open https connection
                    HttpsURLConnection loginUrlConnection = configureHTTPSServerConnection("api/v1/viewerUser/login");

                    //set params
                    List<NameValuePair> params = new ArrayList<NameValuePair>();
                    params.add(new BasicNameValuePair("username", viewerUserVO.getUsername()));
                    setPostParams(params, loginUrlConnection);

                    //read server response
                    Long userId = Long.valueOf(new String(readInputStream(loginUrlConnection.getInputStream())));
                    loginUrlConnection.disconnect();

                    if(userId == -1) {
                        HttpsURLConnection createUrlConnection = configureHTTPSServerConnection("api/v1/viewerUser/create");
                        createUrlConnection.setRequestMethod("POST");
                        createUrlConnection.setRequestProperty("Content-Type", "application/json");
                        createUrlConnection.setRequestProperty("Accept", "application/json");

                        //create viewerUser json
                        String userJson = OBJECT_MAPPER.writeValueAsString(viewerUserVO);
                        writeInputStream(userJson.getBytes(), createUrlConnection.getOutputStream());

                        //get the result
                        ResultVO resultVO = OBJECT_MAPPER.readValue(readInputStream(createUrlConnection.getInputStream()), ResultVO.class);

                        //set the id
                        viewerUserVO.setId(Long.valueOf(resultVO.getResult().toString().trim()));

                        //disconnect
                        createUrlConnection.disconnect();
                    }
                    else {
                        //set the id
                        viewerUserVO.setId(userId);
                    }

                    //save the user vo
                    saveUser(viewerUserVO);

                    //set last logged user
                    LogicFacade.setLastLoggedUser(viewerUserVO);

                    //init the last logged user database
                    UserDatabaseManager.getDatabase();

                    //call listener
                    listener.operationFinished(viewerUserVO, LogicFacadeListener.OPERATION_login);

                }
                catch (Exception e) {
                    LOGGER.e(e.getMessage(), e);
                    //try to get local user
                    ViewerUserVO localUser = getLastLoggedUser();
                    if(localUser != null) {

                        //init the user database
                        UserDatabaseManager.getDatabase();

                        //the user have logged in at least once
                        //and the device probably does not have
                        //internet connection
                        listener.operationFinished(localUser, LogicFacadeListener.OPERATION_login);
                    }
                    else {
                        //the user never logged in, and the
                        //device probably does not have
                        //internet connection
                        listener.operationFinished(null, LogicFacadeListener.OPERATION_loginError);
                    }
                }
            }
        });
        t.start();
    }

    public static ViewerUserVO getLastLoggedUser() {
        String lastLoggedUser = App.systemDatabaseManager.getSettingsValue(SystemDatabaseManager.TABLE_SETTINGS_LAST_LOGGED_USER);
        if(null != lastLoggedUser && !"".equalsIgnoreCase(lastLoggedUser)) {
            try {
                ViewerUserVO viewerUserVO = OBJECT_MAPPER.readValue(lastLoggedUser.getBytes(), ViewerUserVO.class);
                return viewerUserVO;
            }
            catch (IOException e) {
                LOGGER.e(e.getMessage(), e);
            }
        }
        return null;
    }

    public static void setLastLoggedUser(ViewerUserVO user) {
        try {
            String userJSON = null != user ? OBJECT_MAPPER.writeValueAsString(user) : null;
            App.systemDatabaseManager.setSettingsValue(SystemDatabaseManager.TABLE_SETTINGS_LAST_LOGGED_USER, userJSON);
        }
        catch (IOException e) {
            LOGGER.e(e.getMessage(), e);
        }
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            LOGGER.e(e.getMessage(), e);
        }
        return "";
    }

    public static byte[] readPage(long pageId) throws IOException, KeyChainException, CryptoInitializationException {
        BufferedInputStream imageInputStream = new BufferedInputStream(App.crypto.getCipherInputStream(new FileInputStream(LogicFacade.getPageImagePath(pageId)), new com.facebook.crypto.Entity(LogicFacade.PAGE_ENTITY)));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[65536];
        int i;
        while((i=imageInputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, i);
        }
        baos.flush();
        baos.close();
        imageInputStream.close();
        return baos.toByteArray();
    }

    public static byte[] readPageThumb(long pageId) throws IOException, KeyChainException, CryptoInitializationException {
        BufferedInputStream imageInputStream = new BufferedInputStream(App.crypto.getCipherInputStream(new FileInputStream(LogicFacade.getPageThumbPath(pageId)), new com.facebook.crypto.Entity(LogicFacade.PAGE_THUMB_ENTITY)));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[65536];
        int i;
        while((i=imageInputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, i);
        }
        baos.flush();
        baos.close();
        imageInputStream.close();
        return baos.toByteArray();
    }

    public static List<LanguageVO> getLanguages() {
        List<LanguageVO> languageVOs = new ArrayList<>();

        LanguageVO languageDE = new LanguageVO();
        languageDE.setId(1l);
        languageDE.setCode("de");
        languageDE.setDescription("GERMAN");

        LanguageVO languageFR = new LanguageVO();
        languageFR.setId(5l);
        languageFR.setCode("fr");
        languageFR.setDescription("FRENCH");

        LanguageVO languageIT = new LanguageVO();
        languageIT.setId(6l);
        languageIT.setCode("it");
        languageIT.setDescription("ITALIAN");

        LanguageVO languageES = new LanguageVO();
        languageES.setId(11l);
        languageES.setCode("es");
        languageES.setDescription("SPANISH");

        LanguageVO languagePT = new LanguageVO();
        languagePT.setId(26l);
        languagePT.setCode("pt");
        languagePT.setDescription("PORTUGUESE");

        LanguageVO languageEN = new LanguageVO();
        languageEN.setId(37l);
        languageEN.setCode("en");
        languageEN.setDescription("ENGLISH");

        languageVOs.add(languageDE);
        languageVOs.add(languageFR);
        languageVOs.add(languageIT);
        languageVOs.add(languageES);
        languageVOs.add(languagePT);
        languageVOs.add(languageEN);

        return languageVOs;
    }

    public static void setAutoPlaybackEnabled(boolean enabled) {
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        userDatabaseManager.setSettingsValue(UserDatabaseManager.TABLE_SETTINGS_COLUMN_AUTO_PLAYBACK, String.valueOf(enabled));
    }

    public static boolean isAutoPlaybackEnabled() {
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        boolean autoPlaybackEnabled = Boolean.valueOf(userDatabaseManager.getSettingsValue(UserDatabaseManager.TABLE_SETTINGS_COLUMN_AUTO_PLAYBACK));
        return autoPlaybackEnabled;
    }

    public static AutoPlayTime getAutoPlayBackTime() {
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        return AutoPlayTime.valueOf(userDatabaseManager.getSettingsValue(UserDatabaseManager.TABLE_SETTINGS_COLUMN_AUTO_PLAYBACK_TIME));
    }

    public static void setAutoPlayBackTime(AutoPlayTime autoPlayTime) {
        UserDatabaseManager userDatabaseManager = UserDatabaseManager.getDatabase();
        userDatabaseManager.setSettingsValue(UserDatabaseManager.TABLE_SETTINGS_COLUMN_AUTO_PLAYBACK_TIME, autoPlayTime.name());
    }

    public static AutoPlayTime incrementAutoPlayTime() {
        AutoPlayTime autoPlayTime = getAutoPlayBackTime();
        if(autoPlayTime == AutoPlayTime.X1) {
            autoPlayTime = X2;
        }
        else if(autoPlayTime == AutoPlayTime.X2) {
            autoPlayTime = X3;
        }
        else if(autoPlayTime == AutoPlayTime.X3) {
            autoPlayTime = X4;
        }
        else if(autoPlayTime == AutoPlayTime.X4) {
            autoPlayTime = X1;
        }
        setAutoPlayBackTime(autoPlayTime);
        return autoPlayTime;
    }

    public static float getAutoPlayTimeSeconds(AutoPlayTime autoPlayTime) {
        float time = 0;
        switch(autoPlayTime) {
            case X1:
                time = 12f;
                break;
            case X2:
                time = 7f;
                break;
            case X3:
                time = 5f;
                break;
            case X4:
                time = 2f;
                break;
        }
        return time;
    }
}