package com.alchemy.thereaderengine;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;

import com.alchemy.thereaderengine.fragment.LoginFragment;
import com.facebook.FacebookSdk;
import com.noveogroup.android.log.Logger;
import com.noveogroup.android.log.LoggerManager;

public class LoginActivity extends FragmentActivity {

    private static final Logger LOGGER = LoggerManager.getLogger();

    //prevent all views from receiving touch events
    public boolean stopReceiveTouchEvents;

    private LoginFragment loginFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());

        if (savedInstanceState == null) {
            // Add the fragment on initial activity setup
            loginFragment = new LoginFragment();
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(android.R.id.content, loginFragment)
                    .commit();
        } else {
            // Or set the fragment from restored state info
            loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentById(android.R.id.content);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loginFragment.onActivityResult(requestCode, resultCode, data);
    }

    protected void onStart() {
        super.onStart();
    }

    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if(!stopReceiveTouchEvents) {
            return super.dispatchTouchEvent(ev);
        }
        else {
            return false;
        }
    }
}