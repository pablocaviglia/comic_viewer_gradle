package com.alchemy.thereaderengine.model;

import android.graphics.Bitmap;

import com.alchemy.thereaderengine.enums.PublicationDownloadState;
import com.apocalipta.comic.vo.v1.PageVO;

/**
 * Created by pablo on 21/01/15.
 */
public class DownloadPageStatus {

    public PublicationDownloadState publicationDownloadState = PublicationDownloadState.NOT_DOWNLOADED;
    public long publicationId;
    public String publicationName;
    public boolean transferFinished;
    public long lastProgressTimestamp;
    public int page;
    public int qntyPages;
    public PageVO pageVO;
    public long totalBytes;
    public long readBytes;
    public long thumbTotalBytes;
    public long thumbReadBytes;
    public long pageTotalBytes;
    public long pageReadBytes;
    public Bitmap thumbBitmap;

}
