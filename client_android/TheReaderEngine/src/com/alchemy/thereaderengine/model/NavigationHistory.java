package com.alchemy.thereaderengine.model;

/**
 * Created by pablo on 28/04/15.
 */
public class NavigationHistory {

    public byte context;
    public Object[] metadata;

    public NavigationHistory(byte context, Object[] metadata) {
        this.context = context;
        this.metadata = metadata;
    }

    public byte getContext() {
        return context;
    }

    public void setContext(byte context) {
        this.context = context;
    }

    public Object[] getMetadata() {
        return metadata;
    }

    public void setMetadata(Object[] metadata) {
        this.metadata = metadata;
    }
}