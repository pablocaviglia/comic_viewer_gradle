package com.alchemy.thereaderengine.enums;

/**
 * Created by pablo on 24/01/15.
 */
public enum PublicationDescriptorDownloadEnum {

    NOT_UPDATED,
    UPDATED_DESCRIPTOR,
    PENDING_PAGE_IMAGES_UPDATE

}
