package com.alchemy.thereaderengine.enums;

/**
 * Created by pablo on 21/01/15.
 */
public enum PageDownloadState {
    NOT_DOWNLOADED,
    DOWNLOADING,
    DOWNLOADED
}