package com.alchemy.thereaderengine.enums;

/**
 * Created by pablo on 21/01/15.
 */
public enum PublicationDownloadState {
    NOT_DOWNLOADED,
    DOWNLOADING,
    CANCELLED,
    ERROR,
    DOWNLOADED
}