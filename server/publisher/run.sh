#!/bin/bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
if [ -f $DIR/tomcat.pid ]; then
  echo 'Killing previous Tomcat run with pid ' `cat tomcat.pid`
  kill -9 `cat tomcat.pid`
fi
nohup java -jar target/*.jar > /dev/null 2>&1 & echo $! > tomcat.pid
