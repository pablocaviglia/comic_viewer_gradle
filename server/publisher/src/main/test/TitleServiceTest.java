import org.gmi.thereaderengine.common.config.ElasticSearchConfig;
import org.gmi.thereaderengine.common.config.PropertiesConfig;
import org.gmi.thereaderengine.common.elasticsearch.document.TitleDocument;
import org.gmi.thereaderengine.common.elasticsearch.repository.TitleRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;

/**
 * Created by pablo on 24/05/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {PropertiesConfig.class, ElasticSearchConfig.class})
public class TitleServiceTest  {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private TitleRepository titleRepository;

    @Test
    public void findAll() throws Exception {
        LOGGER.info("**** findAll****");
        Page<TitleDocument> titleDocumentPage = titleRepository.findAll(new PageRequest(0, 10, new Sort(new Sort.Order(Sort.Direction.ASC, "code"))));
        LOGGER.info("--> Titles found :: " + titleDocumentPage.getTotalElements());
        showTitles(titleDocumentPage.getContent());
    }

    private void showTitles(List<TitleDocument> titleDocuments) {
        if(titleDocuments != null) {
            for(TitleDocument titleDocument : titleDocuments) {
                if(titleDocument != null) {
                    String infoLog = "ID :: " + titleDocument.getId();
                    if(titleDocument.getName() != null && titleDocument.getName().size() > 0) {
                        infoLog += " NAME :: " + titleDocument.getName().get(0).getValue();
                    }
                    LOGGER.info(infoLog);
                }
            }
        }
    }
}