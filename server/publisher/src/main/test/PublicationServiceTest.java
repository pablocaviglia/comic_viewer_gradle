import com.apocalipta.comic.constants.ProjectAgeRate;

import org.gmi.thereaderengine.common.config.ElasticSearchConfig;
import org.gmi.thereaderengine.common.config.PropertiesConfig;
import org.gmi.thereaderengine.common.elasticsearch.document.LanguageDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.PublicationDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.PublisherDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.i18nDocument;
import org.gmi.thereaderengine.common.elasticsearch.repository.PublicationRepository;
import org.gmi.thereaderengine.common.elasticsearch.repository.PublisherRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by pablo on 11/04/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class, classes = {PropertiesConfig.class, ElasticSearchConfig.class})
public class PublicationServiceTest {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd hh:mm");

    @Autowired
    private PublicationRepository publicationRepository;

    @Autowired
    private PublisherRepository publisherRepository;

    //@Test
    public void savePublication() throws Exception {

        LOGGER.info("**** savePublication ****");

        {

            /**
             *
             */
            LanguageDocument languageDocumentEn = new LanguageDocument();
            languageDocumentEn.setId("37");
            languageDocumentEn.setCode("en");
            languageDocumentEn.setDescription("English");

            /**
             *
             */
            LanguageDocument languageDocumentEs = new LanguageDocument();
            languageDocumentEs.setId("14");
            languageDocumentEs.setCode("es");
            languageDocumentEs.setDescription("Spanish");

            /**
             *
             */
            PublicationDocument publicationDocument = new PublicationDocument();
            publicationDocument.setId("100");

            /**
             *
             */
            List<LanguageDocument> languages = new ArrayList<>();
            languages.add(languageDocumentEn);
            languages.add(languageDocumentEs);
            publicationDocument.setLanguages(languages);

            /**
             *
             */
            publicationDocument.setNativeLanguage(languageDocumentEs);

            /**
             *
             */
            publicationDocument.setOrientalReading(Boolean.valueOf(true).toString());

            /**
             *
             */
            publicationDocument.setAgeRate(ProjectAgeRate.ALL.name());


            /**
             *
             */
            List<i18nDocument> names = new ArrayList<>();

            i18nDocument nameEn = new i18nDocument();
            nameEn.setLanguage(languageDocumentEn);
            nameEn.setValue("name in english");
            names.add(nameEn);

            i18nDocument nameEs = new i18nDocument();
            nameEs.setLanguage(languageDocumentEs);
            nameEs.setValue("nombre en español");
            names.add(nameEs);

            publicationDocument.setName(names);


            /**
             *
             */
            List<i18nDocument> descriptions = new ArrayList<>();

            i18nDocument descriptionEn = new i18nDocument();
            descriptionEn.setLanguage(languageDocumentEn);
            descriptionEn.setValue("description in english");
            descriptions.add(descriptionEn);

            i18nDocument descriptionEs = new i18nDocument();
            descriptionEs.setLanguage(languageDocumentEs);
            descriptionEs.setValue("descripcion en español");
            descriptions.add(descriptionEs);

            publicationDocument.setDescription(descriptions);

            /**
             *
             */
            List<i18nDocument> chapterNames = new ArrayList<>();

            i18nDocument chapterNameEn = new i18nDocument();
            chapterNameEn.setLanguage(languageDocumentEn);
            chapterNameEn.setValue("chapter name in english");
            chapterNames.add(chapterNameEn);

            i18nDocument chapterNameEs = new i18nDocument();
            chapterNameEs.setLanguage(languageDocumentEs);
            chapterNameEs.setValue("nombre de capitulo en español");
            chapterNames.add(chapterNameEs);

            publicationDocument.setChapterName(chapterNames);

            /**
             *
             */
            List<i18nDocument> credits = new ArrayList<>();

            i18nDocument creditsEn = new i18nDocument();
            creditsEn.setLanguage(languageDocumentEn);
            creditsEn.setValue("credits in english");
            credits.add(creditsEn);

            i18nDocument creditsEs = new i18nDocument();
            creditsEs.setLanguage(languageDocumentEs);
            creditsEs.setValue("creditos en español");
            credits.add(creditsEs);

            publicationDocument.setCredits(credits);


            List<String> tags = new ArrayList<>();
            tags.add("tag num 1");
            tags.add("tag num 2");
            tags.add("tag num 3");
            tags.add("tag num 4");
            tags.add("tag num 5");
            tags.add("tag num 6");
            tags.add("tag num 7");
            tags.add("tag num 8");
            publicationDocument.setTags(tags);

            //save
            publicationRepository.save(publicationDocument);
        }
    }

    @Test
    public void findByTags() throws Exception {

        LOGGER.info("**** findByTags ****");

        List<String> tags = new ArrayList<>();
        tags.add("1");
        tags.add("2");
        tags.add("7");

        Page<PublicationDocument> publicationDocumentPage = publicationRepository.findByTagsIn(tags, new PageRequest(0, 19));

        LOGGER.info("--> Publications found :: " + publicationDocumentPage.getTotalElements());

        List<PublicationDocument> publicationDocuments = publicationDocumentPage.getContent();

        for(PublicationDocument publicationDocument : publicationDocuments) {
            LOGGER.info("ID :: " + publicationDocument.getId() + "\tNAME :: " + publicationDocument.getName());

            List<String> pubTags = publicationDocument.getTags();
            String tagsStr = "";
            for(String tag : pubTags) {
                tagsStr += tag + ",";
            }
            LOGGER.info("\tTAGS :: '" + tagsStr + "'");
        }
    }

    @Test
    public void findByKeywords() throws Exception {
        LOGGER.info("**** findByKeywords ****");
        Page<PublicationDocument> publicationDocumentPage = publicationRepository.findViaKeywords("pruebabastantelarga", new PageRequest(0, 19));
        LOGGER.info("--> Publications found :: " + publicationDocumentPage.getTotalElements());
        showPublications(publicationDocumentPage.getContent());
    }

    @Test
    public void findAll() throws Exception {
        LOGGER.info("**** findAll ****");
        Page<PublicationDocument> publicationDocumentPage = publicationRepository.findAll(new PageRequest(0, 19, Sort.Direction.DESC, "firstPublicationDate"));
        LOGGER.info("--> Publications found :: " + publicationDocumentPage.getTotalElements());
        showPublications(publicationDocumentPage.getContent());
    }

    @Test
    public void findAllPublishers() throws Exception {
        LOGGER.info("**** findAll ****");
        Page<PublisherDocument> publisherDocumentPage = publisherRepository.findAll(new PageRequest(0, 19, Sort.Direction.ASC, "name"));
        LOGGER.info("--> Publishers found :: " + publisherDocumentPage.getTotalElements());
    }

    @Test
    public void findByNativeLanguage() throws Exception {
        LOGGER.info("**** findByNativeLanguage ****");
        Page<PublicationDocument> publicationDocumentPage = publicationRepository.findByNativeLanguageId("14", new PageRequest(0, 19));
        LOGGER.info("--> Publications found :: " + publicationDocumentPage.getTotalElements());
        showPublications(publicationDocumentPage.getContent());
    }

    @Test
    public void findByLanguages() throws Exception {

        LOGGER.info("**** findByLanguages ****");

        List<String> languageIds = new ArrayList<>();
        languageIds.add("14");
        languageIds.add("37");

        Page<PublicationDocument> publicationDocumentPage = publicationRepository.findByLanguagesIdIn(languageIds, new PageRequest(0, 19));
        LOGGER.info("--> Publications found :: " + publicationDocumentPage.getTotalElements());
        showPublications(publicationDocumentPage.getContent());
    }

    @Test
    public void findByAgeRate() throws Exception {
        LOGGER.info("**** findByAgeRate ****");
        Page<PublicationDocument> publicationDocumentPage = publicationRepository.findByAgeRate(ProjectAgeRate.ALL.name(), new PageRequest(0, 19));
        LOGGER.info("--> Publications found :: " + publicationDocumentPage.getTotalElements());
        showPublications(publicationDocumentPage.getContent());
    }

    @Test
    public void findByOrientalReading() throws Exception {
        LOGGER.info("**** findByOrientalReading ****");
        Page<PublicationDocument> publicationDocumentPage = publicationRepository.findByOrientalReading(Boolean.valueOf(true).toString(), new PageRequest(0, 19));
        LOGGER.info("--> Publications found :: " + publicationDocumentPage.getTotalElements());
        showPublications(publicationDocumentPage.getContent());
    }

    @Test
    public void findById() throws Exception {
        LOGGER.info("**** findById ****");
        PublicationDocument publicationDocumentPage = publicationRepository.findOne("100");
        List<PublicationDocument> publicationDocuments = new ArrayList<>();
        publicationDocuments.add(publicationDocumentPage);
        showPublications(publicationDocuments);
    }

    @Test
    public void findByPublisherId() throws Exception {
        LOGGER.info("**** findByPublisherId ****");
        Page<PublicationDocument> publicationDocumentPage = publicationRepository.findByPublisherId("1", new PageRequest(0, 10));
        LOGGER.info("--> Publications found :: " + publicationDocumentPage.getTotalElements());
        showPublications(publicationDocumentPage.getContent());
    }

    private void showPublications(List<PublicationDocument> publicationDocuments) {
        if(publicationDocuments != null) {
            for(PublicationDocument publicationDocument : publicationDocuments) {
                if(publicationDocument != null) {

                    String infoLog = "ID :: " + publicationDocument.getId();
                    if(publicationDocument.getName() != null && publicationDocument.getName().size() > 0) {
                        infoLog += "\t\tNAME :: " + publicationDocument.getName().get(0).getValue();
                    }

                    if(publicationDocument.getChapterName() != null && publicationDocument.getChapterName().size() > 0) {
                        infoLog += "\t\tCHAPTER :: " + publicationDocument.getChapterName().get(0).getValue();
                    }

                    if(publicationDocument.getFirstPublicationDate() != null) {
                        infoLog += "\t\tFPD :: \t\t" + sdf.format(new Date(publicationDocument.getFirstPublicationDate()));
                    }

                    LOGGER.info(infoLog);
//                    List<String> pubTags = publicationDocument.getTags();
//                    if(pubTags != null) {
//                        String tagsStr = "";
//                        for(String tag : pubTags) {
//                            tagsStr += tag + ",";
//                        }
//                        LOGGER.info("\tTAGS :: '" + tagsStr + "'");
//                    }
                }
            }
        }
    }
}