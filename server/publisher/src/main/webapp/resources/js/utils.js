function blank(text) {
      return !text || !/\S/.test(text);
}