
function showUploadImagePopup(title, downloadUrl, uploadUrl, imageWidth, imageHeight, maxFileSize, existsImage) {

	var forcedClose = false;
	BootstrapDialog.show({
		message: 'Loading image, please wait...',
		onhide: function(dialogRef) {
			if(!forcedClose) {
				return false;
			}
        },
		onshow: function(dialogRef) {

			checkImage(downloadUrl, function(imageExists) {

				forcedClose = true;
				dialogRef.close();

				var imgUploadDiv = '<div id="imgUploadDropbox" style="width:' + imageWidth + 'px; height: ' + imageHeight + 'px; margin: auto; text-align: center;">';
				if(imageExists) {
					imgUploadDiv +=    '<div class="preview">';
					imgUploadDiv +=    '	<span class="imageHolder">';
					imgUploadDiv +=    '		<img style="width:' + imageWidth + 'px; height: ' + imageHeight + 'px; margin-left: auto; margin-right: auto;" src="' + downloadUrl + '?dummy=' + new Date().getTime() + '" />';
					imgUploadDiv +=    '	</span>';
					imgUploadDiv +=    '</div>';
				}
				else {
					imgUploadDiv +=    '<span class="message" style="margin: 20% 20% 30% 20%; text-align: center; font-size: 24px; ">Drop image here to upload.</span>';
				}
				imgUploadDiv += '</div>';

				$(imgUploadDiv).appendTo('#imgUploadDivHidden');

				//create the dropbox draggable destiny component
				createDropbox("imgUploadDropbox", 1, maxFileSize, uploadUrl, downloadUrl, imageWidth, imageHeight);

				//show the popup
				BootstrapDialog.show({
					title: title,
					message: $("#imgUploadDropbox"),
					buttons: [{
						id: "closeButton",
						label: 'Close',
						cssClass: 'btn-primary',
						hotkey: 27, // Escape.
						action: function(dialogRef) {
							dialogRef.close();
						}
					}]
				});
			});
		}
	});
}

function checkImage(src, callback) {
	var img = new Image();
  	img.onload = function() {
    	callback(true);
  	};
  	img.onerror = function() {
    	callback(false);
  	};

  	img.src = src; // fires off loading of image
}


function createDropbox(htmlComponent, uploadMaxFiles, uploadMaxFileSize, uploadUrl, downloadUrl, reloadImageWidth, reloadImageHeight) {

	var dropbox = $('#' + htmlComponent),
		message = $('.message', dropbox);

	dropbox.filedrop({
		// The name of the $_FILES entry:
		paramname:'file',
		maxfiles: uploadMaxFiles,
    	maxfilesize: uploadMaxFileSize,
		url: uploadUrl,
		downloadUrl: downloadUrl,
		reloadImageWidth: reloadImageWidth,
		reloadImageHeight: reloadImageHeight,

		uploadFinished:function(i,file,response) {
			$('.progressHolder').remove();
			$('.imageHolder img').attr("src", downloadUrl + "?force=" + new Date().getTime());
			$(".imageHolder img").width(reloadImageWidth).height(reloadImageHeight);
		},

    	error: function(err, file) {
			switch(err) {
				case 'BrowserNotSupported':
					showMessage('Your browser does not support HTML5 file uploads!');
					break;
				case 'TooManyFiles':
					alert('Too many files! Please select ' + uploadMaxFiles + ' at most!');
					break;
				case 'FileTooLarge':
					alert(file.name+' is too large! Please upload files up to ' + uploadMaxFileSize + ' mb.');
					break;
				default:
					break;
			}
		},

		// Called before each upload is started
		beforeEach: function(file) {
			return true;
		},

		uploadStarted:function(i, file, len){
			$('.preview').remove();
			createImage(file, reloadImageWidth, reloadImageHeight, dropbox, message);
		},

		progressUpdated: function(i, file, progress) {
			$.data(file).find('.progress').width(progress);
		}
	});

}

function createImage(file, reloadImageWidth, reloadImageHeight, dropbox, message) {

	var template = '<div class="preview">'+
					'<span class="imageHolder">'+
						'<img />'+
						'<span class="uploaded"></span>'+
					'</span>'+
					'<div class="progressHolder">'+
						'<div class="progress"></div>'+
					'</div>'+
				   '</div>';

	var preview = $(template),
		image = $('img', preview);

	var reader = new FileReader();

	image.width = reloadImageWidth;
	image.height = reloadImageHeight;

	reader.onload = function(e){
		// e.target.result holds the DataURL which
		// can be used as a source of the image:
		image.attr('width', reloadImageWidth);
		image.attr('height', reloadImageHeight);
		image.attr('src',e.target.result);
	};

	// Reading the file as a DataURL. When finished,
	// this will trigger the onload function above:
	reader.readAsDataURL(file);

	message.hide();
	preview.appendTo(dropbox);

	// Associating a preview container
	// with the file, using jQuery's $.data():

	$.data(file,preview);
}

function showMessage(msg) {
	message.html(msg);
}