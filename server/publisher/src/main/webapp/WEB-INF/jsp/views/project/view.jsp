<%@page import="com.apocalipta.comic.constants.ProjectAgeRate"%>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"      uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form"%>

<script>

    var projectId = "${project.id}";
    var languagesChanged = false;

    $(function() {

         $("#tags").tagsinput({
            maxTags: 5,
            maxChars: 15,
            trimValue: true
         });

         <c:set var="tags" value="${fn:split(project.tags,',')}"/>
         <c:forEach var="tag" items="${tags}">
            $("#tags").tagsinput('add', '${tag}');
         </c:forEach>

         showProjectTitle('${project.title.id}', '${project.title.code}');

    });

    function showUploadIcon() {
        showUploadImagePopup(
            'Upload Icon',
            '${pageContext.request.contextPath}/project/getIcon/${project.id}',
            '${pageContext.request.contextPath}/api/project/uploadIcon?projectId=${project.id}',
            250, 250, //width,height
            5
        );
    }

    function showUploadCover() {
        showUploadImagePopup(
            'Upload Cover',
            '${pageContext.request.contextPath}/project/getCover/${project.id}',
            '${pageContext.request.contextPath}/api/project/uploadCover?projectId=${project.id}',
            250, 420, //width,height
            5
        );
    }

    function showUploadBackground() {
        showUploadImagePopup(
            'Upload Background',
            '${pageContext.request.contextPath}/project/getBackground/${project.id}',
            '${pageContext.request.contextPath}/api/project/uploadBackground?projectId=${project.id}',
            500, 375, //width,height
            5
        );
    }

    function showAuthors() {

        BootstrapDialog.show({
            title: "Authors",
            message: "<table id='authorsTable'></table>",
            onshown: function(dialogRef) {

                $('#authorsTable').bootstrapTable({
                    method: 'get',
                    url: '${pageContext.request.contextPath}/api/project/getAuthors?projectId=' + projectId,
                    cache: false,
                    showColumns: true,
                    showRefresh: true,
                    columns: [
                    {
                        field: 'id',
                        visible: false,
                    },
                    {
                        field: 'type',
                        title: 'Type',
                        align: 'center',
                        valign: 'middle',
                    },
                    {
                        field: 'name',
                        title: 'Name',
                        align: 'left',
                        valign: 'top',
                    },
                    {
                        title: 'Operations',
                        align: 'center',
                        valign: 'middle',
                        clickToSelect: false,
                        formatter: authorOperateFormatter,
                        events: authorOperateEvents
                    }]
                });
            },
            buttons: [{
                label: 'Close',
                cssClass: 'btn-primary',
                hotkey: 27, // Escape.
                action: function(dialogRef) {
                    dialogRef.close();
                }
            }]
        });
    }

    var titleSelected = false;
    var titleSelectedId;

    function showTitles() {

        var dialogInstance = BootstrapDialog.show({
            title: "Title",
            message: "<table id='titlesTable'></table>",
            onshown: function(dialogRef) {

                $('#titlesTable').bootstrapTable({
                    method: 'get',
                    url: '${pageContext.request.contextPath}/api/title/list',
                    cache: false,
                    clickToSelect: true,
                    pagination: true,
                    pageSize: 5,
                    showColumns: true,
                    showRefresh: true,
                    sidePagination: 'server',
                    columns: [
                    {
                        field: 'state',
                        radio: true
                    },
                    {
                        field: 'id',
                        visible: false
                    },
                    {
                        field: 'code',
                        title: 'Code',
                        align: 'center',
                        valign: 'middle'
                    }],
                    onClickRow: function (row) {
                        var titleActionButton = dialogInstance.getButton('titleActionButton');
                        $("#titleActionButton").html('Select');
                        titleSelected = true;
                        titleSelectedId = row.id;
                    }
                });
            },
            buttons: [{
                id: 'titleActionButton',
                label: 'Close',
                cssClass: 'btn-primary',
                hotkey: 27, // Escape.
                action: function() {
                    if(titleSelected) {
                        var $button = this;
                        $button.disable();
                        $button.spin();

                        setProjectTitle(titleSelectedId, function(status, titleId, titleCode) {

                            $button.enable();
                            $button.stopSpin();

                            if(status) {
                                showProjectTitle(titleId, titleCode);
                                dialogInstance.close();
                            }
                            else {
                                alertBootstrap(BootstrapDialog.TYPE_DANGER, "Error", "Cannot set project title!");
                            }

                        });
                    }
                    else {
                        dialogInstance.close();
                    }
                }
            }]
        });
    }

    function showProjectTitle(titleId, titleCode) {
        var projectTitleHTML;
        if(titleId != '') {
            projectTitleHTML = "Selected Title :: " + titleCode + "&nbsp;<a href='javascript:deleteProjectTitle()'>(x)</a>";
        }
        else {
            projectTitleHTML = "No selected title";
        }
        $("#projectTitleSpan").html(projectTitleHTML);
    }

    function setProjectTitle(titleId, callback) {
        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/project/setTitle",
            data: "projectId=" + projectId + "&titleId=" + titleId,
            success : function(response) {

                if(response != null && response != '') {
                    callback(true, response.id, response.code);
                }
                else {
                    callback(false);
                }
            },
            error : function(e) {
                console.log('save title loading error: ' + e);
                callback(false);
            }
        });
    }

    function deleteProjectTitle() {

        $("#loadingImgDeleteProjectTitle").show();

        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/project/deleteTitle",
            data: "projectId=" + projectId,
            success : function(response) {
                if(response == 'ok') {
                    showProjectTitle('', '');
                }
                else {
                    alertBootstrap(BootstrapDialog.TYPE_DANGER, "Error", "Cannot delete project title!");
                }
                $("#loadingImgDeleteProjectTitle").hide();
            },
            error : function(e) {
                console.log('delete title: ' + e);
                $("#loadingImgDeleteProjectTitle").hide();
                alertBootstrap(BootstrapDialog.TYPE_DANGER, "Error", "Cannot delete project title!");
            }
        });
    }

    function authorOperateFormatter(value, row, index) {
        return [
            '<a class="remove ml10" href="javascript:void(0)" title="Remove">',
                '<i class="glyphicon glyphicon-remove"></i>',
            '</a>'
        ].join('');
    }

    window.authorOperateEvents = {
        'click .remove': function (e, value, row, index) {
            var authorId = row.id;
            $.ajax({
                type : "POST",
                url : "${pageContext.request.contextPath}/api/project/deleteAuthor?authorId=" + authorId,
                success : function(response) {
                    $("#authorsTable tr[data-index=" + index + "]").remove();
                },
                error : function(e) {
                    alertBootstrap(BootstrapDialog.TYPE_DANGER, "Error", "Cannot delete author!");
                }
            });
        }
    };

    function showAddAuthor() {
        BootstrapDialog.show({
            title: 'Add Author',
            message: $("        <div class='input-group'>" +
                       "           <span class='input-group-addon' id='basic-addon1'>Type</span>" +
                       "           <input type='text' id='addAuthorType' class='form-control' placeholder='Put author type here' aria-describedby='basic-addon1'>" +
                       "         </div>" +
                       "         <br>" +
                       "         <div class='input-group'>" +
                       "           <span class='input-group-addon' id='basic-addon1'>Name</span>" +
                       "           <input type='text' id='addAuthorName' class='form-control' placeholder='Put author name here' aria-describedby='basic-addon1'>" +
                       "         </div>"),
            buttons: [{
                id: "saveAuthorButton",
                label: 'Save',
                cssClass: 'btn-primary',
                hotkey: 13, // Enter.
                action: function(dialogRef) {

                    var authorType = $('#addAuthorType').val();
                    var authorName = $('#addAuthorName').val();

                    if(!blank(authorType) && !blank(authorName)) {

                        var $button = this;
                        $button.disable();
                        $button.spin();

                        $.ajax({
                            type : "POST",
                            url : "${pageContext.request.contextPath}/api/project/addAuthor",
                            data: "projectId=" + projectId + "&type=" + authorType + "&name=" + authorName,
                            success : function(response) {
                                dialogRef.close();
                            },
                            error : function(e) {

                            }
                        });
                    }
                }
            }]
        });
    }

    function changeDefaultLanguage() {

        //get selected default language
        var defaultLanguageId = $('input[name=defaultLanguageRadio]:checked').val();

        //disable all radio
        $('input[name=defaultLanguageRadio]').attr("disabled",true);

        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/project/changeDefaultLanguage",
            data: "projectId=" + projectId + "&languageId=" + defaultLanguageId,
            success : function(response) {
                //enable all radio
                $('input[name=defaultLanguageRadio]').attr("disabled",false);
            },
            error : function(e) {
                console.log('Language changing error: ' + e);
                alert("Language changing error: " + e);
                $('input[name=defaultLanguageRadio]').attr("disabled",false);
            }
        });
    }

    function saveI18NData(languageId) {

        //get the input
        var languageId = $("#i18n_" + languageId).data("languageid");
        var tag = $("#i18n_" + languageId).data("tag");
        var value = $("#i18n_" + languageId).val();

        $("#loadingImg_" + languageId).show();

        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/project/saveI18N",
            data: "projectId=" + projectId + "&tag=" + tag + "&languageId=" + languageId + "&value=" + value,
            success : function(response) {

                if(response == 'ok') {
                    //ok
                }
                else {
                    alert("Error saving i18n: " + response);
                }

                $("#loadingImg_" + languageId).hide();
            },
            error : function(e) {
                console.log('i18n loading error: ' + e);
                alert("i18n loading error :: " + e);
            }
        });
    }

    function openProjectLanguagesPopup() {

        var dialogHtml = "<div id='dialog-languages'>" +
                         "     <p>" +
                         "         <select id='availableLanguagesCombo'>" +
                         "             <c:forEach var='language' items='${LANGUAGES}'>" +
                         "                 <option id='${language.id}'>${language.description}</option>" +
                         "             </c:forEach>" +
                         "         </select> &nbsp;&nbsp;" +
                         "         <button type='button' id='addProjectLanguageButton' class='btn btn-default btn-xs' onclick='javascript:addProjectLanguage()'>" +
                         "           <span class='glyphicon glyphicon-plus'></span>&nbsp;Add" +
                         "           &nbsp;" +
                         "           <img id='loadingImg' src='${request.contextPath}/resources/img/ajax-loader.gif' width='20px' style='display: none'  />" +
                         "         </button>" +
                         "     </p>" +
                         "     <p>" +
                         "         <table id='projectLanguagesTable'>" +
                         "             <c:forEach var='projectLanguage' items='${project.languages}'>" +
                         "                 <tr id='languageTR_${projectLanguage.id}' style='height:35px'>" +
                         "                     <td width='25px'>" +
                         "                         <input onchange='javascript:changeDefaultLanguage();' type='radio' id='defaultLanguageRadio' name='defaultLanguageRadio' value='${projectLanguage.id}' <c:if test='${project.defaultLanguage.id eq projectLanguage.id}'>  checked='checked' </c:if>>" +
                         "                     </td>" +
                         "                     <td>" +
                         "                         ${projectLanguage.description}" +
                         "                     </td>" +
                         "                     <td>" +
                         "                         <button onclick='javascript:deleteProjectLanguage(${projectLanguage.id})' style='width: 100px; margin-left: 15px' type='button' class='btn btn-default btn-xs'>" +
                         "                           <span class='glyphicon glyphicon-minus'></span>&nbsp;Remove" +
                         "                         </button>" +
                         "                     </td>" +
                         "                 </tr>" +
                         "             </c:forEach>" +
                         "         </table>" +
                         "     </p>" +
                         " </div>";

        $(dialogHtml).appendTo('#languagesDivHidden');

        BootstrapDialog.show({
            title: 'Languages',
            message: $("#dialog-languages"),
            buttons: [{
                label: 'Close',
                cssClass: 'btn-primary',
                hotkey: 27, // Escape.
                action: function(dialogRef) {
                    if(languagesChanged) {
                        //reload page
                        window.location.reload();
                    }
                    dialogRef.close();
                }
            }]
        });
    }

    function openProjectI18NPopup(tag) {

        BootstrapDialog.show({
            title: tag,
            message: "<div style='display:table-cell; vertical-align:middle; text-align:center'><img id='i18nLoadingImg' src='${request.contextPath}/resources/img/ajax-loader.gif' width='20px' /></div>",
            onshow: function(dialogRef) {

                $.ajax({
                    type : "POST",
                    url : "${pageContext.request.contextPath}/api/project/getI18N",
                    data: "projectId=" + projectId + "&tag=" + tag,
                    success : function(response) {

                        var languageInputHTML = "";

                        <c:forEach var="projectLanguage" items="${project.languages}">

                            var value = "";
                            for(var i in response) {
                                 var languageIdJSON = response[i].languageId;
                                 if(languageIdJSON == ${projectLanguage.id}) {
                                    var valueJSON = response[i].value;
                                    value = valueJSON;
                                    break;
                                 }
                            }

                            //create each language input
                            languageInputHTML += "<br>";
                            languageInputHTML += "<div class='input-group'>";
                            languageInputHTML += "    <span style='min-width:140px;' class='input-group-addon' id='basic-addon1'>${projectLanguage.description}";
                            languageInputHTML += "         <img id='loadingImg_${projectLanguage.id}' src='${request.contextPath}/resources/img/ajax-loader.gif' width='20px' style='display: none'  />";
                            languageInputHTML += "    </span>";
                            languageInputHTML += "    <textarea id='i18n_${projectLanguage.id}' aria-describedby='basic-addon1' placeholder='Write in ${projectLanguage.description} here' data-languageid='${projectLanguage.id}' data-tag='" + tag + "'  class='form-control' rows='3'>" + value + "</textarea>";
                            languageInputHTML += "</div>";

                            $(document).on('focusout','#i18n_${projectLanguage.id}',function() {
                                 saveI18NData(${projectLanguage.id});
                            });

                        </c:forEach>

                        dialogRef.setMessage(languageInputHTML);
                    },
                    error : function(e) {
                        dialogRef.setMessage("ERROR :: Cannot load texts");
                    }
                });
            },
            buttons: [{
                label: 'Close',
                cssClass: 'btn-primary',
                hotkey: 27, // Escape.
                action: function(dialogRef) {
                    dialogRef.close();
                }
            }]
        });
    }

    function deleteProjectLanguage(id) {
        languagesChanged = true;
        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/project/deleteLanguage",
            data: "projectId=" + projectId + "&languageId=" + id,
            success : function(response) {
                if(response == 'ok') {
                    //delete the UI element
                    deleteProjectLanguageTR(id);
                }
                else if(response == 'error_default_language') {
                    alertBootstrap(BootstrapDialog.TYPE_DANGER, "Error", "Cannot delete default language, change it first!");
                }
                else {
                    alertBootstrap(BootstrapDialog.TYPE_DANGER, "Error", "Cannot delete language");
                }
            },
            error : function(e) {
                console.log('Language deleting error: ' + e);
                alertBootstrap(BootstrapDialog.TYPE_DANGER, "Error", "Cannot delete language");
            }
        });
    }

    function addProjectLanguage() {

        languagesChanged = true;
        var languageId = $("#availableLanguagesCombo").children(":selected").attr("id");
        var languageDesc = $("#availableLanguagesCombo").val();

        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/project/addLanguage",
            data: "projectId=" + projectId + "&languageId=" + languageId,
            success : function(response) {
                if(response == 'ok') {
                    //create the new UI element
                    createProjectLanguageTR(languageId, languageDesc);
                }

                $("#loadingImg").hide();
                $('addProjectLanguageButton').prop('disabled', false);
            },
            error : function(e) {
                console.log('Language adding error: ' + e);
                alert("Language adding error: " + e);
                $("#loadingImg").hide();
                $('addProjectLanguageButton').prop('disabled', false);
            }
        });
    }

    function deleteProjectLanguageTR(id) {
        $('table#projectLanguagesTable tr#languageTR_' + id).remove();
    }

    function createProjectLanguageTR(id, description) {

        //creamos el html para mostrar
        //los valores del rectangulo al
        //usuario
        var contenidoHtml = "";
        contenidoHtml += "<tr id='languageTR_" + id + "' style='height:35px'>";
        contenidoHtml += "  <td width='25px'>";
        contenidoHtml += "      <input onchange='javascript:changeDefaultLanguage();' type='radio' id='defaultLanguageRadio' name='defaultLanguageRadio' value='" + id + "'>";
        contenidoHtml += "  </td>";
        contenidoHtml += "  <td>";
        contenidoHtml +=        description;
        contenidoHtml += "  </td>";
        contenidoHtml += "  <td>";
        contenidoHtml += "      <button onclick='javascript:deleteProjectLanguage(" + id + ")' style='width: 100px; margin-left: 15px' type='button' class='btn btn-default btn-xs'>";
        contenidoHtml += "      <span class='glyphicon glyphicon-minus'></span>&nbsp;Remove";
        contenidoHtml += "      </button>";
        contenidoHtml += "  </td>";
        contenidoHtml += "</tr>";

        $('#projectLanguagesTable').append(contenidoHtml);
    }

    function publish() {
        confirmBootstrap(
            BootstrapDialog.TYPE_WARNING,
            "Publish project",
            "Are you sure you want to publish this project?\nThe new version would be " + (${publication.version}+1),
            function(result) {
               if(result) {
                    window.location = "${pageContext.request.contextPath}/publication/publish/${project.id}?redirect=/project/get/${project.id}";;
               }
            }
        )
    }

</script>

<%
//load the age rate enum
//values on the request
request.setAttribute("ageRateEnum", ProjectAgeRate.values());

%>

<c:if test="${null ne publishErrors}">
    <table style="margin: auto;">
        <c:forEach var="error" items="${publishErrors}">
            <tr style="font-weight: bolder;">
                <td>
                    <li><font color="red"><c:out value="${error}" /></font>
                </td>
            </tr>
        </c:forEach>
    </table>
</c:if>

<c:if test="${null ne publishSuccess}">
    <table style="margin: auto;">
        <tr style="font-weight: bolder;">
            <td>
                <font color="green"><c:out value="${publishSuccess}" /></font>
            </td>
        </tr>
    </table>
</c:if>

<form:form method="POST" modelAttribute="projectForm" action="${pageContext.request.contextPath}/project/modify">
    <table style="margin: auto;">
        <tr>
            <td valign="top">
                <h2 style="text-align: center;">Project '${project.projectName}'</h2>
                <table style="margin: auto;">
                    <tr>
                        <td style="font-weight: bolder;"> Project Name </td>
                        <td>
                            <form:hidden path="id" />
                            <form:input path="projectName" size="36" />
                        </td>
                        <td style="height: 40px">
                            <form:errors path="projectName" cssClass="error" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;"> Name </td>
                        <td style="height: 40px">
                            <button onclick="javascript:openProjectI18NPopup('NAME')" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;"> Description</td>
                        <td style="height: 40px">
                            <button onclick="javascript:openProjectI18NPopup('DESCRIPTION')" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;"> Chapter Name</td>
                        <td style="height: 40px">
                            <button onclick="javascript:openProjectI18NPopup('CHAPTER_NAME')" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Title
                        </td>
                        <td style="height: 40px">
                            <button onclick="javascript:showTitles()" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-pencil"></span>&nbsp;Select
                            </button>
                            &nbsp;&nbsp;
                            <span id="projectTitleSpan"></span>
                            <img id='loadingImgDeleteProjectTitle' src='${request.contextPath}/resources/img/ajax-loader.gif' width='20px' style='display: none'  />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Authors
                        </td>
                        <td style="height: 40px">
                            <button onclick="javascript:showAuthors()" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-pencil"></span>&nbsp;View
                            </button>
                            <button onclick="javascript:showAddAuthor()" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-pencil"></span>&nbsp;Add
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Credits
                        </td>
                        <td style="height: 40px">
                            <button onclick="javascript:openProjectI18NPopup('CREDITS')" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Legal
                        </td>
                        <td style="height: 40px">
                            <button onclick="javascript:openProjectI18NPopup('LEGAL')" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Tags
                        </td>
                        <td style="height: 40px">
                            <form:input path="tags" size="80" cssClass="bootstrap-tagsinput" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Native Language
                        </td>
                        <td style="height: 40px">
                            <form:select path="nativeLanguageId">
                                <form:options   items="${LANGUAGES}"
                                                itemValue="id"
                                                itemLabel="description"/>/>
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Genre
                        </td>
                        <td style="height: 40px">
                            <form:select path="genreId">
                                <form:option value="" label="--Please Select"/>
                                <form:options items="${PROJECT_GENRES}" itemValue="id" itemLabel="description" />
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Age Rate
                        </td>
                        <td style="height: 40px">
                            <form:select path="ageRate">
                                <form:options items="${ageRateEnum}" />
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td width="130px" style="font-weight: bolder; height: 40px">
                            Oriental Reading
                        </td>
                        <td>
                            <form:checkbox path="orientalReading" />
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Icon
                        </td>
                        <td style="height: 40px">
                            <button onclick="javascript:showUploadIcon();" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-upload"></span>&nbsp;Upload
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Cover
                        </td>
                        <td style="height: 40px">
                            <button onclick="javascript:showUploadCover()" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-upload"></span>&nbsp;Upload
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Background
                        </td>
                        <td style="height: 40px">
                            <button onclick="javascript:showUploadBackground()" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-upload"></span>&nbsp;Upload
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Languages
                        </td>
                        <td style="height: 40px;" colspan="2" valign="top">
                            <table>
                                <tr>
                                    <td>
                                        <button onclick="javascript:openProjectLanguagesPopup()" style="width: 90px" type="button" class="btn btn-default btn">
                                            <span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Pages (${fn:length(project.pages)})
                        </td>
                        <td style="height: 40px" colspan="2">
                            <div class="btn-group">
                                <button style="width: 90px" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    Action &nbsp;<span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="${pageContext.request.contextPath}/page/view/${project.id}">Edit navigation</a></li>
                                    <li><a href="${pageContext.request.contextPath}/page/gotoReorder/${project.id}">Rearrange pages</a></li>
                                    <li><a href="${pageContext.request.contextPath}/project/display_upload_pages/${project.id}">Upload batch</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <tr><td> <br> </td></tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button type="submit" class="btn btn-default btn-lg">
                                <span class="glyphicon glyphicon-ok"></span>&nbsp;Save
                            </button>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <button type="button" class="btn btn-default btn-lg" onclick="javascript:publish();">
                                <span class="glyphicon glyphicon-eye-open"></span>&nbsp;Publish
                            </button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form:form>