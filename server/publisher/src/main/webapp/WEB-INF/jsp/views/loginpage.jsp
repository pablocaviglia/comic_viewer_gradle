<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Comic Viewer Builder - Login</title>
</head>
<body>
	<form action="<%=getServletContext().getContextPath()%>/j_spring_security_check" method="post">
		<table align="center">
			<tr>
				<td colspan="2" align="center">
					<h1>Login</h1>
					<div style="color: red">${error}</div>
				</td>
			</tr>
			<tr>
				<td>
					<label for="j_username">Username</label>
				</td>
				<td>
					<input id="j_username" name="j_username" type="text" />
				</td>
			</tr>
			<tr>
				<td>
					<label for="j_password">Password</label>
				</td>
				<td>
					<input id="j_password" name="j_password" type="password" />
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<p>
					<input type="submit" value="Login" />
					<p>
					or
					<p>
					<a href="${pageContext.request.contextPath}/userRegistration/display_create"> Register </a>
				</td>
			</tr>
		</table>
	</form>
</body>
</html>