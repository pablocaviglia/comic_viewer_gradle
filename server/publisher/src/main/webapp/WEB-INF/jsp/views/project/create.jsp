<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<body>
	<h2 style="text-align: center;">Create Project</h2>
 	<form:form method="POST" modelAttribute="createProjectForm" action="${pageContext.request.contextPath}/project/create">
		<table style="margin: auto;">
			<tr valign="top">
				<td width="130px">Name:</td>
				<td><form:input path="projectName" /></td>
				<td><form:errors path="projectName" cssClass="error" /></td>
			</tr>
			<tr valign="top">
				<td>Code:</td>
				<td><form:input path="code" /></td>
				<td><form:errors path="code" cssClass="error" /></td>
			</tr>
			<tr valign="top">
				<td>Native Language:</td>
                <td>
                    <form:select path="nativeLanguageId">
                        <form:options   items="${LANGUAGES}"
                                        itemValue="id"
                                        itemLabel="description"/>/>
                    </form:select>
                </td>
				<td><form:errors path="nativeLanguageId" cssClass="error" /></td>
			</tr>
			<tr valign="top">
				<td>Default Language:</td>
                <td>
                    <form:select path="defaultLanguageId">
                        <form:options   items="${LANGUAGES}"
                                        itemValue="id"
                                        itemLabel="description"/>/>
                    </form:select>
                </td>
				<td><form:errors path="defaultLanguageId" cssClass="error" /></td>
			</tr>
			<tr>
				<td colspan="3" align="center">
                    <br>
                    <button type="submit" id="mostrarLauncherButton" class="btn btn-default btn-lg">
                      <span class="glyphicon glyphicon-ok"></span>&nbsp;Save
                    </button>
				</td>
			</tr>
		</table>
	</form:form>
</body>