<body>
    <table style="margin: auto;">
        <tr valign="top">
            <td style="text-align: center;">
                <p>
                <h3>
                    You were successfully registered on the platform!<p>
                    Please go to your mail inbox and complete the registration process.
                    <br><br>
                    *** <b>The Reader Engine Team</b> ***
                </h3>
            </td>
        </tr>
    </table>
</body>