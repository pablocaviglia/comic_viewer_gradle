<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<script>

    var userId = "${user.id}";
    var languagesChanged = false;

    $(function() {

         $("#dialog-i18n").dialog({
            width: 600,
            height: 400,
            autoOpen: false,
            modal: true,
            closeOnEscape: true,
            buttons: {
                Ok: function() {
                    $( this ).dialog( "close" );
                }
            },
            open: function(event, ui) {
                loadI18NData($(this));
            }
        });
    });

    function showUploadAvatar() {
        showUploadImagePopup(
            'Upload Avatar',
            '${pageContext.request.contextPath}/user/getAvatar/${user.id}',
            '${pageContext.request.contextPath}/api/user/uploadAvatar?userId=${user.id}',
            250, 250, //width,height
            5
        );
    }

    function changeDefaultLanguage() {

        //get selected default language
        var defaultLanguageId = $('input[name=defaultLanguageRadio]:checked').val();

        //disable all radio
        $('input[name=defaultLanguageRadio]').attr("disabled",true);

        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/user/changeDefaultLanguage",
            data: "userId=" + userId + "&languageId=" + defaultLanguageId,
            success : function(response) {
                //enable all radio
                $('input[name=defaultLanguageRadio]').attr("disabled",false);
            },
            error : function(e) {
                console.log('Language changing error: ' + e);
                alert("Language changing error: " + e);
                $('input[name=defaultLanguageRadio]').attr("disabled",false);
            }
        });
    }

    function loadI18NData(dialog) {

        var tag = dialog.data('tag');
        dialog.dialog('option', 'title', tag);

        var loadingImg = "<img id='i18nLoadingImg' src='${request.contextPath}/resources/img/ajax-loader.gif' width='20px' />";
        $(" #dialog-i18n ").html(loadingImg);

        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/user/getI18N",
            data: "userId=" + userId + "&tag=" + tag,
            success : function(response) {

                $("#dialog-i18n").html("");

                <c:forEach var="userLanguage" items="${user.languages}">

                    var value = "";
                    for(var i in response) {
                         var languageIdJSON = response[i].languageId;
                         if(languageIdJSON == ${userLanguage.id}) {
                            var valueJSON = response[i].value;
                            value = valueJSON;
                            break;
                         }
                    }

                    //create each language input
                    var languageInputHTML   = "<br>";
                    languageInputHTML      += "<div class='input-group'>";
                    languageInputHTML      += "    <span style='min-width:140px;' class='input-group-addon' id='basic-addon1'>${userLanguage.description}";
                    languageInputHTML      += "         <img id='loadingImg_${userLanguage.id}' src='${request.contextPath}/resources/img/ajax-loader.gif' width='20px' style='display: none'  />";
                    languageInputHTML      += "    </span>";
                    languageInputHTML      += "    <textarea id='i18n_${userLanguage.id}' aria-describedby='basic-addon1' placeholder='Write in ${userLanguage.description} here' data-languageid='${userLanguage.id}' data-tag='" + tag + "'  class='form-control' rows='3'>" + value + "</textarea>";
                    languageInputHTML      += "</div>";

                    $("#dialog-i18n").append(languageInputHTML);

                     $("#i18n_${userLanguage.id}").focusout(function() {
                        saveI18NData(${userLanguage.id});
                     });

                </c:forEach>

            },
            error : function(e) {
                console.log('i18n loading error: ' + e);
                alert("i18n loading error :: " + e);
            }
        });
    }

    function saveI18NData(languageId) {

        //get the input
        var languageId = $("#i18n_" + languageId).data("languageid");
        var tag = $("#i18n_" + languageId).data("tag");
        var value = $("#i18n_" + languageId).val();

        $("#loadingImg_" + languageId).show();

        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/user/saveI18N",
            data: "userId=" + userId + "&tag=" + tag + "&languageId=" + languageId + "&value=" + value,
            success : function(response) {

                if(response == 'ok') {
                    //ok
                }
                else {
                    alert("Error saving i18n: " + response);
                }

                $("#loadingImg_" + languageId).hide();
            },
            error : function(e) {
                console.log('i18n loading error: ' + e);
                alert("i18n loading error :: " + e);
            }
        });
    }

    function openUserLanguagesPopup() {

        var dialogHtml = "<div id='dialog-languages'>" +
                         "     <p>" +
                         "         <select id='availableLanguagesCombo'>" +
                         "             <c:forEach var='language' items='${LANGUAGES}'>" +
                         "                 <option id='${language.id}'>${language.description}</option>" +
                         "             </c:forEach>" +
                         "         </select> &nbsp;&nbsp;" +
                         "         <button type='button' id='addUserLanguageButton' class='btn btn-default btn-xs' onclick='javascript:addUserLanguage()'>" +
                         "           <span class='glyphicon glyphicon-plus'></span>&nbsp;Add" +
                         "           &nbsp;" +
                         "           <img id='loadingImg' src='${request.contextPath}/resources/img/ajax-loader.gif' width='20px' style='display: none'  />" +
                         "         </button>" +
                         "     </p>" +
                         "     <p>" +
                         "         <table id='userLanguagesTable'>" +
                         "             <c:forEach var='userLanguage' items='${user.languages}'>" +
                         "                 <tr id='languageTR_${userLanguage.id}' style='height:35px'>" +
                         "                     <td width='25px'>" +
                         "                         <input onchange='javascript:changeDefaultLanguage();' type='radio' id='defaultLanguageRadio' name='defaultLanguageRadio' value='${userLanguage.id}' <c:if test='${user.defaultLanguage.id eq userLanguage.id}'> checked='checked' </c:if>>" +
                         "                     </td>" +
                         "                     <td>" +
                         "                         ${userLanguage.description}" +
                         "                     </td>" +
                         "                     <td>" +
                         "                         <button onclick='javascript:deleteUserLanguage(${userLanguage.id})' style='width: 100px; margin-left: 15px' type='button' class='btn btn-default btn-xs'>" +
                         "                           <span class='glyphicon glyphicon-minus'></span>&nbsp;Remove" +
                         "                         </button>" +
                         "                     </td>" +
                         "                 </tr>" +
                         "             </c:forEach>" +
                         "         </table>" +
                         "     </p>" +
                         " </div>";

        $(dialogHtml).appendTo('#languagesDivHidden');

        BootstrapDialog.show({
            title: 'Languages',
            message: $("#dialog-languages"),
            buttons: [{
                label: 'Close',
                cssClass: 'btn-primary',
                hotkey: 27, // Escape.
                action: function(dialogRef) {
                    if(languagesChanged) {
                        //reload page
                        window.location.reload();
                    }
                    dialogRef.close();
                }
            }]
        });
    }

    function closeLanguagesPopup() {
        $( "#dialog-languages" ).dialog( "close" );
    }

    function openUserI18NPopup(tag) {
        $( "#dialog-i18n" ).data('tag',tag).dialog( "open" );
    }

    function deleteUserLanguage(id) {

        languagesChanged = true;
        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/user/deleteLanguage",
            data: "userId=" + userId + "&languageId=" + id,
            success : function(response) {
                if(response == 'ok') {
                    //delete the UI element
                    deleteUserLanguageTR(id);
                }
                else if(response == 'error_default_language') {
                    alertBootstrap(BootstrapDialog.TYPE_DANGER, "Error", "Cannot delete default language, change it first!");
                }
                else {
                    alertBootstrap(BootstrapDialog.TYPE_DANGER, "Error", "Cannot delete language");
                }
            },
            error : function(e) {
                console.log('Language deleting error: ' + e);
                alert("Language deleting error :: " + e);
            }
        });
    }

    function addUserLanguage() {

        languagesChanged = true;
        var languageId = $("#availableLanguagesCombo").children(":selected").attr("id");
        var languageDesc = $("#availableLanguagesCombo").val();

        $("#loadingImg").show();
        $('addUserLanguageButton').prop('disabled', true);

        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/user/addLanguage",
            data: "userId=" + userId + "&languageId=" + languageId,
            success : function(response) {
                if(response == 'ok') {
                    //create the new UI element
                    createUserLanguageTR(languageId, languageDesc);
                }

                $("#loadingImg").hide();
                $('addUserLanguageButton').prop('disabled', false);
            },
            error : function(e) {
                console.log('Language adding error: ' + e);
                alert("Language adding error: " + e);
                $("#loadingImg").hide();
                $('addUserLanguageButton').prop('disabled', false);
            }
        });
    }

    function deleteUserLanguageTR(id) {
        $('table#userLanguagesTable tr#languageTR_' + id).remove();
    }

    function createUserLanguageTR(id, description) {

        var contenidoHtml = "";
        contenidoHtml += "<tr id='languageTR_" + id + "' style='height:35px'>";
        contenidoHtml += "  <td width='25px'>";
        contenidoHtml += "      <input onchange='javascript:changeDefaultLanguage();' type='radio' id='defaultLanguageRadio' name='defaultLanguageRadio' value='" + id + "'>";
        contenidoHtml += "  </td>";
        contenidoHtml += "  <td>";
        contenidoHtml +=        description;
        contenidoHtml += "  </td>";
        contenidoHtml += "  <td>";
        contenidoHtml += "      <button onclick='javascript:deleteUserLanguage(" + id + ")' style='width: 100px; margin-left: 15px' type='button' class='btn btn-default btn-xs'>";
        contenidoHtml += "      <span class='glyphicon glyphicon-minus'></span>&nbsp;Remove";
        contenidoHtml += "      </button>";
        contenidoHtml += "  </td>";
        contenidoHtml += "</tr>";

        $('#userLanguagesTable').append(contenidoHtml);
    }

    function publish() {
        confirmBootstrap(
            BootstrapDialog.TYPE_WARNING,
            "Publish user",
            "Are you sure you want to publish the user information?",
            function(result) {
               if(result) {
                    window.location = "${pageContext.request.contextPath}/user/publish/${user.id}";
               }
            }
        )
    }

</script>

<div id="dialog-i18n" title="i18n">

</div>

<c:if test="${null ne saveSuccess}">
    <table style="margin: auto;">
        <tr style="font-weight: bolder;">
            <td>
                <font color="green"><c:out value="${saveSuccess}" /></font>
            </td>
        </tr>
    </table>
</c:if>

<form:form method="POST" modelAttribute="userForm" action="${pageContext.request.contextPath}/user/modify">
<table style="margin: auto;">
	<tr>
		<td valign="top">
			<h2 style="text-align: center;">User '${user.userName}'</h2>
			<table style="margin: auto;">
				<tr>
					<td style="font-weight: bolder;" width="110px"> Name </td>
					<td>
						<form:hidden path="id" />
						<form:input path="name" size="36" />
					</td>
					<td style="height: 40px">
						<form:errors path="name" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;"> Email </td>
					<td>
						<form:input path="email" size="36" />
					</td>
					<td style="height: 40px">
						<form:errors path="email" cssClass="error" />
					</td>
				</tr>
                <tr style="font-weight: bolder;">
                    <td>Password:</td>
                    <td><form:password path="password" /></td>
                    <td><form:errors path="password" cssClass="error" /></td>
                </tr>
                <tr style="font-weight: bolder;">
                    <td>Repeat password:</td>
                    <td><form:password path="repeatPassword" /></td>
                    <td><form:errors path="repeatPassword" cssClass="error" /></td>
                </tr>
				<tr>
					<td style="font-weight: bolder;"> Information</td>
					<td style="height: 40px">
                        <button onclick="javascript:openUserI18NPopup('INFORMATION')" style="width: 90px" type="button" class="btn btn-default btn">
                          <span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit
                        </button>
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;">
						Avatar
					</td>
					<td style="height: 40px">
                        <button onclick="javascript:showUploadAvatar()" style="width: 90px" type="button" class="btn btn-default btn">
                          <span class="glyphicon glyphicon-upload"></span>&nbsp;Upload
                        </button>
					</td>
				</tr>
				<tr>
					<td style="font-weight: bolder;">
						Languages
					</td>
					<td style="height: 40px;" colspan="2" valign="top">
						<table>
							<tr>
								<td>
                                    <button onclick="javascript:openUserLanguagesPopup()" style="width: 90px" type="button" class="btn btn-default btn">
                                      <span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit
                                    </button>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td> <br> </td></tr>
				<tr>
					<td colspan="2" align="center">
                        <button type="submit" class="btn btn-default btn-lg">
                          <span class="glyphicon glyphicon-ok"></span>&nbsp;Save
                        </button>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <button type="button" class="btn btn-default btn-lg" onclick="javascript:publish();">
                          <span class="glyphicon glyphicon-eye-open"></span>&nbsp;Publish
                        </button>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</form:form>