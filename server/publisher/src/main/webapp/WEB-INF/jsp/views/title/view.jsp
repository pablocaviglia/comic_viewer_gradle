<%@page import="com.apocalipta.comic.constants.ProjectAgeRate"%>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"      uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="form"    uri="http://www.springframework.org/tags/form"%>

<script>

    var titleId = "${title.id}";
    var languagesChanged = false;

    $(function() {

    });

    function showUploadIcon() {
        showUploadImagePopup(
            'Upload Icon',
            '${pageContext.request.contextPath}/title/getIcon/${title.id}',
            '${pageContext.request.contextPath}/api/title/uploadIcon?titleId=${title.id}',
            250, 250, //width,height
            5
        );
    }

    function showUploadBackground() {
        showUploadImagePopup(
            'Upload Background',
            '${pageContext.request.contextPath}/title/getBackground/${title.id}',
            '${pageContext.request.contextPath}/api/title/uploadBackground?titleId=${title.id}',
            500, 375, //width,height
            5
        );
    }

    function showAuthors() {

        BootstrapDialog.show({
            title: "Authors",
            message: "<table id='authorsTable'></table>",
            onshown: function(dialogRef) {

                $('#authorsTable').bootstrapTable({
                    method: 'get',
                    url: '${pageContext.request.contextPath}/api/project/getAuthors?projectId=' + projectId,
                    cache: false,
                    showColumns: true,
                    showRefresh: true,
                    columns: [
                    {
                        field: 'id',
                        visible: false,
                    },
                    {
                        field: 'type',
                        title: 'Type',
                        align: 'center',
                        valign: 'middle',
                    },
                    {
                        field: 'name',
                        title: 'Name',
                        align: 'left',
                        valign: 'top',
                    },
                    {
                        title: 'Operations',
                        align: 'center',
                        valign: 'middle',
                        clickToSelect: false,
                        formatter: authorOperateFormatter,
                        events: authorOperateEvents
                    }]
                });
            },
            buttons: [{
                label: 'Close',
                cssClass: 'btn-primary',
                hotkey: 27, // Escape.
                action: function(dialogRef) {
                    dialogRef.close();
                }
            }]
        });
    }

    function authorOperateFormatter(value, row, index) {
        return [
            '<a class="remove ml10" href="javascript:void(0)" title="Remove">',
                '<i class="glyphicon glyphicon-remove"></i>',
            '</a>'
        ].join('');
    }

    window.authorOperateEvents = {
        'click .remove': function (e, value, row, index) {

            var authorId = row.id;
            $.ajax({
                type : "POST",
                url : "${pageContext.request.contextPath}/api/project/deleteAuthor?authorId=" + authorId,
                success : function(response) {
                    $("#authorsTable tr[data-index=" + index + "]").remove();
                },
                error : function(e) {
                    alertBootstrap(BootstrapDialog.TYPE_DANGER, "Error", "Cannot delete author!");
                }
            });
        }
    };

    function showAddAuthor() {
        BootstrapDialog.show({
            title: 'Add Author',
            message: $("        <div class='input-group'>" +
                       "           <span class='input-group-addon' id='basic-addon1'>Type</span>" +
                       "           <input type='text' id='addAuthorType' class='form-control' placeholder='Put author type here' aria-describedby='basic-addon1'>" +
                       "         </div>" +
                       "         <br>" +
                       "         <div class='input-group'>" +
                       "           <span class='input-group-addon' id='basic-addon1'>Name</span>" +
                       "           <input type='text' id='addAuthorName' class='form-control' placeholder='Put author name here' aria-describedby='basic-addon1'>" +
                       "         </div>"),
            buttons: [{
                id: "saveAuthorButton",
                label: 'Save',
                cssClass: 'btn-primary',
                hotkey: 13, // Enter.
                action: function(dialogRef) {

                    var authorType = $('#addAuthorType').val();
                    var authorName = $('#addAuthorName').val();

                    if(!blank(authorType) && !blank(authorName)) {

                        var $button = this;
                        $button.disable();
                        $button.spin();

                        $.ajax({
                            type : "POST",
                            url : "${pageContext.request.contextPath}/api/project/addAuthor",
                            data: "projectId=" + projectId + "&type=" + authorType + "&name=" + authorName,
                            success : function(response) {
                                dialogRef.close();
                            },
                            error : function(e) {

                            }
                        });
                    }
                }
            }]
        });
    }

    function changeDefaultLanguage() {

        //get selected default language
        var defaultLanguageId = $('input[name=defaultLanguageRadio]:checked').val();

        //disable all radio
        $('input[name=defaultLanguageRadio]').attr("disabled",true);

        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/title/changeDefaultLanguage",
            data: "titleId=" + titleId + "&languageId=" + defaultLanguageId,
            success : function(response) {
                //enable all radio
                $('input[name=defaultLanguageRadio]').attr("disabled",false);
            },
            error : function(e) {
                console.log('Language changing error: ' + e);
                alert("Language changing error: " + e);
                $('input[name=defaultLanguageRadio]').attr("disabled",false);
            }
        });
    }

    function saveI18NData(languageId) {

        //get the input
        var languageId = $("#i18n_" + languageId).data("languageid");
        var tag = $("#i18n_" + languageId).data("tag");
        var value = $("#i18n_" + languageId).val();

        $("#loadingImg_" + languageId).show();

        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/title/saveI18N",
            data: "titleId=" + titleId + "&tag=" + tag + "&languageId=" + languageId + "&value=" + value,
            success : function(response) {

                if(response == 'ok') {
                    //ok
                }
                else {
                    alert("Error saving i18n: " + response);
                }

                $("#loadingImg_" + languageId).hide();
            },
            error : function(e) {
                console.log('i18n loading error: ' + e);
                alert("i18n loading error :: " + e);
            }
        });
    }

    function openTitleLanguagesPopup() {

        var dialogHtml = "<div id='dialog-languages'>" +
                         "     <p>" +
                         "         <select id='availableLanguagesCombo'>" +
                         "             <c:forEach var='language' items='${LANGUAGES}'>" +
                         "                 <option id='${language.id}'>${language.description}</option>" +
                         "             </c:forEach>" +
                         "         </select> &nbsp;&nbsp;" +
                         "         <button type='button' id='addTitleLanguageButton' class='btn btn-default btn-xs' onclick='javascript:addTitleLanguage()'>" +
                         "           <span class='glyphicon glyphicon-plus'></span>&nbsp;Add" +
                         "           &nbsp;" +
                         "           <img id='loadingImg' src='${request.contextPath}/resources/img/ajax-loader.gif' width='20px' style='display: none'  />" +
                         "         </button>" +
                         "     </p>" +
                         "     <p>" +
                         "         <table id='titleLanguagesTable'>" +
                         "             <c:forEach var='titleLanguage' items='${title.languages}'>" +
                         "                 <tr id='languageTR_${titleLanguage.id}' style='height:35px'>" +
                         "                     <td width='25px'>" +
                         "                         <input onchange='javascript:changeDefaultLanguage();' type='radio' id='defaultLanguageRadio' name='defaultLanguageRadio' value='${titleLanguage.id}' <c:if test='${title.defaultLanguage.id eq titleLanguage.id}'>  checked='checked' </c:if>>" +
                         "                     </td>" +
                         "                     <td>" +
                         "                         ${titleLanguage.description}" +
                         "                     </td>" +
                         "                     <td>" +
                         "                         <button onclick='javascript:deleteTitleLanguage(${titleLanguage.id})' style='width: 100px; margin-left: 15px' type='button' class='btn btn-default btn-xs'>" +
                         "                           <span class='glyphicon glyphicon-minus'></span>&nbsp;Remove" +
                         "                         </button>" +
                         "                     </td>" +
                         "                 </tr>" +
                         "             </c:forEach>" +
                         "         </table>" +
                         "     </p>" +
                         " </div>";

        $(dialogHtml).appendTo('#languagesDivHidden');

        BootstrapDialog.show({
            title: 'Languages',
            message: $("#dialog-languages"),
            buttons: [{
                label: 'Close',
                cssClass: 'btn-primary',
                hotkey: 27, // Escape.
                action: function(dialogRef) {
                    if(languagesChanged) {
                        //reload page
                        window.location.reload();
                    }
                    dialogRef.close();
                }
            }]
        });
    }

    function openTitleI18NPopup(tag) {

        BootstrapDialog.show({
            title: tag,
            message: "<div style='display:table-cell; vertical-align:middle; text-align:center'><img id='i18nLoadingImg' src='${request.contextPath}/resources/img/ajax-loader.gif' width='20px' /></div>",
            onshow: function(dialogRef) {

                $.ajax({
                    type : "POST",
                    url : "${pageContext.request.contextPath}/api/title/getI18N",
                    data: "titleId=" + titleId + "&tag=" + tag,
                    success : function(response) {

                        var languageInputHTML = "";

                        <c:forEach var="titleLanguage" items="${title.languages}">

                            var value = "";
                            for(var i in response) {
                                 var languageIdJSON = response[i].languageId;
                                 if(languageIdJSON == ${titleLanguage.id}) {
                                    var valueJSON = response[i].value;
                                    value = valueJSON;
                                    break;
                                 }
                            }

                            //create each language input
                            languageInputHTML += "<br>";
                            languageInputHTML += "<div class='input-group'>";
                            languageInputHTML += "    <span style='min-width:140px;' class='input-group-addon' id='basic-addon1'>${titleLanguage.description}";
                            languageInputHTML += "         <img id='loadingImg_${titleLanguage.id}' src='${request.contextPath}/resources/img/ajax-loader.gif' width='20px' style='display: none'  />";
                            languageInputHTML += "    </span>";
                            languageInputHTML += "    <textarea id='i18n_${titleLanguage.id}' aria-describedby='basic-addon1' placeholder='Write in ${titleLanguage.description} here' data-languageid='${titleLanguage.id}' data-tag='" + tag + "'  class='form-control' rows='3'>" + value + "</textarea>";
                            languageInputHTML += "</div>";

                            $(document).on('focusout','#i18n_${titleLanguage.id}',function() {
                                 saveI18NData(${titleLanguage.id});
                            });

                        </c:forEach>

                        dialogRef.setMessage(languageInputHTML);
                    },
                    error : function(e) {
                        dialogRef.setMessage("ERROR :: Cannot load texts");
                    }
                });
            },
            buttons: [{
                label: 'Close',
                cssClass: 'btn-primary',
                hotkey: 27, // Escape.
                action: function(dialogRef) {
                    dialogRef.close();
                }
            }]
        });
    }

    function deleteTitleLanguage(id) {
        languagesChanged = true;
        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/title/deleteLanguage",
            data: "titleId=" + titleId + "&languageId=" + id,
            success : function(response) {
                if(response == 'ok') {
                    //delete the UI element
                    deleteTitleLanguageTR(id);
                }
                else if(response == 'error_default_language') {
                    alertBootstrap(BootstrapDialog.TYPE_DANGER, "Error", "Cannot delete default language, change it first!");
                }
                else {
                    alertBootstrap(BootstrapDialog.TYPE_DANGER, "Error", "Cannot delete language");
                }
            },
            error : function(e) {
                console.log('Language deleting error: ' + e);
                alertBootstrap(BootstrapDialog.TYPE_DANGER, "Error", "Cannot delete language");
            }
        });
    }

    function addTitleLanguage() {

        languagesChanged = true;
        var languageId = $("#availableLanguagesCombo").children(":selected").attr("id");
        var languageDesc = $("#availableLanguagesCombo").val();

        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/title/addLanguage",
            data: "titleId=" + titleId + "&languageId=" + languageId,
            success : function(response) {
                if(response == 'ok') {
                    //create the new UI element
                    createTitleLanguageTR(languageId, languageDesc);
                }

                $("#loadingImg").hide();
                $('addTitleLanguageButton').prop('disabled', false);
            },
            error : function(e) {
                console.log('Language adding error: ' + e);
                alert("Language adding error: " + e);
                $("#loadingImg").hide();
                $('addTitleLanguageButton').prop('disabled', false);
            }
        });
    }

    function deleteTitleLanguageTR(id) {
        $('table#titleLanguagesTable tr#languageTR_' + id).remove();
    }

    function createTitleLanguageTR(id, description) {

        //creamos el html para mostrar
        //los valores del rectangulo al
        //usuario
        var contenidoHtml = "";
        contenidoHtml += "<tr id='languageTR_" + id + "' style='height:35px'>";
        contenidoHtml += "  <td width='25px'>";
        contenidoHtml += "      <input onchange='javascript:changeDefaultLanguage();' type='radio' id='defaultLanguageRadio' name='defaultLanguageRadio' value='" + id + "'>";
        contenidoHtml += "  </td>";
        contenidoHtml += "  <td>";
        contenidoHtml +=        description;
        contenidoHtml += "  </td>";
        contenidoHtml += "  <td>";
        contenidoHtml += "      <button onclick='javascript:deleteTitleLanguage(" + id + ")' style='width: 100px; margin-left: 15px' type='button' class='btn btn-default btn-xs'>";
        contenidoHtml += "      <span class='glyphicon glyphicon-minus'></span>&nbsp;Remove";
        contenidoHtml += "      </button>";
        contenidoHtml += "  </td>";
        contenidoHtml += "</tr>";

        $('#titleLanguagesTable').append(contenidoHtml);
    }

    function publish() {
        confirmBootstrap(
            BootstrapDialog.TYPE_WARNING,
            "Publish title",
            "Are you sure you want to publish this title?\nThe new version would be " + (${title.version}+1),
            function(result) {
               if(result) {
                    window.location = "${pageContext.request.contextPath}/title/publish/${title.id}?redirect=/title/get/${title.id}";;
               }
            }
        )
    }

</script>

<c:if test="${null ne publishErrors}">
    <table style="margin: auto;">
        <c:forEach var="error" items="${publishErrors}">
            <tr style="font-weight: bolder;">
                <td>
                    <li><font color="red"><c:out value="${error}" /></font>
                </td>
            </tr>
        </c:forEach>
    </table>
</c:if>

<c:if test="${null ne publishSuccess}">
    <table style="margin: auto;">
        <tr style="font-weight: bolder;">
            <td>
                <font color="green"><c:out value="${publishSuccess}" /></font>
            </td>
        </tr>
    </table>
</c:if>

<form:form method="POST" modelAttribute="titleForm" action="${pageContext.request.contextPath}/title/modify">
    <table style="margin: auto;">
        <tr>
            <td valign="top">
                <h2 style="text-align: center;">Title '${title.code}'</h2>
                <table style="margin: auto;">
                    <tr>
                        <td style="font-weight: bolder; width: 100px"> Name </td>
                        <td style="height: 40px">
                            <form:hidden path="id" />
                            <button onclick="javascript:openTitleI18NPopup('NAME')" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder; width: 100px"> Information </td>
                        <td style="height: 40px">
                            <form:hidden path="id" />
                            <button onclick="javascript:openTitleI18NPopup('INFORMATION')" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Publications
                        </td>
                        <td style="height: 40px">
                            <button onclick="javascript:showAuthors()" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-pencil"></span>&nbsp;View
                            </button>
                            <button onclick="javascript:showAddAuthor()" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-pencil"></span>&nbsp;Add
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Icon
                        </td>
                        <td style="height: 40px">
                            <button onclick="javascript:showUploadIcon()" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-upload"></span>&nbsp;Upload
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Background
                        </td>
                        <td style="height: 40px">
                            <button onclick="javascript:showUploadBackground()" style="width: 90px" type="button" class="btn btn-default btn">
                                <span class="glyphicon glyphicon-upload"></span>&nbsp;Upload
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td style="font-weight: bolder;">
                            Languages
                        </td>
                        <td style="height: 40px;" colspan="2" valign="top">
                            <table>
                                <tr>
                                    <td>
                                        <button onclick="javascript:openTitleLanguagesPopup()" style="width: 90px" type="button" class="btn btn-default btn">
                                            <span class="glyphicon glyphicon-pencil"></span>&nbsp;Edit
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr><td> <br> </td></tr>
                    <tr>
                        <td colspan="2" align="center">
                            <button type="submit" class="btn btn-default btn-lg">
                                <span class="glyphicon glyphicon-ok"></span>&nbsp;Save
                            </button>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <button type="button" class="btn btn-default btn-lg" onclick="javascript:publish();">
                                <span class="glyphicon glyphicon-eye-open"></span>&nbsp;Publish
                            </button>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</form:form>