<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<head>
	<script>

    $(function() {
        $('#titlesTable').bootstrapTable({
            method: 'get',
            url: '${pageContext.request.contextPath}/api/title/list',
            cache: false,
            height: 400,
            pagination: true,
            pageSize: 10,
            sidePagination: 'server',
            showColumns: true,
            showRefresh: true,
            columns: [
            {
                field: 'id',
                visible: false
            },
            {
                field: 'code',
                title: 'Code',
                align: 'center',
                valign: 'middle'
            },
            {
                field: 'operate',
                title: 'Operate',
                align: 'center',
                valign: 'middle',
                clickToSelect: false,
                formatter: operateFormatter,
                events: operateEvents
            }]
        });
    });

    function operateFormatter(value, row, index) {
        return [
            '<a class="edit ml10" href="javascript:void(0)" title="Edit">',
                '<i class="glyphicon glyphicon-edit">&nbsp;</i>',
            '</a>',
            '<a class="remove ml10" href="javascript:void(0)" title="Remove">',
                '<i class="glyphicon glyphicon-remove"></i>',
            '</a>'
        ].join('');
    }

    window.operateEvents = {
        'click .edit': function (e, value, row, index) {
            window.location = "${pageContext.request.contextPath}/title/get/" + row.id;
        },
        'click .remove': function (e, value, row, index) {
            deleteTitle(row.id, row.projectName);
        }
    };

	function deleteProject(id, name) {
		confirmBootstrap(
		            BootstrapDialog.TYPE_WARNING,
		            "Delete project?",
		            "Are you sure you want to delete title '" + name + "'?",
		            function(result) {
                       if(result) {
		                    window.location = "${pageContext.request.contextPath}/title/delete/" + id;
                       }
	                }
		)
	}

	</script>
</head>

<body>
    <div style="margin: auto; width: 600px">
        <table id="titlesTable">
        </table>
    </div>
</body>