<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<sec:authorize access="hasAnyRole('ADMIN','CONTENT_MANAGER')" var="isAdmin" />
<head>
	<script>

	function changePublicationStatus(projectId) {

		var status = $("#publicationStatus_" + projectId + " option:selected").val();

		$("#publicationStatus_" + projectId).prop("disabled", true);
		$("#loadingImg_" + projectId).show();

        $.ajax({
            type : "POST",
            url : "${pageContext.request.contextPath}/api/publication/changeStatus/" + projectId,
            data: "status=" + status,
            success : function(response) {
                $("#publicationStatus_" + projectId).prop("disabled", false);
                $("#loadingImg_" + projectId).hide();
            },
            error : function(e) {
                $("#publicationStatus_" + projectId).prop("disabled", false);
                $("#loadingImg_" + projectId).hide();
            }
        });
	}

	function publish(id, name, version) {
		var confirmMsg = "Are you sure you want to publish project '" + name + "'?\nThe new version would be " + (version+1);
		if(confirm(confirmMsg)) {
			window.location = "${pageContext.request.contextPath}/publication/publish/" + id;
		}
	}

	function deletePublication(id, name) {
		var confirmMsg = "Are you sure you want to delete the publication '" + name + "'?";
		if(confirm(confirmMsg)) {
			window.location = "${pageContext.request.contextPath}/publication/delete/" + id;
		}
	}

	</script>
</head>

<body>
	<c:choose>
		<c:when test="${not empty publications}">
			<c:if test="${null ne publishErrors}">
				<table style="margin: auto;">
					<c:forEach var="error" items="${publishErrors}">
						<tr style="font-weight: bolder;">
							<td>
								<li><font color="red"><c:out value="${error}" /></font>
							</td>
						</tr>
					</c:forEach>
				</table>
			</c:if>

			<c:if test="${null ne publishSuccess}">
				<table style="margin: auto;">
					<tr style="font-weight: bolder;">
						<td>
							<font color="green"><c:out value="${publishSuccess}" /></font>
						</td>
					</tr>
				</table>
			</c:if>

			<h2 style="text-align: center;">List Publications</h2>
			<table style="margin: auto;">
				<tr style="font-weight: bolder;">
					<td>
						Name
					</td>
					<td align="center">
						Status
					</td>
					<td align="center">
						Version
					</td>
					<td align="center">
						Publish
					</td>
					<td align="center">
						Delete
					</td>
					<td align="center">
						&nbsp;
					</td>
				</tr>
				<c:forEach var="publication" items="${publications}">
					<tr style="height: 35px">
						<td width="150px">
							<a href="${pageContext.request.contextPath}/project/get/${publication.project.id}"><c:out value="${publication.project.projectName}"></c:out></a>
						</td>
						<td align="center">
							<c:choose>
								<c:when test="${isAdmin}">
								    <select id="publicationStatus_${publication.project.id}" data-projectid="${publication.project.id}" onchange="changePublicationStatus('${publication.project.id}');" >
                                        <c:forEach items="${PUBLICATION_STATUS}" var="publicationStatus">
                                            <c:choose>
                                                <c:when test="${publicationStatus == publication.status}">
                                                    <option value="${publicationStatus}" selected="selected">${publicationStatus}</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value="${publicationStatus}">${publicationStatus}</option>
                                                </c:otherwise>
                                            </c:choose>
                                        </c:forEach>
                                    </select>
								</c:when>
								<c:otherwise>
									<c:out value="${publication.status}"></c:out>
								</c:otherwise>
							</c:choose>
						</td>
						<td align="center">
							<c:out value="${publication.version}"></c:out>
						</td>
						<td align="center" width="100px">
							<c:set var="publishText" value="Publish" />
							<c:if test="${publication.version gt 0}">
								<c:set var="publishText" value="Republish" />
							</c:if>
							<a title='${publishText}' href="javascript:publish(${publication.project.id}, '${publication.project.projectName}', ${publication.version})"><img src="${pageContext.request.contextPath}/resources/img/publish_icon.png" style="width: 24px; height: 24px" ></a>
						</td>
						<td align="center">
							<a href="javascript:deletePublication(${publication.id}, '${publication.project.projectName}')"><img src="${pageContext.request.contextPath}/resources/img/delete.jpg" style="width: 18px; height: 18px" ></a>
						</td>
						<td>
							<img id='loadingImg_${publication.project.id}' src='${request.contextPath}/resources/img/ajax-loader.gif' width='20px' style='display: none' />
						</td>
					</tr>
				</c:forEach>
			</table>
			<c:if test="${qntyPages gt 0}">
				<table style="margin: auto;">
					<tr>
						<td>
							<br><br>
						</td>
					</tr>
					<tr>
						<td>
							<c:forEach var="pageIndex" begin="0" end="${qntyPages}">
								<c:choose>
									<c:when test="${currentPage eq pageIndex}">
										<span style="font-size: large;">(${pageIndex+1})</span>
									</c:when>
									<c:otherwise>
										<a href="${pageContext.request.contextPath}/publication/list?page=${pageIndex}">${pageIndex+1}</a>
									</c:otherwise>
								</c:choose>
								&nbsp;
							</c:forEach>

						</td>
					</tr>
				</table>
			</c:if>
		</c:when>
		<c:otherwise>
			<table style="margin: auto;">
				<tr>
					<td>
						<h3>There are no publications!</h3>
					</td>
				</tr>
			</table>
		</c:otherwise>
	</c:choose>
</body>
