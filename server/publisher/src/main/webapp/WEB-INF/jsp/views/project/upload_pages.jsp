<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/dropbox.css" />
    <script>
    	var uploadUrl = '${pageContext.request.contextPath}/api/page/upload?projectId=${project.id}';
    	var uploadMaxFiles = 100;
    	var uploadMaxFileSize = 1000; //mb
    </script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery.filedrop.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/upload_pages.js"></script>
</head>

<body>

	<h2 style="text-align: center;">Project '<a href="${pageContext.request.contextPath}/project/get/${project.id}">${project.projectName}</a>'</h2>
	<br>
	<a href="${pageContext.request.contextPath}/page/finishLastUpload?projectId=${project.id}" class="comicButton">Finish Upload</a>
	<div id="dropbox">
		<span class="message">Drop images here to upload. <br /><i>(a limit of 100 images and 3 mb per file)</i></span>
	</div>
</body>