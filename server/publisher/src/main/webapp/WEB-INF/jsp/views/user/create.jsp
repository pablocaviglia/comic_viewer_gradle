<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<head>

<script type="text/javascript">
    var captchaContainer = null;
    var loadCaptcha = function() {
      captchaContainer = grecaptcha.render('captcha_container', {
        'sitekey' : '6LcV3wMTAAAAAGQmay_2SlrTBYBr0rapDDMYGbzn',
        'callback' : function(response) {
            $( "#saveButton" ).toggleClass("disabled",false);
            console.log(response);
        }
      });
    };
</script>

</head>

<body>
	<h2 style="text-align: center;">Register</h2>
 	<form:form method="POST" modelAttribute="createUserForm" action="${pageContext.request.contextPath}/userRegistration/create">
		<table style="margin: auto;">
			<tr valign="top">
				<td width="130px">Email:</td>
				<td><form:input path="email" /></td>
				<td><form:errors path="email" cssClass="error" /></td>
			</tr>
            <tr valign="top">
                <td>Password:</td>
                <td><form:input path="password" /></td>
                <td><form:errors path="password" cssClass="error" /></td>
            </tr>
            <tr valign="top">
                <td>Repeat password:</td>
                <td><form:input path="repeatPassword" /></td>
                <td><form:errors path="repeatPassword" cssClass="error" /></td>
            </tr>
			<tr valign="top">
				<td>Name:</td>
				<td><form:input path="name" /></td>
				<td><form:errors path="name" cssClass="error" /></td>
			</tr>
			<tr valign="top">
				<td>Language:</td>
                <td>
                    <form:select path="defaultLanguageId">
                        <form:options   items="${LANGUAGES}"
                                        itemValue="id"
                                        itemLabel="description"/>/>
                    </form:select>
                </td>
				<td><form:errors path="defaultLanguageId" cssClass="error" /></td>
			</tr>
			<tr>
			    <td colspan="2" align="center">
			        <p>
			        <div id="captcha_container"></div>
			    </td>
                <td><form:errors path="captcha" cssClass="error" /></td>
			</tr>
			<tr>
				<td colspan="3" align="center">
                    <br>
                    <button type="submit" id="saveButton" class="btn btn-default btn-lg disabled">
                      <span class="glyphicon glyphicon-ok"></span>&nbsp;Save
                    </button>
				</td>
			</tr>
		</table>
	</form:form>
</body>