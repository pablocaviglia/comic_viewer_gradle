<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<body>
    <table style="margin: auto;">
        <tr valign="top">
            <td style="text-align: center;">
                <p>
                <h3>
                    <c:choose>
                        <c:when test="${validRegistration}">
                            Congratulations, you have finished the registration process.<br>
                            Please follow this link to login to the platform<br><br>
                            <a href="${pageContext.request.contextPath}/auth/login">Go to login</a>
                        </c:when>
                        <c:otherwise>
                            <font color="red">Invalid user validation.</font><br><br>
                            Please contact us by mail at <b>newusers@thereaderengine.com</b> providing us<br>your username and we will contact you as soon as possible.
                        </c:otherwise>
                    </c:choose>
                    <br><br>
                    *** <b>The Reader Engine Team</b> ***
                </h3>
            </td>
        </tr>
    </table>
</body>
