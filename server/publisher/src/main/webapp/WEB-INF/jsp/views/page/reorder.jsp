<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script src="${pageContext.request.contextPath}/resources/js/jquery.shapeshift.js"></script>
<script>

$(function() {
	
	$(".thumbsContainer").shapeshift({
	    minColumns: 3
	});
});

function savePageIndexes() {
	
	$('#loaderDiv img').fadeIn();
	var idsOrder = '';
	$(".thumbsContainer").children('#thumbDiv').each(function(i){
		idsOrder += $(this).data('id') + "_";
	});
	
	$.ajax({
		type : "POST",
		url : "${pageContext.request.contextPath}/api/page/saveReorder",
        data: "newIdsOrder=" + idsOrder,
		success : function(response) {
			$('#loaderDiv img').fadeOut();
		},
		error : function(e) {
			console.log('Error: ' + e);
			$('#loaderDiv img').fadeOut();
		}
	});
}

</script>

<h2 style="text-align: center;">Project '<a href="${pageContext.request.contextPath}/project/get/${project.id}">${project.projectName}</a>' <c:if test="${selectedPage ne null }"> / Page ${selectedPage.pageIndex} </c:if> </h2>
<p>

<div>
<div style="margin: auto; height: 50px; width: 200px">
	<div id="loaderDiv" style="float: right; display: inline; margin-left: 5px; height: 36px; width: 36px; margin-top: 5px;">
		<img style="display: none" height="36" width="36" id="loadingDiv" src="${pageContext.request.contextPath}/resources/img/ajax-loader.gif">
	</div>
	<div id="saveDiv" style="float: right; display: inline;">
		<a href="javascript:savePageIndexes()" class="comicButtonMedium">Save</a>
	</div>
</div>
<br>
<div class="thumbsContainer">
<c:forEach var="page" items="${project.pages}">
    <div id="thumbDiv" data-id="${page.id}" data-index="${page.pageIndex}"><img width="90px" src="${pageContext.request.contextPath}/page/thumb/${page.id}"></div>
</c:forEach>
</div>