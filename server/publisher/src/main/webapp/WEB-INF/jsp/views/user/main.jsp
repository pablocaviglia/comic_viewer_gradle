<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<body>
	<h2 style="text-align: center;">Main Menu</h2>
	<table style="margin: auto;">
		<tr>
			<td>
				<li> <a href="${pageContext.request.contextPath}/user/get/<sec:authentication property="principal.id"/>"> My Profile </a>
				<li> <a href="${pageContext.request.contextPath}/project/display_create"> Create Project </a>
				<li> <a href="${pageContext.request.contextPath}/title/display_create"> Create Title </a>
				<li> <a href="${pageContext.request.contextPath}/title/list"> List Titles </a>
				<li> <a href="${pageContext.request.contextPath}/project/list"> List Projects </a>
				<li> <a href="${pageContext.request.contextPath}/publication/list"> List Publications </a>
			</td>
		</tr>
	</table>
</body>
