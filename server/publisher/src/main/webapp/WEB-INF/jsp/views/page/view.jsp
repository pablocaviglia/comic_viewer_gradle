<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/imgareaselect-animated.css" />
<style>
	#sortable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	#sortable li { margin: 0 5px 5px 5px; padding: 5px; font-size: 1.2em; height: 1.5em; }
	html>body #sortable li { height: 1.5em; line-height: 1.2em; }
	.ui-state-highlight { height: 1.5em; line-height: 1.2em; }
</style>

<script src="${pageContext.request.contextPath}/resources/js/jquery.imgareaselect.min.js"></script>
<script>
	
	//navigation items array
	var navigationItems = [];
	var subtitlesArray = [];
	
	var selectedNavigationItemId = -1;
	
	var uploadUrl = '${pageContext.request.contextPath}/api/page/uploadPage?pageId=${selectedPage.id}';
	var downloadUrl = '${pageContext.request.contextPath}/page/image/${selectedPage.id}';
	var redirectUrl = '${pageContext.request.contextPath}/page/view/${project.id}?pageIndex=${selectedPage.pageIndex}';
	var reloadImageWidth = -1;
	var reloadImageHeight = -1;
	
	var uploadMaxFiles = 1;
	var uploadMaxFileSize = 5; //mb
	var projectId = "${project.id}";
	
	$(function() {
		
		$('#subtitlesTD').fadeOut();
		$( "#sortable" ).sortable({placeholder: "ui-state-highlight"});
		$( "#sortable" ).disableSelection();
		
		//load the initial navigation items
		<c:forEach var="navigationItem" items="${navigationItems}" varStatus="i">
		//create the item
		var navigationItemValues_${i.index} = createNavigationItemObject(${navigationItem.id},
																		 ${navigationItem.navigationItemIndex},
																		 ${navigationItem.x},
																		 ${navigationItem.y},
																		 ${navigationItem.width},
																		 ${navigationItem.height});
			
		//add it to the array
		navigationItems.push(navigationItemValues_${i.index});
		//add it to the UI
		createNavigationItemTR(navigationItemValues_${i.index}, true);
		</c:forEach>
		
		$("#updateNavigationIndexesButton").click(updateNavigationItemIndexes);
		
		//fill the subtitle values
		fillSubtitleValues();
						
		//fade out the loading divs
		$('#seleccionesLoadingDiv').fadeOut();
		$('#changeIndexLoadingDiv').fadeOut();
		
		var imageOriginalWidth = ${selectedPage.imageWidth};
		var imageOriginalHeight = ${selectedPage.imageHeight};
		
		if(imageOriginalWidth > 0 && imageOriginalHeight > 0) {
			$('#paginaImagen').css({'width' : '700px'});
		    $('#paginaImagen').imgAreaSelect({ 	handles: true,
				fadeSpeed: 200,
				onSelectStart : imageSelectionStarted,
				onSelectEnd: imageSelectionFinished,
				imageWidth: imageOriginalWidth,
				imageHeight: imageOriginalHeight
			});
		}
		else {
			//could happen that the page image size
			//is not calculated on the server, on that
			//case we show it full sized
		    $('#paginaImagen').imgAreaSelect({ 	handles: true,
		        								fadeSpeed: 200,
		        								onSelectStart : imageSelectionStarted,
		        								onSelectEnd: imageSelectionFinished
		        							});
		}

	});
	
	function updateNavigationItemIndexes() {
		
		$('#loaderDiv img').fadeIn();
		$("#updateNavigationIndexesButton").attr("disabled", "disabled");
		
		var idsOrder = '';
		var navigationItemsList = $("#sortable").children("li");
		for(var i=0; i<navigationItemsList.length; i++) {
			idsOrder += navigationItemsList[i].id.split("_")[1] + "_";
		}
		
		$.ajax({
			type : "POST",
			url : "${pageContext.request.contextPath}/api/page/saveNavigationReorder",
	        data: "newIdsOrder=" + idsOrder,
			success : function(response) {
				$('#loaderDiv img').fadeOut();
				$("#updateNavigationIndexesButton").removeAttr("disabled");
			},
			error : function(e) {
				console.log('Error: ' + e);
				$('#loaderDiv img').fadeOut();
				$("#updateNavigationIndexesButton").removeAttr("disabled");
			}
		});
	}	
		
	function createNavigationItemObject(id, index, x, y, width, height) {
		var navigationItemValues = new Object();
		navigationItemValues.id = id;
		navigationItemValues.index = index;
		navigationItemValues.x = x;
		navigationItemValues.y = y;
		navigationItemValues.width = width;
		navigationItemValues.height = height;
		return navigationItemValues;
	}
	
	function updateNavigationItem(selection, id) {
		
		var x = selection.x1;
		var y = selection.y1;
		var width = selection.width;
		var height = selection.height;
		
		if(width > 0 && height > 0 && x >= 0 && y >= 0) {
			
			$('#seleccionesTableContent').fadeOut();
			$('#seleccionesLoadingDiv').fadeIn();
			
			$.ajax({
				type : "POST",
				url : "${pageContext.request.contextPath}/api/page/updateNavigationItem",
		        data: "x=" + x + "&y=" + y + "&width=" + width + "&height=" + height + "&id=" + id,
				success : function(response) {
					
					//create the new element			
					var navigationItemValues = createNavigationItemObject(response.id,
																		  response.navigationItemIndex,
																		  response.x,
																		  response.y,
																		  response.width,
																		  response.height);
					
					//update the navigation item from the list
					for(var i=0; i<navigationItems.length; i++) {
						var currentNavigationItem = navigationItems[i];
						if(currentNavigationItem.id == id) {
							currentNavigationItem.x = x;
							currentNavigationItem.y = y;
							currentNavigationItem.width = width;
							currentNavigationItem.height = height;
						}
					}
					
					//generate the tr content
					var trHtml = createNavigationItemTR(navigationItemValues, false);
					
					//replace the tr content with the new one
					$('#navigationItem_' + response.id).replaceWith(trHtml);
					
					//do the anim
					$('#seleccionesLoadingDiv').fadeOut();
					$('#seleccionesTableContent').fadeIn();
				},
				error : function(e) {
					console.log('Error: ' + e);
					//do the anim
					$('#seleccionesLoadingDiv').fadeOut();
					$('#seleccionesTableContent').fadeIn();
				}
			});
		}
	}
	
	function addNavigationItem(selection) {
		
		var x = selection.x1;
		var y = selection.y1;
		var width = selection.width;
		var height = selection.height;
		
		if(width > 0 && height > 0 && x >= 0 && y >= 0) {
			
			$('#seleccionesTableContent').fadeOut();
			$('#seleccionesLoadingDiv').fadeIn();
			
			$.ajax({
				type : "POST",
				url : "${pageContext.request.contextPath}/api/page/addNavigationRectangle/${selectedPage.id}",
		        data: "x=" + x + "&y=" + y + "&width=" + width + "&height=" + height,
				success : function(response) {
					
					//create the new element			
					var navigationItemValues = createNavigationItemObject(response.id,
																		  response.navigationItemIndex,
																		  response.x,
																		  response.y,
																		  response.width,
																		  response.height);
					
					//set the id
					selectedNavigationItemId = response.id;
					
					//add it to the list
					navigationItems.push(navigationItemValues);
					
					//fill the subs array
					subtitlesArray['navItemId_' + selectedNavigationItemId] = [];
					var navSubs = subtitlesArray['navItemId_' + selectedNavigationItemId];
					<c:forEach var="language" items="${project.languages}">
						navSubs['subId_${language.id}'] = "";
					</c:forEach>

					//create the new UI element
					createNavigationItemTR(navigationItemValues, true);
					
					//do the anim
					$('#seleccionesLoadingDiv').fadeOut();
					$('#seleccionesTableContent').fadeIn();
				},
				error : function(e) {
					console.log('Error: ' + e);
					//do the anim
					$('#seleccionesLoadingDiv').fadeOut();
					$('#seleccionesTableContent').fadeIn();
				}
			});
		}
	}
	
	function clearSelectedNavigationItemSpans() {
		$('#sortable span').remove();
	}
	
	function showNavigationItem(id) {
		
		var divContent = '';
		
		//set the current selected
		//navigation item
		selectedNavigationItemId = id;
		
		//clear previous selected spans
		clearSelectedNavigationItemSpans();
		
		//set the selected navigation
		//item on the ui
		var navigationItemHtml = $('#navigationItem_'+id).html();
		navigationItemHtml = "<span id='selectedNavigationItemSpan_" + id + "'>*</span>" + navigationItemHtml;
		$('#navigationItem_'+id).html(navigationItemHtml);
		
		
		<c:forEach var="language" items="${project.languages}">
			$('#navitemsub_${language.id}').val(subtitlesArray['navItemId_'+id]['subId_${language.id}']);
		</c:forEach>

		for(var i=0; i<navigationItems.length; i++) {
			var currentNavigationItem = navigationItems[i];
			if(currentNavigationItem.id == id) {
				$('#paginaImagen').imgAreaSelect(
						{
							x1: currentNavigationItem.x, 
						  	y1: currentNavigationItem.y, 
						  	x2: currentNavigationItem.x + currentNavigationItem.width, 
						  	y2: currentNavigationItem.y + currentNavigationItem.height 
						});
			}
		}
		
		$('#subtitlesTD').fadeIn();
		
	}
	
	function deletePage(pageId) {
		window.location = "${pageContext.request.contextPath}/page/deletePage/" + pageId;
	}
	
	function deletePageNavigation(pageId) {
		var confirmMsg = "Are you sure you want all the page navigation of this page?";
		if(confirm(confirmMsg)) {
			window.location = "${pageContext.request.contextPath}/page/deletePageNavigation/" + pageId;
		}
	}
		
	function uploadPageImage(pageId) {
		window.location = "${pageContext.request.contextPath}/page/gotoUploadPageImage/" + pageId;
	}

	function changePageIndex(pageId, selectObj) {
		
		//show the ajax
		$('#changeIndexLoadingDiv').fadeIn();
		
		//disable the select object
		selectObj.disabled = "disabled";
		var newIndex = selectObj.options[selectObj.selectedIndex].value;
		
		window.location = "${pageContext.request.contextPath}/page/changePageIndex/" + pageId + "?newIndex=" + newIndex;
		
	}
	
	function deleteNavigationItem(navigationItemId) {
		window.location = "${pageContext.request.contextPath}/page/deleteNavigationItem/" + navigationItemId;
	}
	
	function createNavigationItemTR(navigationItemValues, append) {
		
		var contenidoLi = "<li class='ui-state-default' id='navigationItem_" + navigationItemValues.id + "'>";
		contenidoLi += "<a href='javascript:showNavigationItem(" + navigationItemValues.id + ")'>Step " + navigationItemValues.index + "</a>";
		contenidoLi += "&nbsp;&nbsp; <a href='javascript:deleteNavigationItem(" + navigationItemValues.id + ")'><img src='${pageContext.request.contextPath}/resources/img/delete.jpg' width='16px'></a>";
		contenidoLi += "</li>";
		
		if(append) {
			$('#sortable').append(contenidoLi);
			$('#navigationItemLoadingCircle').fadeOut();
		}
		
		return contenidoLi;
	}
	
	function fillSubtitleValues() {
		<c:forEach var="navigationItem" items="${navigationItems}">
			subtitlesArray['navItemId_${navigationItem.id}'] = [];
			var navSubs${navigationItem.id} = subtitlesArray['navItemId_${navigationItem.id}'];
			<c:forEach var="language" items="${project.languages}">
				navSubs${navigationItem.id}['subId_${language.id}'] = "${fn:replace(project.getSubtitleValue(navigationItems, navigationItem.id, language.id), '"', '\\"')}";
			</c:forEach>
		</c:forEach>
	}
	
	function saveNavigationItemSubtitles() {
		
		if(selectedNavigationItemId != -1) {
			
			var dataStr = "";
			var rectangleId = "";
			
			dataStr = "rectangleId=" + selectedNavigationItemId;
			<c:forEach var="language" items="${project.languages}">
				var subtitleFieldValue = $('#navitemsub_${language.id}').val();
				subtitlesArray['navItemId_'+selectedNavigationItemId]['subId_${language.id}'] = $('#navitemsub_${language.id}').val();
				dataStr += "&language_" + ${language.id} + "=" + subtitleFieldValue;
			</c:forEach>
		
			//show the tr loading circle
			$('#navigationItemLoadingCircle').fadeIn();
			//disable the button
			$('#navigationItemSubtitleButton').attr("disabled", "disabled");
			
			$.ajax({
				type : "POST",
				url : "${pageContext.request.contextPath}/api/page/addSubtitle/${selectedPage.id}",
		        data: dataStr,
				success : function(response) {
					//remove the tr loading circle
					$('#navigationItemLoadingCircle').fadeOut();
					//enable the button
					$('#navigationItemSubtitleButton').removeAttr("disabled");
				},
				error : function(e) {
					console.log(e);
					//remove the tr loading circle
					$('#navigationItemLoadingCircle').fadeOut();
					//enable the button
					$('#navigationItemSubtitleButton').removeAttr("disabled");
				}
			});
		}
	}
	
	function imageSelectionStarted(img, selection) {

	}
	
	function imageSelectionFinished(img, selection) {

		//if width and height are zero 
		//cancel the selection
		var width = selection.width;
		var height = selection.height;
		if(width == 0 && height == 0) {
			clearSelectedNavigationItemSpans();
			selectedNavigationItemId = -1;
			$('#subtitlesTD').fadeOut();
		}
		else {
			if(selectedNavigationItemId == -1) {
				addNavigationItem(selection);	
				$('#subtitlesTD').fadeIn();
			}
			else {
				updateNavigationItem(selection, selectedNavigationItemId);
			}
		}
	}
	
</script>

<h2 style="text-align: center;">Project '<a href="${pageContext.request.contextPath}/project/get/${project.id}">${project.projectName}</a>' <c:if test="${selectedPage ne null }"> / Page ${selectedPage.pageIndex} </c:if> </h2>
<p>

<c:if test="${fn:length(project.pages) gt 0}">
<table style="margin: auto;">
	<tr>
		<td align="center">
			<c:forEach var="page" items="${project.pages}">
				<c:choose>
					<c:when test="${selectedPage.pageIndex eq page.pageIndex}">
						(${page.pageIndex})
					</c:when>
					<c:otherwise>
						<a style="font-size: large;" href="${pageContext.request.contextPath}/page/view/${project.id}?pageIndex=${page.pageIndex}">${page.pageIndex}</a>
					</c:otherwise>
				</c:choose>
				&nbsp;
			</c:forEach>
		</td>
	</tr>
	<tr>
		<td align="center">
			<h3><a href="${pageContext.request.contextPath}/page/gotoReorder/${project.id}">Rearrange pages</a></h3>
		</td>
	</tr>
</table>
</c:if>

<c:choose>
<c:when test="${selectedPage ne null}">
<p>
<table style="margin: auto;">
	<tr>
		<td align="center" valign="top">
			<table style="margin: auto;">
				<tr>
					<td align="center">
						<img id="paginaImagen" src="${pageContext.request.contextPath}/page/image/${selectedPage.id}">
					</td>
				</tr>
			</table>
		</td>
		<td valign="top">
			<div style="float: left; width: 100%; margin-left: 5px">
			    <p style="font-size: 110%; font-weight: bold; text-align: left;">
			    
			     <fmt:formatNumber var="imageFileSizeKb" value="${selectedPage.imageFileSize / 1024}" maxFractionDigits="0" />
			     <fmt:formatNumber var="imageFileSizeMb" value="${selectedPage.imageFileSize / 1024 / 1024}" maxFractionDigits="2" />
			    
			     -> Image size: ${selectedPage.imageFileSize} (${imageFileSizeKb}KB) (${imageFileSizeMb}MB)<br>
			     -> <a href="javascript:uploadPageImage(${selectedPage.id})">Upload page image</a><br>
			     -> <a href="javascript:deletePage(${selectedPage.id})">Delete this page</a><br>
			     -> <a href="javascript:deletePageNavigation(${selectedPage.id})">Delete page navigation</a><br>
			     -> Change page index &nbsp; 
			     <select id="changeIndexSelect" onchange="javascript:changePageIndex(${selectedPage.id}, this)" >
			     	<c:forEach var="page" items="${project.pages}">
			     		<c:if test="${selectedPage.id eq page.id }">
			     			<c:set var="selected" value="selected" />
			     		</c:if>
			     		<option id="${page.pageIndex}" ${selected}> ${page.pageIndex} </option>
			     		<c:set var="selected" value="" />
			     	</c:forEach>
			     </select>
			     <img height="24px" id="changeIndexLoadingDiv" src="${pageContext.request.contextPath}/resources/img/ajax-loader.gif">
			    </p>
			</div>
			<div style="float: left; width: 100%; margin-left: 5px">
			    <p style="font-size: 110%; font-weight: bold; text-align: left;">
			      Page Navigation 
			    </p>
			</div>
			<div id="seleccionesDiv">
				<img height="36px" id="seleccionesLoadingDiv" src="${pageContext.request.contextPath}/resources/img/ajax-loader.gif">
				<table>
					<table id="seleccionesTableContent">
					</table>
					<table>
						<tr>
							<td valign="top">
								<ul id="sortable">
								</ul>
								<table style="width:100%">
									<tr>
										<td align="center">
											<input id="updateNavigationIndexesButton" type="button" value="Save">
											<div id="loaderDiv" style="float: right; display: inline; margin-left: 5px; height: 36px; width: 36px; margin-top: 5px;">
												<img style="display: none" height="36" width="36" id="loadingDiv" src="${pageContext.request.contextPath}/resources/img/ajax-loader.gif">
											</div>
										</td>
									</tr>
								</table>
							</td>
							<c:if test="${fn:length(project.languages) gt 0}">
							<td valign="top" id="subtitlesTD">
								<table>
									<tr>
										<td class='inputText'>
											<c:forEach var="language" items="${project.languages}" varStatus="i">
											${language.description}: <input type='text' id='navitemsub_${language.id}' value='' /> <br />
											</c:forEach>
											<input id="navigationItemSubtitleButton" type='button' value='Save' onclick="javascript:saveNavigationItemSubtitles();">
											&nbsp;&nbsp;
											<img height='24px' id='navigationItemLoadingCircle' src='${pageContext.request.contextPath}/resources/img/ajax-loader.gif' style="visibility: hidden;">
										</td>
									</tr>
								</table>
							</td>
							</c:if>
						</tr>
					</table>
				</table>
			</div>
  		</td>	
	</tr>
</table>

</c:when>
<c:otherwise>
<h3 style="text-align: center;"> No pages yet! Do you want to <a href="${pageContext.request.contextPath}/project/display_upload_pages/${project.id}">upload</a> ? </h3>
</c:otherwise>
</c:choose>