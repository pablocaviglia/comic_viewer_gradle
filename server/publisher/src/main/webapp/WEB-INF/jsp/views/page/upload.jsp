<head>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/dropbox.css" />
    <script>
    	var uploadUrl = '${pageContext.request.contextPath}/api/page/uploadPage?pageId=${selectedPage.id}';
    	var uploadMaxFiles = 1;
    	var uploadMaxFileSize = 50; //mb
    </script>
	<script src="${pageContext.request.contextPath}/resources/js/jquery.filedrop.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/upload_page_image.js"></script>
</head>

<body>
	<h2 style="text-align: center;">Project '<a href="${pageContext.request.contextPath}/project/get/${project.id}">${project.projectName}</a>' / <a href="${pageContext.request.contextPath}/page/view/${project.id}?pageIndex=${selectedPage.pageIndex}">Page ${selectedPage.pageIndex}</a> / Upload Page Image</h2>
	<br>
	<div id="dropbox">
		<span class="message">Drop page image here to upload. <br /><i>(a limit of 50 mb on image size)</i></span>
	</div>
</body>