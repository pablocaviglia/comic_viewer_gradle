<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <title><sitemesh:write property='title' /></title>

        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="${pageContext.request.contextPath}/resources/css/styles.css" rel="stylesheet"/>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" />

        <script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.1.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script>

        <sitemesh:write property='head' />

    </head>
    <body>
        <div id="container">
            <div id="content">
               <sitemesh:write property='body' />
            </div>
        </div>
    </body>
</html>