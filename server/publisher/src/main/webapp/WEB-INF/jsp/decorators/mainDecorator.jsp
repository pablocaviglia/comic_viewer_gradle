<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
    <head>
        <title><sitemesh:write property='title' /></title>

        <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap-tagsinput.css" rel="stylesheet"/>
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap-dialog.css" rel="stylesheet"/>
        <link href="${pageContext.request.contextPath}/resources/css/bootstrap-table.css" rel="stylesheet"/>
        <link href="${pageContext.request.contextPath}/resources/css/styles.css" rel="stylesheet"/>
        <link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet" />
        <link href="${pageContext.request.contextPath}/resources/css/img_upload_dropbox.css" rel="stylesheet" />

        <script src="${pageContext.request.contextPath}/resources/js/jquery-1.11.1.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.10.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap-tagsinput.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap-dialog.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/bootstrap-table.min.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/utils.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/jquery.filedrop.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/upload_image.js"></script>

        <script>

            //var types = [BootstrapDialog.TYPE_DEFAULT,
            //             BootstrapDialog.TYPE_INFO,
            //             BootstrapDialog.TYPE_PRIMARY,
            //             BootstrapDialog.TYPE_SUCCESS,
            //             BootstrapDialog.TYPE_WARNING,
            //             BootstrapDialog.TYPE_DANGER];
            function alertBootstrap(type, title, message) {
                BootstrapDialog.show({
                    type: type,
                    title: title,
                    message: message,
                    buttons: [{
                        label: 'Ok',
                        cssClass: 'btn-primary',
                        hotkey: 27, // Escape.
                        action: function(dialogRef) {
                            dialogRef.close();
                        }
                    }]
                });
            }

            //var types = [BootstrapDialog.TYPE_DEFAULT,
            //             BootstrapDialog.TYPE_INFO,
            //             BootstrapDialog.TYPE_PRIMARY,
            //             BootstrapDialog.TYPE_SUCCESS,
            //             BootstrapDialog.TYPE_WARNING,
            //             BootstrapDialog.TYPE_DANGER];
            function confirmBootstrap(type, title, message, callbackFunction) {
                BootstrapDialog.confirm({
                    type: type,
                    title: title,
                    message: message,
                    btnCancelLabel: 'No',
                    btnOKLabel: 'Yes',
                    callback: callbackFunction
                });
            }


        </script>

        <sitemesh:write property='head' />

    </head>
    <body>
        <header>
            <div id="header_container">
                <a href="<%=request.getContextPath()%>/main">MAIN MENU</a>
                <div style="right: 50px; top: 0; position: absolute;">
                    <a class="right" style="margin-left: 5px" href="<%=request.getContextPath()%>/auth/logout"> Logout </a>
                    <a class="right" href="${pageContext.request.contextPath}/user/get/<sec:authentication property="principal.id"/>">(<sec:authentication property="principal.username"/>)</a>
                </div>
            </div>
        </header>

        <div id="container">
            <div id="content">
               <sitemesh:write property='body' />
            </div>
        </div>

        <div id="imgUploadDivHidden" style="display: none" />
        <div id="languagesDivHidden" style="display: none" />

        <footer>
            <div id="footer_container">
                Footer Content
            </div>
        </footer>
    </body>
</html>