package org.gmi.thereaderengine.publisher.form;

import org.gmi.thereaderengine.common.jpa.model.Title;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

/**
 * Created by pablo on 10/05/15.
 */
public class ModifyTitleForm {

    private Long id;

    public ModifyTitleForm() {
    }

    public ModifyTitleForm(Title title) {
        this.id = title.getId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}