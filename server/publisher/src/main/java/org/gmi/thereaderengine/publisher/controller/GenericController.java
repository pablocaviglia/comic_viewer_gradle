package org.gmi.thereaderengine.publisher.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.gmi.thereaderengine.common.jpa.model.User;
import org.gmi.thereaderengine.common.jpa.model.UserRole;
import org.gmi.thereaderengine.common.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

public abstract class GenericController {

	@Autowired
	private UserService userService;
	
	protected boolean isAdmin() {
		return isRole(UserRole.ADMIN) ? true : false;
	}
	
	protected boolean isContentManager() {
		return isRole(UserRole.CONTENT_MANAGER) ? true : false;
	}
	
	protected boolean isPublisher() {
		return isRole(UserRole.PUBLISHER) ? true : false;
	}
	
	protected String getLoginView() {
		return "views/loginpage";
	}

	protected boolean isLoggedUserAuthorized(long userId) {
		boolean valid = false;
		User loggedUser = getLoggedUser();
		if(loggedUser != null) {
			valid = isAdmin() || isContentManager() || (isPublisher() && loggedUser.getId() == userId);
		}
		return valid;
	}
	
	protected boolean isRole(UserRole userRole) {
		Object userObj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(userObj instanceof UserDetails) {
			UserDetails userDetails = (UserDetails) userObj;
			@SuppressWarnings("unchecked")
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>((Collection<GrantedAuthority>)userDetails.getAuthorities());
			if(authorities.get(0).getAuthority().equals(userRole.toString())) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}

	protected User getLoggedUser() {
		Object userObj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(userObj instanceof UserDetails) {
			UserDetails userDetails = (UserDetails) userObj;
			User user = userService.findByUsername(userDetails.getUsername());
			return user;
		}
		else {
			return null;
		}
	}	
}