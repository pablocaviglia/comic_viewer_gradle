package org.gmi.thereaderengine.publisher.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 * Handles and retrieves the login or denied page depending on the URI template
 */
@Controller
public class LoginLogoutController {

	private final Logger LOGGER = LoggerFactory.getLogger(getClass());
	
	/**
	 * Handles and retrieves the login JSP page
	 * 
	 * @return the name of the JSP page
	 */
	@RequestMapping(value = "/auth/login", method = {RequestMethod.GET, RequestMethod.POST})
	public String getLoginPage(HttpServletRequest request, ModelMap model) {
		String error = (String)request.getAttribute("error");
		if (error != null && !"".equals(error.trim())) {
			model.put("error", error);
		} 
		else {
			model.put("error", "");
		}

		return "views/loginpage";
	}

    @RequestMapping(value = "/auth/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request) throws ServletException {
        request.logout();
        return "views/loginpage";
    }

    /**
     * Handles and retrieves the denied JSP page. This is shown whenever a
     * regular user tries to access an admin only page.
     *
     * @return the name of the JSP page
     */
	@RequestMapping(value = "/auth/denied", method = RequestMethod.GET)
	public String getDeniedPage() {
		return "views/deniedpage";
	}
}