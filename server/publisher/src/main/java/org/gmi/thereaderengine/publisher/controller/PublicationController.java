package org.gmi.thereaderengine.publisher.controller;

import com.apocalipta.comic.constants.PublicationStatus;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.gmi.thereaderengine.common.exception.BusinessException;
import org.gmi.thereaderengine.common.filter.PublicationFilter;
import org.gmi.thereaderengine.common.jpa.model.Page;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.gmi.thereaderengine.common.jpa.model.Publication;
import org.gmi.thereaderengine.common.jpa.model.User;
import org.gmi.thereaderengine.common.jpa.model.UserRole;
import org.gmi.thereaderengine.common.service.PageService;
import org.gmi.thereaderengine.common.service.ProjectService;
import org.gmi.thereaderengine.common.service.PublicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 20/12/14.
 */
@Controller
public class PublicationController extends GenericController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    // path to the content folder
    private @Value("#{system['CONTENT_PATH']}") String CONTENT_PATH;

    @Autowired
    private PageService pageService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private PublicationService publicationService;

    private static final int RESULTS_PER_PAGE = 10;

    private static boolean LIMIT_TRANSFER_SPEED = false;
    private static final float MAX_BYTE_TRANSFER_SPEED_PER_SECOND = 100 * 1024; // 1 * 1024 = 1kb/s

    @RequestMapping(
            value = "/publication/list",
            method = { RequestMethod.GET, RequestMethod.POST })
    public String list(
            Model model,
            @RequestParam(required=false, defaultValue = "0") Integer page) {

        User loggedUser = getLoggedUser();

        PublicationFilter filter = new PublicationFilter();
        filter.setFirstResult(page * RESULTS_PER_PAGE);
        filter.setMaxResults(RESULTS_PER_PAGE);

        List<Publication> publications;
        int total;

        if(loggedUser.getRole() == UserRole.ADMIN || loggedUser.getRole() == UserRole.CONTENT_MANAGER) {
            publications = publicationService.findPublications(filter);
            total = publicationService.countFindPublications(filter);
        }
        else {
            filter.setPublisher(getLoggedUser());
            publications = publicationService.findPublications(filter);
            total = publicationService.countFindPublications(filter);
        }

        model.addAttribute("currentPage", page);
        model.addAttribute("qntyPages", total / RESULTS_PER_PAGE);
        model.addAttribute("publications", publications);

        return "views/publication/list";
    }

    @RequestMapping(
            value = "/api/publication/changeStatus/{id}",
            method = { RequestMethod.GET, RequestMethod.POST })
    @ResponseBody
    public String changeStatus(@PathVariable Long id, @RequestParam(required = true) PublicationStatus status) throws IOException {
        Project project = projectService.findById(id);
        if(isLoggedUserAuthorized(project.getPublisher().getId())) {
            publicationService.changeStatus(id, status);
            return "ok";
        }
        else {
            return "bad";
        }
    }

    @RequestMapping(
            value = "/publication/publish/{id}",
            method = { RequestMethod.GET, RequestMethod.POST })
    public String publish(@PathVariable Long id, Model model, HttpServletRequest request) throws IOException, BusinessException {

        Project project = projectService.findById(id);

        //if the user is admin or content
        //manager list all the projects
        if(isLoggedUserAuthorized(project.getPublisher().getId())) {

            //verify if project images were uploaded
            FileSystemManager fsManager = VFS.getManager();
            FileObject coverPath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + id + "/" + ProjectService.COVER_NAME);
            FileObject iconPath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + id + "/" + ProjectService.ICON_NAME);

            //error list
            List<String> errors = new ArrayList<String>();

            //verify cover existence
            if(!coverPath.exists()) {
                errors.add("The cover is required before publishing.");
            }
            //verify icon existence
            if(!iconPath.exists()) {
                errors.add("The icon is required before publishing.");
            }

            //get project pages
            List<Page> projectPages = pageService.findByProjectId(id);
            if(projectPages == null || projectPages.size() == 0) {
                errors.add("You need to upload one page before publishing.");
            }

            if(errors.size() == 0) {
                //execute the publishing process
                Publication publication = publicationService.publish(id, getLoggedUser().getId());
                if(publication.getStatus() == PublicationStatus.PUBLISHED) {
                    model.addAttribute("publishSuccess", "The project '" + project.getCode() + "' was successfully published!");
                }
                else {
                    model.addAttribute("publishSuccess", "The publication '" + project.getCode() + "' will be reviewed by our team!");
                }
            }
            else {
                model.addAttribute("publishErrors", errors);
            }
        }

        String redirect = request.getParameter("redirect");
        if(redirect != null && !"".equals(redirect)) {
            return "forward:" + redirect;
        }
        else {
            return list(model, 0);
        }
    }

    @RequestMapping(value="/publication/delete/{id}", method = {RequestMethod.GET, RequestMethod.POST})
    public String delete(@PathVariable Long id, Model model) throws BusinessException {
        Project project = publicationService.findById(id).getProject();
        if(isLoggedUserAuthorized(project.getPublisher().getId())) {
            publicationService.deletePublication(id);
            return "redirect:/publication/list";
        }
        return null;
    }
}