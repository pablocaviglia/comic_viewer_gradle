package org.gmi.thereaderengine.publisher.form;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

public class CreateTitleForm {

	@NotEmpty
 	@Pattern(regexp="[a-z]([a-z]|[0-9]?)*") //start with alpha then alpha or number
	private String code;

    @NotEmpty
    @Pattern(regexp="[A-Za-z0-9 ._-]*") //alpha or number
    private String titleName;

    @NotEmpty
    @Pattern(regexp="[0-9]*") //only number
    private String defaultLanguageId;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public String getDefaultLanguageId() {
        return defaultLanguageId;
    }

    public void setDefaultLanguageId(String defaultLanguageId) {
        this.defaultLanguageId = defaultLanguageId;
    }
}