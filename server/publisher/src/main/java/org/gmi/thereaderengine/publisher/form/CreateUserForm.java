package org.gmi.thereaderengine.publisher.form;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

public class CreateUserForm {

    @NotEmpty
    @Pattern(regexp="[A-Za-z ]*") //alpha or number
	private String name;

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @Pattern(regexp="^[a-zA-Z0-9_!?.]*$")
    private String password;

    @NotEmpty
    @Pattern(regexp="^[a-zA-Z0-9_!?.]*$")
    private String repeatPassword;

    @NotEmpty
    @Pattern(regexp="[0-9]*") //only number
    private String defaultLanguageId;

    private String captcha;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDefaultLanguageId() {
        return defaultLanguageId;
    }

    public void setDefaultLanguageId(String defaultLanguageId) {
        this.defaultLanguageId = defaultLanguageId;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
}