package org.gmi.thereaderengine.publisher.controller;

import com.apocalipta.comic.constants.ProjectI18NTag;
import com.apocalipta.comic.constants.ProjectStatus;
import com.apocalipta.comic.vo.v1.AuthorVO;
import com.apocalipta.comic.vo.v1.ProjectExtendedVO;
import com.apocalipta.comic.vo.v1.ProjectI18NTextVO;
import com.apocalipta.comic.vo.v1.ProjectVO;
import com.apocalipta.comic.vo.v1.SearchResultVO;

import org.apache.commons.io.IOUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSelectInfo;
import org.apache.commons.vfs2.FileSelector;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.gmi.thereaderengine.common.adapter.AuthorAdapter;
import org.gmi.thereaderengine.common.adapter.ProjectAdapter;
import org.gmi.thereaderengine.common.adapter.ProjectI18NTextAdapter;
import org.gmi.thereaderengine.common.exception.BusinessException;
import org.gmi.thereaderengine.common.filter.ProjectFilter;
import org.gmi.thereaderengine.common.jpa.model.Author;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.gmi.thereaderengine.common.jpa.model.ProjectI18NText;
import org.gmi.thereaderengine.common.jpa.model.Publication;
import org.gmi.thereaderengine.common.jpa.model.Title;
import org.gmi.thereaderengine.common.jpa.model.User;
import org.gmi.thereaderengine.common.jpa.model.UserRole;
import org.gmi.thereaderengine.common.service.AuthorService;
import org.gmi.thereaderengine.common.service.LanguageService;
import org.gmi.thereaderengine.common.service.ProjectGenreService;
import org.gmi.thereaderengine.common.service.ProjectService;
import org.gmi.thereaderengine.common.service.Projecti18nTextService;
import org.gmi.thereaderengine.common.service.PublicationService;
import org.gmi.thereaderengine.common.service.TitleService;
import org.gmi.thereaderengine.common.util.ShellClient;
import org.gmi.thereaderengine.publisher.form.CreateProjectForm;
import org.gmi.thereaderengine.publisher.form.ModifyProjectForm;
import org.gmi.thereaderengine.publisher.vo.BootstrapTableVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
public class ProjectController extends GenericController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	// path to the content folder
	private @Value("#{system['CONTENT_PATH']}") String CONTENT_PATH;

    private static final int RESULTS_PER_PAGE = 10;

    private FileSelector allFileSelector = new FileSelector() {
		@Override
		public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
			return true;
		}
		@Override
		public boolean traverseDescendents(FileSelectInfo fileInfo) throws Exception {
			return true;
		}
	};
	
    @Resource(name = "messageSource")
    private MessageSource messageSource;
    
	@Autowired
	private ProjectService projectService;

    @Autowired
    private PublicationService publicationService;

    @Autowired
    private LanguageService languageService;

    @Autowired
    private Projecti18nTextService projecti18nTextService;

    @Autowired
    private ProjectGenreService projectGenreService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private TitleService titleService;
	
	@RequestMapping(
            value = "/project/search",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SearchResultVO<ProjectVO> search(
			@RequestParam(required=false) Integer offset,
			@RequestParam(required=false) String name,
			@RequestParam(required=false) ProjectStatus status) {

        LOGGER.info("Getting projects...  pageNumber: " + offset + ", name: " + name + ", status: " + status);
		
		//filtro de busqueda
		ProjectFilter filter = new ProjectFilter();
		filter.setFirstResult(offset);
		filter.setName((name != null && !name.trim().equals("")) ? name : null );
		filter.setStatus(status != null ? status : null);

		User loggedUser = getLoggedUser();
		if(loggedUser.getRole() != UserRole.ADMIN && loggedUser.getRole() != UserRole.CONTENT_MANAGER) {
			filter.setPublisher(loggedUser);
		}
		
		int total = projectService.countFindProjects(filter);
		List<Project> projects = new ArrayList<Project>();
		if(total > 0) {
			projects = projectService.findProjects(filter);	
		}
		
		//creamos resultado a devolver
		SearchResultVO<ProjectVO> result = new SearchResultVO<ProjectVO>();
		result.setTotal(total);
		for(Project project : projects) {
            ProjectVO projectVO = new ProjectVO();
            ProjectAdapter.loadVO(project, projectVO, false);
			result.getElements().add(projectVO);
		}
		
		return result;
	}

    @RequestMapping(value = "/project/list", method = { RequestMethod.GET, RequestMethod.POST })
    public String gotoList() {
        return "views/project/list";
    }

    @RequestMapping(
            value = "/api/project/list",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody BootstrapTableVO list(
            @RequestParam(required=false, defaultValue = "0") Integer offset,
            HttpServletResponse response) throws IOException {

        ProjectFilter filter = new ProjectFilter();
        filter.setFirstResult(offset);
        filter.setMaxResults(RESULTS_PER_PAGE);

        User loggedUser = getLoggedUser();
        if(loggedUser.getRole() == UserRole.PUBLISHER) {
            filter.setPublisher(getLoggedUser());
        }

        int total = projectService.countFindProjects(filter);
        List<Project> projects = projectService.findProjects(filter);
        List<ProjectVO> projectVOs = new ArrayList<>();
        for(Project project : projects) {
            projectVOs.add(ProjectAdapter.loadExtendedVO(project, new ProjectExtendedVO(), false));
        }

        BootstrapTableVO bootstrapTableVO = new BootstrapTableVO();
        bootstrapTableVO.setTotal(total);
        bootstrapTableVO.setRows(projectVOs);

        return bootstrapTableVO;
    }

    @RequestMapping(value="/project/get/{id}", method = {RequestMethod.GET, RequestMethod.POST})
    public String get(@PathVariable Long id, Model model) {

    	String retView = getLoginView(); //by default if not enough rights

    	//get the project
    	Project project = projectService.findById(id);

    	//can edit the project if...
    	if(isLoggedUserAuthorized(project.getPublisher().getId())) {

    		model.addAttribute("project", project);
    		model.addAttribute("projectForm", new ModifyProjectForm(project));

            //get publication
            Publication publication = publicationService.findPublicationByProjectId(id);
            model.addAttribute("publication", publication);
    		retView = "views/project/view";
    	}
    	
    	return retView;
    }
	
    @RequestMapping(value="/project/delete/{id}", method = {RequestMethod.GET, RequestMethod.POST})
    public String delete(@PathVariable Long id, Model model) throws BusinessException {
    	Project project = projectService.findById(id);
    	if(isLoggedUserAuthorized(project.getPublisher().getId())) {
    		projectService.deleteProject(id);
	        return "redirect:/project/list";
    	}
		return null;
    }
	
	@RequestMapping(value = "/project/modify", method = RequestMethod.POST)
    public String modify(@Valid @ModelAttribute(value="projectForm") ModifyProjectForm projectForm, BindingResult result, Model model){
    	//get the project
    	Project project = projectService.findById(projectForm.getId());
    	//can edit the project if...
    	if(isLoggedUserAuthorized(project.getPublisher().getId())) {
    		
    		//set the project object again
    		model.addAttribute("project", project);
    		
    		if (result.hasErrors()) {
    			return "views/project/view";
    		} 
    		else {
    			//do the project update
				project.setProjectName(projectForm.getProjectName());
				project.setOrientalReading(projectForm.getOrientalReading());
				project.setAgeRate(projectForm.getAgeRate());
                project.setNativeLanguage(languageService.findLanguageById(projectForm.getNativeLanguageId()));
				project.setTags(projectForm.getTags());

                if(null != projectForm.getGenreId()) {
                    project.setGenre(projectGenreService.findProjectGenreById(projectForm.getGenreId()));
                }

                //store it in the database
    			projectService.store(project);
    	        return "redirect:/project/get/" + project.getId();
    		}
    	}
		return null;
	}
	
	@RequestMapping(value = "/project/create", method = RequestMethod.POST)
	public String create(@Valid CreateProjectForm createProjectForm, BindingResult result, ModelMap model) throws BusinessException {

		//validate code
		if(projectService.existsByCode(createProjectForm.getCode())) {
	        Locale locale = LocaleContextHolder.getLocale();                        
	        String projectNameAlreadyExistsErrorMessage = messageSource.getMessage("project.code.already_exists", new Object[0], locale);
			result.addError(new FieldError("createProjectForm", "code", projectNameAlreadyExistsErrorMessage));
		}
		
		if (result.hasErrors()) {
			return "views/project/create";
		}
		else {
			//create the project
			long projectId = projectService.createProject(createProjectForm.getCode(), createProjectForm.getProjectName(), Long.valueOf(createProjectForm.getNativeLanguageId()), Long.valueOf(createProjectForm.getDefaultLanguageId()), getLoggedUser());
	        return "redirect:/project/get/" + projectId;
		}
	}
	
	@RequestMapping(value = "/project/display_upload_pages/{projectId}", method = RequestMethod.GET)
	public String displayUploadPages(@PathVariable Long projectId, ModelMap model) {
		
    	//get the project
    	Project project = projectService.findById(projectId);

		if(isLoggedUserAuthorized(project.getPublisher().getId())) {
			model.addAttribute("project", project);
			try {
				//delete the tmp upload folder
				FileSystemManager fsManager = VFS.getManager();
				FileObject pageUploadPath = fsManager.resolveFile(CONTENT_PATH + "/" + projectId + "/tmp/");
				pageUploadPath.delete(allFileSelector);
			}
			catch (FileSystemException e) {
				LOGGER.error(e.getMessage(), e);
			}
		}

		return "views/project/upload_pages";
	}

	@RequestMapping(value = "/project/display_create", method = RequestMethod.GET)
	public String displayCreateProjectForm(ModelMap model) {
		model.addAttribute("createProjectForm", new CreateProjectForm());
		return "views/project/create";
	}
	
	@RequestMapping("/api/project/uploadIcon")
	public @ResponseBody
	String uploadIcon(@RequestParam MultipartFile file,
					  @RequestParam(required = true) Long projectId) {

		try {
			// get the project
			Project project = projectService.findById(projectId);
			// can upload pages to the project if...
			if (isLoggedUserAuthorized(project.getPublisher().getId())) {
				
				//get the fs manager to create the tmp file
				FileSystemManager fsManager = VFS.getManager();
				FileObject tmpIconPath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/tmp_icon");
				tmpIconPath.createFile();

				// read from multipart inputstream and
				// write into file output stream
				BufferedOutputStream bos = new BufferedOutputStream(tmpIconPath.getContent().getOutputStream(), 1024);
				BufferedInputStream bis = new BufferedInputStream(file.getInputStream());
				byte[] buffer = new byte[1024];
				int i;
				while ((i = bis.read(buffer)) != -1) {
					bos.write(buffer, 0, i);
				}
				
				bos.flush();
				bos.close();
				bis.close();
				
				//convert the image 
				ShellClient.exec(new String[]{
								"cd " + tmpIconPath.getParent().getName().getPath(),
								"convert -resize 512X512! " + tmpIconPath.getName().getPath() + " " + ProjectService.ICON_NAME,
								"rm " + tmpIconPath.getName().getPath()},
						null);
				
				return "{\"status\":\"File was uploaded successfully!\"}";
			} 
			else {
				return null;
			}
		} 
		catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
			return null;
		}
	}

	@RequestMapping("/api/project/uploadCover")
	public @ResponseBody
	String uploadCover(@RequestParam MultipartFile file,
					   @RequestParam(required = true) Long projectId) {

		try {
			// get the project
			Project project = projectService.findById(projectId);
			// can upload pages to the project if...
			if (isLoggedUserAuthorized(project.getPublisher().getId())) {
				
				//get the fs manager to create the tmp file
				FileSystemManager fsManager = VFS.getManager();
				FileObject tmpIconPath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/tmp_cover");
				tmpIconPath.createFile();

				// read from multipart inputstream and
				// write into file output stream
				BufferedOutputStream bos = new BufferedOutputStream(tmpIconPath.getContent().getOutputStream(), 1024);
				BufferedInputStream bis = new BufferedInputStream(file.getInputStream());
				byte[] buffer = new byte[1024];
				int i;
				while ((i = bis.read(buffer)) != -1) {
					bos.write(buffer, 0, i);
				}
				
				bos.flush();
				bos.close();
				bis.close();
				
				//convert the image to png 
				ShellClient.exec(new String[]{
						"cd " + tmpIconPath.getParent().getName().getPath(), 
						"convert " + tmpIconPath.getName().getPath() + " " + ProjectService.COVER_NAME,
						"rm " + tmpIconPath.getName().getPath()}, 
						null);

				return "{\"status\":\"File was uploaded successfully!\"}";
			} 
			else {
				return null;
			}
		} 
		catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
			return null;
		}
	}

    @RequestMapping("/api/project/uploadBackground")
    public @ResponseBody
    String uploadBackground(@RequestParam MultipartFile file,
                            @RequestParam(required = true) Long projectId) {

        try {
            // get the project
            Project project = projectService.findById(projectId);
            // can upload pages to the project if...
            if (isLoggedUserAuthorized(project.getPublisher().getId())) {

                //get the fs manager to create the tmp file
                FileSystemManager fsManager = VFS.getManager();
                FileObject tmpBackgroundPath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/tmp_background");
                tmpBackgroundPath.createFile();

                // read from multipart inputstream and
                // write into file output stream
                BufferedOutputStream bos = new BufferedOutputStream(tmpBackgroundPath.getContent().getOutputStream(), 1024);
                BufferedInputStream bis = new BufferedInputStream(file.getInputStream());
                byte[] buffer = new byte[1024];
                int i;
                while ((i = bis.read(buffer)) != -1) {
                    bos.write(buffer, 0, i);
                }

                bos.flush();
                bos.close();
                bis.close();

                //convert the image to png
                ShellClient.exec(new String[]{
                                "cd " + tmpBackgroundPath.getParent().getName().getPath(),
                                "convert " + tmpBackgroundPath.getName().getPath() + " " + ProjectService.BACKGROUND_NAME,
                                "rm " + tmpBackgroundPath.getName().getPath()},
                        null);

                return "{\"status\":\"File was uploaded successfully!\"}";
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

	@RequestMapping(value = "/project/getIcon/{projectId}", method = RequestMethod.GET)
	public void getIcon(@PathVariable Long projectId, 
						     HttpServletResponse response) throws IOException {

		response.setContentType("image/jpeg");
		
		//get the project
		Project project = projectService.findById(projectId);
		//can upload pages to the project if...
		if (isLoggedUserAuthorized(project.getPublisher().getId())) {
			//get the fs manager
			FileSystemManager fsManager = VFS.getManager();
			FileObject pagePath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/" + ProjectService.ICON_NAME);
			if(pagePath.exists()) {
				//copy the file stream into the response
			    IOUtils.copy(pagePath.getContent().getInputStream(), response.getOutputStream());
			}
		}
	}

    @RequestMapping(value = "/project/getCover/{projectId}", method = RequestMethod.GET)
    public void getCover(@PathVariable Long projectId,
                         HttpServletResponse response) throws IOException {

        //get the project
        Project project = projectService.findById(projectId);
        //can upload pages to the project if...
        if (isLoggedUserAuthorized(project.getPublisher().getId())) {
            //set response type
            response.setContentType("image/jpeg");
            //get the fs manager
            FileSystemManager fsManager = VFS.getManager();
            FileObject pagePath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/" + ProjectService.COVER_NAME);
            if(pagePath.exists()) {
                //copy the file stream into the response
                IOUtils.copy(pagePath.getContent().getInputStream(), response.getOutputStream());
            }
        }
    }

    @RequestMapping(value = "/project/getBackground/{projectId}", method = RequestMethod.GET)
    public void getBackground(@PathVariable Long projectId,
                              HttpServletResponse response) throws IOException {

        //get the project
        Project project = projectService.findById(projectId);
        //can upload pages to the project if...
        if (isLoggedUserAuthorized(project.getPublisher().getId())) {
            //set response type
            response.setContentType("image/jpeg");
            //get the fs manager
            FileSystemManager fsManager = VFS.getManager();
            FileObject pagePath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/" + ProjectService.BACKGROUND_NAME);
            if(pagePath.exists()) {
                //copy the file stream into the response
                IOUtils.copy(pagePath.getContent().getInputStream(), response.getOutputStream());
            }
        }
    }

    @RequestMapping("/api/project/addLanguage")
	public @ResponseBody
	String addLanguage(@RequestParam(required = true) Long projectId,
					   @RequestParam(required = true) Long languageId,
					   HttpServletRequest request) {
		try {
			Project project = projectService.findById(projectId);
			if (isLoggedUserAuthorized(project.getPublisher().getId())) {
				boolean exists = languageService.saveProjectLanguage(projectId, languageId);
				if(!exists) {
					return "ok";
				}
				else {
					return "exists";
				}
			}
			else {
				return null;
			}
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return null;
		}
	}

	@RequestMapping("/api/project/addAuthor")
	public @ResponseBody
	String addAuthor(@RequestParam(required = true) Long projectId,
					 @RequestParam(required = true) String type,
					 @RequestParam(required = true) String name,
					 HttpServletRequest request) {
		try {
			Project project = projectService.findById(projectId);
			if (isLoggedUserAuthorized(project.getPublisher().getId())) {
                Author author = new Author();
                author.setProject(project);
                author.setType(type);
                author.setName(name);
                authorService.store(author);
				return "ok";
			}
			else {
				return null;
			}
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return null;
		}
	}

    @RequestMapping("/api/project/getAuthors")
    public @ResponseBody
    List<AuthorVO> getAuthors(@RequestParam(required = true) Long projectId) {
        try {
            // get the project
            Project project = projectService.findById(projectId);
            // can upload pages to the project if...
            if (isLoggedUserAuthorized(project.getPublisher().getId())) {

                //get project authors
                List<Author> authors = authorService.findByProjectId(projectId);

                //create VOs
                List<AuthorVO> authorVOs = new ArrayList<>();
                for(Author author : authors) {
                    authorVOs.add(AuthorAdapter.toVO(author, new AuthorVO()));
                }

                return authorVOs;
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/project/deleteLanguage")
    public @ResponseBody
    String deleteLanguage(@RequestParam(required = true) Long projectId,
                          @RequestParam(required = true) Long languageId) {
        try {
            // get the project
            Project project = projectService.findById(projectId);
            // can upload pages to the project if...
            if (isLoggedUserAuthorized(project.getPublisher().getId())) {
                if(languageId == project.getDefaultLanguage().getId()) {
                    return "error_default_language";
                }
                //remove it from the project
                languageService.removeProjectLanguage(projectId, languageId);
                return "ok";
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/project/deleteAuthor")
    public @ResponseBody
    String deleteAuthor(@RequestParam(required = true) Long authorId) throws Exception {
        try {
            // get the project
            Author author = authorService.findById(authorId);
            if(author !=  null) {
                Project project = author.getProject();
                if (isLoggedUserAuthorized(project.getPublisher().getId())) {
                    authorService.delete(authorId);
                    return "ok";
                }
            }
            return null;
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw e;
        }
    }

    @RequestMapping("/api/project/changeDefaultLanguage")
    public @ResponseBody
    String changeDefaultLanguage(@RequestParam(required = true) Long projectId,
                                 @RequestParam(required = true) Long languageId) {
        try {
            // get the project
            Project project = projectService.findById(projectId);
            // can upload pages to the project if...
            if (isLoggedUserAuthorized(project.getPublisher().getId())) {
                //change
                languageService.changeDefaultProjectLanguage(projectId, languageId);
                return "ok";
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/project/getI18N")
    public @ResponseBody
    List<ProjectI18NTextVO> getI18N(
            @RequestParam(required = true) Long projectId,
            @RequestParam(required = true) ProjectI18NTag tag) {
        try {
            // get the project
            Project project = projectService.findById(projectId);
            // can upload pages to the project if...
            if (isLoggedUserAuthorized(project.getPublisher().getId())) {
                //get the i18n project texts
                List<ProjectI18NText> projectI18NTextList = projecti18nTextService.findByProjectTag(projectId, tag);
                //transform to VOs
                List<ProjectI18NTextVO> projectI18NTextVOList = new ArrayList<>();
                for(ProjectI18NText projectI18NText : projectI18NTextList) {
                    ProjectI18NTextVO projectI18NTextVO = new ProjectI18NTextVO();
                    ProjectI18NTextAdapter.toVO(projectI18NText, projectI18NTextVO);
                    projectI18NTextVOList.add(projectI18NTextVO);
                }
                return projectI18NTextVOList;
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/project/saveI18N")
    public @ResponseBody
    String saveI18N(
            @RequestParam(required = true) Long projectId,
            @RequestParam(required = true) Long languageId,
            @RequestParam(required = true) ProjectI18NTag tag,
            @RequestParam(required = true) String value) {
        try {
            // get the project
            Project project = projectService.findById(projectId);
            // can upload pages to the project if...
            if (isLoggedUserAuthorized(project.getPublisher().getId())) {
                projecti18nTextService.save(projectId, languageId, tag, value);
                return "ok";
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/project/setTitle")
    public @ResponseBody
    Title setTitle(
            @RequestParam(required = true) Long projectId,
            @RequestParam(required = true) Long titleId) {
        try {
            Project project = projectService.findById(projectId);
            if (isLoggedUserAuthorized(project.getPublisher().getId())) {
                Title title = titleService.setProjectTitle(projectId, titleId);
                return title;
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/project/deleteTitle")
    public @ResponseBody
    String deleteTitle(
            @RequestParam(required = true) Long projectId) {
        try {
            Project project = projectService.findById(projectId);
            if (isLoggedUserAuthorized(project.getPublisher().getId())) {
                titleService.deleteProjectTitle(projectId);
                return "ok";
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

}