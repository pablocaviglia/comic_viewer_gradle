package org.gmi.thereaderengine.publisher.controller;

import com.apocalipta.comic.constants.TitleI18NTag;
import com.apocalipta.comic.vo.v1.TitleI18NTextVO;
import com.apocalipta.comic.vo.v1.TitleVO;

import org.apache.commons.io.IOUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.gmi.thereaderengine.common.adapter.TitleAdapter;
import org.gmi.thereaderengine.common.adapter.TitleI18NTextAdapter;
import org.gmi.thereaderengine.common.exception.BusinessException;
import org.gmi.thereaderengine.common.filter.TitleFilter;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.gmi.thereaderengine.common.jpa.model.Title;
import org.gmi.thereaderengine.common.jpa.model.TitleI18NText;
import org.gmi.thereaderengine.common.jpa.model.User;
import org.gmi.thereaderengine.common.jpa.model.UserRole;
import org.gmi.thereaderengine.common.service.LanguageService;
import org.gmi.thereaderengine.common.service.TitleService;
import org.gmi.thereaderengine.common.service.Titlei18nTextService;
import org.gmi.thereaderengine.common.util.ShellClient;
import org.gmi.thereaderengine.publisher.form.CreateTitleForm;
import org.gmi.thereaderengine.publisher.form.ModifyProjectForm;
import org.gmi.thereaderengine.publisher.form.ModifyTitleForm;
import org.gmi.thereaderengine.publisher.vo.BootstrapTableVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * Created by pablo on 06/05/15.
 */
@Controller
public class TitleController extends GenericController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private static final int RESULTS_PER_PAGE = 5;

    // path to the content folder
    private @Value("#{system['CONTENT_PATH']}") String CONTENT_PATH;

    @Autowired
    private TitleService titleService;

    @Autowired
    private Titlei18nTextService titlei18nTextService;

    @Autowired
    private LanguageService languageService;

    @Resource(name = "messageSource")
    private MessageSource messageSource;

    @RequestMapping(value = "/title/display_create", method = RequestMethod.GET)
    public String displayCreateTitleForm(ModelMap model) {
        model.addAttribute("createTitleForm", new CreateTitleForm());
        return "views/title/create";
    }

    @RequestMapping(value = "/title/list", method = { RequestMethod.GET, RequestMethod.POST })
    public String gotoList() {
        return "views/title/list";
    }

    @RequestMapping(
            value = "/api/title/list",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    BootstrapTableVO list(
            @RequestParam(required=false, defaultValue = "0") Integer offset,
            HttpServletResponse response) throws IOException {

        TitleFilter filter = new TitleFilter();
        filter.setFirstResult(offset);
        filter.setMaxResults(RESULTS_PER_PAGE);

        User loggedUser = getLoggedUser();
        if(loggedUser.getRole() == UserRole.PUBLISHER) {
            filter.setPublisher(getLoggedUser());
        }

        int total = titleService.countFindTitles(filter);
        List<Title> titles = titleService.findTitles(filter);
        List<TitleVO> titlesVOs = new ArrayList<>();
        for(Title title : titles) {
            titlesVOs.add(TitleAdapter.loadVO(title, new TitleVO()));
        }

        BootstrapTableVO bootstrapTableVO = new BootstrapTableVO();
        bootstrapTableVO.setTotal(total);
        bootstrapTableVO.setRows(titlesVOs);

        return bootstrapTableVO;
    }

    @RequestMapping(value = "/title/modify", method = RequestMethod.POST)
    public String modify(@Valid @ModelAttribute(value="titleForm") ModifyTitleForm modifyTitleForm, BindingResult result, Model model) {
        Title title = titleService.findById(modifyTitleForm.getId());
        if(isLoggedUserAuthorized(title.getPublisher().getId())) {

            //set the project object again
            model.addAttribute("title", title);

            if (result.hasErrors()) {
                return "views/title/view";
            }
            else {

                //store it in the database
                titleService.store(title);
                return "redirect:/title/get/" + title.getId();
            }
        }
        return null;
    }

    @RequestMapping(value = "/title/create", method = RequestMethod.POST)
    public String create(@Valid CreateTitleForm createTitleForm, BindingResult result, ModelMap model) throws BusinessException {

        //validate code
        if(titleService.existsByCode(createTitleForm.getCode())) {
            Locale locale = LocaleContextHolder.getLocale();
            String titleCodeAlreadyExistsErrorMessage = messageSource.getMessage("title.code.already_exists", new Object[0], locale);
            result.addError(new FieldError("createTitleForm", "code", titleCodeAlreadyExistsErrorMessage));
        }

        if (result.hasErrors()) {
            return "views/title/create";
        }
        else {
            //create the project
            long projectId = titleService.createTitle(createTitleForm.getCode(), createTitleForm.getTitleName(), Long.valueOf(createTitleForm.getDefaultLanguageId()), getLoggedUser());
            return "redirect:/title/get/" + projectId;
        }
    }

    @RequestMapping(
            value = "/title/publish/{id}",
            method = { RequestMethod.GET, RequestMethod.POST })
    public String publish(@PathVariable Long id) throws BusinessException {

        if(isLoggedUserAuthorized(id)) {
            titleService.publish(id, true);
        }

        return "forward:/title/get/" + id;
    }

    @RequestMapping(value="/title/get/{id}", method = {RequestMethod.GET, RequestMethod.POST})
    public String get(@PathVariable Long id, Model model) {

        String retView = getLoginView(); //by default if not enough rights

        //get the project
        Title title = titleService.findById(id);

        //can edit the project if...
        if(isLoggedUserAuthorized(title.getPublisher().getId())) {
            model.addAttribute("title", title);
            model.addAttribute("titleForm", new ModifyTitleForm(title));
            retView = "views/title/view";
        }

        return retView;
    }

    @RequestMapping("/api/title/getI18N")
    public @ResponseBody
    List<TitleI18NTextVO> getI18N(
            @RequestParam(required = true) Long titleId,
            @RequestParam(required = true) TitleI18NTag tag) {
        try {
            // get the title
            Title title = titleService.findById(titleId);
            // can upload pages to the project if...
            if (isLoggedUserAuthorized(title.getPublisher().getId())) {
                //get the i18n project texts
                List<TitleI18NText> titleI18NTextList = titlei18nTextService.findByTitleAndTag(titleId, tag);
                //transform to VOs
                List<TitleI18NTextVO> titleI18NTextVOList = new ArrayList<>();
                for(TitleI18NText titleI18NText : titleI18NTextList) {
                    TitleI18NTextVO titleI18NTextVO = new TitleI18NTextVO();
                    TitleI18NTextAdapter.toVO(titleI18NText, titleI18NTextVO);
                    titleI18NTextVOList.add(titleI18NTextVO);
                }
                return titleI18NTextVOList;
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping(value = "/title/getIcon/{titleId}", method = RequestMethod.GET)
    public void getIcon(@PathVariable Long titleId,
                        HttpServletResponse response) throws IOException {

        Title title = titleService.findById(titleId);
        if (isLoggedUserAuthorized(title.getPublisher().getId())) {
            //set response type
            response.setContentType("image/jpeg");
            //get the fs manager
            FileSystemManager fsManager = VFS.getManager();
            FileObject imagePath = fsManager.resolveFile(CONTENT_PATH + TitleService.TITLES_LOCATION + titleId + "/" + TitleService.ICON_NAME);
            if(imagePath.exists()) {
                //copy the file stream into the response
                IOUtils.copy(imagePath.getContent().getInputStream(), response.getOutputStream());
            }
        }
    }

    @RequestMapping(value = "/title/getBackground/{titleId}", method = RequestMethod.GET)
    public void getBackground(@PathVariable Long titleId,
                        HttpServletResponse response) throws IOException {

        Title title = titleService.findById(titleId);
        if (isLoggedUserAuthorized(title.getPublisher().getId())) {
            //set response type
            response.setContentType("image/jpeg");
            //get the fs manager
            FileSystemManager fsManager = VFS.getManager();
            FileObject imagePath = fsManager.resolveFile(CONTENT_PATH + TitleService.TITLES_LOCATION + titleId + "/" + TitleService.BACKGROUND_NAME);
            if(imagePath.exists()) {
                //copy the file stream into the response
                IOUtils.copy(imagePath.getContent().getInputStream(), response.getOutputStream());
            }
        }
    }

    @RequestMapping("/api/title/uploadIcon")
    public @ResponseBody
    String uploadIcon(@RequestParam MultipartFile file,
                      @RequestParam(required = true) Long titleId) {

        try {
            Title title = titleService.findById(titleId);
            if (isLoggedUserAuthorized(title.getPublisher().getId())) {

                //get the fs manager to create the tmp file
                FileSystemManager fsManager = VFS.getManager();
                FileObject tmpIconPath = fsManager.resolveFile(CONTENT_PATH + TitleService.TITLES_LOCATION + titleId + "/tmp_icon");
                tmpIconPath.createFile();

                // read from multipart inputstream and
                // write into file output stream
                BufferedOutputStream bos = new BufferedOutputStream(tmpIconPath.getContent().getOutputStream(), 1024);
                BufferedInputStream bis = new BufferedInputStream(file.getInputStream());
                byte[] buffer = new byte[1024];
                int i;
                while ((i = bis.read(buffer)) != -1) {
                    bos.write(buffer, 0, i);
                }

                bos.flush();
                bos.close();
                bis.close();

                //convert the image
                ShellClient.exec(new String[]{
                                "cd " + tmpIconPath.getParent().getName().getPath(),
                                "convert -resize 512X512! " + tmpIconPath.getName().getPath() + " " + TitleService.ICON_NAME,
                                "rm " + tmpIconPath.getName().getPath()},
                        null);

                return "{\"status\":\"File was uploaded successfully!\"}";
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/title/uploadBackground")
    public @ResponseBody
    String uploadBackground(@RequestParam MultipartFile file,
                            @RequestParam(required = true) Long titleId) {

        try {
            Title title = titleService.findById(titleId);
            if (isLoggedUserAuthorized(title.getPublisher().getId())) {

                //get the fs manager to create the tmp file
                FileSystemManager fsManager = VFS.getManager();
                FileObject tmpBackgroundPath = fsManager.resolveFile(CONTENT_PATH + TitleService.TITLES_LOCATION + titleId + "/tmp_background");
                tmpBackgroundPath.createFile();

                // read from multipart inputstream and
                // write into file output stream
                BufferedOutputStream bos = new BufferedOutputStream(tmpBackgroundPath.getContent().getOutputStream(), 1024);
                BufferedInputStream bis = new BufferedInputStream(file.getInputStream());
                byte[] buffer = new byte[1024];
                int i;
                while ((i = bis.read(buffer)) != -1) {
                    bos.write(buffer, 0, i);
                }

                bos.flush();
                bos.close();
                bis.close();

                //convert the image to png
                ShellClient.exec(new String[]{
                                "cd " + tmpBackgroundPath.getParent().getName().getPath(),
                                "convert " + tmpBackgroundPath.getName().getPath() + " " + TitleService.BACKGROUND_NAME,
                                "rm " + tmpBackgroundPath.getName().getPath()},
                        null);

                return "{\"status\":\"File was uploaded successfully!\"}";
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/title/saveI18N")
    public @ResponseBody
    String saveI18N(
            @RequestParam(required = true) Long titleId,
            @RequestParam(required = true) Long languageId,
            @RequestParam(required = true) TitleI18NTag tag,
            @RequestParam(required = true) String value) {
        try {
            Title title = titleService.findById(titleId);
            if (isLoggedUserAuthorized(title.getPublisher().getId())) {
                titlei18nTextService.save(titleId, languageId, tag, value);
                return "ok";
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/title/addLanguage")
    public @ResponseBody
    String addLanguage(@RequestParam(required = true) Long titleId,
                       @RequestParam(required = true) Long languageId,
                       HttpServletRequest request) {
        try {
            Title title = titleService.findById(titleId);
            if (isLoggedUserAuthorized(title.getPublisher().getId())) {
                boolean exists = languageService.saveTitleLanguage(titleId, languageId);
                if(!exists) {
                    return "ok";
                }
                else {
                    return "exists";
                }
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/title/deleteLanguage")
    public @ResponseBody
    String deleteLanguage(@RequestParam(required = true) Long titleId,
                          @RequestParam(required = true) Long languageId) {
        try {
            Title title = titleService.findById(titleId);
            if (isLoggedUserAuthorized(title.getPublisher().getId())) {
                if(languageId == title.getDefaultLanguage().getId()) {
                    return "error_default_language";
                }
                languageService.removeTitleLanguage(titleId, languageId);
                return "ok";
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/title/changeDefaultLanguage")
    public @ResponseBody
    String changeDefaultLanguage(@RequestParam(required = true) Long titleId,
                                 @RequestParam(required = true) Long languageId) {
        try {
            Title title = titleService.findById(titleId);
            if (isLoggedUserAuthorized(title.getPublisher().getId())) {
                //change
                languageService.changeDefaultTitleLanguage(titleId, languageId);
                return "ok";
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }


}