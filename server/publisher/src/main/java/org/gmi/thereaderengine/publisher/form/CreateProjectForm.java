package org.gmi.thereaderengine.publisher.form;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

public class CreateProjectForm {

	@NotEmpty
 	@Pattern(regexp="[a-z]([a-z]|[0-9]?)*") //start with alpha then alpha or number
	private String code;

    @NotEmpty
    @Pattern(regexp="[A-Za-z0-9 ._-]*") //alpha or number
    private String projectName;

    @NotEmpty
    @Pattern(regexp="[0-9]*") //only number
    private String nativeLanguageId;

    @NotEmpty
    @Pattern(regexp="[0-9]*") //only number
    private String defaultLanguageId;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

    public String getNativeLanguageId() {
        return nativeLanguageId;
    }

    public void setNativeLanguageId(String nativeLanguageId) {
        this.nativeLanguageId = nativeLanguageId;
    }

    public String getDefaultLanguageId() {
        return defaultLanguageId;
    }

    public void setDefaultLanguageId(String defaultLanguageId) {
        this.defaultLanguageId = defaultLanguageId;
    }
}