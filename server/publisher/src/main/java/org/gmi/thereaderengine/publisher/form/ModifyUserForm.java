package org.gmi.thereaderengine.publisher.form;

import org.gmi.thereaderengine.common.jpa.model.User;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

/**
 * Created by pablo on 19/03/15.
 */
public class ModifyUserForm {

    private Long id;

    @NotEmpty
    @Pattern(regexp="[A-Za-z ]*") //alpha or number
    private String name;

    @NotEmpty
    @Email
    private String email;

    @NotEmpty
    @Pattern(regexp="^[a-zA-Z0-9_!?.]*$")
    private String password;

    @Pattern(regexp="^[a-zA-Z0-9_!?.]*$")
    private String repeatPassword;

    public ModifyUserForm() {

    }

    public ModifyUserForm(User user) {
        this.id = user.getId();
        this.email = user.getEmail();
        this.name = user.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

}