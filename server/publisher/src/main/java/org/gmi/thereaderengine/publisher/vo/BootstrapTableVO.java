package org.gmi.thereaderengine.publisher.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pablo on 17/04/15.
 */
public class BootstrapTableVO implements Serializable {

    private int total;
    private List rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List getRows() {
        return rows;
    }

    public void setRows(List rows) {
        this.rows = rows;
    }
}