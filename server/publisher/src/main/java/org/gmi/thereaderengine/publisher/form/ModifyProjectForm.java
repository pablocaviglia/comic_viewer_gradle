package org.gmi.thereaderengine.publisher.form;

import com.apocalipta.comic.constants.ProjectAgeRate;

import org.gmi.thereaderengine.common.jpa.model.Project;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Pattern;

public class ModifyProjectForm {

	private Long id;
	
	@NotEmpty
	@Pattern(regexp="[A-Za-z0-9 ._-]*") //alpha or number
	private String projectName;

	private String tags;

    private ProjectAgeRate ageRate;
    private Long nativeLanguageId;
    private Long genreId;

	private Boolean orientalReading;

	public ModifyProjectForm() {

	}
	
	public ModifyProjectForm(Project project) {
		this.id = project.getId();
		this.projectName = project.getProjectName();
		this.orientalReading = project.getOrientalReading();
		this.ageRate = project.getAgeRate();
        this.nativeLanguageId = project.getNativeLanguage().getId();
        if(null != project.getGenre()) {
            this.genreId = project.getGenre().getId();
        }
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Boolean getOrientalReading() {
		return orientalReading;
	}

	public void setOrientalReading(Boolean orientalReading) {
		this.orientalReading = orientalReading;
	}

	public ProjectAgeRate getAgeRate() {
		return ageRate;
	}

	public void setAgeRate(ProjectAgeRate ageRate) {
		this.ageRate = ageRate;
	}

    public Long getNativeLanguageId() {
        return nativeLanguageId;
    }

    public void setNativeLanguageId(Long nativeLanguageId) {
        this.nativeLanguageId = nativeLanguageId;
    }

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

    public Long getGenreId() {
        return genreId;
    }

    public void setGenreId(Long genreId) {
        this.genreId = genreId;
    }
}