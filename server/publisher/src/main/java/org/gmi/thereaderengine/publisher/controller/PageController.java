package org.gmi.thereaderengine.publisher.controller;

import org.apache.commons.io.IOUtils;
import org.apache.commons.vfs2.*;
import org.gmi.thereaderengine.common.jpa.model.NavigationItem;
import org.gmi.thereaderengine.common.jpa.model.Page;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.gmi.thereaderengine.common.service.PageService;
import org.gmi.thereaderengine.common.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.*;

@Controller
public class PageController extends GenericController {

	@SuppressWarnings("unused")
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	// path to the content folder
	@Value("#{system['CONTENT_PATH']}") String CONTENT_PATH;
	
	private FileSelector allFileSelector = new FileSelector() {
		@Override
		public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
			return true;
		}
		@Override
		public boolean traverseDescendents(FileSelectInfo fileInfo) throws Exception {
			return true;
		}
	};
	
	private class NavigationItemVO  {
		
		private Long id;
		private Integer x;
		private Integer y;
		private Integer width;
		private Integer height;
		private Integer navigationItemIndex;
		
		public NavigationItemVO(NavigationItem navigationItem) {
			this.id = navigationItem.getId();
			this.x = navigationItem.getX();
			this.y = navigationItem.getY();
			this.width = navigationItem.getWidth();
			this.height = navigationItem.getHeight();
			this.navigationItemIndex = navigationItem.getNavigationItemIndex();
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Integer getX() {
			return x;
		}
		public void setX(Integer x) {
			this.x = x;
		}
		public Integer getY() {
			return y;
		}
		public void setY(Integer y) {
			this.y = y;
		}
		public Integer getWidth() {
			return width;
		}
		public void setWidth(Integer width) {
			this.width = width;
		}
		public Integer getHeight() {
			return height;
		}
		public void setHeight(Integer height) {
			this.height = height;
		}
		public Integer getNavigationItemIndex() {
			return navigationItemIndex;
		}
		public void setNavigationItemIndex(Integer navigationItemIndex) {
			this.navigationItemIndex = navigationItemIndex;
		}
	}
	
	@Autowired
	private ProjectService projectService;
	
	@Autowired
	private PageService pageService;
	
	@RequestMapping("/api/page/addNavigationRectangle/{pageId}")
	public @ResponseBody NavigationItemVO addNavigationRectangle(
			@PathVariable Long pageId,
			@RequestParam(required = true) Integer x,
			@RequestParam(required = true) Integer y,
			@RequestParam(required = true) Integer width,
			@RequestParam(required = true) Integer height) {
		
		//store the item
		NavigationItem navigationItem = new NavigationItem();
		navigationItem.setX(x);
		navigationItem.setY(y);
		navigationItem.setWidth(width);
		navigationItem.setHeight(height);
		navigationItem = pageService.addNavigationItem(pageId, navigationItem);
		
		return new NavigationItemVO(navigationItem);
	}

	@RequestMapping("/api/page/updateNavigationItem")
	public @ResponseBody NavigationItemVO updateNavigationRectangle(
			@RequestParam(required = true) Long id,
			@RequestParam(required = true) Integer x,
			@RequestParam(required = true) Integer y,
			@RequestParam(required = true) Integer width,
			@RequestParam(required = true) Integer height) {
		
		//store the item
		NavigationItem navigationItem = new NavigationItem();
		navigationItem.setId(id);
		navigationItem.setX(x);
		navigationItem.setY(y);
		navigationItem.setWidth(width);
		navigationItem.setHeight(height);
		pageService.updateNavigationItem(navigationItem);
		
		//recreate the navigation item
		navigationItem = pageService.findNavigationItemById(id);
		
		return new NavigationItemVO(navigationItem);
	}

	@RequestMapping(value = "/page/deleteNavigationItem/{navigationItemId}")
	public String deleteNavigationItem(@PathVariable Long navigationItemId) {
		
		NavigationItem navigationItem = pageService.findNavigationItemById(navigationItemId);
		
		if(navigationItem != null) {
			//find the item to get its page
			Page page = navigationItem.getPage();
			
			//delete the item
			pageService.deleteNavigationItem(navigationItemId);
			
			return "forward:/page/view/" + page.getProject().getId() + "?pageIndex=" + page.getPageIndex();
		}
		else {
			return "forward:/main";
		}
	}

	@RequestMapping("/api/page/upload")
	public @ResponseBody
	String uploadPageBatch(@RequestParam MultipartFile file,
			@RequestParam String index,
			@RequestParam(required = true) Long projectId, HttpServletRequest request) {

		try {
			// get the project
			Project project = projectService.findById(projectId);

			// can upload pages to the project if...
			if (isLoggedUserAuthorized(project.getPublisher().getId())) {
				
				//get the fs manager to create the tmp file
				FileSystemManager fsManager = VFS.getManager();
				FileObject pagePath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/tmp/" + index);
				pagePath.createFile();

                LOGGER.info("Uploading to :: " + pagePath);

				// read from multipart inputstream and
				// write into file output stream
				BufferedOutputStream bos = new BufferedOutputStream(pagePath.getContent().getOutputStream(), 1024);
				BufferedInputStream bis = new BufferedInputStream(file.getInputStream());
				byte[] buffer = new byte[1024];
				int i;
				while ((i = bis.read(buffer)) != -1) {
					bos.write(buffer, 0, i);
				}
				bos.flush();
				bos.close();
				bis.close();

                LOGGER.info("Terminamos file upload!");

				return "{\"status\":\"File was uploaded successfuly!\"}";
			} 
			else {
				return null;
			}
		} 
		catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			return null;
		}
	}
	
	@RequestMapping(value = "/page/deletePage/{pageId}", method = RequestMethod.GET)
	public String deletePage(@PathVariable Long pageId) {
		
		// get the project
		Page page = pageService.findById(pageId);
		if(page == null) { //invalid id to delete
			return null;
		}
		
		//get the associated project
		Project project = page.getProject();

		// can upload pages to the project if...
		if (isLoggedUserAuthorized(project.getPublisher().getId())) {
			try {
				//delete the page
				pageService.deletePage(pageId);
				//redirect to project page
				return "forward:../view/" + project.getId();
			}
			catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
				return null;
			}
		}
		else {
			return null;
		}
	}
	
	@RequestMapping(value = "/page/deletePageNavigation/{pageId}", method = RequestMethod.GET)
	public String deletePageNavigation(@PathVariable Long pageId) {
		
		// get the project
		Page page = pageService.findById(pageId);
		if(page == null) { //invalid id to delete
			return null;
		}
		
		//get the associated project
		Project project = page.getProject();

		// can upload pages to the project if...
		if (isLoggedUserAuthorized(project.getPublisher().getId())) {
			try {
				//delete the page
				pageService.deletePageNavigation(pageId);
				//redirect to project page
				return "redirect:../view/" + project.getId() + "?pageIndex=" + page.getPageIndex();
			}
			catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
				return null;
			}
		}
		else {
			return null;
		}
	}
	
	@RequestMapping(value = "/page/changePageIndex/{pageId}", method = RequestMethod.GET)
	public String changePageIndex(@PathVariable Long pageId, @RequestParam(required=true) Integer newIndex) {
		
		// get the project
		Page page = pageService.findById(pageId);
		if(page == null) { //invalid id to delete
			return null;
		}
		
		//get the associated project
		Project project = page.getProject();

		// can upload pages to the project if...
		if (isLoggedUserAuthorized(project.getPublisher().getId())) {
			try {
				//change the page index
				pageService.changePageIndex(pageId, newIndex, true);
				//redirect to project page
				return "forward:../view/" + project.getId() + "?pageIndex=" + newIndex;
			}
			catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
				return null;
			}
		}
		else {
			return null;
		}
	}
	
	@RequestMapping(value = "/api/page/saveNavigationReorder", method = {RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody String saveNavigationReorder(
			@RequestParam(required = true) String newIdsOrder, 
			ModelMap model) {
		
    	if(newIdsOrder != null && newIdsOrder.trim().length() > 0) {
    		//parse the page ids
    		String[] navigationIds = newIdsOrder.split("_");
    		if(navigationIds != null) {
    			for(int i=0; i<navigationIds.length; i++) {
    				String navigationIdStr = navigationIds[i];    				
    				if(navigationIdStr != null && !navigationIdStr.trim().equals("")) {
    					pageService.changeNavigationItemIndex(new Long(navigationIdStr), (i+1));
    				}
    			}
    		}
    	}
    	
    	return "{\"status\":\"ok\"}";
	}
	
	@RequestMapping(value = "/page/changeNavigationIndex/{pageId}", method = RequestMethod.GET)
	public @ResponseBody String changeNavigationIndex(@PathVariable Long navigationItemId, @RequestParam(required=true) Integer newIndex) {
		
		// get the ni
		NavigationItem navigationItem = pageService.findNavigationItemById(navigationItemId);
		if(navigationItem == null) { //invalid id to delete
			return null;
		}

		Project project = pageService.findProjectByNavigationItemId(navigationItemId);
		
		// can upload pages to the project if...
		if (isLoggedUserAuthorized(project.getPublisher().getId())) {
			try {
				//change the navigation item index
				pageService.changeNavigationItemIndex(navigationItemId, newIndex);
				//redirect to project page
				return "{\"status\":\"ok\"}";
			}
			catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
				return null;
			}
		}
		else {
			return null;
		}
	}
	
	@RequestMapping("/page/finishLastUpload")
	public String finishLastUpload(@RequestParam(required = true) Long projectId) {
		
		// get the project
		Project project = projectService.findById(projectId);

		// can upload pages to the project if...
		if (isLoggedUserAuthorized(project.getPublisher().getId())) {
			try {
                pageService.finishLastUpload(projectId);
			}
			catch (Exception e) {
                LOGGER.error(e.getMessage(), e);
				return null;
			}
		}
		
		return "redirect:view/" + projectId;
	}
	
	@RequestMapping(value = "/page/view/{projectId}", method = RequestMethod.GET)
	public String getPage(@PathVariable Long projectId, 
						  @RequestParam(required = false) Integer pageIndex, 
						  ModelMap model) {
		
    	//get the project
    	Project project = projectService.findById(projectId);

		if (isLoggedUserAuthorized(project.getPublisher().getId())) {
			//sort the project pages by index
			Collections.sort(project.getPages(), new Comparator<Page>() {
				@Override
				public int compare(Page o1, Page o2) {
					return o1.getPageIndex()-o2.getPageIndex();
				}
			});

			//get the selected page index
			int selectedPageIndex = pageIndex == null ? 1 : pageIndex;

			//get the page
			Page selectedPage = project.getPageByIndex(selectedPageIndex);

			//set attributes
			model.addAttribute("project", project);

			if(selectedPage != null) {
				try {
					//get the fs manager
					FileSystemManager fsManager = VFS.getManager();
					FileObject pagePath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/" + selectedPage.getId());

					if(pagePath.exists()) {
						//set the image filesize
						selectedPage.setImageFileSize(pagePath.getContent().getSize());
					}
				}
				catch (FileSystemException e) {
					LOGGER.error(e.getMessage(), e);
				}

				//get the navigation items
				List<NavigationItem> navigationItems = pageService.getNavigationItems(selectedPage.getId());
				Collections.sort(navigationItems, new Comparator<NavigationItem>() {
					@Override
					public int compare(NavigationItem o1, NavigationItem o2) {
						return o1.getNavigationItemIndex()-o2.getNavigationItemIndex();
					}
				});

				//set attributes
				model.addAttribute("selectedPage", selectedPage);
				model.addAttribute("navigationItems", navigationItems);
			}
		}

		return "views/page/view";
	}
	
	
	@RequestMapping(value = "/page/gotoReorder/{projectId}", method = RequestMethod.GET)
	public String gotoReorder(@PathVariable Long projectId, 
						  	  ModelMap model) {
		
    	//get the project
    	Project project = projectService.findById(projectId);

		if (isLoggedUserAuthorized(project.getPublisher().getId())) {
			//sort the project pages by index
			Collections.sort(project.getPages(), new Comparator<Page>() {
				@Override
				public int compare(Page o1, Page o2) {
					return o1.getPageIndex()-o2.getPageIndex();
				}
			});

			//set attributes
			model.addAttribute("project", project);
		}

		return "views/page/reorder";
	}
	
	@RequestMapping(value = "/api/page/saveReorder", method = {RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody String saveReorder(@RequestParam(required = false) String newIdsOrder, 
						  	  				ModelMap model) {
		
    	if(newIdsOrder != null && newIdsOrder.trim().length() > 0) {
    		//parse the page ids
    		String[] pagesIds = newIdsOrder.split("_");
    		if(pagesIds != null) {
    			for(int i=0; i<pagesIds.length; i++) {
    				String pageIdStr = pagesIds[i];    				
    				if(pageIdStr != null && !pageIdStr.trim().equals("")) {
    					pageService.changePageIndex(new Long(pageIdStr), (i+1), false);
    				}
    			}
    		}
    	}
    	
    	return "ok";
	}
		
	@RequestMapping(value = "/page/image/{pageId}", method = RequestMethod.GET)
	public void getPageImage(@PathVariable Long pageId, 
						     HttpServletResponse response) throws IOException {
	    
		//get the page
		Page page = pageService.findById(pageId);
		
		//get the project id
		Long projectId = page.getProject().getId();
		
		//get the project
		Project project = projectService.findById(projectId);
		
		//can upload pages to the project if...
		if (isLoggedUserAuthorized(project.getPublisher().getId())) {
			//set response type
			response.setContentType("image/jpeg");
			//get the fs manager
			FileSystemManager fsManager = VFS.getManager();
			FileObject pagePath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/" + pageId);
			//copy the file stream into the response
		    IOUtils.copy(pagePath.getContent().getInputStream(), response.getOutputStream());
		}
	}
	
	@RequestMapping(value = "/page/thumb/{pageId}", method = RequestMethod.GET)
	public void getPageThumb(@PathVariable Long pageId, 
						     HttpServletResponse response) throws IOException {
	    
		//get the page
		Page page = pageService.findById(pageId);
		
		//get the project id
		Long projectId = page.getProject().getId();
		
		//get the project
		Project project = projectService.findById(projectId);
		
		//can upload pages to the project if...
		if (isLoggedUserAuthorized(project.getPublisher().getId())) {

			//set content type
			response.setContentType("image/jpeg");

			//get the fs manager
			FileSystemManager fsManager = VFS.getManager();
			FileObject pagePath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/" + pageId);
			FileObject thumbPath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/" + pageId + ProjectService.THUMB_SUFFIX);

			//if the thumb does not
			//exist we create it here
			if(!thumbPath.exists()) {
				try {
					pageService.createPageThumbnail(pagePath);
				} 
				catch (Exception e) {
                    LOGGER.error(e.getMessage(), e);
					return;
				}
			}
			//copy the file stream into the response
		    IOUtils.copy(thumbPath.getContent().getInputStream(), response.getOutputStream());
		}
	}
	
	@RequestMapping("/api/page/addSubtitle/{pageId}")
	public @ResponseBody String addSubtitle(
			@PathVariable Long pageId,
			HttpServletRequest request) {
		
		Long navigationItemId = null;
		Map<Long, String> subtitles = new HashMap<Long, String>();
		
		Map<String, String[]> parameters = request.getParameterMap();
		Iterator<String> keysIt = parameters.keySet().iterator();
		while(keysIt.hasNext()) {
			
			String key = keysIt.next();
			String value = parameters.get(key)[0];
			
			if(key.equals("rectangleId")) {
				navigationItemId = new Long(value);
			}
			else if(key.startsWith("language_")) {
				subtitles.put(new Long(key.split("_")[1]), value);
			}
		}
		
		//save the subtitles
		pageService.saveSubtitles(navigationItemId, subtitles);
		
		return "ok";
	}
	
	@RequestMapping(value = "/page/gotoUploadPageImage/{pageId}", method = RequestMethod.GET)
	public String gotoUploadPageImage(@PathVariable Long pageId, ModelMap model) {
		
		//get the page
		Page page = pageService.findById(pageId);
		
		//get the project
		Project project = projectService.findById(page.getProject().getId());

		// can upload pages to the project if...
		if (isLoggedUserAuthorized(project.getPublisher().getId())) {
	    	//set attributes
	    	model.addAttribute("selectedPage", page);
	    	model.addAttribute("project", project);
		}
    	
		return "views/page/upload";
	}

	@RequestMapping("/api/page/uploadPage")
	public @ResponseBody
	String uploadPage(@RequestParam MultipartFile file,
					  @RequestParam(required = true) Long pageId, HttpServletRequest request) {

		try {

			//get the page
			Page page = pageService.findById(pageId);
			
			//get the project id
			Long projectId = page.getProject().getId();
			
			//get the project
			Project project = projectService.findById(projectId);

			// can upload pages to the project if...
			if (isLoggedUserAuthorized(project.getPublisher().getId())) {
				
				//get the fs manager to create the tmp file
				FileSystemManager fsManager = VFS.getManager();
				FileObject pagePath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/" + pageId);
				pagePath.createFile();

				// read from multipart inputstream and
				// write into file output stream
				BufferedOutputStream bos = new BufferedOutputStream(pagePath.getContent().getOutputStream());
				BufferedInputStream bis = new BufferedInputStream(file.getInputStream());
				byte[] buffer = new byte[8192];
				int i;
				while ((i = bis.read(buffer)) != -1) {
					bos.write(buffer, 0, i);
				}
				
				bos.flush();
				bos.close();
				bis.close();
				
                //create the thumb
                pageService.createPageThumbnail(pagePath);

				//update page size and dimensions
				pageService.calculatePageImageDimensionsAndSize(pageId);

                //increment the page version
                pageService.incrementPageVersion(pageId);
				
				return "{\"status\":\"File was uploaded successfuly!\"}";
			} 
			else {
				return null;
			}
		} 
		catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
			return null;
		}
	}
}