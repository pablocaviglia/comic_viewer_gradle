package org.gmi.thereaderengine.publisher.config;

import com.apocalipta.comic.constants.ProjectStatus;
import com.apocalipta.comic.constants.PublicationStatus;
import org.gmi.thereaderengine.common.service.ProjectService;
import org.gmi.thereaderengine.common.util.ImageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.sql.*;

/**
 * Created by pablo on 31/01/15.
 */
public class Backdata {

    private static final Logger LOGGER = LoggerFactory.getLogger(Backdata.class);

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL_LEGACY = "jdbc:mysql://localhost/readerback";
    static final String DB_URL_NEW = "jdbc:mysql://localhost/thereaderengine";
    static final String USER = "root";
    static final String PASS = "root";
    static final String BASE_CONTENT_FOLDER = "/home/pablo/devel/other/content/";

    public static void back() {

        Connection connectionLegacy = null;
        Connection connectionNew = null;
        Statement stmt = null;
        try {
            Class.forName(JDBC_DRIVER);
            LOGGER.info("Connecting to database...");
            connectionLegacy = DriverManager.getConnection(DB_URL_LEGACY, USER, PASS);
            connectionNew = DriverManager.getConnection(DB_URL_NEW, USER, PASS);

            String sql = null;
            ResultSet rs = null;
            PreparedStatement stmtNew = null;

            //clean new database
            stmtNew = connectionNew.prepareStatement("delete from SUBTITLE");
            stmtNew.executeUpdate();
            stmtNew = connectionNew.prepareStatement("delete from PROJECT_I18N_TEXT");
            stmtNew.executeUpdate();
            stmtNew = connectionNew.prepareStatement("delete from PROJECT_LANGUAGE");
            stmtNew.executeUpdate();
            stmtNew = connectionNew.prepareStatement("delete from NAVIGATION_ITEM");
            stmtNew.executeUpdate();
            stmtNew = connectionNew.prepareStatement("delete from PAGE");
            stmtNew.executeUpdate();
            stmtNew = connectionNew.prepareStatement("delete from PUBLICATION");
            stmtNew.executeUpdate();
            stmtNew = connectionNew.prepareStatement("delete from PROJECT");
            stmtNew.executeUpdate();


            //projects
            stmt = connectionLegacy.createStatement();
            sql = "SELECT * FROM PROJECT";
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int projectId = rs.getInt("id");
                String code = rs.getString("code");
                String chapterNameEnglish = rs.getString("chapter_name_english");
                String chapterNameSpanish = rs.getString("chapter_name_spanish");
                String nameEnglish = rs.getString("name_english");
                String nameSpanish = rs.getString("name_spanish");
                String projectName = rs.getString("project_name");
                String ageRate = rs.getString("age_rate");
                String credits = rs.getString("credits");

                LOGGER.info("---> " + nameEnglish);

                //project
                String newProjectSql = "insert into PROJECT(ID, AGE_RATE, CODE, ORIENTAL_READING, PROJECT_NAME, STATUS, DEFAULT_LANGUAGE, NATIVE_LANGUAGE, PUBLISHER) " +
                        "values " +
                        "(" +
                        projectId + ", " +
                        "'" + ageRate + "', " +
                        "'" + code + "', " +
                        "false, " +
                        "'" + projectName + "', " +
                        "'" + ProjectStatus.APPROVED + "', " +
                        "11, " +
                        "11, " +
                        "1 " +
                        ")";

                stmtNew = connectionNew.prepareStatement(newProjectSql);
                stmtNew.executeUpdate();
                stmtNew.close();

                //publication
                String newPublicationSql = "insert into PUBLICATION(STATUS, VERSION, PROJECT) " +
                        "values " +
                        "(" +
                        "'" + PublicationStatus.UNPUBLISHED + "', " +
                        0 + ", " +
                        projectId +
                        ")";

                stmtNew = connectionNew.prepareStatement(newPublicationSql);
                stmtNew.executeUpdate();
                stmtNew.close();

                //languages
                stmtNew = connectionNew.prepareStatement("insert into PROJECT_LANGUAGE(PROJECT_ID, LANGUAGE_ID) VALUES(" + projectId + ", 11)"); //spanish
                stmtNew.executeUpdate();
                stmtNew.close();
                stmtNew = connectionNew.prepareStatement("insert into PROJECT_LANGUAGE(PROJECT_ID, LANGUAGE_ID) VALUES(" + projectId + ", 37)"); //english
                stmtNew.executeUpdate();
                stmtNew.close();

                //i18n name
                stmtNew = connectionNew.prepareStatement("insert into PROJECT_I18N_TEXT(TAG, VALUE, LANGUAGE, PROJECT) VALUES('NAME','" + nameSpanish + "', 11, " + projectId + ")"); //spanish
                stmtNew.executeUpdate();
                stmtNew.close();
                stmtNew = connectionNew.prepareStatement("insert into PROJECT_I18N_TEXT(TAG, VALUE, LANGUAGE, PROJECT) VALUES('NAME','" + nameEnglish + "', 37, " + projectId + ")"); //english
                stmtNew.executeUpdate();
                stmtNew.close();

                //i18n chapter name
                stmtNew = connectionNew.prepareStatement("insert into PROJECT_I18N_TEXT(TAG, VALUE, LANGUAGE, PROJECT) VALUES('CHAPTER_NAME','" + chapterNameSpanish + "', 11, " + projectId + ")"); //spanish
                stmtNew.executeUpdate();
                stmtNew.close();
                stmtNew = connectionNew.prepareStatement("insert into PROJECT_I18N_TEXT(TAG, VALUE, LANGUAGE, PROJECT) VALUES('CHAPTER_NAME','" + chapterNameEnglish + "', 37, " + projectId + ")"); //english
                stmtNew.executeUpdate();
                stmtNew.close();

                //i18n credits
                stmtNew = connectionNew.prepareStatement("insert into PROJECT_I18N_TEXT(TAG, VALUE, LANGUAGE, PROJECT) VALUES('CREDITS','" + credits + "', 11, " + projectId + ")"); //spanish
                stmtNew.executeUpdate();
                stmtNew.close();

            }
            rs.close();
            stmt.close();

            //projects
            stmt = connectionLegacy.createStatement();
            sql = "SELECT * FROM PAGE";
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                long pageId = rs.getLong("id");
                int pageIndex = rs.getInt("page_index");
                long projectId = rs.getLong("project");

                //calculate image dimensions
                File imageFile = new File(BASE_CONTENT_FOLDER, ProjectService.PROJECTS_LOCATION + projectId + "/" + pageId);
                File thumbFile = new File(BASE_CONTENT_FOLDER, ProjectService.PROJECTS_LOCATION + projectId + "/" + pageId + ProjectService.THUMB_SUFFIX);

                //calculate the image dimensions
                ImageInfo imageImageInfo = new ImageInfo(imageFile);
                ImageInfo thumbImageInfo = new ImageInfo(thumbFile);

                LOGGER.info("IM ----> " + imageImageInfo.toString() + "\t" + imageFile.length());
                LOGGER.info("TH ----> " + thumbImageInfo.toString() + "\t" + thumbFile.length());

                stmtNew = connectionNew.prepareStatement(
                        "insert into PAGE(ID, PAGE_INDEX, VERSION, PROJECT, IMAGE_FILESIZE, IMAGE_WIDTH, IMAGE_HEIGHT, THUMB_FILESIZE, THUMB_WIDTH, THUMB_HEIGHT) " +
                        "VALUES(" + pageId + "," + pageIndex + ", 0, " + projectId + ", " + imageFile.length() + ", " + imageImageInfo.getWidth() + ", " + imageImageInfo.getHeight() + "," +
                                thumbFile.length() + ", " + thumbImageInfo.getWidth() + ", " + thumbImageInfo.getHeight() + ")");
                stmtNew.executeUpdate();
                stmtNew.close();
            }


            //navigation items
            stmt = connectionLegacy.createStatement();
            sql = "SELECT * FROM NAVIGATION_ITEM";
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int width = rs.getInt("width");
                int height = rs.getInt("height");
                int navigationIndex = rs.getInt("navigation_index");
                int x = rs.getInt("x");
                int y = rs.getInt("y");
                long page = rs.getLong("page");

                stmtNew = connectionNew.prepareStatement("insert into NAVIGATION_ITEM(WIDTH, HEIGHT, X, Y, NAVIGATION_INDEX, PAGE) VALUES(" + width + "," + height + ", " + x + ", " + y + ", " + navigationIndex + ", " + page + ")");
                stmtNew.executeUpdate();
                stmtNew.close();

            }
            rs.close();
            stmt.close();


            connectionLegacy.close();
            connectionNew.close();
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }// nothing we can do
            try {
                if (connectionLegacy != null)
                    connectionLegacy.close();
                if (connectionNew != null)
                    connectionNew.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
        LOGGER.info("Goodbye!");
    }

//    public static void main(String[] args) {
//        try {
//            Backdata.back();
//        } catch(Exception e) {
//            LOGGER.error(e.getMessage(), e);
//        }
//    }
}