package org.gmi.thereaderengine.publisher.controller;

import com.apocalipta.comic.constants.UserI18NTag;
import com.apocalipta.comic.vo.v1.UserI18NTextVO;

import org.apache.commons.io.IOUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.gmi.thereaderengine.common.adapter.UserI18NTextAdapter;
import org.gmi.thereaderengine.common.exception.BusinessException;
import org.gmi.thereaderengine.common.jpa.model.User;
import org.gmi.thereaderengine.common.jpa.model.UserI18NText;
import org.gmi.thereaderengine.common.service.LanguageService;
import org.gmi.thereaderengine.common.service.UserService;
import org.gmi.thereaderengine.common.service.Useri18nTextService;
import org.gmi.thereaderengine.common.util.ShellClient;
import org.gmi.thereaderengine.publisher.form.CreateUserForm;
import org.gmi.thereaderengine.publisher.form.ModifyUserForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

/**
 * Created by pablo on 19/03/15.
 */
@Controller
public class UserController extends GenericController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private UserService userService;

    @Resource(name = "messageSource")
    private MessageSource messageSource;

    // path to the content folder
    @Value("#{system['CONTENT_PATH']}") String CONTENT_PATH;

    @Autowired
    private Useri18nTextService useri18nTextService;

    @Autowired
    private LanguageService languageService;

    @RequestMapping(value = "/userRegistration/finish/{uid}/{uuid}", method = RequestMethod.GET)
    public String finishRegistration(@PathVariable Long uid,
                                     @PathVariable String uuid,
                                     HttpServletRequest request) {


        boolean validRegistration = userService.validateRegistration(uid, uuid);
        if(validRegistration) {
            userService.deleteRegistration(uid);
        }

        request.setAttribute("validRegistration", validRegistration);
        return "views/user/mail_confirmed";
    }

    @RequestMapping(value = "/userRegistration/display_create", method = RequestMethod.GET)
    public String displayCreateUserForm(ModelMap model) {
        model.addAttribute("createUserForm", new CreateUserForm());
        return "views/user/create";
    }

    @RequestMapping(
            value = "/user/publish/{id}",
            method = { RequestMethod.GET, RequestMethod.POST })
    public String publish(@PathVariable Long id) throws BusinessException {

        if(isLoggedUserAuthorized(id)) {
            userService.publish(id, true);
        }

        return "forward:/user/get/" + id;
    }

    @RequestMapping(value = "/userRegistration/create", method = RequestMethod.POST)
    public String create(@Valid CreateUserForm createUserForm,
                         BindingResult result,
                         ModelMap model,
                         HttpServletRequest request) throws BusinessException, IOException {

        String recaptchaValue = request.getParameter("g-recaptcha-response");

        boolean validCaptcha = userService.validateCaptcha(recaptchaValue);

        if(createUserForm.getPassword() != null && createUserForm.getRepeatPassword() != null) {
            if(!createUserForm.getPassword().equals(createUserForm.getRepeatPassword())) {
                result.addError(new FieldError("createUserForm", "repeatPassword", messageSource.getMessage("user.password.not_equals", new Object[0], LocaleContextHolder.getLocale())));
            }
        }

        if(!validCaptcha) {
            result.addError(new FieldError("createUserForm", "captcha", messageSource.getMessage("user.invalid_captcha", new Object[0], LocaleContextHolder.getLocale())));
        }

        if(userService.existsByUsername(createUserForm.getEmail())) {
            result.addError(new FieldError("createUserForm", "email", messageSource.getMessage("user.email.already_exists", new Object[0], LocaleContextHolder.getLocale())));
        }

        if (result.hasErrors()) {
            return "views/user/create";
        }
        else {
            //create the user
            userService.createUser(createUserForm.getEmail(), createUserForm.getName(), createUserForm.getPassword(), Long.valueOf(createUserForm.getDefaultLanguageId()));
            return "views/user/mail_confirmation";
        }
    }

    @RequestMapping(value="/user/get/{id}", method = {RequestMethod.GET, RequestMethod.POST})
    public String get(@PathVariable Long id, Model model) {

        String retView = getLoginView(); //by default if not enough rights
        if(isLoggedUserAuthorized(id)) {
            User user = userService.findById(id);
            model.addAttribute("user", user);
            model.addAttribute("userForm", new ModifyUserForm(user));
            retView = "views/user/view";
        }

        return retView;
    }

    @RequestMapping(value = "/user/modify", method = RequestMethod.POST)
    public String modify(@Valid @ModelAttribute(value="userForm") ModifyUserForm userForm, BindingResult result, Model model) throws BusinessException {
        //get the user
        User user = userService.findById(userForm.getId());
        //can edit the user if...
        if(isLoggedUserAuthorized(user.getId())) {

            //set the project object again
            model.addAttribute("user", user);

            if(null != userForm.getPassword()       && !"".equals(userForm.getPassword().trim()) &&
               null != userForm.getRepeatPassword() && !"".equals(userForm.getRepeatPassword().trim())) {
                if(!userForm.getPassword().equals(userForm.getRepeatPassword())) {
                    result.addError(new FieldError("createUserForm", "repeatPassword", messageSource.getMessage("user.password.not_equals", new Object[0], LocaleContextHolder.getLocale())));
                }
            }

            if (result.hasErrors()) {
                userForm.setPassword(null);
                userForm.setRepeatPassword(null);
                return "views/user/view";
            }
            else {
                //do the user update
                user.setName(userForm.getName());
                user.setEmail(userForm.getEmail());
                user.setPassword(userForm.getPassword());

                //store it in the database
                userService.store(user);
                return "redirect:/user/get/" + user.getId();
            }
        }
        return null;
    }

    @RequestMapping("/api/user/getI18N")
    public @ResponseBody
    List<UserI18NTextVO> getI18N(
            @RequestParam(required = true) Long userId,
            @RequestParam(required = true) UserI18NTag tag) {
        try {
            // get the user
            if (isLoggedUserAuthorized(userId)) {
                //get the i18n project texts
                List<UserI18NText> userI18NTextList = useri18nTextService.findByUserTag(userId, tag);
                //transform to VOs
                List<UserI18NTextVO> userI18NTextVOList = new ArrayList<UserI18NTextVO>();
                for(UserI18NText userI18NText : userI18NTextList) {
                    UserI18NTextVO userI18NTextVO = new UserI18NTextVO();
                    UserI18NTextAdapter.toVO(userI18NText, userI18NTextVO);
                    userI18NTextVOList.add(userI18NTextVO);
                }
                return userI18NTextVOList;
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/user/saveI18N")
    public @ResponseBody
    String save18N(
            @RequestParam(required = true) Long userId,
            @RequestParam(required = true) Long languageId,
            @RequestParam(required = true) UserI18NTag tag,
            @RequestParam(required = true) String value) {
        try {
            // can upload pages to the project if...
            if (isLoggedUserAuthorized(userId)) {
                useri18nTextService.save(userId, languageId, tag, value);
                return "ok";
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/user/addLanguage")
    public @ResponseBody
    String addLanguage(@RequestParam(required = true) Long userId,
                       @RequestParam(required = true) Long languageId) {
        try {
            if (isLoggedUserAuthorized(userId)) {
                boolean exists = languageService.saveUserLanguage(userId, languageId);
                if(!exists) {
                    return "ok";
                }
                else {
                    return "exists";
                }
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/user/deleteLanguage")
    public @ResponseBody
    String deleteLanguage(@RequestParam(required = true) Long userId,
                          @RequestParam(required = true) Long languageId) {
        try {
            if (isLoggedUserAuthorized(userId)) {
                User user = userService.findById(userId);
                if(languageId == user.getDefaultLanguage().getId()) {
                    return "error_default_language";
                }
                languageService.removeUserLanguage(userId, languageId);
                return "ok";
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping("/api/user/changeDefaultLanguage")
    public @ResponseBody
    String changeDefaultLanguage(@RequestParam(required = true) Long userId,
                                 @RequestParam(required = true) Long languageId) {
        try {
            if (isLoggedUserAuthorized(userId)) {
                //change
                languageService.changeDefaultUserLanguage(userId, languageId);
                return "ok";
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

    @RequestMapping(value = "/user/getAvatar/{userId}", method = RequestMethod.GET)
    public void getAvatar(@PathVariable Long userId,
                         HttpServletResponse response) throws IOException {

        if (isLoggedUserAuthorized(userId)) {
            //set response type
            response.setContentType("image/jpeg");
            //get the fs manager
            FileSystemManager fsManager = VFS.getManager();
            FileObject pagePath = fsManager.resolveFile(CONTENT_PATH + UserService.USERS_LOCATION + userId + "/" + UserService.AVATAR_NAME);
            if(pagePath.exists()) {
                //copy the file stream into the response
                IOUtils.copy(pagePath.getContent().getInputStream(), response.getOutputStream());
            }
        }
    }

    @RequestMapping("/api/user/uploadAvatar")
    public @ResponseBody
    String uploadAvatar(@RequestParam MultipartFile file,
                        @RequestParam(required = true) Long userId, HttpServletRequest request) {

        try {
            // can upload pages to the project if...
            if (isLoggedUserAuthorized(userId)) {

                //get the fs manager to create the tmp file
                FileSystemManager fsManager = VFS.getManager();
                FileObject tmpIconPath = fsManager.resolveFile(CONTENT_PATH + UserService.USERS_LOCATION + userId + "/tmp_avatar");
                tmpIconPath.createFile();

                // read from multipart inputstream and
                // write into file output stream
                BufferedOutputStream bos = new BufferedOutputStream(tmpIconPath.getContent().getOutputStream(), 1024);
                BufferedInputStream bis = new BufferedInputStream(file.getInputStream());
                byte[] buffer = new byte[1024];
                int i = -1;
                while ((i = bis.read(buffer)) != -1) {
                    bos.write(buffer, 0, i);
                }

                bos.flush();
                bos.close();
                bis.close();

                //convert the image
                ShellClient.exec(new String[]{
                                "cd " + tmpIconPath.getParent().getName().getPath(),
                                "convert -resize 512X512! " + tmpIconPath.getName().getPath() + " " + tmpIconPath.getParent().getName().getPath() + "/" + UserService.AVATAR_NAME,
                                "rm " + tmpIconPath.getName().getPath()},
                        null);

                return "{\"status\":\"File was uploaded successfuly!\"}";
            }
            else {
                return null;
            }
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }
}