package org.gmi.thereaderengine.publisher.web;

import com.apocalipta.comic.constants.PublicationStatus;

import org.gmi.thereaderengine.common.jpa.model.UserRole;
import org.gmi.thereaderengine.common.service.LanguageService;
import org.gmi.thereaderengine.common.service.ProjectGenreService;
import org.gmi.thereaderengine.common.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;

/**
 * Created by pablo on 13/12/14.
 */
@Component
public class WebInitComponent implements ApplicationListener<ContextRefreshedEvent> {

    private static Logger LOGGER = LoggerFactory.getLogger(WebInitComponent.class);

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private LanguageService languageService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private ProjectGenreService projectGenreService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        LOGGER.info("***************************** Initializing The Reader Engine Server (Publisher)... *****************************");

        //LANGUAGES
        servletContext.setAttribute("LANGUAGES", languageService.findAllByRelevance());

        //PROJECT GENRES
        servletContext.setAttribute("PROJECT_GENRES", projectGenreService.findAll());

        //USER ROLES
        servletContext.setAttribute("USER_ROLES", UserRole.values());

        //PUBLICATION STATUS
        servletContext.setAttribute("PUBLICATION_STATUS", PublicationStatus.values());

    }
}