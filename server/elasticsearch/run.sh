#!/bin/bash
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
if [ -f $DIR/es.pid ]; then
  echo 'Killing previous ElasticSearch run with pid ' `cat es.pid`
  kill -9 `cat es.pid`
fi

export ES_MIN_MEM=4g
export ES_MAX_MEM=4g

./elasticsearch*/bin/elasticsearch -d -p es.pid
