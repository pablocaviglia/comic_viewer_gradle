package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.jpa.model.Language;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.gmi.thereaderengine.common.jpa.model.Title;
import org.gmi.thereaderengine.common.jpa.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by pablo on 08/01/15.
 */
@Repository
public class LanguageDaoImpl extends GenericDaoImpl<Language> implements LanguageDao {

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private TitleDao titleDao;

    @Autowired
    private UserDao userDao;

    @Override
    public long count() {
        TypedQuery<Long> namedQuery = entityManager.createNamedQuery("Language.count", Long.class);
        return namedQuery.getSingleResult();
    }

    @Transactional
    @Override
    public void insert(List<Language> languageList) {
        for(Language language : languageList) {
            entityManager.persist(language);
        }
    }

    @Override
    public List<Language> findAllByRelevance() {
        TypedQuery<Language> namedQuery = entityManager.createNamedQuery("Language.findAllByRelevance", Language.class);
        List<Language> languages = namedQuery.getResultList();
        return languages;
    }

    @Override
    public Language findLanguageByCode(String code) {
        TypedQuery<Language> namedQuery = entityManager.createNamedQuery("Language.findByCode", Language.class);
        namedQuery.setParameter("code", code);

        try {
            Language language = namedQuery.getSingleResult();
            return language;
        }
        catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public boolean saveUserLanguage(long userId, long languageId) {

        boolean exists = false;

        User user = userDao.findById(userId);
        Language language = findById(languageId);

        //create collection if necessary
        List<Language> userLanguages = user.getLanguages();
        if(userLanguages == null)  {
            userLanguages = new ArrayList<Language>();
        }

        //check existence
        for(Language currentLanguage : userLanguages) {
            if(currentLanguage.getId() == languageId) {
                exists = true;
                break;
            }
        }

        if(!exists) {
            //add language
            userLanguages.add(language);
            //store user
            userDao.store(user);
        }

        return exists;
    }

    @Override
    public boolean saveTitleLanguage(long titleId, long languageId) {

        boolean exists = false;

        Title title = titleDao.findById(titleId);
        Language language = findById(languageId);

        //create collection if necessary
        List<Language> languages = title.getLanguages();
        if(languages == null)  {
            languages = new ArrayList<>();
        }

        //check existence
        for(Language currentLanguage : languages) {
            if(currentLanguage.getId() == languageId) {
                exists = true;
                break;
            }
        }

        if(!exists) {
            //add language
            languages.add(language);
            //store project
            titleDao.store(title);
        }

        return exists;
    }

    @Override
    public void removeTitleLanguage(long titleId, long languageId) {

        Title title = titleDao.findById(titleId);
        List<Language> languages = title.getLanguages();

        //search for the language to delete
        Iterator<Language> languageIt = languages.iterator();
        while(languageIt.hasNext()) {
            Language language = languageIt.next();
            if(language.getId() == languageId) {
                languageIt.remove();
                break;
            }
        }

        //save the project
        titleDao.store(title);

    }

    @Override
    public boolean saveProjectLanguage(long projectId, long languageId) {

        boolean exists = false;

        Project project = projectDao.findById(projectId);
        Language language = findById(languageId);

        //create collection if necessary
        List<Language> projectLanguages = project.getLanguages();
        if(projectLanguages == null)  {
            projectLanguages = new ArrayList<>();
        }

        //check existence
        for(Language currentLanguage : projectLanguages) {
            if(currentLanguage.getId() == languageId) {
                exists = true;
                break;
            }
        }

        if(!exists) {
            //add language
            projectLanguages.add(language);
            //store project
            projectDao.store(project);
        }

        return exists;
    }

    @Override
    public void removeProjectLanguage(long projectId, long languageId) {

        //get the project
        Project project = projectDao.findById(projectId);
        List<Language> languages = project.getLanguages();

        //search for the language to delete
        Iterator<Language> languageIt = languages.iterator();
        while(languageIt.hasNext()) {
            Language language = languageIt.next();
            if(language.getId() == languageId) {
                languageIt.remove();
                break;
            }
        }

        //save the project
        projectDao.store(project);

    }

    @Override
    public void removeUserLanguage(long userId, long languageId) {

        //get the project
        User user = userDao.findById(userId);
        List<Language> languages = user.getLanguages();

        //search for the language to delete
        Iterator<Language> languageIt = languages.iterator();
        while(languageIt.hasNext()) {
            Language language = languageIt.next();
            if(language.getId() == languageId) {
                languageIt.remove();
                break;
            }
        }

        //save the user
        userDao.store(user);

    }

    @Override
    public void changeDefaultProjectLanguage(long projectId, long languageId) {

        //get project
        Project project = projectDao.findById(projectId);

        //find new default language
        Language defaultLanguageNew = findById(languageId);

        //add new language if it
        //does not already exists
        boolean exists = false;
        for(Language language : project.getLanguages()) {
            if(language.getId() == languageId) {
                exists = true;
            }
        }

        //if the new default language
        //does not exists in the list of
        //the project languages, add it
        if(!exists) {
            project.getLanguages().add(defaultLanguageNew);
        }

        //set new default language
        project.setDefaultLanguage(defaultLanguageNew);

        projectDao.store(project);
    }

    @Override
    public void changeDefaultUserLanguage(long userId, long languageId) {

        //get user
        User user = userDao.findById(userId);

        //find new default language
        Language defaultLanguageNew = findById(languageId);

        //add new language if it
        //does not already exists
        boolean exists = false;
        for(Language language : user.getLanguages()) {
            if(language.getId() == languageId) {
                exists = true;
            }
        }

        //if the new default language
        //does not exists in the list of
        //the project languages, add it
        if(!exists) {
            user.getLanguages().add(defaultLanguageNew);
        }

        //set new default language
        user.setDefaultLanguage(defaultLanguageNew);

        userDao.store(user);
    }

    @Override
    public void changeDefaultTitleLanguage(long titleId, long languageId) {

        Title title = titleDao.findById(titleId);
        Language defaultLanguageNew = findById(languageId);

        //add new language if it
        //does not already exists
        boolean exists = false;
        for(Language language : title.getLanguages()) {
            if(language.getId() == languageId) {
                exists = true;
            }
        }

        if(!exists) {
            title.getLanguages().add(defaultLanguageNew);
        }

        //set new default language
        title.setDefaultLanguage(defaultLanguageNew);

        titleDao.store(title);
    }
}