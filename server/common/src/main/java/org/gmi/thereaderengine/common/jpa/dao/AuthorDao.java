package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.jpa.model.Author;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by pablo on 16/04/15.
 */
public interface AuthorDao extends GenericDao<Author> {

    List<Author> findByProjectId(long projectId);

    @Transactional
    void deleteByProjectId(long projectId);
}
