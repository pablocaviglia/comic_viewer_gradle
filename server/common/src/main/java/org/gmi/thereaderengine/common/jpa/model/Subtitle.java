package org.gmi.thereaderengine.common.jpa.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({ 
	@NamedQuery(name = "Subtitle.findByNavigationItemIdAndLanguage", 
			query = "select s from Subtitle s where s.navigationItem.id = :navigationItemId and s.language.id = :languageId"),
	@NamedQuery(name = "Subtitle.deleteByNavigationItemAndLanguage", 
			query = "delete from Subtitle s where s.navigationItem.id = :navigationItemId and s.language.id = :languageId"),
			
})
@Table(name = "SUBTITLE", indexes = {
		@Index(columnList = "ID", name = "subtitleIdIndex"),
		@Index(columnList = "NAVIGATION_ITEM", name = "subtitleNavigationItemIndex"),
		@Index(columnList = "LANGUAGE", name = "subtitleLanguageIndex"),
})
public class Subtitle implements Serializable {
	
	private static final long serialVersionUID = 1017915959957012342L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="NAVIGATION_ITEM")
	private NavigationItem navigationItem;
	
	@ManyToOne
	@JoinColumn(name="LANGUAGE")
	private Language language;

	@Column(name = "SUBTITLE", nullable = false, length = 8192)
	private String subtitle;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NavigationItem getNavigationItem() {
		return navigationItem;
	}

	public void setNavigationItem(NavigationItem navigationItem) {
		this.navigationItem = navigationItem;
	}

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
}