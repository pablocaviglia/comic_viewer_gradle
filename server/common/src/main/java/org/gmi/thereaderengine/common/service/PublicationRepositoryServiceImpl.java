package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.ProjectAgeRate;
import com.apocalipta.comic.constants.ProjectI18NTag;
import com.apocalipta.comic.constants.TitleI18NTag;

import org.gmi.thereaderengine.common.adapter.AuthorAdapter;
import org.gmi.thereaderengine.common.adapter.LanguageAdapter;
import org.gmi.thereaderengine.common.adapter.ProjectGenreAdapter;
import org.gmi.thereaderengine.common.adapter.ProjectI18NTextAdapter;
import org.gmi.thereaderengine.common.adapter.TitleAdapter;
import org.gmi.thereaderengine.common.adapter.TitleI18NTextAdapter;
import org.gmi.thereaderengine.common.adapter.UserAdapter;
import org.gmi.thereaderengine.common.elasticsearch.document.AuthorDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.LanguageDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.PublicationDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.PublicationGenreDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.PublisherDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.TitleDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.i18nDocument;
import org.gmi.thereaderengine.common.elasticsearch.repository.PublicationRepository;
import org.gmi.thereaderengine.common.elasticsearch.repository.PublisherRepository;
import org.gmi.thereaderengine.common.jpa.model.ProjectI18NText;
import org.gmi.thereaderengine.common.jpa.model.Publication;
import org.gmi.thereaderengine.common.jpa.model.TitleI18NText;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

/**
 * Created by pablo on 11/04/15.
 */
@Service
@Singleton
public class PublicationRepositoryServiceImpl implements PublicationRepositoryService {

    @Autowired
    private PublisherRepository publisherRepository;

    @Autowired
    private PublicationRepository publicationRepository;

    @Autowired
    private Projecti18nTextService projecti18nTextService;

    @Autowired
    private Titlei18nTextService titlei18nTextService;

    @Override
    public void save(Publication publication) {

        //new document with id
        PublicationDocument publicationDocument = new PublicationDocument();
        publicationDocument.setId(publication.getId().toString());

        //languages
        publicationDocument.setLanguages(new ArrayList<>());
        publication.getProject().getLanguages().forEach(language -> publicationDocument.getLanguages().add(LanguageAdapter.toDocument(language, new LanguageDocument())));

        //authors
        if(null != publication.getProject().getAuthors()) {
            publicationDocument.setAuthors(new ArrayList<>());
            publication.getProject().getAuthors().forEach(author -> publicationDocument.getAuthors().add(AuthorAdapter.toDocument(author, new AuthorDocument())));
        }

        //native language
        publicationDocument.setNativeLanguage(LanguageAdapter.toDocument(publication.getProject().getNativeLanguage(), new LanguageDocument()));

        //oriental reading
        publicationDocument.setOrientalReading(publication.getProject().getOrientalReading().toString());

        //age rate
        publicationDocument.setAgeRate(publication.getProject().getAgeRate().name());

        //genre
        if(null != publication.getProject().getGenre()) {
            publicationDocument.setPublicationGenre(ProjectGenreAdapter.loadDocument(publication.getProject().getGenre(), new PublicationGenreDocument()));
        }

        //title
        if(null != publication.getProject().getTitle()) {
            publicationDocument.setTitle(TitleAdapter.toDocument(publication.getProject().getTitle(), new TitleDocument()));
            publicationDocument.getTitle().setName(new ArrayList<>());
            List<TitleI18NText> nameTitleI18NTextList = titlei18nTextService.findByTitleAndTag(publication.getProject().getTitle().getId(), TitleI18NTag.NAME);
            nameTitleI18NTextList.forEach(titleI18NText -> publicationDocument.getTitle().getName().add(TitleI18NTextAdapter.toDocument(titleI18NText, new i18nDocument())));
        }

        //name
        publicationDocument.setName(new ArrayList<>());
        List<ProjectI18NText> nameProjectI18NTextList = projecti18nTextService.findByProjectTag(publication.getProject().getId(), ProjectI18NTag.NAME);
        nameProjectI18NTextList.forEach(projectI18NText -> publicationDocument.getName().add(ProjectI18NTextAdapter.toDocument(projectI18NText, new i18nDocument())));

        //description
        publicationDocument.setDescription(new ArrayList<>());
        List<ProjectI18NText> descriptionProjectI18NTextList = projecti18nTextService.findByProjectTag(publication.getProject().getId(), ProjectI18NTag.DESCRIPTION);
        descriptionProjectI18NTextList.forEach(projectI18NText -> publicationDocument.getDescription().add(ProjectI18NTextAdapter.toDocument(projectI18NText, new i18nDocument())));

        //chapter name
        publicationDocument.setChapterName(new ArrayList<>());
        List<ProjectI18NText> chapterNameProjectI18NTextList = projecti18nTextService.findByProjectTag(publication.getProject().getId(), ProjectI18NTag.CHAPTER_NAME);
        chapterNameProjectI18NTextList.forEach(projectI18NText -> publicationDocument.getChapterName().add(ProjectI18NTextAdapter.toDocument(projectI18NText, new i18nDocument())));

        //credits
        publicationDocument.setCredits(new ArrayList<>());
        List<ProjectI18NText> creditsNameProjectI18NTextList = projecti18nTextService.findByProjectTag(publication.getProject().getId(), ProjectI18NTag.CREDITS);
        creditsNameProjectI18NTextList.forEach(projectI18NText -> publicationDocument.getCredits().add(ProjectI18NTextAdapter.toDocument(projectI18NText, new i18nDocument())));

        //publisher
        publicationDocument.setPublisher(UserAdapter.toDocument(publication.getProject().getPublisher(), new PublisherDocument()));

        //tags
        String tags = publication.getProject().getTags();
        if(null != tags && !"".equalsIgnoreCase(tags.trim())) {
            String[] tagsArray = tags.split(",");
            publicationDocument.setTags(new ArrayList<>());
            for(String tag : tagsArray) {
                publicationDocument.getTags().add(tag);
            }
        }

        //first publication date
        publicationDocument.setFirstPublicationDate(publication.getFirstPublicationDate());

        //save the document
        save(publicationDocument);
    }

    @Override
    public PublicationDocument save(PublicationDocument publicationDocument) {

        //save the publication
        publicationRepository.save(publicationDocument);

        //save the publisher
        publisherRepository.save(publicationDocument.getPublisher());

        return publicationDocument;
    }

    @Override
    public PublicationDocument findById(String id) {
        return publicationRepository.findOne(id);
    }

    @Override
    public Page<PublicationDocument> findByOrientalReading(boolean orientalReading, Pageable pageable) {
        return publicationRepository.findByOrientalReading(Boolean.valueOf(orientalReading).toString(), pageable);
    }

    @Override
    public Page<PublicationDocument> findByAgeRate(ProjectAgeRate ageRate, Pageable pageable) {
        return publicationRepository.findByAgeRate(ageRate.name(), pageable);
    }

    @Override
    public Page<PublicationDocument> findByTagsIn(List<String> tags, Pageable pageable) {
        return publicationRepository.findByTagsIn(tags, pageable);
    }

    @Override
    public Page<PublicationDocument> findByNativeLanguageId(String nativeLanguageId, Pageable pageable) {
        return publicationRepository.findByNativeLanguageId(nativeLanguageId, pageable);
    }

    @Override
    public Page<PublicationDocument> findByLanguagesIdIn(List<String> languageIds, Pageable pageable) {
        return publicationRepository.findByLanguagesIdIn(languageIds, pageable);
    }

    @Override
    public Page<PublicationDocument> findViaKeywords(String keywords, Pageable pageable) {
        return publicationRepository.findViaKeywords(keywords, pageable);
    }

}
