package org.gmi.thereaderengine.common.jpa.dao;

import com.apocalipta.comic.constants.ProjectI18NTag;

import org.gmi.thereaderengine.common.jpa.model.ProjectI18NText;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by pablo on 08/01/15.
 */
public interface Projecti18nTextDao extends GenericDao<ProjectI18NText> {

    ProjectI18NText findByProjectLanguageAndTag(long projectId, long languageId, ProjectI18NTag tag);

    Map<Long, Map<Long, Map<String, String>>> findProjectsBaseI18NTexts(Set<Long> projectIds, Set<Long> languageIds);

    List<ProjectI18NText> findByProject(long projectId);

    List<ProjectI18NText> findByProjectTag(long projectId, ProjectI18NTag tag);

    @Transactional
    void deleteByProjectId(long projectId);
}
