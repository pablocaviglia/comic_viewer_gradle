package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.JSONApiVersion;

import org.apache.commons.vfs2.FileSystemException;
import org.gmi.thereaderengine.common.exception.BusinessException;
import org.gmi.thereaderengine.common.jpa.model.User;

import java.io.FileNotFoundException;
import java.io.InputStream;

public interface UserService {

    public static final String USERS_LOCATION = "users/";
    public static final String AVATAR_NAME = "avatar.jpg";
    public static final String USER_DESCRIPTOR = "descriptor.json";

    User findById(Long id);
    User findByUsername(String username);
    boolean existsByUsername(String username);
    void verifyInitialData();
    boolean validateCaptcha(String recaptchaResponse);
    void createUser(String email, String name, String password, Long defaultLanguageId) throws BusinessException;

    void publish(long userId, boolean incrementVersion) throws BusinessException;

    boolean validateRegistration(long userId, String uuid);

    void store(User user) throws BusinessException;
    void deleteRegistration(long userId);

    InputStream findByIdJSON(long publisherId, JSONApiVersion version) throws FileNotFoundException, FileSystemException;
}
