package org.gmi.thereaderengine.common.service;

import org.gmi.thereaderengine.common.jpa.model.Author;

import java.util.List;

/**
 * Created by pablo on 16/04/15.
 */
public interface AuthorService {
    void store(Author author);
    List<Author> findByProjectId(long projectId);
    Author findById(long id);
    void delete(long id);
    void deleteByProjectId(long projectId);
}
