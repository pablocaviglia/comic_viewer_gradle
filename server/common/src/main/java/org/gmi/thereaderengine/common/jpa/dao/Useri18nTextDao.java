package org.gmi.thereaderengine.common.jpa.dao;

import com.apocalipta.comic.constants.UserI18NTag;

import org.gmi.thereaderengine.common.jpa.model.UserI18NText;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by pablo on 08/01/15.
 */
public interface Useri18nTextDao extends GenericDao<UserI18NText> {

    UserI18NText findByUserLanguageAndTag(long userId, long languageId, UserI18NTag tag);

    Map<Long, Map<Long, Map<String, String>>> findUsersBaseI18NTexts(Set<Long> userIds, Set<Long> languageIds);

    List<UserI18NText> findByUser(long userId);
    List<UserI18NText> findByUserTag(long userId, UserI18NTag tag);

}
