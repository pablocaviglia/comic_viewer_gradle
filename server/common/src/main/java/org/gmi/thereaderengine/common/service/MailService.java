package org.gmi.thereaderengine.common.service;

import javax.mail.MessagingException;

/**
 * Created by pablo on 24/03/15.
 */
public interface MailService {

    void sendUserRegisterConfirmationMail(long userId, String uuid);

}
