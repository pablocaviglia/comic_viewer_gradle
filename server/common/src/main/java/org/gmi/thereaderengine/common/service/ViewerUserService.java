package org.gmi.thereaderengine.common.service;

import org.gmi.thereaderengine.common.jpa.model.ViewerUser;

/**
 * Created by pablo on 05/03/15.
 */
public interface ViewerUserService {
    long existsByUsername(String username);
    ViewerUser findByUsername(String username);
    long create(ViewerUser viewerUser);
}
