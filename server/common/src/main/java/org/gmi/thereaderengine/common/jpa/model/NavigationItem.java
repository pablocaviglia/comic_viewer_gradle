package org.gmi.thereaderengine.common.jpa.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@NamedQueries({ 
	@NamedQuery(name = "NavigationItem.findById", 
			query = "select ni from NavigationItem ni where ni.id = :id"),
	@NamedQuery(name = "NavigationItem.findByPageId", 
			query = "select ni from NavigationItem ni where ni.page.id = :pageId"), 
	@NamedQuery(name = "NavigationItem.deleteById", 
			query = "delete from NavigationItem ni where ni.id = :navigationItemId"),
	@NamedQuery(name = "NavigationItem.deleteByPageId", 
			query = "delete from NavigationItem ni where ni.page.id = :pageId"),
	@NamedQuery(name = "NavigationItem.getLastNavigationItemIndex", 
			query = "select max(ni.navigationItemIndex) from NavigationItem ni where ni.page.id = :pageId"),
	@NamedQuery(name = "NavigationItem.findByPageIdAndIndex", 
			query = "select ni from NavigationItem ni where ni.page.id = :pageId and ni.navigationItemIndex = :navigationItemIndex"),
})
@Table(name = "NAVIGATION_ITEM", indexes = {
		@Index(columnList = "ID", name = "navigationItemIdIndex"),
        @Index(columnList = "PAGE", name = "navigationItemPageIndex"),
        @Index(columnList = "NAVIGATION_INDEX", name = "navigationItemIndexIndex"),
})
public class NavigationItem implements Serializable {
	
	private static final long serialVersionUID = -3969758639575258980L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="PAGE")
	private Page page;
	
	@Column(name = "X")
	private Integer x;
	
	@Column(name = "Y")
	private Integer y;
	
	@Column(name = "WIDTH")
	private Integer width;
	
	@Column(name = "HEIGHT")
	private Integer height;

	@Column(name = "NAVIGATION_INDEX")
	private Integer navigationItemIndex;
	
    @OneToMany(fetch=FetchType.LAZY, mappedBy="navigationItem")
    private List<Subtitle> subtitles = new ArrayList<Subtitle>();
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Page getPage() {
		return page;
	}

	public void setPage(Page page) {
		this.page = page;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getNavigationItemIndex() {
		return navigationItemIndex;
	}

	public void setNavigationItemIndex(Integer navigationItemIndex) {
		this.navigationItemIndex = navigationItemIndex;
	}

	public List<Subtitle> getSubtitles() {
		return subtitles;
	}

	public void setSubtitles(List<Subtitle> subtitles) {
		this.subtitles = subtitles;
	}
}