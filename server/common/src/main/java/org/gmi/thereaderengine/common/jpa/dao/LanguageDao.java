package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.jpa.model.Language;

import java.util.List;

/**
 * Created by pablo on 08/01/15.
 */
public interface LanguageDao extends GenericDao<Language> {

    long count();
    void insert(List<Language> languageList);

    List<Language> findAllByRelevance();
    Language findLanguageByCode(String code);

    boolean saveUserLanguage(long userId, long languageId);
    boolean saveProjectLanguage(long projectId, long languageId);
    boolean saveTitleLanguage(long titleId, long languageId);

    void removeTitleLanguage(long titleId, long languageId);
    void removeProjectLanguage(long projectId, long languageId);
    void removeUserLanguage(long userId, long languageId);

    void changeDefaultProjectLanguage(long projectId, long languageId);
    void changeDefaultUserLanguage(long userId, long languageId);
    void changeDefaultTitleLanguage(long titleId, long languageId);

}
