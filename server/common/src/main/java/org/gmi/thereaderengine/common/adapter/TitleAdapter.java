package org.gmi.thereaderengine.common.adapter;

import com.apocalipta.comic.vo.v1.PublisherVO;
import com.apocalipta.comic.vo.v1.TitleVO;

import org.apache.commons.codec.language.bm.Lang;
import org.gmi.thereaderengine.common.elasticsearch.document.LanguageDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.PublisherDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.TitleDocument;
import org.gmi.thereaderengine.common.jpa.model.Language;
import org.gmi.thereaderengine.common.jpa.model.Title;
import org.gmi.thereaderengine.common.jpa.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 14/05/15.
 */
public class TitleAdapter {

    public static TitleVO loadVO(Title title, com.apocalipta.comic.vo.v1.TitleVO titleVO) {

        titleVO.setId(title.getId());
        titleVO.setCode(title.getCode());
        titleVO.setVersion(title.getVersion());

        //publisher
        titleVO.setPublisherVO(UserAdapter.loadVO(title.getPublisher(), new PublisherVO()));

        //default language
        com.apocalipta.comic.vo.v1.LanguageVO defaultLanguageVO = new com.apocalipta.comic.vo.v1.LanguageVO();
        LanguageAdapter.toVO(title.getDefaultLanguage(), defaultLanguageVO);
        titleVO.setDefaultLanguage(defaultLanguageVO);

        //languages
        List<Language> languageList = title.getLanguages();
        titleVO.setLanguages(new ArrayList<>());
        if(languageList != null && languageList.size() > 0) {
            for(Language language : languageList) {
                com.apocalipta.comic.vo.v1.LanguageVO languageVO = new com.apocalipta.comic.vo.v1.LanguageVO();
                LanguageAdapter.toVO(language, languageVO);
                titleVO.getLanguages().add(languageVO);
            }
        }

        return titleVO;
    }

    public static TitleDocument toDocument(Title title, TitleDocument titleDocument) {

        titleDocument.setId(title.getId().toString());
        titleDocument.setCode(title.getCode());
        titleDocument.setCodeNotAnalyzed(title.getCode());
        titleDocument.setPublisher(UserAdapter.toDocument(title.getPublisher(), new PublisherDocument()));

        titleDocument.setLanguages(new ArrayList<>());
        for(Language language : title.getLanguages()) {
            titleDocument.getLanguages().add(LanguageAdapter.toDocument(language, new LanguageDocument()));
        }

        return titleDocument;
    }
}