package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.JSONApiVersion;
import com.apocalipta.comic.constants.UserI18NTag;
import com.google.gson.Gson;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.gmi.thereaderengine.common.adapter.UserAdapter;
import org.gmi.thereaderengine.common.elasticsearch.document.PublisherDocument;
import org.gmi.thereaderengine.common.elasticsearch.repository.PublisherRepository;
import org.gmi.thereaderengine.common.jpa.dao.LanguageDao;
import org.gmi.thereaderengine.common.jpa.dao.UserDao;
import org.gmi.thereaderengine.common.jpa.dao.UserRegistrationDao;
import org.gmi.thereaderengine.common.exception.BusinessException;
import org.gmi.thereaderengine.common.jpa.model.Language;
import org.gmi.thereaderengine.common.jpa.model.User;
import org.gmi.thereaderengine.common.jpa.model.UserI18NText;
import org.gmi.thereaderengine.common.jpa.model.UserRegistration;
import org.gmi.thereaderengine.common.jpa.model.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.inject.Singleton;

@Service
@Singleton
public class UserServiceImpl extends GenericService implements UserService {

    //json mapper
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private @Value("#{system['CONTENT_PATH']}") String CONTENT_PATH;

    @Autowired
	private UserDao userDao;

    @Autowired
    private UserRegistrationDao userRegistrationDao;

    @Autowired
    private LanguageDao languageDao;

    @Autowired
    private MailService mailService;

    @Autowired
    private Useri18nTextService useri18nTextService;

    @Autowired
    private PublisherRepository publisherRepository;

    @Override
    public User findById(Long id) {
        return userDao.findById(id);
    }

    @Override
	public User findByUsername(String username) {
        return userDao.findByUsername(username);
	}

    @Override
    public boolean existsByUsername(String username) {
        return userDao.existsByUsername(username);
    }

    @Override
    public void verifyInitialData() {

        Language englishLanguage = languageDao.findLanguageByCode("en");
        List<Language> languages = new ArrayList<Language>();
        languages.add(englishLanguage);

        User user = findByUsername("diego@apocalipta.com");
        if(user == null) {
            user = new User();
            user.setName("diego");
            user.setUserName("diego@apocalipta.com");
            user.setPassword("diego");
            user.setEmail("diego@apocalipta.com");
            user.setVersion(1l);
            user.setCreationDate(System.currentTimeMillis());
            user.setDefaultLanguage(englishLanguage);
            user.setLanguages(languages);
            user.setRole(UserRole.CONTENT_MANAGER);
            userDao.store(user);
        }

        user = findByUsername("pablo@apocalipta.com");
        if(user == null) {
            user = new User();
            user.setName("pablo");
            user.setUserName("pablo@apocalipta.com");
            user.setPassword("pablo");
            user.setEmail("pablo@apocalipta.com");
            user.setVersion(1l);
            user.setCreationDate(System.currentTimeMillis());
            user.setDefaultLanguage(englishLanguage);
            user.setLanguages(languages);
            user.setRole(UserRole.ADMIN);
            userDao.store(user);
        }
    }

    @Override
    public boolean validateCaptcha(String recaptchaResponse) {
        try {
            CloseableHttpClient httpclient = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost("https://www.google.com/recaptcha/api/siteverify");
            List <NameValuePair> nvps = new ArrayList <NameValuePair>();
            nvps.add(new BasicNameValuePair("secret", "6LcV3wMTAAAAAL2C0CAH5uDx3EitJJ6GfR3DzeCH"));
            nvps.add(new BasicNameValuePair("response", recaptchaResponse));
            httpPost.setEntity(new UrlEncodedFormEntity(nvps));
            CloseableHttpResponse response = httpclient.execute(httpPost);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            BufferedInputStream bis = new BufferedInputStream(response.getEntity().getContent());
            byte[] buffer = new byte[1024];
            int i;
            while((i=bis.read(buffer)) != -1) {
                baos.write(buffer, 0, i);
            }

            String responseContent = new String(baos.toByteArray());
            CaptchaResponse capRes = new Gson().fromJson(responseContent, CaptchaResponse.class);

            return capRes.isSuccess();
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return false;
    }

    @Override
    public void createUser(String email, String name, String password, Long defaultLanguageId) throws BusinessException {

        long registrationDate = System.currentTimeMillis();

        //get the default language
        Language defaultLanguage = languageDao.findById(defaultLanguageId);

        //create user
        User user = new User();
        user.setUserName(email);
        user.setEmail(email);
        user.setName(name);
        user.setRole(UserRole.PUBLISHER);
        user.setPassword(password);
        user.setDefaultLanguage(defaultLanguage);
        user.setCreationDate(registrationDate);
        user.setVersion(0l);

        //languages
        user.setLanguages(new ArrayList<>());
        user.getLanguages().add(defaultLanguage);

        //store it
        user = userDao.store(user);

        //create the user registration object
        UserRegistration userRegistration = new UserRegistration();
        String uuid = UUID.randomUUID().toString();
        userRegistration.setUserId(user.getId());
        userRegistration.setUuid(uuid);
        userRegistration.setRegistrationDate(registrationDate);

        //store it
        userRegistrationDao.store(userRegistration);

        //send the registration mail
        mailService.sendUserRegisterConfirmationMail(user.getId(), uuid);

    }

    @Override
    public void publish(long userId, boolean incrementVersion) throws BusinessException {
        try {
            User user = findById(userId);
            if(incrementVersion) {
                user.setVersion(user.getVersion() + 1);
                userDao.store(user);
            }
            //publish on elastic search
            publisherRepository.save(UserAdapter.toDocument(user, new PublisherDocument()));
            //create the VO
            createPublisherJSON_v1(findById(userId));
        }
        catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private void createPublisherJSON_v1(User user) throws IOException {

        com.apocalipta.comic.vo.v1.PublisherVO publisherVO = new com.apocalipta.comic.vo.v1.PublisherVO();
        UserAdapter.loadVO(user, publisherVO);

        //find user i18n 'information' texts
        List<UserI18NText> userI18NTextListName = useri18nTextService.findByUserTag(user.getId(), UserI18NTag.INFORMATION);
        if(userI18NTextListName != null && userI18NTextListName.size() > 0) {
            publisherVO.setInformation(new HashMap<Long, String>());
            for(UserI18NText userI18NText : userI18NTextListName) {
                publisherVO.getInformation().put(userI18NText.getLanguage().getId(), userI18NText.getValue());
            }
        }

        //get the fs manager
        FileSystemManager fsManager = VFS.getManager();

        //create the publication folder
        FileObject usersFolder = fsManager.resolveFile(CONTENT_PATH + USERS_LOCATION + user.getId() + "/");

        //create json
        FileObject descriptorFile = fsManager.resolveFile(usersFolder, USER_DESCRIPTOR + "_" + JSONApiVersion.v1);
        descriptorFile.createFile();
        OBJECT_MAPPER.writeValue(new File(descriptorFile.getURL().getFile()), publisherVO);

    }

    @Override
    public boolean validateRegistration(long userId, String uuid) {

        //verify if the user was correctly validated
        boolean validRegistration = userRegistrationDao.validateRegistration(userId, uuid);

        if(validRegistration) {
            //publish the user on ES
            User user = findById(userId);
            publisherRepository.save(UserAdapter.toDocument(user, new PublisherDocument()));
        }

        return validRegistration;
    }

    @Override
    public void store(User user) throws BusinessException {
        userDao.store(user);
    }

    @Override
    @Transactional
    public void deleteRegistration(long userId) {
        userRegistrationDao.deleteByUserId(userId);
    }

    @Override
    public InputStream findByIdJSON(long publisherId, JSONApiVersion version) throws FileNotFoundException, FileSystemException {
        FileSystemManager fsManager = VFS.getManager();
        String jsonFilename = UserService.USER_DESCRIPTOR;
        FileObject jsonPath = fsManager.resolveFile(CONTENT_PATH + UserService.USERS_LOCATION + publisherId + "/" + jsonFilename + "_" + version);
        if(jsonPath.exists()) {
            return new FileInputStream(jsonPath.getName().getPath());
        }
        return null;
    }

    class CaptchaResponse {
        public boolean success;
        public boolean isSuccess() {
            return success;
        }
        public void setSuccess(boolean success) {
            this.success = success;
        }
    }
}