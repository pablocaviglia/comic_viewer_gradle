package org.gmi.thereaderengine.common.adapter;

import com.apocalipta.comic.vo.v1.LanguageVO;
import com.apocalipta.comic.vo.v1.PublisherVO;

import org.gmi.thereaderengine.common.elasticsearch.document.PublisherDocument;
import org.gmi.thereaderengine.common.jpa.model.Language;
import org.gmi.thereaderengine.common.jpa.model.User;

import java.util.ArrayList;

/**
 * Created by pablo on 22/02/15.
 */
public class UserAdapter {

    public static PublisherVO loadVO(User user, PublisherVO publisherVO) {
        publisherVO.setId(user.getId());
        publisherVO.setName(user.getName());
        publisherVO.setVersion(user.getVersion());
        publisherVO.setDefaultLanguage(LanguageAdapter.toVO(user.getDefaultLanguage(), new LanguageVO()));
        publisherVO.setLanguages(new ArrayList<>());
        publisherVO.setCreationDate(user.getCreationDate());
        for(Language language : user.getLanguages()) {
            publisherVO.getLanguages().add(LanguageAdapter.toVO(language, new LanguageVO()));
        }
        return publisherVO;
    }

    public static PublisherDocument toDocument(User user, PublisherDocument publisherDocument) {
        publisherDocument.setId(user.getId().toString());
        publisherDocument.setEmail(user.getEmail());
        publisherDocument.setName(user.getName());
        publisherDocument.setNameNotAnalyzed(user.getName());
        publisherDocument.setUserName(user.getUserName());
        return publisherDocument;
    }
}