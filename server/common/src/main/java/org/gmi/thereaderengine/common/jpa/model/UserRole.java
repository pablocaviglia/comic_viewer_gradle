package org.gmi.thereaderengine.common.jpa.model;

public enum UserRole {
	ADMIN,
	CONTENT_MANAGER,
	PUBLISHER
}