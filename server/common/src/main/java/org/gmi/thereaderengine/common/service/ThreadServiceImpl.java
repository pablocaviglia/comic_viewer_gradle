package org.gmi.thereaderengine.common.service;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import javax.inject.Singleton;

/**
 * Created by pablo on 25/03/15.
 */
@Service
@Singleton
public class ThreadServiceImpl extends GenericService implements ThreadService {

    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    public ThreadServiceImpl() {
        threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(5);
        threadPoolTaskExecutor.setMaxPoolSize(10);
        threadPoolTaskExecutor.setQueueCapacity(200);
        threadPoolTaskExecutor.initialize();
    }

    @Override
    public void execute(Runnable runnable) {
        threadPoolTaskExecutor.execute(runnable);
    }
}