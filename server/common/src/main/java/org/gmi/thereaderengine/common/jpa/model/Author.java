package org.gmi.thereaderengine.common.jpa.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Created by pablo on 16/04/15.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "Author.findByProject",
                query = "select a from Author a where a.project.id = :projectId"),
        @NamedQuery(name = "Author.deleteByProjectId",
                query = "delete from Author a where a.project.id = :projectId")
})
@Table(name = "AUTHOR", indexes = {
        @Index(columnList = "ID", name = "authorIdIndex"),
        @Index(columnList = "PROJECT", name = "authorProjectIndex"),
})
public class Author implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name="PROJECT")
    private Project project;

    @Column(name = "TYPE")
    private String type;

    @Column(name = "NAME")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}