package org.gmi.thereaderengine.common.service;

/**
 * Created by pablo on 25/03/15.
 */
public interface ThreadService {
    void execute(Runnable runnable);
}
