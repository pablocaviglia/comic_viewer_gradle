package org.gmi.thereaderengine.common.jpa.dao;

import com.apocalipta.comic.constants.TitleI18NTag;

import org.gmi.thereaderengine.common.jpa.model.TitleI18NText;

import java.util.List;

/**
 * Created by pablo on 06/05/15.
 */
public interface TitleI18NTextDao extends GenericDao<TitleI18NText> {

    TitleI18NText findByTitleLanguageAndTag(long titleId, long languageId, TitleI18NTag tag);
    List<TitleI18NText> findByTitleAndTag(long titleId, TitleI18NTag tag);

}
