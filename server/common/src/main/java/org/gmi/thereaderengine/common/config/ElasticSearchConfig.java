package org.gmi.thereaderengine.common.config;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.gmi.thereaderengine.common.elasticsearch.constant.IndexNameEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.data.elasticsearch.annotations.Setting;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by pablo on 11/04/15.
 */
@Configuration
@EnableElasticsearchRepositories(basePackages = "org.gmi.thereaderengine.common.elasticsearch")
public class ElasticSearchConfig {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Value("#{system['elasticsearch.nodes']}")
    private String NODES;

    @Value("#{system['elasticsearch.nodes.http']}")
    private String NODES_HTTP;

    @Value("classpath:/config/elasticsearch/settings.json")
    private Resource settingsJSON;

    @Bean
    public Client client() throws IOException {

        Settings settings = ImmutableSettings.builder()
                .put("cluster.name", "thereaderengine_cluster")
                .put("node.name", "thereaderengine_node")
                .put("discovery.zen.ping.multicast.enabled", false)
                .build();

        TransportClient client = new TransportClient(settings);
        String[] nodesStr = NODES.split(",");
        for(String nodeStr : nodesStr) {
            String[] nodeData = nodeStr.split(":");
            String nodeIp = nodeData[0];
            String nodePort = nodeData[1];
            TransportAddress address = new InetSocketTransportAddress(nodeIp, Integer.valueOf(nodePort));
            client.addTransportAddress(address);
        }

        //create indexes
        createIndexesAsync();

        return client;
    }

    private void createIndexesAsync() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String[] nodesHttpStr = NODES_HTTP.split(",");
                for(String nodeHttpStr : nodesHttpStr) {
                    String[] nodeData = nodeHttpStr.split(":");
                    String nodeIp = nodeData[0];
                    String nodePort = nodeData[1];

                    boolean indexesInitialized = false;
                    while(!indexesInitialized) {
                        try {
                            //initialize settings for each node and index
                            initializeSettings(nodeIp, nodePort, IndexNameEnum.PUBLICATION);
                            initializeSettings(nodeIp, nodePort, IndexNameEnum.PUBLISHER);
                            initializeSettings(nodeIp, nodePort, IndexNameEnum.TITLE);
                            indexesInitialized = true;
                        }
                        catch(Exception e) {
                            LOGGER.error("Cannot create ElasticSearch indexes, retrying in 5 seconds (" + e.getMessage() + ")");
                            try {Thread.sleep(5000);} catch (InterruptedException e1) {}
                        }
                    }
                }
            }
        }).start();
    }

    private void initializeSettings(String nodeIp, String nodePort, String indexName) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        InputStream inputStream = new BufferedInputStream(settingsJSON.getInputStream());
        byte[] buffer = new byte[1024];
        int i = -1;
        while(-1 != (i=inputStream.read(buffer))) {
            stringBuilder.append(new String(buffer, 0, i));
        }

        String url = "http://" + nodeIp + ":" + nodePort + "/" + indexName;
        HttpClient client = HttpClientBuilder.create().build();
        HttpPut request = new HttpPut(url);
        StringEntity input = new StringEntity(stringBuilder.toString());
        input.setContentType("application/json");
        request.setEntity(input);
        HttpResponse response = client.execute(request);

        if(response.getStatusLine().getStatusCode() == 200) {
            LOGGER.info("ElasticSearch index '" + indexName + "' :: successfully created!");
        }
        else if(response.getStatusLine().getStatusCode() == 400) {
            LOGGER.info("ElasticSearch index '" + indexName + "' :: already exists");
        }
        else {
            LOGGER.error("ElasticSearch index '" + indexName + "' :: error creating");
            LOGGER.error(response.toString());
        }
    }

    @Bean
    public ElasticsearchOperations elasticsearchTemplate() throws IOException {
        return new ElasticsearchTemplate(client());
    }
}