package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.jpa.model.ViewerUser;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * Created by pablo on 05/03/15.
 */
@Repository
public class ViewerUserDaoImpl extends GenericDaoImpl<ViewerUser> implements ViewerUserDao {

    @Override
    public long existsByUsername(String username) {

        //get the user from the database
        Query query = entityManager.createQuery("select vu.id from ViewerUser vu where vu.username = :username");
        query.setParameter("username", username);

        try {
            long id = (Long) query.getSingleResult();
            return id;
        }
        catch (NoResultException e) {
            return -1;
        }
    }

    @Override
    public ViewerUser findByUsername(String username) {
        TypedQuery<ViewerUser> namedQuery = entityManager.createNamedQuery("ViewerUser.findByUsername", ViewerUser.class);
        namedQuery.setParameter("name", username);
        try {
            ViewerUser user = namedQuery.getSingleResult();
            return user;
        }
        catch(NoResultException e) {
            return null;
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }

    }
}
