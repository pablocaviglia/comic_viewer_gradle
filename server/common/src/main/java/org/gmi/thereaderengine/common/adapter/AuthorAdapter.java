package org.gmi.thereaderengine.common.adapter;

import com.apocalipta.comic.vo.v1.AuthorVO;

import org.gmi.thereaderengine.common.elasticsearch.document.AuthorDocument;
import org.gmi.thereaderengine.common.jpa.model.Author;

/**
 * Created by pablo on 17/04/15.
 */
public class AuthorAdapter {

    public static AuthorVO toVO(Author author, com.apocalipta.comic.vo.v1.AuthorVO authorVO) {
        authorVO.setId(author.getId());
        authorVO.setType(author.getType());
        authorVO.setName(author.getName());
        return authorVO;
    }

    public static AuthorDocument toDocument(Author author, AuthorDocument authorDocument) {
        authorDocument.setId(author.getId().toString());
        authorDocument.setType(author.getType());
        authorDocument.setName(author.getName());
        return authorDocument;
    }
}