package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.JSONApiVersion;
import com.apocalipta.comic.constants.PublicationStatus;

import org.apache.commons.vfs2.FileSystemException;
import org.gmi.thereaderengine.common.exception.BusinessException;
import org.gmi.thereaderengine.common.filter.PublicationFilter;
import org.gmi.thereaderengine.common.jpa.model.Publication;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by pablo on 22/12/14.
 */
public interface PublicationService {

    String PUBLICATIONS_LOCATION = "publications/";

    String PUBLICATION_DESCRIPTOR = "descriptor.json";
    String PUBLICATION_DESCRIPTOR_PREVIEW = "descriptor_preview.json";

    List<Publication> findPublications(PublicationFilter filter);
    int countFindPublications(PublicationFilter filter);
    Publication findById(long publicationId);

    long findPublicationIdByPageId(long pageId);

    int findVersionByPublicationId(long publicationId);

    InputStream findByIdJSON(long publicationId, JSONApiVersion version, boolean full) throws FileSystemException, FileNotFoundException;
    Publication store(Publication publication);

    void changeStatus(long projectId, PublicationStatus status) throws IOException;

    Publication publish(long publicationId, long publisherUserId) throws IOException, BusinessException;
    Publication findPublicationByProjectId(long projectId);

    void deletePublication(long publicationId) throws BusinessException;

}
