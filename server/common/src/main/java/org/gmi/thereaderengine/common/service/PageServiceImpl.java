package org.gmi.thereaderengine.common.service;

import org.apache.commons.vfs2.*;
import org.gmi.thereaderengine.common.jpa.dao.PageDao;
import org.gmi.thereaderengine.common.exception.BusinessException;
import org.gmi.thereaderengine.common.jpa.model.NavigationItem;
import org.gmi.thereaderengine.common.jpa.model.Page;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.gmi.thereaderengine.common.jpa.model.Subtitle;
import org.gmi.thereaderengine.common.util.ImageInfo;
import org.gmi.thereaderengine.common.util.ShellClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.*;

import javax.inject.Singleton;

@Service
@Singleton
public class PageServiceImpl extends GenericService implements PageService {

	private FileSelector allFileSelector = new FileSelector() {
        @Override
        public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
            return true;
        }
        @Override
        public boolean traverseDescendents(FileSelectInfo fileInfo) throws Exception {
            return true;
        }
    };

    // path to the content folder
	private @Value("#{system['CONTENT_PATH']}") String CONTENT_PATH;

	@Autowired
	private PageDao pageDao;
	
	@Autowired
	private ProjectService projectService;

    @Autowired
    private LanguageService languageService;

	public synchronized Page store(Page page) throws BusinessException {
		
		if(page.getId() != null) {
			throw new BusinessException("The page must be a new one to be saved.");
		}
		
		//increment the index
		int nextIndex = getLastPageIndex(page.getProject().getId()) + 1;
		page.setPageIndex(nextIndex);
		
		return pageDao.store(page);
	}
	
	@Override
	public Page findById(long pageId) {
		return pageDao.findById(pageId);
	}
	
	@Override
	public int getLastPageIndex(long projectId) {
		return pageDao.getLastPageIndex(projectId);
	}
	
	@Override
	public int getLastNavigationItemIndex(long pageId) {
		return pageDao.getLastNavigationItemIndex(pageId);
	}
	
	@Override
	public NavigationItem findNavigationItemById(long navigationItemId) {
		return pageDao.findNavigationItemById(navigationItemId);
	}
	
	@Override
	public synchronized NavigationItem addNavigationItem(long pageId, NavigationItem navigationItem) {
		//increment the index
		int nextIndex = getLastNavigationItemIndex(pageId) + 1;
		navigationItem.setNavigationItemIndex(nextIndex);
		return pageDao.addNavigationItem(pageId, navigationItem);
	}
	
	@Transactional
	@Override
	public void deleteNavigationItem(long navigationItemId) {
		
		//find the item to get its page
		Page page = findNavigationItemById(navigationItemId).getPage();
		
		//delete the navigation 
		//item subtitles first
		NavigationItem toDeleteNavigationItem = findNavigationItemById(navigationItemId);
		List<Subtitle> subtitles = toDeleteNavigationItem.getSubtitles();
		for(Subtitle subtitle : subtitles) {
			pageDao.deleteByNavigationItemAndLanguage(toDeleteNavigationItem.getId(), subtitle.getLanguage().getId());
		}
		
		//delete the item
		pageDao.deleteNavigationItem(navigationItemId);
		
		//get all the navigation items
		List<NavigationItem> navigationItems = getNavigationItems(page.getId());
		int navigationItemsSize = navigationItems.size();
		
		//set a new id for each element
		for(int i=0; i<navigationItemsSize; i++) {
			NavigationItem navigationItem = navigationItems.get(i);
			pageDao.updateNavigationItemIndex(navigationItem.getId(), i+1);
		}
	}
	
	@Transactional
	@Override
	public void deletePage(long pageId) {
		
		//find the page to get the project
		Project project = findById(pageId).getProject();
		
		//get the page to delete
		Page toDeletePage = findById(pageId);
		
		//delete the subtitles first
		List<NavigationItem> navigationItems = toDeletePage.getNavigationItems();
		for(NavigationItem navigationItem : navigationItems) {
			List<Subtitle> subtitles = navigationItem.getSubtitles();
			for(Subtitle subtitle : subtitles) {
				pageDao.deleteByNavigationItemAndLanguage(navigationItem.getId(), subtitle.getLanguage().getId());
			}
		}
		
		//delete the page
		pageDao.deletePage(pageId);
		
		try {
			//delete the image from the FS
			FileSystemManager fsManager = VFS.getManager();
			FileObject pageImagePath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + project.getId() + "/" + pageId);
			pageImagePath.delete();
		} 
		catch (FileSystemException e) {
            LOGGER.error(e.getMessage(), e);
		}
	}
	
	@Transactional
	@Override
	public List<NavigationItem> getNavigationItems(long pageId) {
		List<NavigationItem> navigationItems = pageDao.getNavigationItems(pageId);
		for(NavigationItem navigationItem : navigationItems) {
			//load its subtitles
			navigationItem.getSubtitles().size();
		}
		return navigationItems;
	}

	@Override
	public List<Page> findByProjectId(long projectId) {
		return pageDao.findByProjectId(projectId);
	}
	
	@Override
	public void changePageIndex(long pageId, int newIndex, boolean setAffected) {
		
		//get the page to change index
		Page page = pageDao.findById(pageId);

        if(setAffected) {
            //get the affected page
            Page affectedPage = pageDao.findByProjectIdAndIndex(page.getProject().getId(), newIndex);
            pageDao.updatePageIndex(affectedPage.getId(), page.getPageIndex());
        }

		//swap the indexes
		pageDao.updatePageIndex(page.getId(), newIndex);

	}
	
	@Transactional
	@Override
	public void changeNavigationItemIndex(long navigationItemId, int newIndex) {
		pageDao.updateNavigationItemIndex(navigationItemId, newIndex);
	}
	
	@Transactional
	@Override
	public void saveSubtitles(long navigationItemId, Map<Long, String> subtitles) {
		
		//get the navigation item
		NavigationItem navigationItem = findNavigationItemById(navigationItemId);
		
		//iterate the list of subtitles
		Iterator<Long> subtitlesKeysIt = subtitles.keySet().iterator();
		while(subtitlesKeysIt.hasNext()) {
			long currentSubtitleLanguageId = subtitlesKeysIt.next();
			String currentSubtitleValue = subtitles.get(currentSubtitleLanguageId);
			
			//verify if the subtitle language
			//already exists for this navigation item
			Subtitle subtitle = pageDao.findSubtitleByNavigationItemAndLanguage(navigationItemId, currentSubtitleLanguageId);
			
			//create the new subtitle
			if(subtitle == null) {
				//create the subtitle
				subtitle = new Subtitle();
				subtitle.setLanguage(languageService.findLanguageById(currentSubtitleLanguageId));
				subtitle.setNavigationItem(navigationItem);
				subtitle.setSubtitle(currentSubtitleValue);
				
				//store it
				pageDao.addSubtitle(subtitle);
			}
			else {
				//just update its value
				pageDao.updateSubtitleValue(subtitle.getId(), currentSubtitleValue);
			}
		}
	}

	@Transactional
	@Override
	public void updatePageSize(long pageId, int width, int height) {
		pageDao.updatePageSize(pageId, width, height);
	}

	@Override
	public void updateNavigationItem(NavigationItem navigationItem) {
		pageDao.updateNavigationItem(navigationItem);
	}

	@Transactional
	@Override
	public void deletePageNavigation(long pageId) {
		List<NavigationItem> navigationItems = getNavigationItems(pageId);
		for(NavigationItem navigationItem : navigationItems) {
			//delete the navigation 
			//item subtitles first
			List<Subtitle> subtitles = navigationItem.getSubtitles();
			for(Subtitle subtitle : subtitles) {
				pageDao.deleteByNavigationItemAndLanguage(navigationItem.getId(), subtitle.getLanguage().getId());
			}
			//delete the item
			pageDao.deleteNavigationItem(navigationItem.getId());
		}
	}

	@Override
	public Project findProjectByNavigationItemId(long navigationItemId) {
		
		NavigationItem navigationItem = pageDao.findNavigationItemById(navigationItemId);
		if(navigationItem != null) {
			return navigationItem.getPage().getProject();
		}
		
		return null;
	}

    @Override
    public void finishLastUpload(Long projectId) throws IOException, BusinessException, InterruptedException {

        // get the project
        Project project = projectService.findById(projectId);

        //get the project tmp upload folder
        FileSystemManager fsManager = VFS.getManager();
        FileObject pageUploadTmpPath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/tmp/");

        if(pageUploadTmpPath.exists()) {
            //list tmp files
            FileObject[] pageTmpFiles = pageUploadTmpPath.getChildren();

            //sort the files array
            Arrays.sort(pageTmpFiles, new Comparator<FileObject>() {
                @Override
                public int compare(FileObject o1, FileObject o2) {
                    try {
                        Integer index1 = new Integer(o1.getName().getBaseName());
                        Integer index2 = new Integer(o2.getName().getBaseName());
                        return index1 - index2;
                    } catch (NumberFormatException e) {
                        //the file name is not numeric
                        return o1.getName().getBaseName().compareTo(o2.getName().getBaseName());
                    }
                }
            });

            //iterate the list of files
            for(FileObject currentTmpFile : pageTmpFiles) {

                //create the page on the db
                Page page = new Page();
                page.setProject(project);
                page = store(page);

                //copy the image file to the
                //definitive folder
                FileObject currentFinalFile = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/" + page.getId());
                currentFinalFile.copyFrom(currentTmpFile, allFileSelector);

                //create the thumb
                createPageThumbnail(currentFinalFile);

                //get the path to the page image and thumb
                FileObject pageImage = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + project.getId() + "/" + page.getId());
                FileObject pageThumb = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + project.getId() + "/" + page.getId() + ProjectService.THUMB_SUFFIX);

                //calculate the file sizes
                page.setImageFileSize(pageImage.getContent().getSize());
                page.setThumbFileSize(pageThumb.getContent().getSize());

                //calculate the image dimensions
                ImageInfo imageImageInfo = new ImageInfo(pageImage);
                ImageInfo thumbImageInfo = new ImageInfo(pageThumb);

                //set dimensions
                page.setImageWidth(imageImageInfo.getWidth());
                page.setImageHeight(imageImageInfo.getHeight());
                page.setThumbWidth(thumbImageInfo.getWidth());
                page.setThumbHeight(thumbImageInfo.getHeight());

                //update the page to save
                //image dimensions and sizes
                pageDao.store(page);

            }

            //delete the tmp folder
            FileObject pageUploadPath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + projectId + "/tmp/");
            pageUploadPath.delete(allFileSelector);
        }
    }

    @Override
    public void calculatePageImageDimensionsAndSize(long pageId) throws IOException, InterruptedException, BusinessException {

        //get the page
        Page page = findById(pageId);
        Project project = page.getProject();

        //fs manager
        FileSystemManager fsManager = VFS.getManager();

        //get the path to the page image and thumb
        FileObject pageImage = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + project.getId() + "/" + page.getId());
        FileObject pageThumb = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + project.getId() + "/" + page.getId() + ProjectService.THUMB_SUFFIX);

        //calculate the file sizes
        page.setImageFileSize(pageImage.getContent().getSize());
        page.setThumbFileSize(pageThumb.getContent().getSize());

        //calculate the image dimensions
        ImageInfo imageImageInfo = new ImageInfo(pageImage);
        ImageInfo thumbImageInfo = new ImageInfo(pageThumb);

        //set dimensions
        page.setImageWidth(imageImageInfo.getWidth());
        page.setImageHeight(imageImageInfo.getHeight());
        page.setThumbWidth(thumbImageInfo.getWidth());
        page.setThumbHeight(thumbImageInfo.getHeight());

        //update the new image
        //dimensions and sizes
        pageDao.store(page);

    }

    @Override
    public void incrementPageVersion(long pageId) {
        Page page = findById(pageId);
        page.setVersion(page.getVersion()+1);
        pageDao.store(page);
    }

    @Transactional
    @Override
    public void deleteByNavigationItemAndLanguage(long navigationItemId, long languageId) {
        pageDao.deleteByNavigationItemAndLanguage(navigationItemId, languageId);
    }

    @Override
    public void createPageThumbnail(FileObject pagePath) throws IOException, InterruptedException {
        ShellClient.exec(new String[]{
						"cd " + pagePath.getParent().getName().getPath(),
						"convert -resize X200 " + pagePath.getName().getBaseName() + " " + pagePath.getName().getBaseName() + ProjectService.THUMB_SUFFIX
				},
				null);
    }
}