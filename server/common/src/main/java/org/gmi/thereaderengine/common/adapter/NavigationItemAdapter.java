package org.gmi.thereaderengine.common.adapter;

import org.gmi.thereaderengine.common.jpa.model.NavigationItem;
import org.gmi.thereaderengine.common.jpa.model.Subtitle;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 12/01/15.
 */
public class NavigationItemAdapter {

    public static void toVO(NavigationItem navigationItem, com.apocalipta.comic.vo.v1.NavigationItemVO navigationItemVO) {

        navigationItemVO.setId(navigationItem.getId());
        navigationItemVO.setX(navigationItem.getX());
        navigationItemVO.setY(navigationItem.getY());
        navigationItemVO.setWidth(navigationItem.getWidth());
        navigationItemVO.setHeight(navigationItem.getHeight());
        navigationItemVO.setNavigationItemIndex(navigationItem.getNavigationItemIndex());

        List<com.apocalipta.comic.vo.v1.SubtitleVO> subtitleVOList = new ArrayList<com.apocalipta.comic.vo.v1.SubtitleVO>();
        navigationItemVO.setSubtitleVOList(subtitleVOList);

        if(navigationItem.getSubtitles() != null && navigationItem.getSubtitles().size() > 0) {
            for(Subtitle subtitle : navigationItem.getSubtitles()) {
                com.apocalipta.comic.vo.v1.SubtitleVO subtitleVO = new com.apocalipta.comic.vo.v1.SubtitleVO();
                SubtitleAdapter.toVO(subtitle, subtitleVO);
                subtitleVOList.add(subtitleVO);
            }
        }
    }
}