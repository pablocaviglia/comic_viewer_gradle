package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.ProjectI18NTag;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.gmi.thereaderengine.common.jpa.dao.LanguageDao;
import org.gmi.thereaderengine.common.jpa.dao.ProjectDao;
import org.gmi.thereaderengine.common.jpa.dao.Projecti18nTextDao;
import org.gmi.thereaderengine.common.jpa.model.Language;
import org.gmi.thereaderengine.common.jpa.model.NavigationItem;
import org.gmi.thereaderengine.common.jpa.model.Page;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.gmi.thereaderengine.common.jpa.model.ProjectI18NText;
import org.gmi.thereaderengine.common.jpa.model.Subtitle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Singleton;
import javax.persistence.Query;

/**
 * Created by pablo on 08/01/15.
 */
@Service
@Singleton
public class Projecti18nTextServiceImpl extends GenericService implements Projecti18nTextService {

    @Autowired
    private Projecti18nTextDao projecti18nTextDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private LanguageDao languageDao;

    @Transactional
    @Override
    public synchronized void save(long projectId, long languageId, ProjectI18NTag tag, String value) {

        ProjectI18NText projectI18NText = projecti18nTextDao.findByProjectLanguageAndTag(projectId, languageId, tag);
        if(projectI18NText == null) {
            projectI18NText = new ProjectI18NText();
        }

        Project project = projectDao.findById(projectId);
        Language language = languageDao.findById(languageId);

        projectI18NText.setProject(project);
        projectI18NText.setLanguage(language);
        projectI18NText.setTag(tag);
        projectI18NText.setValue(value);

        projecti18nTextDao.store(projectI18NText);
    }

    @Override
    public ProjectI18NText findByProjectLanguageAndTag(long projectId, long languageId, ProjectI18NTag tag) {
        return projecti18nTextDao.findByProjectLanguageAndTag(projectId, languageId, tag);
    }

    @Override
    public List<ProjectI18NText> findByProject(long projectId) {
        return projecti18nTextDao.findByProject(projectId);
    }

    @Override
    public List<ProjectI18NText> findByProjectTag(long projectId, ProjectI18NTag tag) {
        return projecti18nTextDao.findByProjectTag(projectId, tag);
    }

    @Override
    public Map<Long, Map<Long, Map<String, String>>> findPublicationsBaseI18NTexts(Set<Long> projectIds, Set<Long> languageIds) {
        return projecti18nTextDao.findProjectsBaseI18NTexts(projectIds, languageIds);
    }

    @Override
    public void deleteByProjectId(long projectId) {
        projecti18nTextDao.deleteByProjectId(projectId);
    }
}