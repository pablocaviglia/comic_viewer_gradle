package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.ProjectAgeRate;
import com.apocalipta.comic.constants.ProjectI18NTag;
import com.apocalipta.comic.constants.ProjectStatus;
import com.apocalipta.comic.constants.PublicationStatus;
import org.apache.commons.vfs2.*;
import org.gmi.thereaderengine.common.jpa.dao.ProjectDao;
import org.gmi.thereaderengine.common.exception.BusinessException;
import org.gmi.thereaderengine.common.filter.ProjectFilter;
import org.gmi.thereaderengine.common.jpa.model.Language;
import org.gmi.thereaderengine.common.jpa.model.NavigationItem;
import org.gmi.thereaderengine.common.jpa.model.Page;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.gmi.thereaderengine.common.jpa.model.Publication;
import org.gmi.thereaderengine.common.jpa.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

@Service
@Singleton
public class ProjectServiceImpl extends GenericService implements ProjectService {

	//values read from properties files
	private @Value("#{system['CONTENT_PATH']}") String CONTENT_PATH;

    @Autowired
    private LanguageService languageService;

	@Autowired
	private Projecti18nTextService projecti18nTextService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private PublicationService publicationService;

    @Autowired
    private PageService pageService;

    private FileSelector allFileSelector = new FileSelector() {
		@Override
		public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
			return true;
		}
		@Override
		public boolean traverseDescendents(FileSelectInfo fileInfo) throws Exception {
			return true;
		}
	};
	
	private FileSelector projectFileSelector = new FileSelector() {
		@Override
		public boolean traverseDescendents(FileSelectInfo fileInfo) throws Exception {
			return isValidFile(fileInfo);
		}
		@Override
		public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
			return isValidFile(fileInfo);
		}
		
		private boolean isValidFile(FileSelectInfo fileInfo) {
			if(fileInfo.getFile().getName().getBaseName().equals(".svn") || 
			   fileInfo.getFile().getName().getBaseName().equals("bin") ||
			   fileInfo.getFile().getName().getBaseName().equals("build") ||
			   fileInfo.getFile().getName().getBaseName().equals("gen")) {
				return false;
			}
			else {
				return true;
			}
		}
	};
	
	@Override
	@Transactional(readOnly = true)
	public Project findById(long projectId) {
		Project project = projectDao.findById(projectId);
		if(project != null) {
			project.getPages().size();
		}
		return project;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Project> findProjects(ProjectFilter filter) {
		return projectDao.findProjects(filter);
	}
	
	@Override
	@Transactional(readOnly = true)
	public int countFindProjects(ProjectFilter filter) {
		return projectDao.countFindProjects(filter);
	}
	
	@Override
	public void store(Project project) {
		projectDao.store(project);
	}
	
    @Transactional
	public long createProject(String code, String projectName, long nativeLanguageId, long defaultLanguageId, User publisher) throws BusinessException {
		long generatedProjectId;
		try {
            //create the project object
            Project project = new Project();
            project.setCode(code);
            project.setProjectName(projectName);
            project.setStatus(ProjectStatus.FIRST_REGISTRATION);
            project.setOrientalReading(false);
            project.setAgeRate(ProjectAgeRate.ALL);
            project.setPublisher(publisher);

            //set selected native language
            Language nativeLanguage = languageService.findLanguageById(nativeLanguageId);
            project.setNativeLanguage(nativeLanguage);

            //set selected default language
            Language defaultLanguage = languageService.findLanguageById(defaultLanguageId);
            project.setDefaultLanguage(defaultLanguage);

            project.setLanguages(new ArrayList<>());
            project.getLanguages().add(defaultLanguage);

            //store it on the database
			generatedProjectId = projectDao.store(project).getId();
            project = projectDao.findById(generatedProjectId);

            //set the default name
            projecti18nTextService.save(generatedProjectId, defaultLanguageId, ProjectI18NTag.NAME, projectName);

            //create the publication
            Publication publication = new Publication();
            publication.setStatus(PublicationStatus.UNPUBLISHED);
            publication.setProject(project);
            publicationService.store(publication);
        }
		catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
			throw new BusinessException(e.getMessage());
		}

        return generatedProjectId;
	}
	
	@Override
	public boolean existsByCode(String code) {
		return projectDao.existsByCode(code);
	}

	@Override
	@Transactional(readOnly = false)
	public void deleteProject(long projectId) throws BusinessException {
		
		Project project = findById(projectId);
		try {
			
			//get the fs manager
			FileSystemManager fsManager = VFS.getManager();

			//delete fs project resources 
			FileObject projectContentFolder = fsManager.resolveFile(CONTENT_PATH + "/" + project.getId() + "/");
			projectContentFolder.delete(allFileSelector);

			//delete pages
			List<Page> pages = project.getPages();
			for(Page page : pages) {
				
				//delete navigation items from db
				List<NavigationItem> navigationItems = page.getNavigationItems();
				for(NavigationItem navigationItem : navigationItems) {
					pageService.deleteNavigationItem(navigationItem.getId());
				}
				
				//delete pages from db
                pageService.deletePage(page.getId());
			}

			//delete i18n
			projecti18nTextService.deleteByProjectId(projectId);

            //delete authors
            authorService.deleteByProjectId(projectId);

            //delete the publication
            Publication publication = publicationService.findPublicationByProjectId(projectId);
            publicationService.deletePublication(publication.getId());

			//delete the project
			projectDao.delete(projectId);
		
		} 
		catch (FileSystemException e) {
            LOGGER.error(e.getMessage(), e);
			throw new BusinessException(e.getMessage());
		}
	}
}