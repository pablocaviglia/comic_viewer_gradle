package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.jpa.model.ViewerUser;

/**
 * Created by pablo on 05/03/15.
 */
public interface ViewerUserDao extends GenericDao<ViewerUser> {

    long existsByUsername(String username);
    ViewerUser findByUsername(String username);

}
