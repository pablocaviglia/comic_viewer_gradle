package org.gmi.thereaderengine.common.jpa.model;

import com.apocalipta.comic.constants.UserI18NTag;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Created by pablo on 03/04/15.
 */
@Entity
@Table(name = "USER_I18N_TEXT", indexes = {
        @Index(columnList = "ID", name = "userI18NTextIdIndex"),
        @Index(columnList = "USER", name = "userI18NUserIndex"),
        @Index(columnList = "LANGUAGE", name = "userI18NLanguageIndex"),
        @Index(columnList = "TAG", name = "userI18NTagIndex"),
})
@NamedQueries({
        @NamedQuery(name = "UserI18NText.findByUserLanguageAndTag",
                query = "select u from UserI18NText u where u.user.id = :userId and u.language.id = :languageId and u.tag = :tag"),
        @NamedQuery(name = "UserI18NText.findByUser",
                query = "select u from UserI18NText u where u.user.id = :userId"),
        @NamedQuery(name = "UserI18NText.findByUserTag",
                query = "select u from UserI18NText u where u.user.id = :userId and u.tag = :tag"),
})
public class UserI18NText implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name="USER")
    private User user;

    @ManyToOne
    @JoinColumn(name="LANGUAGE")
    private Language language;

    @Column(name = "TAG", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private UserI18NTag tag;

    @Column(name = "VALUE", nullable = true, length = 8192)
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public UserI18NTag getTag() {
        return tag;
    }

    public void setTag(UserI18NTag tag) {
        this.tag = tag;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}