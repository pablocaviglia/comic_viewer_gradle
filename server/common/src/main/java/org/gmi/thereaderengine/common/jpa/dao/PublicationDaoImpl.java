package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.filter.PublicationFilter;
import org.gmi.thereaderengine.common.jpa.model.Publication;
import org.gmi.thereaderengine.common.jpa.model.Subtitle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by pablo on 22/12/14.
 */
@Repository
public class PublicationDaoImpl extends GenericDaoImpl<Publication> implements PublicationDao {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    @SuppressWarnings("unchecked")
    public List findPublicationPreviews(PublicationFilter filter) {

        LOGGER.debug("Find publications");

        //build the query
        Query query = buildQueryfindPublicationPreviews(filter, false);

        //set max rows filters
        if (filter.getFirstResult() != null) {
            query.setFirstResult(filter.getFirstResult());
        }
        if (filter.getMaxResults() != null) {
            query.setMaxResults(filter.getMaxResults());
        }

        //status
        if(filter.getStatus() != null) {
            query.setParameter("status", filter.getStatus());
        }

        //execute the query
        return query.getResultList();
    }

    @Override
    public Publication findPublicationByProjectId(long projectId) {
        TypedQuery<Publication> namedQuery = entityManager.createNamedQuery("Publication.findByProjectId", Publication.class);
        namedQuery.setParameter("projectId", projectId);
        try {
            Publication publication = namedQuery.getSingleResult();
            return publication;
        }
        catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public int findVersionByPublicationId(long publicationId) {
        TypedQuery<Integer> namedQuery = entityManager.createNamedQuery("Publication.findVersionById", Integer.class);
        namedQuery.setParameter("id", publicationId);
        return namedQuery.getSingleResult();
    }

    @Override
    public int countFindPublicationPreviews(PublicationFilter filter) {
        LOGGER.debug("Count find projects");
        Query query = buildQueryfindPublicationPreviews(filter, true);

        //where
        if(filter.getStatus() != null) {
            query.setParameter("status", filter.getStatus());
        }

        long result = (Long) query.getSingleResult();
        return (int) result;
    }

    private Query buildQueryfindPublicationPreviews(PublicationFilter filter, boolean count) {

        StringBuilder sb = new StringBuilder();

        if (count) {
            sb.insert(0, " select count(publication) as qntyPublications ");
        }
        else {
            sb.insert(0, " select " +
                    "publication.id, publication.version, " +
                    "project.id, " +
                    "project.ageRate, project.orientalReading, " +
                    "project.defaultLanguage.id as defaultLanguageId");
        }

        //from
        sb.append(" from " + Publication.class.getName() + " publication ");

        //where
        if(filter.getStatus() != null) {
            sb.append(" where status = :status ");
        }

        Query query = entityManager.createQuery(sb.toString());
        return query;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Publication> findPublications(PublicationFilter filter) {
        LOGGER.debug("Find publications");
        Query query = buildQueryfindPublications(filter, false);
        if (filter.getFirstResult() != null) {
            query.setFirstResult(filter.getFirstResult());
        }
        if (filter.getMaxResults() != null) {
            query.setMaxResults(filter.getMaxResults());
        }

        return query.getResultList();
    }

    @Override
    public int countFindPublications(PublicationFilter filter) {
        LOGGER.debug("Count find projects");
        Query query = buildQueryfindPublications(filter, true);
        long result = (Long) query.getSingleResult();
        return (int) result;
    }

    private Query buildQueryfindPublications(PublicationFilter filter, boolean count) {

        StringBuilder sb = new StringBuilder();

        //status
        if (filter.getStatus() != null) {
            sb.append(" where p.status = :status");
        }

        //publisher
        if (filter.getPublisher() != null) {
            if (sb.length() == 0) {
                sb.append(" where ");
            } else {
                sb.append(" and ");
            }
            sb.append(" p.project.publisher.id = :publisherId ");
        }

        sb.insert(0, " from " + Publication.class.getName() + " p ");

        if (count) {
            sb.insert(0, " select count(p) as qntyPublications ");
        }

        Query query = entityManager.createQuery(sb.toString());

        //status
        if (filter.getStatus() != null) {
            query.setParameter("status", filter.getStatus());
        }

        //publisher
        if (filter.getPublisher() != null) {
            query.setParameter("publisherId", filter.getPublisher().getId());
        }

        return query;
    }
}