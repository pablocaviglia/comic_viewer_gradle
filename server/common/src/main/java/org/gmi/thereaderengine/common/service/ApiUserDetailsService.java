package org.gmi.thereaderengine.common.service;

import org.gmi.thereaderengine.common.jpa.dao.UserDao;
import org.gmi.thereaderengine.common.jpa.model.User;
import org.gmi.thereaderengine.common.jpa.model.UserSpring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 12/12/14.
 */
public class ApiUserDetailsService implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("You have entered an invalid username or password!");
        } else {

            //verify if the user was already verified
            boolean pendingVerification = userDao.existsPendingUserRegistrationValidation(user.getId());
            if(!pendingVerification) {
                List<GrantedAuthority> roles = new ArrayList<GrantedAuthority>();
                roles.add(new SimpleGrantedAuthority(user.getRole().name()));
                //transform it into Spring Security model
                UserDetails userDetails = new UserSpring(user, username, user.getPassword(), roles);
                return userDetails;
            }
            else {
                throw new DisabledException("Your user was not verified yet. Please check your mailbox to finish the registration process.");
            }
        }
    }
}