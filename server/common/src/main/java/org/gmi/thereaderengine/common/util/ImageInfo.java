package org.gmi.thereaderengine.common.util;

import org.apache.commons.vfs2.FileObject;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.io.*;
import java.util.Iterator;

@SuppressWarnings("all")
public class ImageInfo {
	
	private int height;
	private int width;
	
	public ImageInfo(File file) throws IOException {
		InputStream is = new FileInputStream(file);
		try {
			processStream(is);
		} finally {
			is.close();
		}
	}

	public ImageInfo(FileObject file) throws IOException {
		InputStream is = file.getContent().getInputStream();
		try {
			processStream(is);
		} finally {
			is.close();
		}
	}

	public ImageInfo(InputStream is) throws IOException {
		processStream(is);
	}

	public ImageInfo(byte[] bytes) throws IOException {
		InputStream is = new ByteArrayInputStream(bytes);
		try {
			processStream(is);
		} finally {
			is.close();
		}
	}

	private void processStream(InputStream is) throws IOException {
		ImageInputStream in = ImageIO.createImageInputStream(is);
		try {
		    final Iterator<ImageReader> readers = ImageIO.getImageReaders(in);
		    if (readers.hasNext()) {
		        ImageReader reader = readers.next();
		        try {
		            reader.setInput(in);
		            width = reader.getWidth(0);
		            height = reader.getHeight(0);
		        } 
		        finally {
		            reader.dispose();
		        }
		    }
		} finally {
		    if (in != null) in.close();
		}
	}
	
	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	@Override
	public String toString() {
		return "Width : " + width + "\t Height : " + height;
	}
}