package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.jpa.model.ProjectGenre;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by pablo on 16/04/15.
 */
public interface ProjectGenreDao extends GenericDao<ProjectGenre> {

    long count();
    void insert(List<ProjectGenre> genresList);
}
