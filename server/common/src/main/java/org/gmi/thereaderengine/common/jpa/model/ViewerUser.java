package org.gmi.thereaderengine.common.jpa.model;

import com.apocalipta.comic.constants.Gender;
import com.apocalipta.comic.constants.UserPlatform;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Created by pablo on 04/03/15.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "ViewerUser.findByUsername",
                query = "select vu from ViewerUser vu where vu.username = :username")
})
@Table(name = "VIEWER_USER", indexes = {
        @Index(columnList = "ID", name = "viewerUserIdIndex"),
        @Index(columnList = "USERNAME", name = "viewerUserNameIndex"),
})
public class ViewerUser {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "USERNAME", nullable = false, unique = true)
    private String username;

    @Column(name = "FIRST_NAME", nullable = true)
    private String firstName;

    @Column(name = "LAST_NAME", nullable = true)
    private String lastName;

    @Column(name = "FULL_NAME", nullable = true)
    private String fullName;

    @Column(name = "LOCALE", nullable = true)
    private String locale;

    @Column(name = "PLATFORM", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private UserPlatform platform;

    @Column(name = "GENDER", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @Column(name = "PHOTO_URL", nullable = true)
    private String photoUrl;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public UserPlatform getPlatform() {
        return platform;
    }

    public void setPlatform(UserPlatform platform) {
        this.platform = platform;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
