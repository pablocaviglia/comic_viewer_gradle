package org.gmi.thereaderengine.common.jpa.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

public class GenericDaoImpl<T> implements GenericDao<T> {

	protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

	@PersistenceContext
	protected EntityManager entityManager;

	private Class<T> type;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public GenericDaoImpl() {
        Type t = getClass().getGenericSuperclass();
        if (t instanceof ParameterizedType) {
	        ParameterizedType pt = (ParameterizedType) t;
	        type = (Class) pt.getActualTypeArguments()[0];
        } else {
        	type = (Class) t;
        }
    }

	@Override
	@Transactional
	public T store(T t) {
		LOGGER.debug("Storing " + type.getSimpleName() + " " + t);
		return entityManager.merge(t);
	}
	
	@Override
	@Transactional
	public void delete(Long id) {
		LOGGER.debug("Deleting " + type.getSimpleName() + " " + id);
		entityManager.remove(entityManager.find(type, id));
	}

	@Override
	@Transactional(readOnly = true)
	public T findById(Long id) {
		LOGGER.debug("Find  " + type.getSimpleName() + " " + id);
		return entityManager.find(type, id);
	}

	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<T> findAll() {
		LOGGER.debug("Find all " + type.getSimpleName());
		Query query = entityManager.createQuery("from " + type.getSimpleName());
		return query.getResultList();
	}
	
	@Override
	public void flush() {
		entityManager.flush();
	}
}
