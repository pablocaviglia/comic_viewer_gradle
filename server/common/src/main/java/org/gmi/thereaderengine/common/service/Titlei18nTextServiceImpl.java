package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.TitleI18NTag;

import org.gmi.thereaderengine.common.jpa.dao.LanguageDao;
import org.gmi.thereaderengine.common.jpa.dao.TitleDao;
import org.gmi.thereaderengine.common.jpa.dao.TitleI18NTextDao;
import org.gmi.thereaderengine.common.jpa.model.Language;
import org.gmi.thereaderengine.common.jpa.model.Title;
import org.gmi.thereaderengine.common.jpa.model.TitleI18NText;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.inject.Singleton;

/**
 * Created by pablo on 06/05/15.
 */
@Service
@Singleton
public class Titlei18nTextServiceImpl extends GenericService implements Titlei18nTextService {

    @Autowired
    private TitleI18NTextDao titleI18NTextDao;

    @Autowired
    private TitleDao titleDao;

    @Autowired
    private LanguageDao languageDao;

    @Transactional
    @Override
    public void save(long titleId, long languageId, TitleI18NTag tag, String value) {

        TitleI18NText titleI18NText = titleI18NTextDao.findByTitleLanguageAndTag(titleId, languageId, tag);
        if(titleI18NText == null) {
            titleI18NText = new TitleI18NText();
        }

        Title title = titleDao.findById(titleId);
        Language language = languageDao.findById(languageId);

        titleI18NText.setTitle(title);
        titleI18NText.setLanguage(language);
        titleI18NText.setTag(tag);
        titleI18NText.setValue(value);

        titleI18NTextDao.store(titleI18NText);
    }

    @Override
    public List<TitleI18NText> findByTitleAndTag(long titleId, TitleI18NTag tag) {
        return titleI18NTextDao.findByTitleAndTag(titleId, tag);
    }
}
