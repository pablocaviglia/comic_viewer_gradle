package org.gmi.thereaderengine.common.jpa.dao;

import com.apocalipta.comic.constants.ProjectI18NTag;
import org.gmi.thereaderengine.common.jpa.model.ProjectI18NText;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.*;

/**
 * Created by pablo on 08/01/15.
 */
@Repository
public class Projecti18nTextDaoImpl extends GenericDaoImpl<ProjectI18NText> implements Projecti18nTextDao {

    @Override
    public ProjectI18NText findByProjectLanguageAndTag(long projectId, long languageId, ProjectI18NTag tag) {
        //get the user from the database
        TypedQuery<ProjectI18NText> namedQuery = entityManager.createNamedQuery("ProjectI18NText.findByProjectLanguageAndTag", ProjectI18NText.class);
        namedQuery.setParameter("projectId", projectId);
        namedQuery.setParameter("languageId", languageId);
        namedQuery.setParameter("tag", tag);

        try {
            ProjectI18NText projectI18NText = namedQuery.getSingleResult();
            return projectI18NText;
        }
        catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Map<Long, Map<Long, Map<String, String>>> findProjectsBaseI18NTexts(Set<Long> projectIds, Set<Long> languageIds) {

        //Map<+ProjectId+, Map<+languageId+, Map<+TAG+, +VALUE+>>>
        Map<Long, Map<Long, Map<String, String>>> projectsI18NTexts = new HashMap<Long, Map<Long, Map<String, String>>>();

        //query str
        StringBuilder sb = new StringBuilder();
        sb.append("select projectI18NText.project.id, projectI18NText.language.id, projectI18NText.tag, projectI18NText.value ");
        sb.append("from " + ProjectI18NText.class.getName() + " projectI18NText ");
        sb.append("where projectI18NText.project.id in (:projectIds) ");
        sb.append("and projectI18NText.language.id in (:languageIds) ");
        sb.append("and projectI18NText.tag in (:tags) ");

        Set<ProjectI18NTag> tags = new HashSet<ProjectI18NTag>();
        tags.add(ProjectI18NTag.NAME);
        tags.add(ProjectI18NTag.CHAPTER_NAME);
        tags.add(ProjectI18NTag.DESCRIPTION);

        //build the query
        Query query = entityManager.createQuery(sb.toString());
        query.setParameter("projectIds", projectIds);
        query.setParameter("languageIds", languageIds);
        query.setParameter("tags", tags);

        List resultList = query.getResultList();

        //create the structured result
        for(Object resultElement : resultList) {

            Object[] resultElementRaw = (Object[]) resultElement;
            long projectId = (Long) resultElementRaw[0];
            long languageId = (Long) resultElementRaw[1];
            ProjectI18NTag tag = (ProjectI18NTag) resultElementRaw[2];
            String value = (String) resultElementRaw[3];

            //verify if project exists on map
            if(projectsI18NTexts.get(projectId) == null) {
                //create project map
                projectsI18NTexts.put(projectId, new HashMap<Long, Map<String, String>>());
            }

            //get project languages map
            Map<String,String> languageMap = projectsI18NTexts.get(projectId).get(languageId);
            if(languageMap == null) {
                //create project languages map
                projectsI18NTexts.get(projectId).put(languageId, new HashMap<String, String>());
            }

            //get project language tag and value map
            Map<String, String> projectLanguageTagValueMap = projectsI18NTexts.get(projectId).get(languageId);
            projectLanguageTagValueMap.put(tag.name(), value);

        }

        return projectsI18NTexts;
    }

    @Override
    public List<ProjectI18NText> findByProject(long projectId) {
        //get the user from the database
        TypedQuery<ProjectI18NText> namedQuery = entityManager.createNamedQuery("ProjectI18NText.findByProject", ProjectI18NText.class);
        namedQuery.setParameter("projectId", projectId);

        List<ProjectI18NText> projectI18NTextList = namedQuery.getResultList();
        return projectI18NTextList;
    }

    @Override
    public List<ProjectI18NText> findByProjectTag(long projectId, ProjectI18NTag tag) {
        //get the user from the database
        TypedQuery<ProjectI18NText> namedQuery = entityManager.createNamedQuery("ProjectI18NText.findByProjectTag", ProjectI18NText.class);
        namedQuery.setParameter("projectId", projectId);
        namedQuery.setParameter("tag", tag);

        List<ProjectI18NText> projectI18NTextList = namedQuery.getResultList();
        return projectI18NTextList;
    }

    @Transactional
    @Override
    public void deleteByProjectId(long projectId) {
        Query deletePageByIdNamedQuery = entityManager.createNamedQuery("ProjectI18NText.deleteByProjectId");
        deletePageByIdNamedQuery.setParameter("projectId", projectId);
        deletePageByIdNamedQuery.executeUpdate();
    }
}
