package org.gmi.thereaderengine.common.elasticsearch.repository;

import org.gmi.thereaderengine.common.elasticsearch.document.PublicationDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * Created by pablo on 11/04/15.
 */
public interface PublicationRepository extends ElasticsearchRepository<PublicationDocument, String> {

    Page<PublicationDocument> findAll(Pageable pageable);

    Page<PublicationDocument> findByTitleId(String titleId, Pageable pageable);
    Page<PublicationDocument> findByPublisherId(String publisherId, Pageable pageable);
    Page<PublicationDocument> findByOrientalReading(String orientalReading, Pageable pageable);
    Page<PublicationDocument> findByNativeLanguageId(String nativeLanguageId, Pageable pageable);
    Page<PublicationDocument> findByTagsIn(List<String> tags, Pageable pageable);
    Page<PublicationDocument> findByLanguagesIdIn(List<String> languageIds, Pageable pageable);
    Page<PublicationDocument> findByAgeRate(String ageRate, Pageable pageable);

    @Query("{\"multi_match\": " +
            " {" +
            "    \"query\":      \"?0\"," +
            "    \"fields\":     [ \"name.value^6\", \"chapterName.value^5\", \"description.value^4\", \"tags\"]" +
            "    }" +
            "}")
    Page<PublicationDocument> findViaKeywords(String keywords, Pageable pageable);

}
