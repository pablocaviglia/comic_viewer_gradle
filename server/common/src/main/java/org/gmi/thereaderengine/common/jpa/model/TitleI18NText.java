package org.gmi.thereaderengine.common.jpa.model;

import com.apocalipta.comic.constants.TitleI18NTag;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Created by pablo on 06/05/15.
 */
@Entity
@Table(name = "TITLE_I18N_TEXT", indexes = {
        @Index(columnList = "ID", name = "titleI18NTextIdIndex"),
        @Index(columnList = "TITLE", name = "titleI18NTitleIndex"),
        @Index(columnList = "LANGUAGE", name = "titleI18NLanguageIndex"),
        @Index(columnList = "TAG", name = "titleI18NTagIndex"),
})
@NamedQueries({
        @NamedQuery(name = "TitleI18NText.findByTitleLanguageAndTag",
                query = "select t from TitleI18NText t where t.title.id = :titleId and t.language.id = :languageId and t.tag = :tag"),
        @NamedQuery(name = "TitleI18NText.findByTitleAndTag",
                query = "select t from TitleI18NText t where t.title.id = :titleId and t.tag = :tag"),
})
public class TitleI18NText implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name="TITLE")
    private Title title;

    @ManyToOne
    @JoinColumn(name="LANGUAGE")
    private Language language;

    @Column(name = "TAG", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private TitleI18NTag tag;

    @Column(name = "VALUE", nullable = true, length = 8192)
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public TitleI18NTag getTag() {
        return tag;
    }

    public void setTag(TitleI18NTag tag) {
        this.tag = tag;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }
}