package org.gmi.thereaderengine.common.filter;

import com.apocalipta.comic.constants.ProjectStatus;
import org.gmi.thereaderengine.common.jpa.model.User;

public class ProjectFilter {
	
	private ProjectStatus status;
	private String name;
	private User publisher;

	/**
	 * firstResult Indicates the index of the firstResult
	 */
	private Integer firstResult;

	/**
	 * maxResult Indicates the maximum number of result to retrieve in a query
	 */
	private Integer maxResults;
	
	public ProjectFilter() {
	}
	
	public ProjectFilter(User publisher) {
		this.publisher = publisher;
	}

	public ProjectStatus getStatus() {
		return status;
	}

	public void setStatus(ProjectStatus status) {
		this.status = status;
	}

	public Integer getFirstResult() {
		return firstResult;
	}

	public void setFirstResult(Integer firstResult) {
		this.firstResult = firstResult;
	}

	public Integer getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(Integer maxResults) {
		this.maxResults = maxResults;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getPublisher() {
		return publisher;
	}

	public void setPublisher(User publisher) {
		this.publisher = publisher;
	}
}