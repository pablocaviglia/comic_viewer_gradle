package org.gmi.thereaderengine.common.jpa.dao;

import java.util.List;

public interface GenericDao<T> {

	T store(T t);

	void delete(Long id);

	T findById(Long id);

	List<T> findAll();

	void flush();

}