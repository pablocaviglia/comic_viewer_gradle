package org.gmi.thereaderengine.common.service;

import org.gmi.thereaderengine.common.jpa.model.Language;

import java.io.IOException;
import java.util.List;

/**
 * Created by pablo on 08/01/15.
 */
public interface LanguageService {

    void verifyInitialData() throws IOException;
    List<Language> getLanguages();
    boolean saveProjectLanguage(long projectId, long languageId);
    boolean saveUserLanguage(long userId, long languageId);
    boolean saveTitleLanguage(long titleId, long languageId);

    List<Language> findAllByRelevance();

    void removeProjectLanguage(long projectId, long languageId);
    void removeUserLanguage(long userId, long languageId);
    void removeTitleLanguage(long titleId, long languageId);

    void changeDefaultProjectLanguage(long projectId, long languageId);
    void changeDefaultUserLanguage(long userId, long languageId);
    void changeDefaultTitleLanguage(long titleId, long languageId);

    Language findLanguageById(long languageId);
    Language findLanguageByCode(String code);

}
