package org.gmi.thereaderengine.common.adapter;

import org.gmi.thereaderengine.common.elasticsearch.document.LanguageDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.i18nDocument;
import org.gmi.thereaderengine.common.jpa.model.TitleI18NText;

/**
 * Created by pablo on 12/01/15.
 */
public class TitleI18NTextAdapter {

    public static com.apocalipta.comic.vo.v1.TitleI18NTextVO toVO(TitleI18NText titleI18NText, com.apocalipta.comic.vo.v1.TitleI18NTextVO titleI18NTextVO) {
        titleI18NTextVO.setId(titleI18NText.getId());
        titleI18NTextVO.setLanguageId(titleI18NText.getLanguage().getId());
        titleI18NTextVO.setTag(titleI18NText.getTag().name());
        titleI18NTextVO.setValue(titleI18NText.getValue());
        return titleI18NTextVO;
    }

    public static i18nDocument toDocument(TitleI18NText titleI18NText, i18nDocument i18nDocument) {
        i18nDocument.setLanguage(LanguageAdapter.toDocument(titleI18NText.getLanguage(), new LanguageDocument()));
        i18nDocument.setValue(titleI18NText.getValue());
        return i18nDocument;
    }
}