package org.gmi.thereaderengine.common.adapter;

import com.apocalipta.comic.vo.v1.PublisherVO;
import org.gmi.thereaderengine.common.jpa.model.Publication;

/**
 * Created by pablo on 12/01/15.
 */
public class PublicationAdapter {

    public static void toVO(Publication publication, com.apocalipta.comic.vo.v1.PublicationVO publicationVO, boolean includePages) {
        publicationVO.setId(publication.getId());
        publicationVO.setStatus(publication.getStatus().name());
        publicationVO.setVersion(publication.getVersion());
        if(publication.getProject() != null) {
            com.apocalipta.comic.vo.v1.ProjectVO projectVO = new com.apocalipta.comic.vo.v1.ProjectVO();

            //load project
            ProjectAdapter.loadVO(publication.getProject(), projectVO, includePages);

            //load publisher
            if(publication.getProject().getPublisher() != null) {
                publicationVO.setPublisherVO(new PublisherVO());
                UserAdapter.loadVO(publication.getProject().getPublisher(), publicationVO.getPublisherVO());
            }

            //set vo
            publicationVO.setProjectVO(projectVO);
        }
    }
}