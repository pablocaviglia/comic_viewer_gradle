package org.gmi.thereaderengine.common.jpa.model;

import com.apocalipta.comic.constants.ProjectI18NTag;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Created by pablo on 08/01/15.
 */
@Entity
@Table(name = "PROJECT_I18N_TEXT", indexes = {
        @Index(columnList = "ID", name = "projectI18NTextIdIndex"),
        @Index(columnList = "PROJECT", name = "projectI18NProjectIndex"),
        @Index(columnList = "LANGUAGE", name = "projectI18NLanguageIndex"),
        @Index(columnList = "TAG", name = "projectI18NTagIndex"),
})
@NamedQueries({
        @NamedQuery(name = "ProjectI18NText.findByProjectLanguageAndTag",
                query = "select p from ProjectI18NText p where p.project.id = :projectId and p.language.id = :languageId and p.tag = :tag"),
        @NamedQuery(name = "ProjectI18NText.findByProject",
                query = "select p from ProjectI18NText p where p.project.id = :projectId"),
        @NamedQuery(name = "ProjectI18NText.findByProjectTag",
                query = "select p from ProjectI18NText p where p.project.id = :projectId and p.tag = :tag"),
        @NamedQuery(name = "ProjectI18NText.deleteByProjectId",
                query = "delete from ProjectI18NText p where p.project.id = :projectId")
})
public class ProjectI18NText implements Serializable {

    private static final long serialVersionUID = -3289575957596214015L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name="PROJECT")
    private Project project;

    @ManyToOne
    @JoinColumn(name="LANGUAGE")
    private Language language;

    @Column(name = "TAG", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private ProjectI18NTag tag;

    @Column(name = "VALUE", nullable = true, length = 8192)
    private String value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public ProjectI18NTag getTag() {
        return tag;
    }

    public void setTag(ProjectI18NTag tag) {
        this.tag = tag;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}