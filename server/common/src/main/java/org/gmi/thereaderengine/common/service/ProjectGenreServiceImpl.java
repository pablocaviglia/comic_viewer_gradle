package org.gmi.thereaderengine.common.service;

import org.gmi.thereaderengine.common.jpa.dao.ProjectGenreDao;
import org.gmi.thereaderengine.common.jpa.model.ProjectGenre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Singleton;

/**
 * Created by pablo on 16/04/15.
 */
@Service
@Singleton
public class ProjectGenreServiceImpl extends GenericService implements ProjectGenreService {

    @Autowired
    private ProjectGenreDao projectGenreDao;

    private static final Map<Long, ProjectGenre> projectGenreMapById = new HashMap<Long, ProjectGenre>();

    @Override
    public void verifyInitialData() throws IOException {
        if(projectGenreDao.count() == 0) {

            List<ProjectGenre> genres = new ArrayList<>();

            genres.add(new ProjectGenre(1l, "Action"));
            genres.add(new ProjectGenre(2l, "Adventure"));
            genres.add(new ProjectGenre(3l, "Comedy"));
            genres.add(new ProjectGenre(4l, "Drama"));
            genres.add(new ProjectGenre(5l, "Fantasy"));
            genres.add(new ProjectGenre(6l, "Historical"));
            genres.add(new ProjectGenre(7l, "Horror"));
            genres.add(new ProjectGenre(8l, "Martial Arts"));
            genres.add(new ProjectGenre(9l, "Mature"));
            genres.add(new ProjectGenre(10l, "Mecha"));
            genres.add(new ProjectGenre(11l, "Mystery"));
            genres.add(new ProjectGenre(12l, "Romance"));
            genres.add(new ProjectGenre(13l, "School Life"));
            genres.add(new ProjectGenre(14l, "Sci-Fi"));
            genres.add(new ProjectGenre(15l, "Slice of Life"));
            genres.add(new ProjectGenre(16l, "Sports"));
            genres.add(new ProjectGenre(17l, "Superhero"));
            genres.add(new ProjectGenre(18l, "Supernatural"));

            projectGenreDao.insert(genres);
        }

        //load the genres in
        //to get faster access
        List<ProjectGenre> projectGenres = projectGenreDao.findAll();
        for(ProjectGenre projectGenre : projectGenres) {
            projectGenreMapById.put(projectGenre.getId(), projectGenre);
        }
    }

    @Override
    public List<ProjectGenre> findAll() {
        return projectGenreDao.findAll();
    }

    @Override
    public ProjectGenre findProjectGenreById(long projectGenreId) {
        return projectGenreMapById.get(projectGenreId);
    }

    @Override
    public List<ProjectGenre> getProjectGenres() {
        return new ArrayList(projectGenreMapById.values());
    }
}