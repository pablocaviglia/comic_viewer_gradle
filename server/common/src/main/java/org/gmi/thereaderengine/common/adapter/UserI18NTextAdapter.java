package org.gmi.thereaderengine.common.adapter;

import org.gmi.thereaderengine.common.jpa.model.UserI18NText;

/**
 * Created by pablo on 12/01/15.
 */
public class UserI18NTextAdapter {

    public static void toVO(UserI18NText userI18NText, com.apocalipta.comic.vo.v1.UserI18NTextVO userI18NTextVO) {
        userI18NTextVO.setId(userI18NText.getId());
        userI18NTextVO.setLanguageId(userI18NText.getLanguage().getId());
        userI18NTextVO.setTag(userI18NText.getTag().name());
        userI18NTextVO.setValue(userI18NText.getValue());
    }
}