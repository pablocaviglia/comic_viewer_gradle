package org.gmi.thereaderengine.common.adapter;

import org.gmi.thereaderengine.common.jpa.model.Page;

/**
 * Created by pablo on 12/01/15.
 */
public class PageAdapter {

    public static void toVO(Page page, com.apocalipta.comic.vo.v1.PageVO pageVO) {
        pageVO.setId(page.getId());
        pageVO.setPageIndex(page.getPageIndex());
        pageVO.setVersion(page.getVersion());

        pageVO.setImageFileSize(page.getImageFileSize());
        pageVO.setThumbFileSize(page.getThumbFileSize());

        pageVO.setImageWidth(page.getImageWidth());
        pageVO.setImageHeight(page.getImageHeight());

        pageVO.setThumbWidth(page.getThumbWidth());
        pageVO.setThumbHeight(page.getThumbHeight());
    }
}