package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.jpa.model.Author;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * Created by pablo on 16/04/15.
 */
@Repository
public class AuthorDaoImpl extends GenericDaoImpl<Author> implements AuthorDao {

    @Override
    public List<Author> findByProjectId(long projectId) {
        TypedQuery<Author> namedQuery = entityManager.createNamedQuery("Author.findByProject", Author.class);
        namedQuery.setParameter("projectId", projectId);
        List<Author> authors = namedQuery.getResultList();
        return authors;
    }

    @Transactional
    @Override
    public void deleteByProjectId(long projectId) {
        Query deletePageByIdNamedQuery = entityManager.createNamedQuery("Author.deleteByProjectId");
        deletePageByIdNamedQuery.setParameter("projectId", projectId);
        deletePageByIdNamedQuery.executeUpdate();
    }
}