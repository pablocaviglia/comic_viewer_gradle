package org.gmi.thereaderengine.common.service;

import org.gmi.thereaderengine.common.jpa.dao.LanguageDao;
import org.gmi.thereaderengine.common.jpa.model.Language;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Singleton;

/**
 * Created by pablo on 08/01/15.
 */
@Service
@Singleton
public class LanguageServiceImpl extends GenericService implements LanguageService {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private ApplicationContext context;

    @Autowired
    private LanguageDao languageDao;

    //map language objects to language ids and codes
    private static final Map<Long, Language> languagesById = new HashMap<Long, Language>();
    private static final Map<String, Language> languagesByCode = new HashMap<String, Language>();

    @Override
    public void verifyInitialData() throws IOException {

        if(languageDao.count() == 0) {

            //languages list
            List<Language> languageList = new ArrayList<Language>();

            {
                //german
                Language language = new Language();
                language.setId(1l);
                language.setCode("de");
                language.setDescription("German");
                language.setRelevance(1000);
                languageList.add(language);
            }

            {
                //french
                Language language = new Language();
                language.setId(5l);
                language.setCode("fr");
                language.setDescription("French");
                language.setRelevance(1000);
                languageList.add(language);
            }

            {
                //italian
                Language language = new Language();
                language.setId(6l);
                language.setCode("it");
                language.setDescription("Italian");
                language.setRelevance(1000);
                languageList.add(language);
            }

            {
                //spanish
                Language language = new Language();
                language.setId(11l);
                language.setCode("es");
                language.setDescription("Spanish");
                language.setRelevance(2);
                languageList.add(language);
            }

            {
                //portuguese
                Language language = new Language();
                language.setId(26l);
                language.setCode("pt");
                language.setDescription("Portuguese");
                language.setRelevance(1000);
                languageList.add(language);
            }

            {
                //english
                Language language = new Language();
                language.setId(37l);
                language.setCode("en");
                language.setDescription("English");
                language.setRelevance(1);
                languageList.add(language);
            }

            //persist initial data
            languageDao.insert(languageList);

        }

        //load the languages in
        //to get faster access
        List<Language> languageList = languageDao.findAll();
        for(Language language : languageList) {
            //by id
            languagesById.put(language.getId(), language);
            //by code
            languagesByCode.put(language.getCode(), language);
        }
    }

    @Override
    public boolean saveProjectLanguage(long projectId, long languageId) {
        return languageDao.saveProjectLanguage(projectId, languageId);
    }

    @Override
    public boolean saveUserLanguage(long userId, long languageId) {
        return languageDao.saveUserLanguage(userId, languageId);
    }

    @Override
    public boolean saveTitleLanguage(long titleId, long languageId) {
        return languageDao.saveTitleLanguage(titleId, languageId);
    }

    @Override
    public List<Language> findAllByRelevance() {
        return languageDao.findAllByRelevance();
    }

    @Override
    public void removeProjectLanguage(long projectId, long languageId) {
        languageDao.removeProjectLanguage(projectId, languageId);
    }

    @Override
    public void removeUserLanguage(long userId, long languageId) {
        languageDao.removeUserLanguage(userId, languageId);
    }

    @Override
    public void removeTitleLanguage(long titleId, long languageId) {
        languageDao.removeTitleLanguage(titleId, languageId);
    }

    @Override
    public void changeDefaultProjectLanguage(long projectId, long languageId) {
        languageDao.changeDefaultProjectLanguage(projectId, languageId);
    }

    @Override
    public void changeDefaultUserLanguage(long userId, long languageId) {
        languageDao.changeDefaultUserLanguage(userId, languageId);
    }

    @Override
    public void changeDefaultTitleLanguage(long titleId, long languageId) {
        languageDao.changeDefaultTitleLanguage(titleId, languageId);
    }

    @Override
    public Language findLanguageById(long languageId) {
        return languagesById.get(languageId);
    }

    @Override
    public Language findLanguageByCode(String code) {
        return languagesByCode.get(code);
    }

    @Override
    public List<Language> getLanguages() {
        return new ArrayList(languagesById.values());
    }

    private String readData(InputStream inputStream) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int i;
        while((i=inputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, i);
        }
        baos.flush();
        baos.close();
        inputStream.close();
        byte[] data = baos.toByteArray();
        return new String(data);
    }
}