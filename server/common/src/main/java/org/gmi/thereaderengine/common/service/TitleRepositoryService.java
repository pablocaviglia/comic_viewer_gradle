package org.gmi.thereaderengine.common.service;

import org.gmi.thereaderengine.common.jpa.model.Title;

/**
 * Created by pablo on 16/05/15.
 */
public interface TitleRepositoryService {

    void save(Title title);

}
