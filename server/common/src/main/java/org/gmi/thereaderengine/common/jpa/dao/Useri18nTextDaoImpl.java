package org.gmi.thereaderengine.common.jpa.dao;

import com.apocalipta.comic.constants.UserI18NTag;

import org.gmi.thereaderengine.common.jpa.model.UserI18NText;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * Created by pablo on 08/01/15.
 */
@Repository
public class Useri18nTextDaoImpl extends GenericDaoImpl<UserI18NText> implements Useri18nTextDao {

    @Override
    public UserI18NText findByUserLanguageAndTag(long userId, long languageId, UserI18NTag tag) {
        TypedQuery<UserI18NText> namedQuery = entityManager.createNamedQuery("UserI18NText.findByUserLanguageAndTag", UserI18NText.class);
        namedQuery.setParameter("userId", userId);
        namedQuery.setParameter("languageId", languageId);
        namedQuery.setParameter("tag", tag);

        try {
            UserI18NText userI18NText = namedQuery.getSingleResult();
            return userI18NText;
        }
        catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public Map<Long, Map<Long, Map<String, String>>> findUsersBaseI18NTexts(Set<Long> userIds, Set<Long> languageIds) {

        //Map<+UserId+, Map<+languageId+, Map<+TAG+, +VALUE+>>>
        Map<Long, Map<Long, Map<String, String>>> usersI18NTexts = new HashMap<Long, Map<Long, Map<String, String>>>();

        //query str
        StringBuilder sb = new StringBuilder();
        sb.append("select userI18NText.user.id, userI18NText.language.id, userI18NText.tag, userI18NText.value ");
        sb.append("from " + UserI18NText.class.getName() + " userI18NText ");
        sb.append("where userI18NText.user.id in (:userIds) ");
        sb.append("and userI18NText.language.id in (:languageIds) ");
        sb.append("and userI18NText.tag in (:tags) ");

        Set<UserI18NTag> tags = new HashSet<UserI18NTag>();
        tags.add(UserI18NTag.INFORMATION);

        //build the query
        Query query = entityManager.createQuery(sb.toString());
        query.setParameter("userIds", userIds);
        query.setParameter("languageIds", languageIds);
        query.setParameter("tags", tags);

        List resultList = query.getResultList();

        //create the structured result
        for(Object resultElement : resultList) {

            Object[] resultElementRaw = (Object[]) resultElement;
            long userId = (Long) resultElementRaw[0];
            long languageId = (Long) resultElementRaw[1];
            UserI18NTag tag = (UserI18NTag) resultElementRaw[2];
            String value = (String) resultElementRaw[3];

            //verify if user exists on map
            if(usersI18NTexts.get(userId) == null) {
                //create user map
                usersI18NTexts.put(userId, new HashMap<Long, Map<String, String>>());
            }

            //get user languages map
            Map<String,String> languageMap = usersI18NTexts.get(userId).get(languageId);
            if(languageMap == null) {
                //create user languages map
                usersI18NTexts.get(userId).put(languageId, new HashMap<String, String>());
            }

            //get user language tag and value map
            Map<String, String> userLanguageTagValueMap = usersI18NTexts.get(userId).get(languageId);
            userLanguageTagValueMap.put(tag.name(), value);

        }

        return usersI18NTexts;
    }

    @Override
    public List<UserI18NText> findByUser(long userId) {
        TypedQuery<UserI18NText> namedQuery = entityManager.createNamedQuery("UserI18NText.findByUser", UserI18NText.class);
        namedQuery.setParameter("userId", userId);
        List<UserI18NText> userI18NTextList = namedQuery.getResultList();
        return userI18NTextList;
    }

    @Override
    public List<UserI18NText> findByUserTag(long userId, UserI18NTag tag) {
        TypedQuery<UserI18NText> namedQuery = entityManager.createNamedQuery("UserI18NText.findByUserTag", UserI18NText.class);
        namedQuery.setParameter("userId", userId);
        namedQuery.setParameter("tag", tag);
        List<UserI18NText> userI18NTextList = namedQuery.getResultList();
        return userI18NTextList;
    }
}