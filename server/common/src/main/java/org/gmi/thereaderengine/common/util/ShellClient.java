package org.gmi.thereaderengine.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.io.*;

public class ShellClient {

    private static Logger LOGGER = LoggerFactory.getLogger(ShellClient.class);
	
	public static String exec(String[] commands, final ShellClientListener listener) throws IOException, InterruptedException {

        final Marker noNewLineMarker = MarkerFactory.getMarker("NO_NEW_LINE");
		for(String command: commands) {
            LOGGER.info(" " + command, noNewLineMarker);
		}
        LOGGER.info("");
		
		final StringBuffer strBuf = new StringBuffer();

		final Process proc = Runtime.getRuntime().exec("/bin/bash", null, new File("/bin"));

		PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(proc.getOutputStream())), true);
		for(int i=0; i<commands.length; i++) {
			out.println(commands[i]);
		}
		out.println("exit");
		out.close();

		try {
			
			Thread t1 = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
						String line = "";
						do {
							if(listener != null) {
								listener.shellMessage(line);
							}
							else {
                                LOGGER.info(line);
							}
							strBuf.append(line);
						}
						while ((line = in.readLine()) != null);
						in.close();
					} 
					catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
					}
				}
			});

			Thread t2 = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						BufferedReader inError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
						String line = "";
						do {
							if(listener != null) {
								listener.shellMessage(line);
							}
							else {
								LOGGER.info(line, noNewLineMarker);
							}
							strBuf.append(line);
						}
						while ((line = inError.readLine()) != null);
						inError.close();
					} 
					catch (Exception e) {
                        LOGGER.error(e.getMessage(), e);
					}
				}
			});
			
			t1.start();
			t2.start();
			
			t1.join();
			t2.join();
			
			proc.waitFor();
			proc.destroy();
			
		} 
		catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
		}
		
		if(listener != null) {
			listener.executionFinished();
		}

		return strBuf.toString();
	}
}