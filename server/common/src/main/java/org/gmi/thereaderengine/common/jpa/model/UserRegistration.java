package org.gmi.thereaderengine.common.jpa.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Created by pablo on 31/03/15.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "UserRegistration.findByUserId",
                query = "select ur.id from UserRegistration ur where ur.userId = :userId"),
        @NamedQuery(name = "UserRegistration.validate",
                query = "select ur.id from UserRegistration ur where ur.userId = :userId and ur.uuid = :uuid"),
        @NamedQuery(name = "UserRegistration.deleteByUserId",
                query = "delete from UserRegistration ur where ur.userId = :userId"),
})
@Table(name = "USER_REGISTRATION", indexes = {
        @Index(columnList = "ID", name = "userRegistrationIdIndex"),
        @Index(columnList = "USER_ID", name = "userRegistrationUserIdIndex"),
        @Index(columnList = "REGISTRATION_DATE", name = "userRegistrationRegistrationDateIndex"),
})
public class UserRegistration implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "USER_ID", nullable = false, unique = true)
    private Long userId;

    @Column(name = "UUID", nullable = false)
    private String uuid;

    @Column(name = "REGISTRATION_DATE", nullable = false)
    private Long registrationDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Long getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Long registrationDate) {
        this.registrationDate = registrationDate;
    }
}