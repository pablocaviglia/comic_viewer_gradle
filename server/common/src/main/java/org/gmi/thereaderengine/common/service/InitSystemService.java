package org.gmi.thereaderengine.common.service;

import org.gmi.thereaderengine.common.util.ShellClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.inject.Singleton;
import javax.mail.MessagingException;
import java.io.IOException;

@Service
@Singleton
public class InitSystemService {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private LanguageService languageService;

    @Autowired
    private ProjectGenreService projectGenreService;

    @Autowired
    private UserService userService;

	@Autowired
	private MailService mailService;

    @PostConstruct
	public void initialize() {
		//validate if imageMagick is installed
		validateImageMagick();
        //initial database data
        verifyInitialData();
	}

    private void verifyInitialData() {
        try {
            languageService.verifyInitialData();
            userService.verifyInitialData();
            projectGenreService.verifyInitialData();
        }
        catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
	
	/**
	 * Validate if imageMagick is installed
	 */
	private void validateImageMagick() {
		try {
			//convert the image
			String convertImageRetval = ShellClient.exec(new String[]{"convert -version"}, null);
			if(convertImageRetval.indexOf("command not found") != -1 || convertImageRetval.indexOf("orden no encontrada") != -1) {
				System.err.println("**********************************");
				System.err.println();
				System.err.println("ImageMagick is not installed on the system, please install it with the following command:");
				System.err.println("sudo apt-get install imagemagick");
				System.err.println();
				System.err.println("**********************************");
				System.exit(0);
			}
		} 
		catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
		}
		catch (InterruptedException e) {
            LOGGER.error(e.getMessage(), e);
		}
	}
}