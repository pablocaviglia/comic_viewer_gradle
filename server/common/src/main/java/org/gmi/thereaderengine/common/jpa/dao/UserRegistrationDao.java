package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.jpa.model.UserRegistration;

/**
 * Created by pablo on 31/03/15.
 */
public interface UserRegistrationDao extends GenericDao<UserRegistration> {

    boolean validateRegistration(long userId, String uuid);
    void deleteByUserId(long userId);

}
