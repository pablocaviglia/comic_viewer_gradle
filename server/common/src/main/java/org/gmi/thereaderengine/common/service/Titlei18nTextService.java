package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.TitleI18NTag;

import org.gmi.thereaderengine.common.jpa.model.TitleI18NText;

import java.util.List;

/**
 * Created by pablo on 08/01/15.
 */
public interface Titlei18nTextService {

    void save(long titleId, long languageId, TitleI18NTag tag, String value);
    List<TitleI18NText> findByTitleAndTag(long titleId, TitleI18NTag tag);

}
