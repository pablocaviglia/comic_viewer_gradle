package org.gmi.thereaderengine.common.adapter;

import org.gmi.thereaderengine.common.elasticsearch.document.LanguageDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.i18nDocument;
import org.gmi.thereaderengine.common.jpa.model.ProjectI18NText;

/**
 * Created by pablo on 12/01/15.
 */
public class ProjectI18NTextAdapter {

    public static com.apocalipta.comic.vo.v1.ProjectI18NTextVO toVO(ProjectI18NText projectI18NText, com.apocalipta.comic.vo.v1.ProjectI18NTextVO projectI18NTextVO) {
        projectI18NTextVO.setId(projectI18NText.getId());
        projectI18NTextVO.setLanguageId(projectI18NText.getLanguage().getId());
        projectI18NTextVO.setTag(projectI18NText.getTag().name());
        projectI18NTextVO.setValue(projectI18NText.getValue());
        return projectI18NTextVO;
    }

    public static i18nDocument toDocument(ProjectI18NText projectI18NText, i18nDocument i18nDocument) {
        i18nDocument.setLanguage(LanguageAdapter.toDocument(projectI18NText.getLanguage(), new LanguageDocument()));
        i18nDocument.setValue(projectI18NText.getValue());
        return i18nDocument;
    }
}