package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.ProjectI18NTag;
import org.gmi.thereaderengine.common.jpa.model.ProjectI18NText;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by pablo on 08/01/15.
 */
public interface Projecti18nTextService {

    void save(long projectId, long languageId, ProjectI18NTag tag, String value);

    ProjectI18NText findByProjectLanguageAndTag(long projectId, long languageId, ProjectI18NTag tag);

    List<ProjectI18NText> findByProject(long projectId);

    List<ProjectI18NText> findByProjectTag(long projectId, ProjectI18NTag tag);

    Map<Long, Map<Long, Map<String, String>>> findPublicationsBaseI18NTexts(Set<Long> projectIds, Set<Long> languageIds);

    void deleteByProjectId(long projectId);
}
