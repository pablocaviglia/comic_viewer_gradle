package org.gmi.thereaderengine.common.service;

import org.gmi.thereaderengine.common.jpa.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.inject.Singleton;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

/**
 * Created by pablo on 24/03/15.
 */
@Service
@Singleton
public class MailServiceImpl extends GenericService implements MailService {

    @Autowired
    private UserService userService;

    @Autowired
    private ThreadService threadService;

    @Override
    public void sendUserRegisterConfirmationMail(final long userId, final String uuid) {

        Runnable r = () -> {
            User user = userService.findById(userId);
            String to = user.getEmail();
            String from = "registration@thereaderengine.com";
            String host = "localhost";

            // Create properties, get Session
            Properties props = new Properties();

            //http://docs.oracle.com/javaee/6/api/javax/mail/Session.html
            // If using static Transport.send(),
            // need to specify which host to send it to
            props.put("mail.smtp.host", host);
            // To see what is going on behind the scene
            props.put("mail.debug", "true");
            Session session = Session.getInstance(props);

            try {
                // Instantiate a message
                Message msg = new MimeMessage(session);

                //Set message attributes
                msg.setFrom(new InternetAddress(from));
                InternetAddress[] address = {new InternetAddress(to)};
                msg.setRecipients(Message.RecipientType.TO, address);
                msg.setSubject("The Reader Engine - User registration");
                msg.setSentDate(new Date());

                // Set message content
                msg.setText("To finish the user registration please go to the following link:\n\n " +
                        "http://thereaderengine.com/userRegistration/finish/" + userId +"/" + uuid);

                //Send the message
                Transport.send(msg);
            }
            catch (MessagingException mex) {
                // Prints all nested (chained) exceptions as well
                mex.printStackTrace();
            }
        };

        threadService.execute(r);
    }
}
