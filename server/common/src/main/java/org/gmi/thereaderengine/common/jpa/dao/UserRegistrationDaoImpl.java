package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.jpa.model.UserRegistration;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 * Created by pablo on 31/03/15.
 */
@Repository
public class UserRegistrationDaoImpl extends GenericDaoImpl<UserRegistration> implements UserRegistrationDao {


    @Override
    public boolean validateRegistration(long userId, String uuid) {

        TypedQuery<Long> namedQuery = entityManager.createNamedQuery("UserRegistration.validate", Long.class);
        namedQuery.setParameter("userId", userId);
        namedQuery.setParameter("uuid", uuid);
        try {
            namedQuery.getSingleResult();
            return true;
        }
        catch(NoResultException e) {
            return false;
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return false;
        }
    }

    @Override
    public void deleteByUserId(long userId) {
        Query namedQuery = entityManager.createNamedQuery("UserRegistration.deleteByUserId");
        namedQuery.setParameter("userId", userId);
        namedQuery.executeUpdate();
    }
}
