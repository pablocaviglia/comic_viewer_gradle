package org.gmi.thereaderengine.common.elasticsearch.document;

import org.gmi.thereaderengine.common.elasticsearch.constant.IndexNameEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.List;

/**
 * Created by pablo on 14/05/15.
 */
@Document(indexName = IndexNameEnum.TITLE, type = IndexNameEnum.TITLE, shards = 1, replicas = 0)
public class TitleDocument {

    @Id
    @Field(type = FieldType.String, store = true)
    private String id;

    @Field(type = FieldType.String, store = true)
    private String code;

    /**
     * This field have the same value than 'name' except
     * that indicating that this field index will not be
     * analyzed allows us to sort by this field
     */
    @Field(type = FieldType.String, store = true, index = FieldIndex.not_analyzed)
    private String codeNotAnalyzed;

    @Field(type = FieldType.Object, store = true)
    private List<i18nDocument> name;

    @Field(type = FieldType.Object, store = true)
    private PublisherDocument publisher;

    @Field(type = FieldType.Object, store = true)
    private List<LanguageDocument> languages;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<i18nDocument> getName() {
        return name;
    }

    public void setName(List<i18nDocument> name) {
        this.name = name;
    }

    public PublisherDocument getPublisher() {
        return publisher;
    }

    public void setPublisher(PublisherDocument publisher) {
        this.publisher = publisher;
    }

    public List<LanguageDocument> getLanguages() {
        return languages;
    }

    public void setLanguages(List<LanguageDocument> languages) {
        this.languages = languages;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCodeNotAnalyzed() {
        return codeNotAnalyzed;
    }

    public void setCodeNotAnalyzed(String codeNotAnalyzed) {
        this.codeNotAnalyzed = codeNotAnalyzed;
    }
}