package org.gmi.thereaderengine.common.service;

import org.gmi.thereaderengine.common.jpa.model.ProjectGenre;

import java.io.IOException;
import java.util.List;

/**
 * Created by pablo on 15/04/15.
 */
public interface ProjectGenreService {

    void verifyInitialData() throws IOException;
    List<ProjectGenre> findAll();

    ProjectGenre findProjectGenreById(long projectGenreId);

    List<ProjectGenre> getProjectGenres();
}
