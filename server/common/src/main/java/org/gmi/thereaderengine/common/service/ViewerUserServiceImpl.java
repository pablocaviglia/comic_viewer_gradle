package org.gmi.thereaderengine.common.service;

import org.gmi.thereaderengine.common.jpa.dao.ViewerUserDao;
import org.gmi.thereaderengine.common.jpa.model.ViewerUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.inject.Singleton;

/**
 * Created by pablo on 05/03/15.
 */
@Service
@Singleton
public class ViewerUserServiceImpl extends GenericService implements ViewerUserService  {

    @Autowired
    private ViewerUserDao viewerUserDao;

    @Override
    public long existsByUsername(String username) {
        return viewerUserDao.existsByUsername(username);
    }

    @Override
    public ViewerUser findByUsername(String username) {
        return viewerUserDao.findByUsername(username);
    }

    @Override
    public long create(ViewerUser viewerUser) {
        viewerUser = viewerUserDao.store(viewerUser);
        return viewerUser.getId();
    }
}
