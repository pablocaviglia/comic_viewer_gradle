package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.JSONApiVersion;
import com.apocalipta.comic.constants.ProjectI18NTag;
import com.apocalipta.comic.constants.TitleI18NTag;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.codehaus.jackson.map.ObjectMapper;
import org.gmi.thereaderengine.common.adapter.TitleAdapter;
import org.gmi.thereaderengine.common.exception.BusinessException;
import org.gmi.thereaderengine.common.filter.TitleFilter;
import org.gmi.thereaderengine.common.jpa.dao.LanguageDao;
import org.gmi.thereaderengine.common.jpa.dao.ProjectDao;
import org.gmi.thereaderengine.common.jpa.dao.TitleDao;
import org.gmi.thereaderengine.common.jpa.model.Language;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.gmi.thereaderengine.common.jpa.model.ProjectI18NText;
import org.gmi.thereaderengine.common.jpa.model.Title;
import org.gmi.thereaderengine.common.jpa.model.TitleI18NText;
import org.gmi.thereaderengine.common.jpa.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.inject.Singleton;

/**
 * Created by pablo on 06/05/15.
 */
@Service
@Singleton
public class TitleServiceImpl extends GenericService implements TitleService {

    //json mapper
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private @Value("#{system['CONTENT_PATH']}") String CONTENT_PATH;

    @Autowired
    private TitleDao titleDao;

    @Autowired
    private Titlei18nTextService titlei18nTextService;

    @Autowired
    private LanguageDao languageDao;

    @Autowired
    private TitleRepositoryService titleRepositoryService;

    @Autowired
    private ProjectDao projectDao;

    @Override
    public Title findById(long titleId) {
        return titleDao.findById(titleId);
    }

    @Override
    public long createTitle(String code, String name, Long defaultLanguageId, User publisher) throws BusinessException {

        //get the default language
        Language defaultLanguage = languageDao.findById(defaultLanguageId);

        //create title
        Title title = new Title();
        title.setCode(code);
        title.setVersion(0l);
        title.setDefaultLanguage(defaultLanguage);
        title.setPublisher(publisher);

        //add languages
        title.setLanguages(new ArrayList<>());
        title.getLanguages().add(defaultLanguage);

        //store it
        title = titleDao.store(title);

        //save the language texts
        titlei18nTextService.save(title.getId(), defaultLanguageId, TitleI18NTag.NAME, name);

        return title.getId();
    }

    @Override
    public void store(Title title) {
        titleDao.store(title);
    }

    @Override
    public boolean existsByCode(String code) {
        return titleDao.existsByCode(code);
    }

    @Override
    public void publish(long titleId, boolean incrementVersion) throws BusinessException {
        try {
            Title title = findById(titleId);
            if(incrementVersion) {
                title.setVersion(title.getVersion() + 1);
                titleDao.store(title);
            }
            //publish on elastic search
            titleRepositoryService.save(title);
            //create the VO
            createTitleJSON_v1(title);
        }
        catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    @Override
    public Title setProjectTitle(long projectId, long titleId) throws BusinessException {
        Title title = findById(titleId);
        Project project = projectDao.findById(projectId);
        project.setTitle(title);
        projectDao.store(project);
        return title;
    }

    @Override
    public void deleteProjectTitle(long projectId) throws BusinessException {
        Project project = projectDao.findById(projectId);
        project.setTitle(null);
        projectDao.store(project);
    }

    private void createTitleJSON_v1(Title title) throws IOException {

        com.apocalipta.comic.vo.v1.TitleVO titleVO = new com.apocalipta.comic.vo.v1.TitleVO();
        TitleAdapter.loadVO(title, titleVO);

        //find project i18n 'name' texts
        List<TitleI18NText> titleI18NTextListName = titlei18nTextService.findByTitleAndTag(title.getId(), TitleI18NTag.NAME);
        if(titleI18NTextListName != null && titleI18NTextListName.size() > 0) {
            titleVO.setName(new HashMap<>());
            for(TitleI18NText titleI18NText : titleI18NTextListName) {
                titleVO.getName().put(titleI18NText.getLanguage().getId(), titleI18NText.getValue());
            }
        }

        //find project i18n 'information' texts
        List<TitleI18NText> titleI18NTextListInformation = titlei18nTextService.findByTitleAndTag(title.getId(), TitleI18NTag.INFORMATION);
        if(titleI18NTextListInformation != null && titleI18NTextListInformation.size() > 0) {
            titleVO.setInformation(new HashMap<>());
            for(TitleI18NText titleI18NText : titleI18NTextListInformation) {
                titleVO.getInformation().put(titleI18NText.getLanguage().getId(), titleI18NText.getValue());
            }
        }

        //get the fs manager
        FileSystemManager fsManager = VFS.getManager();

        //create the publication folder
        FileObject titlesFolder = fsManager.resolveFile(CONTENT_PATH + TITLES_LOCATION + title.getId() + "/");

        //create json
        FileObject descriptorFile = fsManager.resolveFile(titlesFolder, TITLE_DESCRIPTOR + "_" + JSONApiVersion.v1);
        descriptorFile.createFile();
        OBJECT_MAPPER.writeValue(new File(descriptorFile.getURL().getFile()), titleVO);

    }

    @Override
    public int countFindTitles(TitleFilter filter) {
        return titleDao.countFindTitles(filter);
    }

    @Override
    public List<Title> findTitles(TitleFilter filter) {
        return titleDao.findTitles(filter);
    }

    @Override
    public InputStream findByIdJSON(long titleId, JSONApiVersion version) throws FileNotFoundException, FileSystemException {
        FileSystemManager fsManager = VFS.getManager();
        FileObject jsonPath = fsManager.resolveFile(CONTENT_PATH + TitleService.TITLES_LOCATION + titleId + "/" + TitleService.TITLE_DESCRIPTOR + "_" + version);
        return new FileInputStream(jsonPath.getName().getPath());
    }
}