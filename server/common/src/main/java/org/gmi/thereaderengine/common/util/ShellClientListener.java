package org.gmi.thereaderengine.common.util;

public interface ShellClientListener {

	void shellMessage(String msg);
	void executionFinished();
	
}
