package org.gmi.thereaderengine.common.elasticsearch.document;

import org.gmi.thereaderengine.common.elasticsearch.constant.IndexNameEnum;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Setting;

/**
 * Created by pablo on 27/04/15.
 */
@Setting(settingPath = "config/elasticsearch/settings.json")
@Document(indexName = IndexNameEnum.PUBLISHER, type = IndexNameEnum.PUBLISHER, shards = 1, replicas = 0)
public class PublisherDocument {

    @Field(type = FieldType.String, store = true)
    private String id;

    @Field(type = FieldType.String, store = true)
    private String userName;

    @Field(type = FieldType.String, store = true)
    private String email;

    @Field(type = FieldType.String, store = true, indexAnalyzer = "index_ngram", searchAnalyzer = "search_ngram")
    private String name;

    /**
     * This field have the same value than 'name' except
     * that indicating that this field index will not be
     * analyzed allows us to sort by this field
     */
    @Field(type = FieldType.String, store = true, index = FieldIndex.not_analyzed)
    private String nameNotAnalyzed;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameNotAnalyzed() {
        return nameNotAnalyzed;
    }

    public void setNameNotAnalyzed(String nameNotAnalyzed) {
        this.nameNotAnalyzed = nameNotAnalyzed;
    }
}