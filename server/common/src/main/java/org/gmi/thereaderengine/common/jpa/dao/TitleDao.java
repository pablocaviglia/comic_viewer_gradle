package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.filter.TitleFilter;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.gmi.thereaderengine.common.jpa.model.Title;

import java.util.List;

/**
 * Created by pablo on 06/05/15.
 */
public interface TitleDao extends GenericDao<Title> {

    boolean existsByCode(String code);

    List<Title> findTitles(TitleFilter filter);

    int countFindTitles(TitleFilter filter);
}
