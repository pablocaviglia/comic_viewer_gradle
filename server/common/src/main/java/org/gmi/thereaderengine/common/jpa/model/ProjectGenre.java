package org.gmi.thereaderengine.common.jpa.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Created by pablo on 15/04/15.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "ProjectGenre.findAll",
                query = "select pg from ProjectGenre pg order by pg.description asc"),
        @NamedQuery(name = "ProjectGenre.count",
                query = "select count(pg.id) from ProjectGenre pg"),
})
@Table(name = "PROJECT_GENRE", indexes = {
        @Index(columnList = "ID", name = "projectGenreIdIndex"),
})
public class ProjectGenre implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "DESCRIPTION", nullable = true)
    private String description;

    public ProjectGenre() {
    }

    public ProjectGenre(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}