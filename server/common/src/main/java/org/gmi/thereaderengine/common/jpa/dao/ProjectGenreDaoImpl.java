package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.jpa.model.ProjectGenre;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import javax.persistence.TypedQuery;

/**
 * Created by pablo on 16/04/15.
 */
@Repository
public class ProjectGenreDaoImpl extends GenericDaoImpl<ProjectGenre> implements ProjectGenreDao {

    @Override
    public long count() {
        TypedQuery<Long> namedQuery = entityManager.createNamedQuery("ProjectGenre.count", Long.class);
        return namedQuery.getSingleResult();
    }

    @Transactional
    @Override
    public void insert(List<ProjectGenre> genresList) {
        for(ProjectGenre genre: genresList) {
            entityManager.persist(genre);
        }
    }

    @Override
    public List<ProjectGenre> findAll() {
        TypedQuery<ProjectGenre> namedQuery = entityManager.createNamedQuery("ProjectGenre.findAll", ProjectGenre.class);
        List<ProjectGenre> genres = namedQuery.getResultList();
        return genres;
    }

}