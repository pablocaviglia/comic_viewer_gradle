package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.TitleI18NTag;

import org.gmi.thereaderengine.common.adapter.TitleAdapter;
import org.gmi.thereaderengine.common.adapter.TitleI18NTextAdapter;
import org.gmi.thereaderengine.common.elasticsearch.document.TitleDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.i18nDocument;
import org.gmi.thereaderengine.common.elasticsearch.repository.TitleRepository;
import org.gmi.thereaderengine.common.jpa.model.Title;
import org.gmi.thereaderengine.common.jpa.model.TitleI18NText;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

/**
 * Created by pablo on 16/05/15.
 */
@Service
@Singleton
public class TitleRepositoryServiceImpl implements TitleRepositoryService {

    @Autowired
    private TitleRepository titleRepository;

    @Autowired
    private Titlei18nTextService titlei18nTextService;

    @Override
    public void save(Title title) {

        //new document with id
        TitleDocument titleDocument = TitleAdapter.toDocument(title, new TitleDocument());
        titleDocument.setId(title.getId().toString());

        //name
        titleDocument.setName(new ArrayList<>());
        List<TitleI18NText> nameTitleI18NTextList = titlei18nTextService.findByTitleAndTag(title.getId(), TitleI18NTag.NAME);
        nameTitleI18NTextList.forEach(titleI18NText -> titleDocument.getName().add(TitleI18NTextAdapter.toDocument(titleI18NText, new i18nDocument())));

        //persist on elasticsearch
        titleRepository.save(titleDocument);
    }
}