package org.gmi.thereaderengine.common.service;

import org.gmi.thereaderengine.common.exception.BusinessException;
import org.gmi.thereaderengine.common.filter.ProjectFilter;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.gmi.thereaderengine.common.jpa.model.User;

import java.util.List;

public interface ProjectService {

    public static final String PROJECTS_LOCATION = "projects/";
    public static final String THUMB_SUFFIX = "_thumb";
	public static final String ICON_NAME = "icon.png";
	public static final String COVER_NAME = "cover.jpg";
    public static final String BACKGROUND_NAME = "background.jpg";

	Project findById(long projectId);
	List<Project> findProjects(ProjectFilter filter);
	int countFindProjects(ProjectFilter filter);
	long createProject(String code, String projectName, long nativeLanguageId, long defaultLanguageId, User publisher) throws BusinessException;
	void deleteProject(long projectId) throws BusinessException;
	void store(Project project);
	boolean existsByCode(String code);
	
}