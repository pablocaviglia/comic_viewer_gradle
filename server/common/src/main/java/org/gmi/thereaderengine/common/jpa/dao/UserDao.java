package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.jpa.model.User;

public interface UserDao extends GenericDao<User> {

    User findByUsername(String username);

    boolean existsByUsername(String username);

    boolean existsPendingUserRegistrationValidation(long userId);

}