package org.gmi.thereaderengine.common.filter;

import com.apocalipta.comic.constants.PublicationStatus;
import org.gmi.thereaderengine.common.jpa.model.User;

/**
 * Created by pablo on 22/12/14.
 */
public class PublicationFilter {

    private PublicationStatus status;

    private User publisher;

    private String userLanguageCode;

    /**
     * firstResult Indicates the index of the firstResult
     */
    private Integer firstResult;

    private String keywords;

    /**
     * maxResult Indicates the maximum number of result to retrieve in a query
     */
    private Integer maxResults;

    public PublicationFilter() {
    }

    public PublicationFilter(User publisher) {
        this.publisher = publisher;
    }

    public PublicationStatus getStatus() {
        return status;
    }

    public void setStatus(PublicationStatus status) {
        this.status = status;
    }

    public Integer getFirstResult() {
        return firstResult;
    }

    public void setFirstResult(Integer firstResult) {
        this.firstResult = firstResult;
    }

    public Integer getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public User getPublisher() {
        return publisher;
    }

    public void setPublisher(User publisher) {
        this.publisher = publisher;
    }

    public String getUserLanguageCode() {
        return userLanguageCode;
    }

    public void setUserLanguageCode(String userLanguageCode) {
        this.userLanguageCode = userLanguageCode;
    }
}
