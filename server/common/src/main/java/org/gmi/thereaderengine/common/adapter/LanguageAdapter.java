package org.gmi.thereaderengine.common.adapter;

import org.gmi.thereaderengine.common.elasticsearch.document.LanguageDocument;
import org.gmi.thereaderengine.common.jpa.model.Language;

/**
 * Created by pablo on 12/01/15.
 */
public class LanguageAdapter {

    public static com.apocalipta.comic.vo.v1.LanguageVO toVO(Language language, com.apocalipta.comic.vo.v1.LanguageVO languageVO) {
        languageVO.setId(language.getId());
        languageVO.setCode(language.getCode());
        languageVO.setDescription(language.getDescription());
        return languageVO;
    }

    public static LanguageDocument toDocument(Language language, LanguageDocument languageDocument) {
        languageDocument.setId(language.getId().toString());
        languageDocument.setCode(language.getCode());
        languageDocument.setDescription(language.getDescription());
        return languageDocument;
    }
}