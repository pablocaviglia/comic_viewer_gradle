package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.jpa.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

@Repository
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {

	@Override
	public User findByUsername(String username) {
		TypedQuery<User> namedQuery = entityManager.createNamedQuery("User.findByUsername", User.class);
		namedQuery.setParameter("userName", username);
		try {
			User user = namedQuery.getSingleResult();
			return user;
		}
        catch(NoResultException e) {
            return null;
        }
		catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
			return null;
		}
	}

	@Override
	public boolean existsByUsername(String username) {
		Query query = entityManager.createQuery("select u.userName from User u where u.userName = :userName");
		query.setParameter("userName", username);
		try {
			query.getSingleResult();
			return true;
		}
		catch (NoResultException e) {
			return false;
		}
	}

	@Override
	public boolean existsPendingUserRegistrationValidation(long userId) {
		Query query = entityManager.createNamedQuery("UserRegistration.findByUserId");
		query.setParameter("userId", userId);
		try {
			query.getSingleResult();
			return true;
		}
		catch (NoResultException e) {
			return false;
		}
	}
}