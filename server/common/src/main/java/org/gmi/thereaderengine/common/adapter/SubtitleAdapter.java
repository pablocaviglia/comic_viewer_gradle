package org.gmi.thereaderengine.common.adapter;

import org.gmi.thereaderengine.common.jpa.model.Subtitle;

/**
 * Created by pablo on 12/01/15.
 */
public class SubtitleAdapter {

    public static void toVO(Subtitle subtitle, com.apocalipta.comic.vo.v1.SubtitleVO subtitleVO) {
        subtitleVO.setSubtitle(subtitle.getSubtitle());
        subtitleVO.setLanguageId(subtitle.getLanguage().getId());
    }
}