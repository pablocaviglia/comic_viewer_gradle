package org.gmi.thereaderengine.common.jpa.model;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

/**
 * Created by pablo on 03/04/15.
 */
public class UserSpring extends org.springframework.security.core.userdetails.User {

    private Long id;
    private UserRole role;
    private String email;
    private String name;
    private Language defaultLanguage;
    private List<Language> languages;

    public UserSpring(User user, String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
        this.id = user.getId();
        this.role = user.getRole();
        this.email = user.getEmail();
        this.name = user.getName();
        this.defaultLanguage = user.getDefaultLanguage();
        this.languages = user.getLanguages();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Language getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(Language defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }
}