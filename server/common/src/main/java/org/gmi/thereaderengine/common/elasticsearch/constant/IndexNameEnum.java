package org.gmi.thereaderengine.common.elasticsearch.constant;

/**
 * Created by pablo on 03/05/15.
 */
public class IndexNameEnum {

    public static final String PUBLICATION = "publication";
    public static final String PUBLISHER = "publisher";
    public static final String TITLE = "title";

}
