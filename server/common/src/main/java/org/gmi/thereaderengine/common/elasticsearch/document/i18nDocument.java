package org.gmi.thereaderengine.common.elasticsearch.document;

import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * Created by pablo on 12/04/15.
 */
public class i18nDocument {

    @Field(type = FieldType.Object, store = true)
    private LanguageDocument language;

    @Field(type = FieldType.String, store = true, indexAnalyzer = "index_ngram", searchAnalyzer = "search_ngram")
    private String value;

    public LanguageDocument getLanguage() {
        return language;
    }

    public void setLanguage(LanguageDocument language) {
        this.language = language;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
