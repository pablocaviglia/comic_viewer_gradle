package org.gmi.thereaderengine.common.jpa.model;

import com.apocalipta.comic.constants.ProjectAgeRate;
import com.apocalipta.comic.constants.ProjectStatus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@NamedQueries({
        @NamedQuery(name = "Project.findByCode",
                query = "select p from Project p where p.code = :code"),
})
@Table(name = "PROJECT", indexes = {
		@Index(columnList = "ID", name = "projectIdIndex"),
		@Index(columnList = "PUBLISHER", name = "projectPublisherIndex"),
		@Index(columnList = "CODE", name = "projectCodeIndex"),
})
public class Project implements Serializable {

	private static final long serialVersionUID = 2415617745731820277L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;

	@Column(name = "STATUS", nullable = false)
	@Enumerated(value = EnumType.STRING)
	private ProjectStatus status;

	@Column(name = "AGE_RATE", nullable = false)
	@Enumerated(value = EnumType.STRING)
	private ProjectAgeRate ageRate;

	@ManyToOne
	@JoinColumn(name="PUBLISHER", columnDefinition="")
	private User publisher;

	@ManyToOne
	@JoinColumn(name="GENRE", columnDefinition="", nullable = true)
	private ProjectGenre genre;

	@ManyToOne
	@JoinColumn(name="TITLE", columnDefinition="")
	private Title title;

	@Column(name = "ORIENTAL_READING", nullable = false)
	private Boolean orientalReading;

    /**
	 * Start with letters. No spaces.
	 * May contain uppercase or lowercase letters
	 * ('A' through 'Z'), numbers, and underscores ('_').
	 */
	@Column(name = "CODE", nullable = false, unique = true)
	private String code;

	/**
	 * Project name
	 */
	@Column(name = "PROJECT_NAME", nullable = false)
	private String projectName;

	@Column(name = "TAGS", nullable = true)
	private String tags;

    @ManyToOne
    @JoinColumn(name="NATIVE_LANGUAGE", columnDefinition="", nullable = false)
    private Language nativeLanguage;

    @ManyToOne
    @JoinColumn(name="DEFAULT_LANGUAGE", columnDefinition="", nullable = false)
    private Language defaultLanguage;

    @ManyToMany(fetch= FetchType.EAGER)
    @JoinTable(
            name="PROJECT_LANGUAGE",
            joinColumns={@JoinColumn(name="PROJECT_ID", referencedColumnName="ID")},
            inverseJoinColumns={@JoinColumn(name="LANGUAGE_ID", referencedColumnName="ID")})
    private List<Language> languages;

	@OneToMany(fetch=FetchType.LAZY, mappedBy="project")
	private List<Page> pages = new ArrayList<Page>();

	@OneToMany(fetch=FetchType.LAZY, mappedBy="project")
	private List<Author> authors = new ArrayList<Author>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProjectStatus getStatus() {
		return status;
	}

	public void setStatus(ProjectStatus status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public User getPublisher() {
		return publisher;
	}

	public void setPublisher(User publisher) {
		this.publisher = publisher;
	}

	public List<Page> getPages() {
		return pages;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public Page getPageByIndex(int index) {
		if(pages != null) {
			for(Page page : pages) {
				if(page.getPageIndex() == index) {
					return page;
				}
			}
		}
		return null;
	}

	public void setPages(List<Page> pages) {
		this.pages = pages;
	}

	public Boolean getOrientalReading() {
		return orientalReading;
	}

	public void setOrientalReading(Boolean orientalReading) {
		this.orientalReading = orientalReading;
	}

	public ProjectAgeRate getAgeRate() {
		return ageRate;
	}

	public void setAgeRate(ProjectAgeRate ageRate) {
		this.ageRate = ageRate;
	}

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public Language getNativeLanguage() {
        return nativeLanguage;
    }

    public void setNativeLanguage(Language nativeLanguage) {
        this.nativeLanguage = nativeLanguage;
    }

    public Language getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(Language defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

	public List<Author> getAuthors() {
		return authors;
	}

	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public ProjectGenre getGenre() {
		return genre;
	}

	public void setGenre(ProjectGenre genre) {
		this.genre = genre;
	}

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    @Override
	public String toString() {
		return id != null ? id.toString() : "-1";
	}

    public String getSubtitleValue(List<NavigationItem> navigationItems, long navigationItemId, long languageId) {
        if(navigationItems != null) {
            for(NavigationItem navigationItem : navigationItems) {
                if(navigationItem.getId() == navigationItemId) {
                    List<Subtitle> subtitles = navigationItem.getSubtitles();
                    if(subtitles != null) {
                        for(Subtitle subtitle : subtitles) {
                            Language subtitleLanguage = subtitle.getLanguage();
                            if(subtitleLanguage != null) {
                                if(subtitleLanguage.getId() == languageId) {
                                    return subtitle.getSubtitle();
                                }
                            }
                        }
                    }
                }
            }
        }
        return "";
    }
}