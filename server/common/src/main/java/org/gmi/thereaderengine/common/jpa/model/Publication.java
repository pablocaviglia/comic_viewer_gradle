package org.gmi.thereaderengine.common.jpa.model;

import com.apocalipta.comic.constants.PublicationStatus;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by pablo on 21/12/14.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "Publication.findByProjectId",
                query = "select p from Publication p where p.project.id = :projectId"),
        @NamedQuery(name = "Publication.findById",
                query = "select p from Publication p where p.id = :id"),
        @NamedQuery(name = "Publication.findVersionById",
                query = "select p.version from Publication p where p.id = :id")
})
@Table(name = "PUBLICATION", indexes = {
        @Index(columnList = "ID", name = "publicationIdIndex"),
        @Index(columnList = "PROJECT", name = "publicationProjectIndex"),
        @Index(columnList = "FIRST_PUBLICATION_DATE", name = "firstPublicationDateIndex"),
})
public class Publication implements Serializable {

    private static final long serialVersionUID = 1800704579974538571L;

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "STATUS", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private PublicationStatus status;

    @Column(name = "VERSION")
    private Integer version;

    @Column(name = "FIRST_PUBLICATION_DATE")
    private Long firstPublicationDate;

    @ManyToOne
    @JoinColumn(name="PROJECT", nullable = true)
    private Project project;

    @PrePersist
    void preInsert() {
        if(version == null) {
            version = 0;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PublicationStatus getStatus() {
        return status;
    }

    public void setStatus(PublicationStatus status) {
        this.status = status;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Long getFirstPublicationDate() {
        return firstPublicationDate;
    }

    public void setFirstPublicationDate(Long firstPublicationDate) {
        this.firstPublicationDate = firstPublicationDate;
    }
}