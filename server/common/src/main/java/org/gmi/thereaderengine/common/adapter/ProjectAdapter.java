package org.gmi.thereaderengine.common.adapter;


import com.apocalipta.comic.vo.v1.AuthorVO;
import com.apocalipta.comic.vo.v1.ProjectGenreVO;
import com.apocalipta.comic.vo.v1.ProjectVO;
import com.apocalipta.comic.vo.v1.TitleVO;

import org.gmi.thereaderengine.common.jpa.model.Language;
import org.gmi.thereaderengine.common.jpa.model.NavigationItem;
import org.gmi.thereaderengine.common.jpa.model.Page;
import org.gmi.thereaderengine.common.jpa.model.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pablo on 12/01/15.
 */
public class ProjectAdapter {

    public static com.apocalipta.comic.vo.v1.ProjectExtendedVO loadExtendedVO(Project project, com.apocalipta.comic.vo.v1.ProjectExtendedVO projectExtendedVO, boolean includePages) {

        projectExtendedVO.setId(project.getId());
        projectExtendedVO.setCode(project.getCode());
        projectExtendedVO.setProjectName(project.getProjectName());
        projectExtendedVO.setStatus(project.getStatus().name());

        loadVO(project, projectExtendedVO, includePages);

        return projectExtendedVO;
    }

    public static ProjectVO loadVO(Project project, com.apocalipta.comic.vo.v1.ProjectVO projectVO, boolean includePages) {

        projectVO.setAgeRate(project.getAgeRate().name());
        projectVO.setOrientalReading(project.getOrientalReading());

        //genre
        if(project.getGenre() != null) {
            projectVO.setGenre(ProjectGenreAdapter.loadVO(project.getGenre(), new ProjectGenreVO()));
        }

        //authors
        if(project.getAuthors() != null) {
            projectVO.setAuthors(new ArrayList<>());
            project.getAuthors().forEach(author -> projectVO.getAuthors().add(AuthorAdapter.toVO(author, new AuthorVO())));
        }

        //title
        if(project.getTitle() != null) {
            projectVO.setTitleVO(TitleAdapter.loadVO(project.getTitle(), new TitleVO()));
        }

        //set native language
        com.apocalipta.comic.vo.v1.LanguageVO nativeLanguageVO = new com.apocalipta.comic.vo.v1.LanguageVO();
        LanguageAdapter.toVO(project.getNativeLanguage(), nativeLanguageVO);
        projectVO.setNativeLanguage(nativeLanguageVO);

        //set default language
        com.apocalipta.comic.vo.v1.LanguageVO defaultLanguageVO = new com.apocalipta.comic.vo.v1.LanguageVO();
        LanguageAdapter.toVO(project.getDefaultLanguage(), defaultLanguageVO);
        projectVO.setDefaultLanguage(defaultLanguageVO);

        //languages
        List<Language> languageList = project.getLanguages();
        projectVO.setLanguages(new ArrayList<>());
        if(languageList != null && languageList.size() > 0) {
            for(Language language : languageList) {
                com.apocalipta.comic.vo.v1.LanguageVO languageVO = new com.apocalipta.comic.vo.v1.LanguageVO();
                LanguageAdapter.toVO(language, languageVO);
                projectVO.getLanguages().add(languageVO);
            }
        }

        if(includePages) {
            //pages
            List<Page> pageList = project.getPages();
            projectVO.setPages(new ArrayList<>());
            if(pageList != null && pageList.size() > 0) {
                for(Page page : pageList) {
                    com.apocalipta.comic.vo.v1.PageVO pageVO = new com.apocalipta.comic.vo.v1.PageVO();
                    PageAdapter.toVO(page, pageVO);
                    projectVO.getPages().add(pageVO);

                    //load navigation items
                    List<NavigationItem> navigationItemList = page.getNavigationItems();
                    if(navigationItemList != null && navigationItemList.size() > 0) {
                        pageVO.setNavigationItemVOList(new ArrayList<com.apocalipta.comic.vo.v1.NavigationItemVO>());
                        for(NavigationItem navigationItem : navigationItemList) {
                            com.apocalipta.comic.vo.v1.NavigationItemVO navigationItemVO = new com.apocalipta.comic.vo.v1.NavigationItemVO();
                            NavigationItemAdapter.toVO(navigationItem, navigationItemVO);
                            pageVO.getNavigationItemVOList().add(navigationItemVO);
                        }
                    }
                }
            }
        }

        return projectVO;
    }
}