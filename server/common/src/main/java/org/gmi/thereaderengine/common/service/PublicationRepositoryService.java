package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.ProjectAgeRate;

import org.gmi.thereaderengine.common.elasticsearch.document.PublicationDocument;
import org.gmi.thereaderengine.common.jpa.model.Publication;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by pablo on 11/04/15.
 */
public interface PublicationRepositoryService {

    void save(Publication publication);

    PublicationDocument save(PublicationDocument publicationDocument);
    PublicationDocument findById(String id);
    Page<PublicationDocument> findByOrientalReading(boolean  orientalReading, Pageable pageable);
    Page<PublicationDocument> findByAgeRate(ProjectAgeRate ageRate, Pageable pageable);
    Page<PublicationDocument> findByTagsIn(List<String> tags, Pageable pageable);
    Page<PublicationDocument> findByNativeLanguageId(String nativeLanguageId, Pageable pageable);
    Page<PublicationDocument> findByLanguagesIdIn(List<String> languageIds, Pageable pageable);
    Page<PublicationDocument> findViaKeywords(String keywords, Pageable pageable);

}