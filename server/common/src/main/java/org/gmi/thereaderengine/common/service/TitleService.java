package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.JSONApiVersion;

import org.apache.commons.vfs2.FileSystemException;
import org.gmi.thereaderengine.common.exception.BusinessException;
import org.gmi.thereaderengine.common.filter.ProjectFilter;
import org.gmi.thereaderengine.common.filter.TitleFilter;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.gmi.thereaderengine.common.jpa.model.Title;
import org.gmi.thereaderengine.common.jpa.model.User;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by pablo on 06/05/15.
 */
public interface TitleService {

    String TITLES_LOCATION = "titles/";
    String ICON_NAME = "icon.jpg";
    String BACKGROUND_NAME = "background.jpg";
    String TITLE_DESCRIPTOR = "descriptor.json";

    Title findById(long titleId);
    long createTitle(String code, String name, Long defaultLanguageId, User publisher) throws BusinessException;
    void store(Title title);
    boolean existsByCode(String code);
    void publish(long titleId, boolean incrementVersion) throws BusinessException;
    Title setProjectTitle(long projectId, long titleId) throws BusinessException;
    void deleteProjectTitle(long projectId) throws BusinessException;

    int countFindTitles(TitleFilter filter);
    List<Title> findTitles(TitleFilter filter);

    InputStream findByIdJSON(long publicationId, JSONApiVersion version) throws FileNotFoundException, FileSystemException;
}
