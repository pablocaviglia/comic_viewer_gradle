package org.gmi.thereaderengine.common.adapter;


import com.apocalipta.comic.vo.v1.ProjectGenreVO;

import org.gmi.thereaderengine.common.elasticsearch.document.PublicationGenreDocument;
import org.gmi.thereaderengine.common.jpa.model.ProjectGenre;

/**
 * Created by pablo on 12/01/15.
 */
public class ProjectGenreAdapter {

    public static ProjectGenreVO loadVO(ProjectGenre projectGenre, com.apocalipta.comic.vo.v1.ProjectGenreVO projectGenreVO) {
        projectGenreVO.setId(projectGenre.getId());
        projectGenreVO.setDescription(projectGenre.getDescription());
        return projectGenreVO;
    }

    public static PublicationGenreDocument loadDocument(ProjectGenre projectGenre, PublicationGenreDocument publicationGenreDocument) {
        publicationGenreDocument.setId(projectGenre.getId().toString());
        publicationGenreDocument.setDescription(projectGenre.getDescription());
        return publicationGenreDocument;
    }
}