package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.vo.v1.AuthorVO;

import org.gmi.thereaderengine.common.jpa.dao.AuthorDao;
import org.gmi.thereaderengine.common.jpa.model.Author;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.inject.Singleton;

/**
 * Created by pablo on 16/04/15.
 */
@Service
@Singleton
public class AuthorServiceImpl extends GenericService implements AuthorService {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private AuthorDao authorDao;

    @Override
    public void store(Author author) {
        authorDao.store(author);
    }

    @Override
    public List<Author> findByProjectId(long projectId) {
        return authorDao.findByProjectId(projectId);
    }

    @Override
    public Author findById(long id) {
        return authorDao.findById(id);
    }

    @Override
    public void delete(long id) {
        authorDao.delete(id);
    }

    @Override
    public void deleteByProjectId(long projectId) {
        authorDao.deleteByProjectId(projectId);
    }
}