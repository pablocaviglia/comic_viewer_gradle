package org.gmi.thereaderengine.common.util;

/**
 * Created by pablo on 20/12/14.
 */
public class KeystoreUtil {

    public static String createJavaByteArrayCode(byte[] keystore) {
        String javaCode = "final byte[] data = {";
        if(keystore != null) {
            for(byte b : keystore) {
                javaCode += (b + ",");
            }
        }
        //replace last ','
        if(javaCode.indexOf(",") != -1) {
            javaCode = javaCode.substring(0, javaCode.length()-1);
        }
        javaCode += "};";
        return javaCode;
    }
}