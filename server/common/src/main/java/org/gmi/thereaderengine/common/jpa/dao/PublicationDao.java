package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.filter.PublicationFilter;
import org.gmi.thereaderengine.common.jpa.model.Publication;

import java.util.List;

/**
 * Created by pablo on 22/12/14.
 */
public interface PublicationDao extends GenericDao<Publication> {

    List findPublicationPreviews(PublicationFilter filter);

    Publication findPublicationByProjectId(long projectId);

    int countFindPublicationPreviews(PublicationFilter filter);

    int findVersionByPublicationId(long publicationId);

    List<Publication> findPublications(PublicationFilter filter);

    int countFindPublications(PublicationFilter filter);
}
