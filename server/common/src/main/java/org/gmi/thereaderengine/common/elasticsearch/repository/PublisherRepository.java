package org.gmi.thereaderengine.common.elasticsearch.repository;

import org.gmi.thereaderengine.common.elasticsearch.document.PublisherDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Created by pablo on 01/05/15.
 */
public interface PublisherRepository extends ElasticsearchRepository<PublisherDocument, String> {

    Page<PublisherDocument> findAll(Pageable pageable);

}