package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.UserI18NTag;

import org.gmi.thereaderengine.common.jpa.dao.LanguageDao;
import org.gmi.thereaderengine.common.jpa.dao.UserDao;
import org.gmi.thereaderengine.common.jpa.dao.Useri18nTextDao;
import org.gmi.thereaderengine.common.jpa.model.Language;
import org.gmi.thereaderengine.common.jpa.model.User;
import org.gmi.thereaderengine.common.jpa.model.UserI18NText;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Singleton;

/**
 * Created by pablo on 08/01/15.
 */
@Service
@Singleton
public class Useri18nTextServiceImpl extends GenericService implements Useri18nTextService {

    @Autowired
    private Useri18nTextDao useri18nTextDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private LanguageDao languageDao;

    @Transactional
    @Override
    public synchronized void save(long userId, long languageId, UserI18NTag tag, String value) {

        UserI18NText useri18NText = useri18nTextDao.findByUserLanguageAndTag(userId, languageId, tag);
        if(useri18NText == null) {
            useri18NText = new UserI18NText();
        }

        User user = userDao.findById(userId);
        Language language = languageDao.findById(languageId);

        useri18NText.setUser(user);
        useri18NText.setLanguage(language);
        useri18NText.setTag(tag);
        useri18NText.setValue(value);

        useri18nTextDao.store(useri18NText);
    }

    @Override
    public UserI18NText findByUserLanguageAndTag(long userId, long languageId, UserI18NTag tag) {
        return useri18nTextDao.findByUserLanguageAndTag(userId, languageId, tag);
    }

    @Override
    public List<UserI18NText> findByUser(long userId) {
        return useri18nTextDao.findByUser(userId);
    }

    @Override
    public List<UserI18NText> findByUserTag(long userId, UserI18NTag tag) {
        return useri18nTextDao.findByUserTag(userId, tag);
    }

    @Override
    public Map<Long, Map<Long, Map<String, String>>> findUsersBaseI18NTexts(Set<Long> userIds, Set<Long> languageIds) {
        return useri18nTextDao.findUsersBaseI18NTexts(userIds, languageIds);
    }
}
