package org.gmi.thereaderengine.common.service;

import org.apache.commons.vfs2.FileObject;
import org.gmi.thereaderengine.common.exception.BusinessException;
import org.gmi.thereaderengine.common.jpa.model.NavigationItem;
import org.gmi.thereaderengine.common.jpa.model.Page;
import org.gmi.thereaderengine.common.jpa.model.Project;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface PageService {

	void changeNavigationItemIndex(long navigationItemId, int newIndex);
	void changePageIndex(long pageId, int newIndex, boolean setAffected);
	void deletePage(long pageId);
	void deletePageNavigation(long pageId);
	void deleteNavigationItem(long navigationItemId);
	NavigationItem findNavigationItemById(long navigationItemId);
	List<NavigationItem> getNavigationItems(long pageId);
	NavigationItem addNavigationItem(long pageId, NavigationItem navigationItem);
	void updateNavigationItem(NavigationItem navigationItem);
	Page store(Page page) throws BusinessException;
	Page findById(long pageId);
	Project findProjectByNavigationItemId(long navigationItemId);
	List<Page> findByProjectId(long projectId);
	int getLastPageIndex(long projectId);
	int getLastNavigationItemIndex(long pageId);
	void saveSubtitles(long navigationItemId, Map<Long, String> subtitles);
	void updatePageSize(long pageId, int width, int height);
    void finishLastUpload(Long projectId) throws IOException, BusinessException, InterruptedException;
    void createPageThumbnail(FileObject pagePath) throws IOException, InterruptedException;

    void calculatePageImageDimensionsAndSize(long pageId) throws IOException, InterruptedException, BusinessException;

    void incrementPageVersion(long pageId);

    void deleteByNavigationItemAndLanguage(long navigationItemId, long languageId);

}