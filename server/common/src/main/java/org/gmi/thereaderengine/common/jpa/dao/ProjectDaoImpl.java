package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.filter.ProjectFilter;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import java.util.List;

@Repository
public class ProjectDaoImpl extends GenericDaoImpl<Project> implements ProjectDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<Project> findProjects(ProjectFilter filter) {
		Query query = buildQueryFindProjects(filter, false);
		if (filter.getFirstResult() != null) {
			query.setFirstResult(filter.getFirstResult());
		}
		if (filter.getMaxResults() != null) {
			query.setMaxResults(filter.getMaxResults());
		}

		return query.getResultList();
	}
	
	@Override
	public int countFindProjects(ProjectFilter filter) {
		Query query = buildQueryFindProjects(filter, true);
		long result = (Long) query.getSingleResult();
		return (int) result;
	}
	
	private Query buildQueryFindProjects(ProjectFilter filter, boolean count) {
		
		StringBuilder sb = new StringBuilder();
		
		//status
		if (filter.getStatus() != null) {
			sb.append(" where p.status = :status");
		}
		
		//name
		if (filter.getName() != null) {
			if (sb.length() == 0) {
				sb.append(" where ");
			} else {
				sb.append(" and ");
			}
			sb.append(" p.name = :name ");
		}

		//publisher
		if (filter.getPublisher() != null) {
			if (sb.length() == 0) {
				sb.append(" where ");
			} else {
				sb.append(" and ");
			}
			sb.append(" p.publisher.id = :publisherId ");
		}

		sb.insert(0, " from " + Project.class.getName() + " p ");
		
		if (count) {
			sb.insert(0, " select count(p) as qntyProjects ");
		}

		Query query = entityManager.createQuery(sb.toString());
		
		//status
		if (filter.getStatus() != null) {
			query.setParameter("status", filter.getStatus());
		}
		
		//name
		if (filter.getName() != null) {
			query.setParameter("name", filter.getName());
		}
		
		//publisher
		if (filter.getPublisher() != null) {
			query.setParameter("publisherId", filter.getPublisher().getId());
		}		
		
		return query;
	}
	
	@Override
	public boolean existsByCode(String code) {
		Query query = entityManager.createQuery("select p.code from Project p where p.code = :code");
		query.setParameter("code", code);
		try {
			query.getSingleResult();
			return true;
		} 
		catch (NoResultException e) {
			return false;
		}
	}
}