package org.gmi.thereaderengine.common.jpa.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Created by pablo on 08/01/15.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "Language.findAllByRelevance",
                query = "select l from Language l order by l.relevance asc, l.description asc"),
        @NamedQuery(name = "Language.findByCode",
                query = "select l from Language l where l.code = :code"),
        @NamedQuery(name = "Language.findById",
                query = "select l from Language l where l.id = :languageId"),
        @NamedQuery(name = "Language.count",
                query = "select count(l.id) from Language l"),
})
@Table(name = "LANGUAGE", indexes = {
        @Index(columnList = "ID", name = "languageIdIndex"),
})
public class Language implements Serializable {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "CODE", nullable = false)
    private String code;

    @Column(name = "DESCRIPTION", nullable = true)
    private String description;

    @Column(name = "RELEVANCE", nullable = false)
    private int relevance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRelevance() {
        return relevance;
    }

    public void setRelevance(int relevance) {
        this.relevance = relevance;
    }
}