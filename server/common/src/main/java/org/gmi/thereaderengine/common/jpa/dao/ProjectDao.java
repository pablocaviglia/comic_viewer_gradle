package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.filter.ProjectFilter;
import org.gmi.thereaderengine.common.jpa.model.Project;

import java.util.List;

public interface ProjectDao extends GenericDao<Project> {

	List<Project> findProjects(ProjectFilter filter);
	int countFindProjects(ProjectFilter filter);
	boolean existsByCode(String code);

}