package org.gmi.thereaderengine.common.elasticsearch.document;

import org.gmi.thereaderengine.common.elasticsearch.constant.IndexNameEnum;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.annotations.Mapping;
import org.springframework.data.elasticsearch.annotations.Setting;

import java.util.List;

/**
 * Created by pablo on 11/04/15.
 */
@Document(indexName = IndexNameEnum.PUBLICATION, type = IndexNameEnum.PUBLICATION, shards = 1, replicas = 0)
public class PublicationDocument {

    @Id
    @Field(type = FieldType.String, store = true)
    private String id;

    @Field(type = FieldType.Object, store = true)
    private List<i18nDocument> name;

    @Field(type = FieldType.Object, store = true)
    private List<i18nDocument> description;

    @Field(type = FieldType.Object, store = true)
    private List<i18nDocument> chapterName;

    @Field(type = FieldType.Object, store = true)
    private List<i18nDocument> credits;

    @Field(type = FieldType.Object, store = true)
    private LanguageDocument nativeLanguage;

    @Field(type = FieldType.Object, store = true)
    private PublicationGenreDocument publicationGenre;

    @Field(type = FieldType.String, store = true)
    private String ageRate;

    @Field(type = FieldType.String, store = true)
    private String orientalReading;

    @Field(type = FieldType.Long, store = true)
    private Long firstPublicationDate;

    @Field(type = FieldType.Object, store = true)
    private List<LanguageDocument> languages;

    @Field(type= FieldType.String, store = true, indexAnalyzer = "index_ngram", searchAnalyzer = "search_ngram")
    private List<String> tags;

    @Field(type = FieldType.Object, store = true)
    private List<AuthorDocument> authors;

    @Field(type = FieldType.Object, store = true)
    private PublisherDocument publisher;

    @Field(type = FieldType.Object, store = true)
    private TitleDocument title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<i18nDocument> getName() {
        return name;
    }

    public void setName(List<i18nDocument> name) {
        this.name = name;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<i18nDocument> getDescription() {
        return description;
    }

    public void setDescription(List<i18nDocument> description) {
        this.description = description;
    }

    public List<i18nDocument> getChapterName() {
        return chapterName;
    }

    public void setChapterName(List<i18nDocument> chapterName) {
        this.chapterName = chapterName;
    }

    public List<i18nDocument> getCredits() {
        return credits;
    }

    public void setCredits(List<i18nDocument> credits) {
        this.credits = credits;
    }

    public LanguageDocument getNativeLanguage() {
        return nativeLanguage;
    }

    public void setNativeLanguage(LanguageDocument nativeLanguage) {
        this.nativeLanguage = nativeLanguage;
    }

    public String getAgeRate() {
        return ageRate;
    }

    public void setAgeRate(String ageRate) {
        this.ageRate = ageRate;
    }

    public String getOrientalReading() {
        return orientalReading;
    }

    public void setOrientalReading(String orientalReading) {
        this.orientalReading = orientalReading;
    }

    public PublicationGenreDocument getPublicationGenre() {
        return publicationGenre;
    }

    public void setPublicationGenre(PublicationGenreDocument publicationGenre) {
        this.publicationGenre = publicationGenre;
    }

    public List<LanguageDocument> getLanguages() {
        return languages;
    }

    public void setLanguages(List<LanguageDocument> languages) {
        this.languages = languages;
    }

    public List<AuthorDocument> getAuthors() {
        return authors;
    }

    public void setAuthors(List<AuthorDocument> authors) {
        this.authors = authors;
    }

    public PublisherDocument getPublisher() {
        return publisher;
    }

    public void setPublisher(PublisherDocument publisher) {
        this.publisher = publisher;
    }

    public TitleDocument getTitle() {
        return title;
    }

    public void setTitle(TitleDocument title) {
        this.title = title;
    }

    public Long getFirstPublicationDate() {
        return firstPublicationDate;
    }

    public void setFirstPublicationDate(Long firstPublicationDate) {
        this.firstPublicationDate = firstPublicationDate;
    }
}