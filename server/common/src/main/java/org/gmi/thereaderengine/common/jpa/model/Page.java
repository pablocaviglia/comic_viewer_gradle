package org.gmi.thereaderengine.common.jpa.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;

@Entity
@NamedQueries({ 
	@NamedQuery(name = "Page.findByProject", 
			query = "select p from Page p where p.project.id = :projectId"),
	@NamedQuery(name = "Page.findByProjectIdAndIndex", 
			query = "select p from Page p where p.project.id = :projectId and p.pageIndex = :pageIndex"),
	@NamedQuery(name = "Page.getLastPageIndex", 
			query = "select max(p.pageIndex) from Page p where p.project.id = :projectId"),
	@NamedQuery(name = "Page.deleteById", 
			query = "delete from Page p where p.id = :pageId"),
})
@Table(name = "PAGE", indexes = {
        @Index(columnList = "ID", name = "pageIdIndex"),
        @Index(columnList = "PROJECT", name = "pageProjectIndex"),
        @Index(columnList = "PAGE_INDEX", name = "pageIndexIndex"),
})
public class Page implements Serializable {

	private static final long serialVersionUID = -5155741523780789965L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;

	@ManyToOne
	@JoinColumn(name="PROJECT")
	private Project project;
	
	@Column(name = "PAGE_INDEX")
	private Integer pageIndex;

    @Column(name = "IMAGE_FILESIZE")
    private Long imageFileSize;

    @Column(name = "IMAGE_WIDTH")
    private Integer imageWidth;

    @Column(name = "IMAGE_HEIGHT")
    private Integer imageHeight;

    @Column(name = "THUMB_FILESIZE")
    private Long thumbFileSize;

    @Column(name = "THUMB_WIDTH")
    private Integer thumbWidth;

    @Column(name = "THUMB_HEIGHT")
    private Integer thumbHeight;

    @Column(name = "VERSION")
    private Integer version;

    @OneToMany(fetch=FetchType.LAZY, mappedBy="page")
    private List<NavigationItem> navigationItems = new ArrayList<NavigationItem>();

    @PrePersist
    void preInsert() {
        if(version == null) {
            version = 0;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public long getImageFileSize() {
        return imageFileSize;
    }

    public void setImageFileSize(long imageFileSize) {
        this.imageFileSize = imageFileSize;
    }

    public int getImageWidth() {
        return imageWidth;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public int getImageHeight() {
        return imageHeight;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

    public long getThumbFileSize() {
        return thumbFileSize;
    }

    public void setThumbFileSize(long thumbFileSize) {
        this.thumbFileSize = thumbFileSize;
    }

    public int getThumbWidth() {
        return thumbWidth;
    }

    public void setThumbWidth(int thumbWidth) {
        this.thumbWidth = thumbWidth;
    }

    public int getThumbHeight() {
        return thumbHeight;
    }

    public void setThumbHeight(int thumbHeight) {
        this.thumbHeight = thumbHeight;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public List<NavigationItem> getNavigationItems() {
        return navigationItems;
    }

    public void setNavigationItems(List<NavigationItem> navigationItems) {
        this.navigationItems = navigationItems;
    }
}