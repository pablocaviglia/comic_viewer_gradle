package org.gmi.thereaderengine.common.service;

import com.apocalipta.comic.constants.JSONApiVersion;
import com.apocalipta.comic.constants.ProjectI18NTag;
import com.apocalipta.comic.constants.PublicationStatus;
import com.apocalipta.comic.constants.TitleI18NTag;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSelectInfo;
import org.apache.commons.vfs2.FileSelector;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.codehaus.jackson.map.ObjectMapper;
import org.gmi.thereaderengine.common.adapter.PublicationAdapter;
import org.gmi.thereaderengine.common.exception.BusinessException;
import org.gmi.thereaderengine.common.filter.PublicationFilter;
import org.gmi.thereaderengine.common.jpa.dao.ProjectDao;
import org.gmi.thereaderengine.common.jpa.dao.PublicationDao;
import org.gmi.thereaderengine.common.jpa.dao.UserDao;
import org.gmi.thereaderengine.common.jpa.model.Page;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.gmi.thereaderengine.common.jpa.model.ProjectI18NText;
import org.gmi.thereaderengine.common.jpa.model.Publication;
import org.gmi.thereaderengine.common.jpa.model.TitleI18NText;
import org.gmi.thereaderengine.common.jpa.model.User;
import org.gmi.thereaderengine.common.jpa.model.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

import javax.inject.Singleton;

/**
 * Created by pablo on 22/12/14.
 */
@Service
@Singleton
public class PublicationServiceImpl extends GenericService implements PublicationService {

    //json mapper
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private @Value("#{system['CONTENT_PATH']}") String CONTENT_PATH;

    @Autowired
    private PageService pageService;

    @Autowired
    private PublicationRepositoryService publicationRepositoryService;

    @Autowired
    private PublicationDao publicationDao;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private Projecti18nTextService projecti18nTextService;

    @Autowired
    private Titlei18nTextService titlei18nTextService;

    @Autowired
    private Useri18nTextService useri18nTextService;

    @Autowired
    private LanguageService languageService;

    @Autowired
    private UserService userService;

    @Autowired
    private TitleService titleService;

    private FileSelector allFileSelector = new FileSelector() {
        @Override
        public boolean includeFile(FileSelectInfo fileInfo) throws Exception {
            return true;
        }
        @Override
        public boolean traverseDescendents(FileSelectInfo fileInfo) throws Exception {
            return true;
        }
    };

    @Override
    public List<Publication> findPublications(PublicationFilter filter) {
        return publicationDao.findPublications(filter);
    }

    @Override
    public int countFindPublications(PublicationFilter filter) {
        return publicationDao.countFindPublicationPreviews(filter);
    }

    @Override
    @Transactional(readOnly = true)
    public Publication findById(long publicationId) {
        Publication publication = publicationDao.findById(publicationId);
        return publication;
    }

    @Override
    public long findPublicationIdByPageId(long pageId) {
        long publicationId = -1;
        Page page = pageService.findById(pageId);
        long projectId = page.getProject().getId();
        Publication publication = publicationDao.findPublicationByProjectId(projectId);
        if(publication != null) {
            publicationId = publication.getId();
        }
        return publicationId;
    }

    @Override
    public int findVersionByPublicationId(long publicationId) {
        return publicationDao.findVersionByPublicationId(publicationId);
    }

    @Override
    public InputStream findByIdJSON(long publicationId, JSONApiVersion version, boolean full) throws FileNotFoundException, FileSystemException {
        FileSystemManager fsManager = VFS.getManager();
        String jsonFilename = full ? PublicationService.PUBLICATION_DESCRIPTOR : PublicationService.PUBLICATION_DESCRIPTOR_PREVIEW;
        FileObject jsonPath = fsManager.resolveFile(CONTENT_PATH + PublicationService.PUBLICATIONS_LOCATION + publicationId + "/" + jsonFilename + "_" + version);
        return new FileInputStream(jsonPath.getName().getPath());
    }

    @Override
    public Publication store(Publication publication) {
        return publicationDao.store(publication);
    }

    @Override
    public void changeStatus(long projectId, PublicationStatus status) throws IOException {
        Publication publication = findPublicationByProjectId(projectId);
        publication.setStatus(status);
        publicationDao.store(publication);
    }

    @Override
    public Publication publish(long projectId, long publisherUserId) throws IOException, BusinessException {

        //get the project
        Project project = projectDao.findById(projectId);

        //get the publishing user
        User publishingUser = userDao.findById(publisherUserId);

        PublicationStatus publicationStatus = PublicationStatus.PENDING_APPROVAL;
        if(publishingUser.getRole() != UserRole.PUBLISHER) {
            publicationStatus = PublicationStatus.PUBLISHED;
        }

        //get the publication
        Publication publication = findPublicationByProjectId(projectId);
        if(publication == null) {
            publication = new Publication();
            publication.setProject(project);
            publication.setStatus(publicationStatus);
            publication.setVersion(0);
            publication = store(publication);
        }

        //set first publication date
        if(null == publication.getFirstPublicationDate() || publication.getFirstPublicationDate().longValue() == 0) {
            publication.setFirstPublicationDate(System.currentTimeMillis());
        }

        //calculate the new version
        int newVersion = publication.getVersion() + 1;

        //get the fs manager
        FileSystemManager fsManager = VFS.getManager();

        //create the publication folder
        FileObject publicationFolder = fsManager.resolveFile(CONTENT_PATH + PUBLICATIONS_LOCATION + publication.getId() + "/");
        publicationFolder.delete(allFileSelector);
        publicationFolder.createFolder();

        //cover
        FileObject coverPath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + publication.getProject().getId() + "/" + ProjectService.COVER_NAME);
        FileObject coverPathPublication = fsManager.resolveFile(CONTENT_PATH + PUBLICATIONS_LOCATION + publication.getId() + "/" + ProjectService.COVER_NAME);
        coverPathPublication.copyFrom(coverPath, allFileSelector);

        //icon
        FileObject iconPath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + publication.getProject().getId() + "/" + ProjectService.ICON_NAME);
        FileObject iconPathPublication = fsManager.resolveFile(CONTENT_PATH + PUBLICATIONS_LOCATION + publication.getId() + "/" + ProjectService.ICON_NAME);
        iconPathPublication.copyFrom(iconPath, allFileSelector);

        //background
        FileObject backgroundPath = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + publication.getProject().getId() + "/" + ProjectService.BACKGROUND_NAME);
        if(backgroundPath.exists()) {
            FileObject backgroundPathPublication = fsManager.resolveFile(CONTENT_PATH + PUBLICATIONS_LOCATION + publication.getId() + "/" + ProjectService.BACKGROUND_NAME);
            backgroundPathPublication.copyFrom(backgroundPath, allFileSelector);
        }

        //copy images
        for(Page page : publication.getProject().getPages()) {
            //get the path to the page image and thumb
            FileObject pageImage = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + publication.getProject().getId() + "/" + page.getId());
            FileObject pageThumb = fsManager.resolveFile(CONTENT_PATH + ProjectService.PROJECTS_LOCATION + publication.getProject().getId() + "/" + page.getId() + "_thumb");
            //destiny
            FileObject pageImagePublication = fsManager.resolveFile(CONTENT_PATH + PUBLICATIONS_LOCATION + publication.getId() + "/" + page.getId());
            FileObject pageThumbPublication = fsManager.resolveFile(CONTENT_PATH + PUBLICATIONS_LOCATION + publication.getId() + "/" + page.getId() + ProjectService.THUMB_SUFFIX);
            //copy
            pageImagePublication.copyFrom(pageImage, allFileSelector);
            pageThumbPublication.copyFrom(pageThumb, allFileSelector);
        }

        //save the new status and version
        publication.setVersion(newVersion);
        publication.setStatus(publicationStatus);
        store(publication);

        //publish to elasticsearch
        publicationRepositoryService.save(publication);

        //republish the publisher descriptor
        userService.publish(project.getPublisher().getId(), false);

        //republish the title descriptor
        if(null != project.getTitle()) {
            titleService.publish(project.getTitle().getId(), false);
        }

        //create full json
        createPublicationJSON_v1(publication, true);

        //create short json (without pages and so on)
        createPublicationJSON_v1(publication, false);

        return publication;
    }

    @Override
    public Publication findPublicationByProjectId(long projectId) {
        return publicationDao.findPublicationByProjectId(projectId);
    }

    @Override
    public void deletePublication(long publicationId) throws BusinessException {
        publicationDao.delete(publicationId);
    }

    private void createPublicationJSON_v1(Publication publication, boolean includePages) throws IOException {

        com.apocalipta.comic.vo.v1.PublicationVO publicationVO = new com.apocalipta.comic.vo.v1.PublicationVO();
        PublicationAdapter.toVO(publication, publicationVO, includePages);

        //find project i18n 'name' texts
        List<ProjectI18NText> projectI18NTextListName = projecti18nTextService.findByProjectTag(publication.getProject().getId(), ProjectI18NTag.NAME);
        if(null != projectI18NTextListName && projectI18NTextListName.size() > 0) {
            publicationVO.getProjectVO().setName(new HashMap<>());
            for(ProjectI18NText projectI18NText : projectI18NTextListName) {
                publicationVO.getProjectVO().getName().put(projectI18NText.getLanguage().getId(), projectI18NText.getValue());
            }
        }

        //find project i18n 'chapter name' texts
        List<ProjectI18NText> projectI18NTextListChapterName = projecti18nTextService.findByProjectTag(publication.getProject().getId(), ProjectI18NTag.CHAPTER_NAME);
        if(null != projectI18NTextListChapterName && projectI18NTextListChapterName.size() > 0) {
            publicationVO.getProjectVO().setChapterName(new HashMap<>());
            for(ProjectI18NText projectI18NText : projectI18NTextListChapterName) {
                publicationVO.getProjectVO().getChapterName().put(projectI18NText.getLanguage().getId(), projectI18NText.getValue());
            }
        }

        //find project i18n 'description' texts
        List<ProjectI18NText> projectI18NTextListDescription = projecti18nTextService.findByProjectTag(publication.getProject().getId(), ProjectI18NTag.DESCRIPTION);
        if(null != projectI18NTextListDescription && projectI18NTextListDescription.size() > 0) {
            publicationVO.getProjectVO().setDescription(new HashMap<>());
            for(ProjectI18NText projectI18NText : projectI18NTextListDescription) {
                publicationVO.getProjectVO().getDescription().put(projectI18NText.getLanguage().getId(), projectI18NText.getValue());
            }
        }


        if(includePages) {
            //find project i18n 'credits' texts
            List<ProjectI18NText> projectI18NTextListCredits = projecti18nTextService.findByProjectTag(publication.getProject().getId(), ProjectI18NTag.CREDITS);
            if(null != projectI18NTextListCredits && projectI18NTextListCredits.size() > 0) {
                publicationVO.getProjectVO().setCredits(new HashMap<>());
                for(ProjectI18NText projectI18NText : projectI18NTextListCredits) {
                    publicationVO.getProjectVO().getCredits().put(projectI18NText.getLanguage().getId(), projectI18NText.getValue());
                }
            }

            //find project i18n 'legal' texts
            List<ProjectI18NText> projectI18NTextListLegal = projecti18nTextService.findByProjectTag(publication.getProject().getId(), ProjectI18NTag.LEGAL);
            if(null != projectI18NTextListLegal && projectI18NTextListLegal.size() > 0) {
                publicationVO.getProjectVO().setLegal(new HashMap<>());
                for(ProjectI18NText projectI18NText : projectI18NTextListLegal) {
                    publicationVO.getProjectVO().getLegal().put(projectI18NText.getLanguage().getId(), projectI18NText.getValue());
                }
            }

            //find title i18n 'information' texts
            if(null != publication.getProject().getTitle()) {
                List<TitleI18NText> titleI18NTextListInformation = titlei18nTextService.findByTitleAndTag(publication.getProject().getTitle().getId(), TitleI18NTag.INFORMATION);
                if(null != titleI18NTextListInformation && titleI18NTextListInformation.size() > 0) {
                    publicationVO.getProjectVO().getTitleVO().setInformation(new HashMap<>());
                    for(TitleI18NText titleI18NText : titleI18NTextListInformation) {
                        publicationVO.getProjectVO().getTitleVO().getInformation().put(titleI18NText.getLanguage().getId(), titleI18NText.getValue());
                    }
                }
            }

        }

        //get the fs manager
        FileSystemManager fsManager = VFS.getManager();

        //create the publication folder
        FileObject publicationFolder = fsManager.resolveFile(CONTENT_PATH + PUBLICATIONS_LOCATION + publication.getId() + "/");
        FileObject jsonFile = fsManager.resolveFile(publicationFolder, (includePages ? PUBLICATION_DESCRIPTOR : PUBLICATION_DESCRIPTOR_PREVIEW) + "_" + JSONApiVersion.v1);

        //create json
        OBJECT_MAPPER.writeValue(new File(jsonFile.getURL().getFile()), publicationVO);

    }
}