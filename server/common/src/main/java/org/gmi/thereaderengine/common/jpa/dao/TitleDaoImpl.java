package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.filter.ProjectFilter;
import org.gmi.thereaderengine.common.filter.TitleFilter;
import org.gmi.thereaderengine.common.jpa.model.Project;
import org.gmi.thereaderengine.common.jpa.model.Title;
import org.springframework.stereotype.Repository;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 * Created by pablo on 06/05/15.
 */
@Repository
public class TitleDaoImpl extends GenericDaoImpl<Title> implements TitleDao {


    @Override
    public boolean existsByCode(String code) {
        Query query = entityManager.createQuery("select t.code from Title t where t.code = :code");
        query.setParameter("code", code);
        try {
            query.getSingleResult();
            return true;
        }
        catch (NoResultException e) {
            return false;
        }
    }

    @Override
    public List<Title> findTitles(TitleFilter filter) {
        Query query = buildQueryFindTitles(filter, false);
        if (filter.getFirstResult() != null) {
            query.setFirstResult(filter.getFirstResult());
        }
        if (filter.getMaxResults() != null) {
            query.setMaxResults(filter.getMaxResults());
        }

        return query.getResultList();
    }

    @Override
    public int countFindTitles(TitleFilter filter) {
        Query query = buildQueryFindTitles(filter, true);
        long result = (Long) query.getSingleResult();
        return (int) result;
    }

    private Query buildQueryFindTitles(TitleFilter filter, boolean count) {

        StringBuilder sb = new StringBuilder();

        //publisher
        if (filter.getPublisher() != null) {
            if (sb.length() == 0) {
                sb.append(" where ");
            } else {
                sb.append(" and ");
            }
            sb.append(" t.publisher.id = :publisherId ");
        }

        sb.insert(0, " from " + Title.class.getName() + " t ");

        if (count) {
            sb.insert(0, " select count(t) as qntyTitles ");
        }

        Query query = entityManager.createQuery(sb.toString());

        //publisher
        if (filter.getPublisher() != null) {
            query.setParameter("publisherId", filter.getPublisher().getId());
        }

        return query;
    }
}