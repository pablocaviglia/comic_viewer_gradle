package org.gmi.thereaderengine.common.jpa.dao;

import com.apocalipta.comic.constants.TitleI18NTag;

import org.gmi.thereaderengine.common.jpa.model.TitleI18NText;
import org.springframework.stereotype.Repository;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

/**
 * Created by pablo on 06/05/15.
 */
@Repository
public class TitleI18NTextDaoImpl extends GenericDaoImpl<TitleI18NText> implements TitleI18NTextDao {

    @Override
    public TitleI18NText findByTitleLanguageAndTag(long titleId, long languageId, TitleI18NTag tag) {

        TypedQuery<TitleI18NText> namedQuery = entityManager.createNamedQuery("TitleI18NText.findByTitleLanguageAndTag", TitleI18NText.class);
        namedQuery.setParameter("titleId", titleId);
        namedQuery.setParameter("languageId", languageId);
        namedQuery.setParameter("tag", tag);

        try {
            TitleI18NText titleI18NText = namedQuery.getSingleResult();
            return titleI18NText;
        }
        catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<TitleI18NText> findByTitleAndTag(long titleId, TitleI18NTag tag) {

        TypedQuery<TitleI18NText> namedQuery = entityManager.createNamedQuery("TitleI18NText.findByTitleAndTag", TitleI18NText.class);
        namedQuery.setParameter("titleId", titleId);
        namedQuery.setParameter("tag", tag);

        List<TitleI18NText> titleI18NTexts = namedQuery.getResultList();
        return titleI18NTexts;
    }
}