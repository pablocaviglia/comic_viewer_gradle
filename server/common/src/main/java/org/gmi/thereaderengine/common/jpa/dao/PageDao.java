package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.jpa.model.NavigationItem;
import org.gmi.thereaderengine.common.jpa.model.Page;
import org.gmi.thereaderengine.common.jpa.model.Subtitle;

import java.util.List;

public interface PageDao extends GenericDao<Page> {

	NavigationItem findByPageIdAndIndex(long pageId, int index);
	int getLastPageIndex(long projectId);
	int getLastNavigationItemIndex(long pageId);
	NavigationItem addNavigationItem(long pageId, NavigationItem navigationItem);
	NavigationItem findNavigationItemById(long navigationItemId);
	List<NavigationItem> findNavigationItemsByPageId(long pageId);
	List<NavigationItem> getNavigationItems(long pageId);
	List<Page> findByProjectId(long projectId);
	Page findByProjectIdAndIndex(long projectId, int index);
	void deletePage(long pageId);
	void deleteNavigationItem(long navigationItemId);
	void updateNavigationItemIndex(long navigationItemId, int newIndex);
	void updatePageIndex(long pageId, int newIndex);
	Subtitle findSubtitleByNavigationItemAndLanguage(long navigationItemId, long languageId);
	Subtitle addSubtitle(Subtitle subtitle);
	void updateSubtitleValue(long subtitleId, String newValue);
	void deleteByNavigationItemAndLanguage(long navigationItemId, long languageId);
	void updatePageSize(long pageId, int width, int height);
	void updateNavigationItem(NavigationItem navigationItem);
	
}