package org.gmi.thereaderengine.common.jpa.dao;

import org.gmi.thereaderengine.common.jpa.model.NavigationItem;
import org.gmi.thereaderengine.common.jpa.model.Page;
import org.gmi.thereaderengine.common.jpa.model.Subtitle;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class PageDaoImpl extends GenericDaoImpl<Page> implements PageDao {

    public int getLastPageIndex(long projectId) {
		try {
			//get the user from the database
			TypedQuery<Integer> namedQuery = entityManager.createNamedQuery("Page.getLastPageIndex", Integer.class);
			namedQuery.setParameter("projectId", projectId);
			
			Integer max = namedQuery.getSingleResult();
			return max != null ? max : 0;
		} 
		catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
			return -1;
		}
	}
	
	@Override
	public int getLastNavigationItemIndex(long pageId) {
		try {
			//get the user from the database
			TypedQuery<Integer> namedQuery = entityManager.createNamedQuery("NavigationItem.getLastNavigationItemIndex", Integer.class);
			namedQuery.setParameter("pageId", pageId);
			
			Integer max = namedQuery.getSingleResult();
			return max != null ? max : 0;
		} 
		catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
			return -1;
		}
	}
	
	@Transactional
	@Override
	public NavigationItem addNavigationItem(long pageId, NavigationItem navigationItem) {
		//get the page
		Page page = findById(pageId);
		//add the page to the navigation item
		navigationItem.setPage(page);
		return entityManager.merge(navigationItem);
	}
	
	@Transactional
	@Override
	public void deleteNavigationItem(long navigationItemId) {
		Query namedQuery = entityManager.createNamedQuery("NavigationItem.deleteById");
		namedQuery.setParameter("navigationItemId", navigationItemId);
		namedQuery.executeUpdate();
	}

	@Transactional
	@Override
	public void deletePage(long pageId) {
		
		//first delete the associated navigation items
		Query deleteNavigationItemsByPageIdNamedQuery = entityManager.createNamedQuery("NavigationItem.deleteByPageId");
		deleteNavigationItemsByPageIdNamedQuery.setParameter("pageId", pageId);
		deleteNavigationItemsByPageIdNamedQuery.executeUpdate();
		
		//then delete the page
		Query deletePageByIdNamedQuery = entityManager.createNamedQuery("Page.deleteById");
		deletePageByIdNamedQuery.setParameter("pageId", pageId);
		deletePageByIdNamedQuery.executeUpdate();
		
	}
	
	@Override
	public NavigationItem findNavigationItemById(long navigationItemId) {
		try {
			//get the user from the database
			TypedQuery<NavigationItem> namedQuery = entityManager.createNamedQuery("NavigationItem.findById", NavigationItem.class);
			namedQuery.setParameter("id", navigationItemId);
			return namedQuery.getSingleResult();
		} 
		catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
			return null;
		}
	}

	@Override
	public List<NavigationItem> getNavigationItems(long pageId) {
		try {
			//get the user from the database
			TypedQuery<NavigationItem> namedQuery = entityManager.createNamedQuery("NavigationItem.findByPageId", NavigationItem.class);
			namedQuery.setParameter("pageId", pageId);
			return namedQuery.getResultList();
		} 
		catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
			return null;
		}
	}

    @Transactional
	@Override
	public void updatePageIndex(long pageId, int newIndex) {
		Query query = entityManager.createQuery("update Page p set p.pageIndex = :newIndex where p.id = :pageId");
		query.setParameter("pageId", pageId);
		query.setParameter("newIndex", newIndex);
		query.executeUpdate();
	}

    @Transactional
    @Override
	public void updateNavigationItemIndex(long navigationItemId, int newIndex) {
		Query query = entityManager.createQuery("update NavigationItem ni set ni.navigationItemIndex = :newIndex where ni.id = :navigationItemId");
		query.setParameter("navigationItemId", navigationItemId);
		query.setParameter("newIndex", newIndex);
		query.executeUpdate();
	}

	@Override
	public List<Page> findByProjectId(long projectId) {
		try {
			//get the user from the database
			TypedQuery<Page> namedQuery = entityManager.createNamedQuery("Page.findByProject", Page.class);
			namedQuery.setParameter("projectId", projectId);
			return namedQuery.getResultList();
		} 
		catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
			return null;
		}
	}
	
	@Override
	public List<NavigationItem> findNavigationItemsByPageId(long pageId) {
		try {
			//get the user from the database
			TypedQuery<NavigationItem> namedQuery = entityManager.createNamedQuery("NavigationItem.findByPageId", NavigationItem.class);
			namedQuery.setParameter("pageId", pageId);
			return namedQuery.getResultList();
		} 
		catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
			return null;
		}
	}
	
	@Override
	public Page findByProjectIdAndIndex(long projectId, int index) {
		try {
			//get the user from the database
			TypedQuery<Page> namedQuery = entityManager.createNamedQuery("Page.findByProjectIdAndIndex", Page.class);
			namedQuery.setParameter("projectId", projectId);
			namedQuery.setParameter("pageIndex", index);
			return namedQuery.getSingleResult();
		} 
		catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
			return null;
		}
	}
	
	@Override
	public NavigationItem findByPageIdAndIndex(long pageId, int index) {
		try {
			//get the user from the database
			TypedQuery<NavigationItem> namedQuery = entityManager.createNamedQuery("NavigationItem.findByPageIdAndIndex", NavigationItem.class);
			namedQuery.setParameter("pageId", pageId);
			namedQuery.setParameter("navigationItemIndex", index);
			return namedQuery.getSingleResult();
		} 
		catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
			return null;
		}
	}
	
	@Override 
	public Subtitle findSubtitleByNavigationItemAndLanguage(long navigationItemId, long languageId) {
		
		//get the user from the database
		TypedQuery<Subtitle> namedQuery = entityManager.createNamedQuery("Subtitle.findByNavigationItemIdAndLanguage", Subtitle.class);
		namedQuery.setParameter("navigationItemId", navigationItemId);
		namedQuery.setParameter("languageId", languageId);
		
		try {
			Subtitle subtitle = namedQuery.getSingleResult();
			return subtitle;
		}
		catch (NoResultException e) {
			return null;
		}
	}
	
	@Transactional
	@Override
	public Subtitle addSubtitle(Subtitle subtitle) {
		return entityManager.merge(subtitle);
	}
	
	@Transactional
	@Override
	public void updateSubtitleValue(long subtitleId, String newValue) {
		Query query = entityManager.createQuery("update Subtitle s set s.subtitle = :newValue where s.id = :subtitleId");
		query.setParameter("subtitleId", subtitleId);
		query.setParameter("newValue", newValue);
		query.executeUpdate();
	}

    @Transactional
    @Override
	public void deleteByNavigationItemAndLanguage(long navigationItemId, long languageId) {
		Query namedQuery = entityManager.createNamedQuery("Subtitle.deleteByNavigationItemAndLanguage");
		namedQuery.setParameter("navigationItemId", navigationItemId);
		namedQuery.setParameter("languageId", languageId);
		namedQuery.executeUpdate();
	}
	
	@Transactional
	@Override
	public void updatePageSize(long pageId, int width, int height) {
		Query query = entityManager.createQuery("update Page p set p.imageWidth = :width, p.imageHeight = :height where p.id = :pageId");
		query.setParameter("pageId", pageId);
		query.setParameter("width", width);
		query.setParameter("height", height);
		query.executeUpdate();
	}

	@Transactional
	@Override
	public void updateNavigationItem(NavigationItem navigationItem) {
		
		Query query = entityManager.createQuery("update NavigationItem ni set ni.x = :x, ni.y = :y, ni.width = :width, ni.height = :height where ni.id = :id");
		query.setParameter("x", navigationItem.getX());
		query.setParameter("y", navigationItem.getY());
		query.setParameter("width", navigationItem.getWidth());
		query.setParameter("height", navigationItem.getHeight());
		query.setParameter("id", navigationItem.getId());
		query.executeUpdate();
		
	}
}