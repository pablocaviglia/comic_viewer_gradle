package org.gmi.thereaderengine.common.jpa.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@NamedQueries({
	@NamedQuery(name = "User.findByUsername",
				query = "select u from User u where u.userName = :userName")
})
@Table(name = "USER", indexes = {
		@Index(columnList = "ID", name = "userIdIndex"),
		@Index(columnList = "USERNAME", name = "userNameIndex"),
		@Index(columnList = "EMAIL", name = "emailIndex"),
})
public class User implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "ROLE", nullable = false)
	@Enumerated(value = EnumType.STRING)
	private UserRole role;

	@Column(name = "USERNAME", nullable = false, unique = true)
	private String userName;

	@Column(name = "EMAIL", nullable = false, unique = true)
	private String email;

	@Column(name = "PASSWORD", nullable = false)
	private String password;

	@Column(name = "NAME", nullable = false, unique = false)
	private String name;

	@Column(name = "CREATION_DATE", nullable = false)
	private Long creationDate;

	@ManyToOne
	@JoinColumn(name="DEFAULT_LANGUAGE", columnDefinition="", nullable = false)
	private Language defaultLanguage;

	@ManyToMany(fetch= FetchType.LAZY)
	@JoinTable(
			name="USER_LANGUAGE",
			joinColumns={@JoinColumn(name="USER_ID", referencedColumnName="ID")},
			inverseJoinColumns={@JoinColumn(name="LANGUAGE_ID", referencedColumnName="ID")})
	private List<Language> languages;

    @Column(name = "VERSION", nullable = false)
    private Long version;

    public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public UserRole getRole() {
		return role;
	}

	public void setRole(UserRole role) {
		this.role = role;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Language getDefaultLanguage() {
		return defaultLanguage;
	}

	public void setDefaultLanguage(Language defaultLanguage) {
		this.defaultLanguage = defaultLanguage;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

	public Long getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Long creationDate) {
		this.creationDate = creationDate;
	}
}