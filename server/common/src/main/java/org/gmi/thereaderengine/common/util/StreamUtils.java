package org.gmi.thereaderengine.common.util;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Created by pablo on 19/04/15.
 */
public class StreamUtils {

    public static String getInputStreamContent(InputStream inputStream) throws IOException {
        return IOUtils.toString(inputStream, Charset.forName("UTF-8"));
    }
}
