package org.gmi.thereaderengine.common.filter;

import org.gmi.thereaderengine.common.jpa.model.User;

/**
 * Created by pablo on 14/05/15.
 */
public class TitleFilter {

    private User publisher;
    private Integer firstResult;
    private Integer maxResults;

    public User getPublisher() {
        return publisher;
    }

    public void setPublisher(User publisher) {
        this.publisher = publisher;
    }

    public Integer getFirstResult() {
        return firstResult;
    }

    public void setFirstResult(Integer firstResult) {
        this.firstResult = firstResult;
    }

    public Integer getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }
}
