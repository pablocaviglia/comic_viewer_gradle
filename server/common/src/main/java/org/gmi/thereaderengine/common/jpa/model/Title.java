package org.gmi.thereaderengine.common.jpa.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Created by pablo on 06/05/15.
 */
@Entity
@Table(name = "TITLE", indexes = {
        @Index(columnList = "ID", name = "titleIdIndex"),
        @Index(columnList = "PUBLISHER", name = "titlePublisherIndex"),
        @Index(columnList = "CODE", name = "titleCodeIndex"),
})
public class Title implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name="PUBLISHER", columnDefinition="")
    private User publisher;

    /**
     * Start with letters. No spaces.
     * May contain uppercase or lowercase letters
     * ('A' through 'Z'), numbers, and underscores ('_').
     */
    @Column(name = "CODE", nullable = false, unique = true)
    private String code;

    @ManyToOne
    @JoinColumn(name="DEFAULT_LANGUAGE", columnDefinition="", nullable = false)
    private Language defaultLanguage;

    @ManyToMany(fetch= FetchType.EAGER)
    @JoinTable(
            name="TITLE_LANGUAGE",
            joinColumns={@JoinColumn(name="TITLE_ID", referencedColumnName="ID")},
            inverseJoinColumns={@JoinColumn(name="LANGUAGE_ID", referencedColumnName="ID")})
    private List<Language> languages;

    @Column(name = "VERSION", nullable = false)
    private Long version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Language getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(Language defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public User getPublisher() {
        return publisher;
    }

    public void setPublisher(User publisher) {
        this.publisher = publisher;
    }
}