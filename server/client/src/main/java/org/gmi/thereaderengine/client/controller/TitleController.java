package org.gmi.thereaderengine.client.controller;

import com.apocalipta.comic.constants.JSONApiVersion;

import org.apache.commons.io.IOUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.gmi.thereaderengine.common.elasticsearch.document.PublicationDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.PublisherDocument;
import org.gmi.thereaderengine.common.elasticsearch.document.TitleDocument;
import org.gmi.thereaderengine.common.elasticsearch.repository.TitleRepository;
import org.gmi.thereaderengine.common.service.ProjectService;
import org.gmi.thereaderengine.common.service.PublicationService;
import org.gmi.thereaderengine.common.service.TitleService;
import org.gmi.thereaderengine.common.util.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by pablo on 24/05/15.
 */
@Controller
public class TitleController extends GenericController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    // path to the content folder
    private @Value("#{system['CONTENT_PATH']}") String CONTENT_PATH;

    private static final int TITLES_PER_PAGE = 10;

    @Autowired
    private TitleService titleService;

    @Autowired
    private TitleRepository titleRepository;


    @RequestMapping(
            value = "/api/{version}/title/search",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces= MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void search(
            @PathVariable JSONApiVersion version,
            @RequestParam(required=false, defaultValue = "0") Integer offset,
            @RequestParam(required=false) String keywords,
            HttpServletResponse response) throws IOException {

        //transform offset into page number
        offset = offset / TITLES_PER_PAGE;

        Page<TitleDocument> titleDocumentPage = titleRepository.findAll(new PageRequest(offset, TITLES_PER_PAGE, new Sort(new Sort.Order(Sort.Direction.ASC, "code"))));

        //create json result
        copyStringToOutput(createTitleListJSon(titleDocumentPage, version), response.getOutputStream());
    }

    @RequestMapping(
            value = "/api/v1/title/downloadIcon/{id}/{version}",
            method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public void downloadIcon(@PathVariable Long id, HttpServletResponse response) throws IOException {

        //get the fs manager
        FileSystemManager fsManager = VFS.getManager();
        FileObject iconPath = fsManager.resolveFile(CONTENT_PATH + TitleService.TITLES_LOCATION + id + "/" + TitleService.ICON_NAME);

        if(iconPath != null && iconPath.exists()) {
            //copy the file stream into the response
            IOUtils.copy(iconPath.getContent().getInputStream(), response.getOutputStream());
            response.flushBuffer();
        }
    }

    @RequestMapping(
            value ="/api/{version}/title/getDescriptor/{id}",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces= MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void getDescriptor(
            @PathVariable Long id,
            @PathVariable JSONApiVersion version,
            HttpServletResponse response) throws IOException {
        InputStream descriptorInputStream = titleService.findByIdJSON(id, version);
        if(descriptorInputStream != null) {
            IOUtils.copy(descriptorInputStream, response.getOutputStream());
        }
    }

    private String createTitleListJSon(Page<TitleDocument> titleDocumentPage, JSONApiVersion version) {

        long totalElements = titleDocumentPage.getTotalElements();

        final StringBuilder jsonBuilder = new StringBuilder();
        jsonBuilder.append("{");
        jsonBuilder.append("    \"total\": " + totalElements);

        if(totalElements > 0) {
            jsonBuilder.append(",\"elements\": [");
            final int[] i = {0};
            titleDocumentPage.forEach(publicationDocument -> {
                try {
                    String titleJson = StreamUtils.getInputStreamContent(titleService.findByIdJSON(Long.valueOf(publicationDocument.getId()), version));
                    if (i[0] > 0) {
                        jsonBuilder.append(",");
                    }
                    jsonBuilder.append(titleJson);
                    i[0]++;
                } catch (Exception e) {
                    LOGGER.info(e.getMessage(), e);
                }
            });
            jsonBuilder.append("]");
        }

        jsonBuilder.append("}");

        return jsonBuilder.toString();
    }
}