package org.gmi.thereaderengine.client.config;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.gmi.thereaderengine.common.util.KeystoreUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by pablo on 12/12/14.
 */
@SpringBootApplication
public class TomcatConfig {

    private static Logger LOGGER = LoggerFactory.getLogger(TomcatConfig.class);

    private static final int HTTPS_PORT = 9443;
    private static final int HTTP_PORT = 9444;

    private Connector sslConnector;

    @Autowired
    private ApplicationContext context;

    @Bean
    public EmbeddedServletContainerFactory servletContainer() {
        TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();

        factory.addAdditionalTomcatConnectors(createSslConnector());

        //configure the default connector (http) port
        factory.addConnectorCustomizers(new TomcatConnectorCustomizer() {
            @Override
            public void customize(Connector connector) {
                if(connector != sslConnector) {
                    connector.setPort(HTTP_PORT);
                }
            }
        });

        return factory;
    }

    private Connector createSslConnector() {

        //print the client keystore
//        printClientKeystore();

        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
        try {

            //read the keystore from the jar
            //and write it to a tmp file
            Resource keystoreResource = context.getResource("classpath:config/server.keystore");
            byte[] keystoreData = readKeystore(keystoreResource.getInputStream());
            File tmpKeystoreFile = File.createTempFile("keystore", "");
            writeKeystore(tmpKeystoreFile, keystoreData);

            //keystore information
            final String keystoreFile = tmpKeystoreFile.getAbsolutePath();
            final String keystorePass = "pablo!!84";
            final String keystoreType = "pkcs12";
            final String keystoreProvider = "SunJSSE";
            final String keystoreAlias = "comics_tomcat";

            connector.setScheme("https");
            connector.setAttribute("clientAuth", "true");
            connector.setPort(HTTPS_PORT);
            connector.setSecure(true);
            protocol.setSSLEnabled(true);

            //gzip compression
            connector.setProperty("compression", "on");
            connector.setProperty("compressionMinSize", "512");
            connector.setProperty("compressableMimeType",
                            MediaType.APPLICATION_JSON_VALUE + "," +
                            MediaType.TEXT_PLAIN_VALUE);

            //keystore
            protocol.setKeystoreFile(keystoreFile);
            protocol.setKeystorePass(keystorePass);
            protocol.setKeystoreType(keystoreType);
            protocol.setProperty("keystoreProvider", keystoreProvider);
            protocol.setKeyAlias(keystoreAlias);

            //truststore
            protocol.setTruststoreFile(keystoreFile);
            protocol.setTruststorePass(keystorePass);

            protocol.setPort(HTTPS_PORT);

            return connector;
        }
        catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new IllegalStateException("cant access keystore: [" + "keystore" + "] or truststore: [" + "keystore" + "]", e);
        }
    }

    private void printClientKeystore() {
        try {
            LOGGER.info("***************** CLIENT KEYSTORE *****************");
            Resource keystoreResource = context.getResource("classpath:config/client.keystore");
            byte[] keystoreData = readKeystore(keystoreResource.getInputStream());
            FileOutputStream fos = new FileOutputStream(File.createTempFile("clientkeystore", ""));
            fos.write(keystoreData);
            fos.flush();
            fos.close();
            String clientKeystoreJavaCode = KeystoreUtil.createJavaByteArrayCode(keystoreData);
            LOGGER.info(clientKeystoreJavaCode);

        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    private void writeKeystore(File location, byte[] data) throws IOException {
        FileOutputStream fos = new FileOutputStream(location);
        fos.write(data);
        fos.flush();
        fos.close();
    }

    private byte[] readKeystore(InputStream inputStream) throws IOException {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int i;
        while((i=inputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, i);
        }
        baos.flush();
        baos.close();
        inputStream.close();
        byte[] keystore = baos.toByteArray();
        return keystore;
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(TomcatConfig.class, args);
    }
}