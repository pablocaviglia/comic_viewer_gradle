package org.gmi.thereaderengine.client.controller;

import com.apocalipta.comic.constants.ResultStatus;
import com.apocalipta.comic.vo.v1.ResultVO;

import org.gmi.thereaderengine.common.jpa.model.ViewerUser;
import org.gmi.thereaderengine.common.service.ViewerUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by pablo on 05/03/15.
 */
@Controller
public class ViewerUserController extends GenericController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private ViewerUserService viewerUserService;

    @RequestMapping(
            value = "/api/v1/viewerUser/login",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces=MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public String login(@RequestParam(required=true) String username) {
        LOGGER.debug("logging in user :: " + username);
        long userId = viewerUserService.existsByUsername(username);
        return String.valueOf(userId);
    }

    @RequestMapping(
            value = "/api/v1/viewerUser/create",
            method = {RequestMethod.POST},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResultVO create(@RequestBody ViewerUser viewerUser) {
        LOGGER.debug("creating user :: " + viewerUser.getUsername());
        ResultVO resultVO = new ResultVO();
        try {
            long id = viewerUserService.create(viewerUser);
            resultVO.setResultStatus(ResultStatus.OK);
            resultVO.setResult(id);
        }
        catch(Exception e) {
            resultVO.setResultStatus(ResultStatus.ERROR);
        }
        return resultVO;
    }
}