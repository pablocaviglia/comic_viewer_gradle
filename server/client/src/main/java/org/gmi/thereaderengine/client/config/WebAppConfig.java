package org.gmi.thereaderengine.client.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by pablo on 12/12/14.
 */
@Configuration
@ComponentScan(basePackages = { "org.gmi.thereaderengine" })
@EnableWebMvc
public class WebAppConfig {

}