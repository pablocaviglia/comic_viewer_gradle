package org.gmi.thereaderengine.client.controller;

import com.apocalipta.comic.constants.JSONApiVersion;

import org.apache.commons.io.IOUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.gmi.thereaderengine.common.elasticsearch.document.PublisherDocument;
import org.gmi.thereaderengine.common.elasticsearch.repository.PublisherRepository;
import org.gmi.thereaderengine.common.service.UserService;
import org.gmi.thereaderengine.common.util.StreamUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by pablo on 05/04/15.
 */
@Controller
public class PublisherController extends GenericController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    // path to the content folder
    private @Value("#{system['CONTENT_PATH']}") String CONTENT_PATH;

    private static final int PUBLISHERS_PER_PAGE = 10;

    @Autowired
    private UserService userService;

    @Autowired
    private PublisherRepository publisherRepository;

    @RequestMapping(
            value = "/api/{version}/publisher/search",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void search(
            @PathVariable JSONApiVersion version,
            @RequestParam(required=false, defaultValue = "0") Integer offset,
            @RequestParam(required=false) String keywords,
            HttpServletResponse response) throws IOException {

        //transform offset into page number
        offset = offset / PUBLISHERS_PER_PAGE;

        Page<PublisherDocument> publisherDocumentPage = publisherRepository.findAll(new PageRequest(offset, PUBLISHERS_PER_PAGE, new Sort(new Sort.Order(Sort.Direction.ASC, "nameNotAnalyzed"))));
        copyStringToOutput(createPublisherListJSon(publisherDocumentPage, version), response.getOutputStream());
    }

    @RequestMapping(
            value ="/api/{version}/publisher/getDescriptor/{id}",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces= MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void getDescriptor(
            @PathVariable Long id,
            @PathVariable JSONApiVersion version,
            HttpServletResponse response) throws IOException {
        InputStream descriptorInputStream = userService.findByIdJSON(id, version);
        if(descriptorInputStream != null) {
            IOUtils.copy(descriptorInputStream, response.getOutputStream());
        }
    }

    @RequestMapping(
            value = "/api/v1/user/downloadAvatar/{id}/{version}",
            method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public void downloadAvatar(@PathVariable Long id, HttpServletResponse response) throws IOException {

        //get the fs manager
        FileSystemManager fsManager = VFS.getManager();
        FileObject coverPath = fsManager.resolveFile(CONTENT_PATH + UserService.USERS_LOCATION + id + "/" + UserService.AVATAR_NAME);

        if(coverPath != null && coverPath.exists()) {
            //copy the file stream into the response
            IOUtils.copy(coverPath.getContent().getInputStream(), response.getOutputStream());
            response.flushBuffer();
        }
    }

    private String createPublisherListJSon(Page<PublisherDocument> publisherDocumentPage, JSONApiVersion version) {

        long totalElements = publisherDocumentPage.getTotalElements();

        final StringBuilder jsonBuilder = new StringBuilder();
        jsonBuilder.append("{");
        jsonBuilder.append("    \"total\": " + totalElements);

        if(totalElements > 0) {
            jsonBuilder.append(",\"elements\": [");
            final int[] i = {0};
            publisherDocumentPage.forEach(publisherDocument -> {
                try {
                    InputStream publisherJSonInputStream = userService.findByIdJSON(Long.valueOf(publisherDocument.getId()), version);
                    if(publisherJSonInputStream != null) {
                        String publisherJson = StreamUtils.getInputStreamContent(publisherJSonInputStream);
                        if(i[0] > 0) {
                            jsonBuilder.append(",");
                        }
                        jsonBuilder.append(publisherJson);
                        i[0]++;
                    }
                }
                catch (Exception e) {
                    LOGGER.info(e.getMessage(), e);
                }
            });
            jsonBuilder.append("]");
        }
        jsonBuilder.append("}");

        return jsonBuilder.toString();
    }
}