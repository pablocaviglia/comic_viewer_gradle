package org.gmi.thereaderengine.client.controller;

import com.apocalipta.comic.constants.JSONApiVersion;

import org.apache.commons.io.IOUtils;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.gmi.thereaderengine.common.elasticsearch.document.PublicationDocument;
import org.gmi.thereaderengine.common.elasticsearch.repository.PublicationRepository;
import org.gmi.thereaderengine.common.service.ProjectService;
import org.gmi.thereaderengine.common.service.PublicationService;
import org.gmi.thereaderengine.common.util.StreamUtils;
import org.gmi.thereaderengine.common.util.ThrottledOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by pablo on 20/12/14.
 */
@Controller
public class PublicationController extends GenericController {

    private final Logger LOGGER = LoggerFactory.getLogger(getClass());

    // path to the content folder
    private @Value("#{system['CONTENT_PATH']}") String CONTENT_PATH;

    @Autowired
    private PublicationRepository publicationRepository;

    @Autowired
    private PublicationService publicationService;

    private static boolean LIMIT_TRANSFER_SPEED = false;
    private static final float MAX_BYTE_TRANSFER_SPEED_PER_SECOND = 100 * 1024; // 1 * 1024 = 1kb/s

    private static final int PUBLICATIONS_PER_PAGE = 10;

    @RequestMapping(
            value = "/api/{version}/publication/search",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void search(
            @PathVariable JSONApiVersion version,
            @RequestParam(required=false, defaultValue = "0") Integer offset,
            @RequestParam(required=false) String keywords,
            HttpServletResponse response) throws IOException {

        //transform offset into page number
        offset = offset / PUBLICATIONS_PER_PAGE;

        Page<PublicationDocument> publicationDocumentPage;

        if(null != keywords && !"".equalsIgnoreCase(keywords.trim())) {
            publicationDocumentPage = publicationRepository.findViaKeywords(keywords, new PageRequest(offset, PUBLICATIONS_PER_PAGE, Sort.Direction.DESC, "firstPublicationDate"));
        }
        else {
            publicationDocumentPage = publicationRepository.findAll(new PageRequest(offset, PUBLICATIONS_PER_PAGE, Sort.Direction.DESC, "firstPublicationDate"));
        }

        copyStringToOutput(createPublicationListJSon(publicationDocumentPage, version), response.getOutputStream());
    }

    @RequestMapping(
            value = "/api/{version}/publication/search_by_publisher",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void searchByPublisher(
            @PathVariable JSONApiVersion version,
            @RequestParam(required=false, defaultValue = "0") Integer offset,
            @RequestParam(required=true) String publisherId,
            HttpServletResponse response) throws IOException {

        //transform offset into page number
        offset = offset / PUBLICATIONS_PER_PAGE;

        Page<PublicationDocument> publicationDocumentPage = null;

        if(null != publisherId && !"".equalsIgnoreCase(publisherId.trim())) {
            publicationDocumentPage = publicationRepository.findByPublisherId(publisherId, new PageRequest(offset, PUBLICATIONS_PER_PAGE, Sort.Direction.DESC, "firstPublicationDate"));
        }

        copyStringToOutput(createPublicationListJSon(publicationDocumentPage, version), response.getOutputStream());
    }

    @RequestMapping(
            value = "/api/{version}/publication/search_by_title",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void searchByTitle(
            @PathVariable JSONApiVersion version,
            @RequestParam(required=false, defaultValue = "0") Integer offset,
            @RequestParam(required=true) String titleId,
            HttpServletResponse response) throws IOException {

        //transform offset into page number
        offset = offset / PUBLICATIONS_PER_PAGE;

        Page<PublicationDocument> publicationDocumentPage = null;

        if(null != titleId && !"".equalsIgnoreCase(titleId.trim())) {
            publicationDocumentPage = publicationRepository.findByTitleId(titleId, new PageRequest(offset, PUBLICATIONS_PER_PAGE, Sort.Direction.DESC, "firstPublicationDate"));
        }

        copyStringToOutput(createPublicationListJSon(publicationDocumentPage, version), response.getOutputStream());
    }

    @RequestMapping(
            value ="/api/{version}/publication/getDescriptor/{id}",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void getDescriptor(
            @PathVariable Long id,
            @PathVariable JSONApiVersion version,
            HttpServletResponse response) throws IOException {
        IOUtils.copy(publicationService.findByIdJSON(id, version, true), response.getOutputStream());
    }

    @RequestMapping(
            value ="/api/{version}/publication/getVersion/{id}",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces=MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public void getVersion(@PathVariable Long id, HttpServletResponse response) throws IOException {
        OutputStream outputStream = response.getOutputStream();
        outputStream.write(String.valueOf(publicationService.findVersionByPublicationId(id)).getBytes());
        outputStream.flush();
        outputStream.close();
    }

    @RequestMapping(
            value = "/api/v1/publication/download/page/{pageIdList}",
            method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public void downloadPage(@PathVariable String pageIdList, HttpServletResponse response) throws IOException {

        String[] pageIds =  pageIdList.split(",");
        if(pageIds != null && pageIdList.length() > 0) {

            //get the publication id
            long publicationId = publicationService.findPublicationIdByPageId(Long.parseLong(pageIds[0]));

            for(String pageId : pageIds) {

                //get the fs manager
                FileSystemManager fsManager = VFS.getManager();
                FileObject pagePath = fsManager.resolveFile(CONTENT_PATH + PublicationService.PUBLICATIONS_LOCATION + publicationId + "/" + pageId.toString());
                FileObject thumbPath = fsManager.resolveFile(CONTENT_PATH + PublicationService.PUBLICATIONS_LOCATION + publicationId + "/" + pageId.toString() + ProjectService.THUMB_SUFFIX);

                if(pagePath != null && pagePath.exists()) {
                    if(LIMIT_TRANSFER_SPEED) {
                        //thumb
                        IOUtils.copy(thumbPath.getContent().getInputStream(), new ThrottledOutputStream(response.getOutputStream(), MAX_BYTE_TRANSFER_SPEED_PER_SECOND));
                        //page
                        IOUtils.copy(pagePath.getContent().getInputStream(), new ThrottledOutputStream(response.getOutputStream(), MAX_BYTE_TRANSFER_SPEED_PER_SECOND));
                    }
                    else {
                        //thumb
                        IOUtils.copy(thumbPath.getContent().getInputStream(), response.getOutputStream());
                        //page
                        IOUtils.copy(pagePath.getContent().getInputStream(), response.getOutputStream());
                    }
                }
            }
            //flush buffers
            response.flushBuffer();
        }
    }

    @RequestMapping(
            value = "/api/v1/publication/downloadBackground/{id}/{version}",
            method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public void downloadBackground(@PathVariable Long id, HttpServletResponse response) throws IOException {

        //get the fs manager
        FileSystemManager fsManager = VFS.getManager();
        FileObject coverPath = fsManager.resolveFile(CONTENT_PATH + PublicationService.PUBLICATIONS_LOCATION + id + "/" + ProjectService.BACKGROUND_NAME);

        if(coverPath != null && coverPath.exists()) {
            //copy the file stream into the response
            IOUtils.copy(coverPath.getContent().getInputStream(), response.getOutputStream());
            response.flushBuffer();
        }
    }

    @RequestMapping(
            value = "/api/v1/publication/downloadIcon/{id}/{version}",
            method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public void downloadIcon(@PathVariable Long id, HttpServletResponse response) throws IOException {

        //get the fs manager
        FileSystemManager fsManager = VFS.getManager();
        FileObject coverPath = fsManager.resolveFile(CONTENT_PATH + PublicationService.PUBLICATIONS_LOCATION + id + "/" + ProjectService.ICON_NAME);

        if(coverPath != null && coverPath.exists()) {
            //copy the file stream into the response
            IOUtils.copy(coverPath.getContent().getInputStream(), response.getOutputStream());
            response.flushBuffer();
        }
    }

    @RequestMapping(
            value = "/api/v1/publication/downloadCover/{id}/{version}",
            method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public void downloadCover(@PathVariable Long id, HttpServletResponse response) throws IOException {

        //get the fs manager
        FileSystemManager fsManager = VFS.getManager();
        FileObject coverPath = fsManager.resolveFile(CONTENT_PATH + PublicationService.PUBLICATIONS_LOCATION + id + "/" + ProjectService.COVER_NAME);

        if(coverPath != null && coverPath.exists()) {
            //copy the file stream into the response
            IOUtils.copy(coverPath.getContent().getInputStream(), response.getOutputStream());
            response.flushBuffer();
        }
    }

    @RequestMapping(
            value = "/api/v1/publication/downloadThumb/{publicationId}/{thumbId}/{version}",
            method = {RequestMethod.GET, RequestMethod.POST})
    @ResponseBody
    public void downloadThumb(@PathVariable Long publicationId, @PathVariable Long thumbId, HttpServletResponse response) throws IOException {

        //get the fs manager
        FileSystemManager fsManager = VFS.getManager();
        FileObject thumbPath = fsManager.resolveFile(CONTENT_PATH + PublicationService.PUBLICATIONS_LOCATION + publicationId + "/" + thumbId + ProjectService.THUMB_SUFFIX);

        if(thumbPath != null && thumbPath.exists()) {
            //copy the file stream into the response
            IOUtils.copy(thumbPath.getContent().getInputStream(), response.getOutputStream());
            response.flushBuffer();
        }
    }

    private String createPublicationListJSon(Page<PublicationDocument> publicationDocumentPage, JSONApiVersion version) {

        long totalElements = publicationDocumentPage.getTotalElements();

        final StringBuilder jsonBuilder = new StringBuilder();
        jsonBuilder.append("{");
        jsonBuilder.append("    \"total\": " + totalElements);

        if(totalElements > 0) {
            jsonBuilder.append(",\"elements\": [");
            final int[] i = {0};
            publicationDocumentPage.forEach(publicationDocument -> {
                try {
                    String publicationJson = StreamUtils.getInputStreamContent(publicationService.findByIdJSON(Long.valueOf(publicationDocument.getId()), version, false));
                    if(i[0] > 0) {
                        jsonBuilder.append(",");
                    }
                    jsonBuilder.append(publicationJson);
                    i[0]++;
                }
                catch (Exception e) {
                    LOGGER.info(e.getMessage(), e);
                }
            });
            jsonBuilder.append("]");
        }

        jsonBuilder.append("}");

        return jsonBuilder.toString();
    }
}